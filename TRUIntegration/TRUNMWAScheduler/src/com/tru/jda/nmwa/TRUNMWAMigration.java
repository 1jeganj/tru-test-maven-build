package com.tru.jda.nmwa;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;

/**
 * The Class TRUProfileMigration.
 */
public class TRUNMWAMigration extends GenericService{

	/** The NMWA Migration stage main procedure. */
	private String mNmwaMigStageMainProcedure;
	
	/** The Profile Migration target procedure. */
	private String mNmwaMigTargetProcedure;
	
	/** The Migration decryption directory. */
	private String mMigrationDecryDirectory;
	
	/** The Data source. */
	private DataSource mDataSource;
	
	/** The Migration directory. */
	private String mMigrationDirectory;
	
	/** The Initial. */
	private String mInitial;
	
	/** The Delta. */
	private String mDelta;
	
	/** The Email Mask */
	private boolean mEmailMask;
	
	/** The Use FileList */
	private boolean mUseFileList;	
	
	
	/** The Data source jndi name. */
	private String mDataSourceJNDIName;

	/** The migration file list. */
	private String mMigrationFileList;
	
	
	/**
	 * Gets the migration file list.
	 *
	 * @return the migration file list
	 */
	public String getMigrationFileList() {
		return mMigrationFileList;
	}

	/**
	 * Sets the migration file list.
	 *
	 * @param pMigrationFileList the migration file list.
	 */
	public void setMigrationFileList(String pMigrationFileList) {
		mMigrationFileList = pMigrationFileList;
	}

	
	/**
	 * Checks if is email mask.
	 *
	 * @return true, if is email mask
	 */
	public boolean isUseFileList() {
		return mUseFileList;
	}

	/**
	 * Sets the email mask.
	 *
	 * @param pUseFileList the new email mask
	 */
	public void setUseFileList(boolean pUseFileList) {
		mUseFileList = pUseFileList;
	}


	/**
	 * Checks if is email mask.
	 *
	 * @return true, if is email mask
	 */
	public boolean isEmailMask() {
		return mEmailMask;
	}

	/**
	 * Sets the email mask.
	 *
	 * @param pEmailMask the new email mask
	 */
	public void setEmailMask(boolean pEmailMask) {
		mEmailMask = pEmailMask;
	}

	/**
	 * Gets the nmwa migration stage main procedure.
	 *
	 * @return the nmwa migration stage main procedure
	 */
	public String getNmwaMigStageMainProcedure() {
		return mNmwaMigStageMainProcedure;
	}

	/**
	 * Sets the nmwa migration stage main procedure.
	 *
	 * @param pNmwaMigStageMainProcedure the new nmwa migration stage main procedure
	 */
	public void setNmwaMigStageMainProcedure(String pNmwaMigStageMainProcedure) {
		mNmwaMigStageMainProcedure = pNmwaMigStageMainProcedure;
	}

	/**
	 * Gets the nmwa migration target procedure.
	 *
	 * @return the nmwa migration target procedure
	 */
	public String getNmwaMigTargetProcedure() {
		return mNmwaMigTargetProcedure;
	}

	/**
	 * Sets the nmwa mig target procedure.
	 *
	 * @param pNmwaMigTargetProcedure the new nmwa mig target procedure
	 */
	public void setNmwaMigTargetProcedure(String pNmwaMigTargetProcedure) {
		mNmwaMigTargetProcedure = pNmwaMigTargetProcedure;
	}
	
	/**
	 * Gets the data source jndi name.
	 *
	 * @return the data source jndi name
	 */
	public String getDataSourceJNDIName() {
		return mDataSourceJNDIName;
	}

	/**
	 * Sets the data source jndi name.
	 *
	 * @param pDataSourceJNDIName the new data source jndi name
	 */
	public void setDataSourceJNDIName(String pDataSourceJNDIName) {
		mDataSourceJNDIName = pDataSourceJNDIName;
	}
	
	/**
	 * Gets the migration decry directory.
	 *
	 * @return the migration decry directory
	 */
	public String getMigrationDecryDirectory() {
		return mMigrationDecryDirectory;
	}

	/**
	 * Sets the migration decry directory.
	 *
	 * @param pMigrationDecryDirectory the new migration decry directory
	 */
	public void setMigrationDecryDirectory(String pMigrationDecryDirectory) {
		mMigrationDecryDirectory = pMigrationDecryDirectory;
	}

	/**
	 * Gets the migration directory.
	 *
	 * @return the migration directory
	 */
	public String getMigrationDirectory() {
		return mMigrationDirectory;
	}

	/**
	 * Sets the migration directory.
	 *
	 * @param pMigrationDirectory the new migration directory
	 */
	public void setMigrationDirectory(String pMigrationDirectory) {
		mMigrationDirectory = pMigrationDirectory;
	}
	
	/**
	 * Gets the initial.
	 *
	 * @return the initial
	 */
	public String getInitial() {
		return mInitial;
	}

	/**
	 * Sets the initial.
	 *
	 * @param pInitial the new initial
	 */
	public void setInitial(String pInitial) {
		mInitial = pInitial;
	}
	
	/**
	 * Gets the delta.
	 *
	 * @return the delta
	 */
	public String getDelta() {
		return mDelta;
	}

	/**
	 * Sets the delta.
	 *
	 * @param pDelta the new delta
	 */
	public void setDelta(String pDelta) {
		mDelta = pDelta;
	}
	
	/**
	 * Execute profile migration stage main procedure.
	 */
	public void executeNMWAMigStageMainProcedure(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.executeNMWAMigStageMainProcedure()");
		}
		String fileName = null;				
		if(isUseFileList()){
			fileName = getMigrationFileList();
		}else{
			fileName = getNMWAMigrationFileName();
		}
		vlogDebug("NMWA Migration file Name: {0}", fileName);
		boolean flag = executeStoredProcedure(getNmwaMigStageMainProcedure(),getMigrationDirectory(),fileName,TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG);
		if(flag){
			deleteNMWAMigrationFile();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUNMWAMigration.executeProfileMigStageMainProcedure()");
		}
	}
	
	
	/**
	 * Execute profile migration Initial.
	 */
	public void executeNMWAMigInital(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.executeNMWAMigInital()");
		}
		String emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG_N;
		if(isEmailMask()){
			emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG;
		}
		
		executeStoredProcedure(getNmwaMigTargetProcedure(), getInitial());

		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.executeProfileMigInital()");
		}
	}
	
	/**
	 * Execute profile migration Delta.
	 */

	public void executeNMWAMigDelta(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.executeNMWAMigDelta()");
		}
		String emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG_N;
		if(isEmailMask()){
			emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG;
		}

		executeStoredProcedure(getNmwaMigTargetProcedure(), getDelta(),emailMask);

		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.executeProfileMigDelta()");
		}
	}
	
	/**
	 * Execute stored procedure.
	 *
	 * @param pStoredProcedure the stored procedure
	 * @param pParams - String
	 * @return true, if successful
	 */
	private boolean executeStoredProcedure(String pStoredProcedure, String... pParams) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.executeStoredProcedure()");
		}
		if (this.mDataSource != null) {
			Connection con = null;
			try {
				con = getDatasource().getConnection();
				CallableStatement callableStatement = con.prepareCall(pStoredProcedure);
				if (pParams != null) {
					for (int i = TRUNMWASchedulerConstants.NUMBER_ZERO; i < pParams.length; i++) {
						callableStatement.setString((i + TRUNMWASchedulerConstants.NUMBER_ONE), pParams[i]);
					}
				}
				callableStatement.execute();
				if (isLoggingDebug()) {
					logDebug("Exit from :: TRUProfileMigration.executeStoredProcedure()");
				}
				return true;

			} catch (SQLException exs) {
				if (isLoggingError()) {
					logError("SQLException while executing stored procedure:", exs);
				}
				return false;
			}
		} else {
			vlogError("SQL DataSource : {0} is not configure ", getDataSourceJNDIName());
			if (isLoggingDebug()) {
				logDebug("Exit from :: TRUProfileMigration.executeStoredProcedure()");
			}
			return false;
		}

	}
	
	
	/**
	 * Gets the nmwa migration file name.
	 *
	 * @return the nmwa migration file name
	 */
	private String getNMWAMigrationFileName(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.getNMWAMigrationFileName()");
		}
		int counter = TRUNMWASchedulerConstants.NUMBER_ZERO;
		String fileName = null;
		File folder = new File(getMigrationDecryDirectory());
		File[] listOfFiles = folder.listFiles();
		if(listOfFiles != null && listOfFiles.length >TRUNMWASchedulerConstants.NUMBER_ZERO){
			fileName =	listOfFiles[TRUNMWASchedulerConstants.NUMBER_ZERO].getName();
			for(counter = TRUNMWASchedulerConstants.NUMBER_ONE; counter < listOfFiles.length; counter++){
				fileName =	fileName + TRUNMWASchedulerConstants.COMA_SEPERATOR + listOfFiles[counter].getName();
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUNMWAMigration.getNMWAMigrationFileName()--> filename is " + fileName);
			logDebug("Exit from :: TRUNMWAMigration.getNMWAMigrationFileName()");
		}
		return fileName;
	}
	
	
	/**
	 * Delete profile migration file.
	 *
	 * @return true, if successful
	 */
	private boolean deleteNMWAMigrationFile(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUNMWAMigration.deleteNMWAMigrationFile()");
		}
		boolean flag = false;
		File folder = new File(getMigrationDecryDirectory());
		File[] listOfFiles = folder.listFiles();
		if(listOfFiles != null){
			for(File file : listOfFiles){
				flag = file.delete();
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUNMWAMigration.deleteProfileMigrationFile()");
		}
		return flag;
	}
	
	
	/**
	 * Gets the datasource.
	 *
	 * @return the datasource
	 */
	public DataSource getDatasource() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.getDatasource()");
		}
		if (this.mDataSource == null && !StringUtils.isBlank(this.mDataSourceJNDIName)) {
			try {
				Context ctx = new javax.naming.InitialContext();
				this.mDataSource = (DataSource) ctx.lookup(this.mDataSourceJNDIName);
			} catch (NamingException ene) {
				if (isLoggingError()) {
					logError("NamingException while looking JNDI resource:", ene);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.getDatasource()");
		}
		return mDataSource;
	}
}
