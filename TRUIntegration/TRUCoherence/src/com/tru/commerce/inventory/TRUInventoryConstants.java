package com.tru.commerce.inventory;

/**
 * This class hold all Inventory related constants.
 */
public class TRUInventoryConstants {

	/** Constant to hold PURCHASE_METHOD_ONLINE. */
	public static final String PURCHASE_METHOD_ONLINE = "Online Purchase Method";
	/** Constant to hold PURCHASE_METHOD_STORE. */
	public static final String PURCHASE_METHOD_STORE = "Store Purchase Method";
	/** Constant to hold PURCHASE_METHOD_STORE. */
	public static final String QUERY_AVAILABILITY_STATUS_ONLINE = "Query Availability Status Online";
	/** Constant to hold PURCHASE_METHOD_STORE. */
	public static final String QUERY_AVAILABILITY_STATUS_STORE = "Query Availability Status Store";
	/** Constant to hold QUERY_STOCK_LEVEL_ONLINE. */
	public static final String QUERY_STOCK_LEVEL_ONLINE = "Query Stock Level Online";
	/** Constant to hold QUERY_STOCK_LEVEL_STORE. */
	public static final String QUERY_STOCK_LEVEL_STORE = "Query Stock Level Store";
	/** Constant to hold QUERY_BULK_S2H_STOCK_LEVEL. */
	public static final String QUERY_BULK_S2H_STOCK_LEVEL = "Query Bulk S2H Stock Level";
	/** Constant to hold QUERY_BULK_STOCK_LEVEL. */
	public static final String QUERY_BULK_STOCK_LEVEL = "Query Bulk Stock Level";
	
	/** Constant to hold QUERY_BULK_S2H_STOCK_LEVEL. */
    public static final String QUERY_BULK_S2H_REGISTRY_STOCK_LEVEL = "Query Bulk S2H Registry Stock Level";

	/** Constant to hold VALIDATE_MULTI_STORE_INVENTORY. */
	public static final String VALIDATE_MULTI_STORE_INVENTORY = "Validate Multi Store Inventory";
	
	/** The Constant COHERENCE. */
	public static final String COHERENCE= "coherence";
	
	/** The Constant QUERY_STOCK_LEVEL. */
	public static final String QUERY_STOCK_LEVEL= "queryStockLevel";
	
	/** The Constant QUERY_AVAILABILITY. */
	public static final String QUERY_AVAILABILITY= "queryAvailability";
	
	/** The Constant UPDATE_STOCK_LEVEL. */
	public static final String UPDATE_STOCK_LEVEL= "updateStockLevel";
	
	/** Constant to hold COHERENCE_IN_STOCK. */
	public static final long COHERENCE_IN_STOCK = 16771;
	/** Constant to hold COHERENCE_OUT_OF_STOCK. */
	static final long COHERENCE_OUT_OF_STOCK = 16770;
	/** Constant to hold LIMITED_STOCK. */
	static final long LIMITED_STOCK = 16769;
	/** Constant to hold DEFAULT_STOCK_LEVEL. */
	static final long DEFAULT_STOCK_LEVEL = -1L;
	/** Constant to hold TEST_MODE_STOCK_LEVEL. */
	static final long TEST_MODE_STOCK_LEVEL = 99L;
	
	/** The Constant INVENTORY_STATUS_SUCCEED. */
	public static final int INVENTORY_STATUS_SUCCEED = 1;
	
	/** Constant to hold one. */
	public static final int STATUS_DECREMENT_VALUE = 1;
	
	
	/** Constant to hold zero. */
	public static final int INT_ZERO = 0;
	
	/** Constant to hold zero. */
	public static final int DEFAULT_STATUS = 0;
	/** Constant to hold ITEM_QANTITY. */
	public static final String ITEM_QANTITY = "ItemQuantity";
	/** Constant to hold ATC_QUANITY. */
	public static final String ATC_QUANITY = "AtcQuantity";
	/** Constant to hold ATC_STATUS. */
	public static final String ATC_STATUS = "AtcStatus";
	/** Constant to hold MIN_INVENTORY. */
	public static final long MIN_INVENTORY = 1L;
	/** Constant to hold INVENTORY_LEVEL_ZERO. */
	public static final long INVENTORY_LEVEL_ZERO = 0L;
	/** Constant to hold QUERY_AVAILABILITY_STATUS_ONLINE_EXCEPTION. */
	public static final String QUERY_AVAILABILITY_STATUS_ONLINE_EXCEPTION = "queryAvailabilityStatus: Catalog Ref Id null" ;
	/** Constant to hold QUERY_AVAILABILITY_STATUS. */
	public static final String QUERY_AVAILABILITY_STATUS = "queryAvailabilityStatus(";
	/** Constant to hold LEFT_BRACES. */
	public static final String LEFT_BRACES = "): ";
	/** Constant to hold EQUAL_BACKWARD_SLASH. */
	public static final String EQUAL_BACKWARD_SLASH = "=\"";
	/** Constant to hold BACKWARD_SLASH. */
	public static final String BACKWARD_SLASH = "\"";
	/** Constant to hold QUERY_STOCK_LEVEL_ONLINE_EXCEPTION. */
	public static final String QUERY_STOCK_LEVEL_ONLINE_EXCEPTION = "queryStockLevel: Catalog Ref Id is null";
	/** Constant to hold QUERY_STOCK_LEVEL_STORE_EXCEPTION. */
	public static final String QUERY_STOCK_LEVEL_STORE_EXCEPTION = "queryStockLevel: Catalog Ref Id or location is null ";
	/** Constant to hold QUERY_AVAILABILITY_STATUS_STORE_EXCEPTION. */
	public static final String QUERY_AVAILABILITY_STATUS_STORE_EXCEPTION = "queryAvailabilityStatus: Catalog Ref Id null or location is null ";
	
	/** The Constant DEFAULT_STORE_STOCK_LEVEL. */
	public static final long DEFAULT_STORE_STOCK_LEVEL=50L;
	
	/** The Constant INVENTORY_AVAILABILITY. */
	public static final String INVENTORY_AVAILABILITY= "inventory_availability";
	
	/** The Constant INVENTORY_CONFIG. */
	public static final String INVENTORY_CONFIG= "inventoryConfig";

	/** The Constant COHERENCE_IN_STOCK_INT. */
	public static final int COHERENCE_IN_STOCK_INT = 16771;
	
	/** The Constant COHERENCE_OUT_OF_STOCK_INT. */
	static final int COHERENCE_OUT_OF_STOCK_INT = 16770;
	
	/** The Constant STR_PAGE. */
	public static final String STR_PAGE = "page";
	
	/** The Constant STR_RESPONSE_CODE. */
	public static final String STR_RESPONSE_CODE = "Response code";
	
	/** The Constant STR_ERROR. */
	public static final String STR_ERROR = "Error";
	
	/** The Constant DEFAULT_RESPONSE_CODE. */
	public static final String DEFAULT_RESPONSE_CODE = "500";
	
	/** The Constant DEFAULT_ERROR_MESSAGE. */
	public static final String DEFAULT_ERROR_MESSAGE = "getting Inventory Failed";
	
	/** The Constant STR_CART_PAGE. */
	public static final String STR_CART_PAGE = "cart";	
    
	/** The Constant OPERATION_NAME. */
	public static final String OPERATION_NAME = "ShoppingCart";
	
	/** The Constant TASK_NAME. */
	public static final String TASK_NAME = "AddToCart";
	
	/** The Constant COHERENCE_FLAG_CACHE_FLUSH. */
	public static final String COHERENCE_FLAG_CACHE_FLUSH = "CoherenceFlagCacheFlush";
}
