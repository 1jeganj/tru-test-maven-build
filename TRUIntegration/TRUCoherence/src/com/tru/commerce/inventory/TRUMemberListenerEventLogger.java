package com.tru.commerce.inventory;


import atg.nucleus.GenericService;

import com.tangosol.net.MemberEvent;
import com.tangosol.net.MemberListener;

/**
 * The Class TRUMemberListenerEventLogger.
 */
public class TRUMemberListenerEventLogger extends GenericService implements MemberListener {

	/** The Constant COHERENCE_PROXY_PORT. */
	private static final String COHERENCE_PROXY_PORT = "coherence.proxy.port";
	
	/** The Constant COHERENCE_PROXY_ADDRESS. */
	private static final String COHERENCE_PROXY_ADDRESS = "coherence.proxy.address";
	
	/** The Constant IS_LEFT. */
	private static final String IS_LEFT = " is Left : ";
	
	/** The Constant IS_LEAVING. */
	private static final String IS_LEAVING = " is Leaving : ";
	
	/** The Constant PORT. */
	private static final String PORT = " Port: ";
	
	/** The Constant JOINED_ON. */
	private static final String JOINED_ON = " Joined on ";
	
	/** The m server address. */
	private String mServerAddress = System.getProperty(COHERENCE_PROXY_ADDRESS);
	
	/** The m port. */
	private String mPort = System.getProperty(COHERENCE_PROXY_PORT);
	
	/* (non-Javadoc)
	 * @see com.tangosol.net.MemberListener#memberJoined(com.tangosol.net.MemberEvent)
	 */
	@Override
	public void memberJoined(MemberEvent pEvent) {
		if(isLoggingInfo()){
			logInfo(mServerAddress + JOINED_ON + pEvent.getMember().getMachineName()+ PORT + mPort);
		}
	}

	/* (non-Javadoc)
	 * @see com.tangosol.net.MemberListener#memberLeaving(com.tangosol.net.MemberEvent)
	 */
	@Override
	public void memberLeaving(MemberEvent pEvent) {
		if(isLoggingInfo()){
			logInfo(mServerAddress + IS_LEAVING + pEvent.getMember().getMachineName() + PORT + mPort);
		}
	}

	/* (non-Javadoc)
	 * @see com.tangosol.net.MemberListener#memberLeft(com.tangosol.net.MemberEvent)
	 */
	@Override
	public void memberLeft(MemberEvent pEvent) {
		if(isLoggingError()){
			logError(mServerAddress + IS_LEFT + pEvent.getMember().getMachineName() + PORT + mPort);
		}
	}

}