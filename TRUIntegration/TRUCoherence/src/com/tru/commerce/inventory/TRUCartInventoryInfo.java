package com.tru.commerce.inventory;

/**
 * The Class TRUCartInventoryInfo will crate a new tru inventory info object
 * to query the coherence server.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCartInventoryInfo {

	

	/** The catalog ref id. */
	String mCatalogRefId;
	

	/** The quantity. */
	long mQuantity;
	

	/** The location id. */
	String mLocationId;
	
	/**
	 * Instantiates a new TRU cart inventory info.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pQuantity the quantity
	 * @param pLocationId the location id
	 */
	public TRUCartInventoryInfo(String pCatalogRefId, long pQuantity, String pLocationId) {
		this.mCatalogRefId = pCatalogRefId;
		this.mQuantity = pQuantity;
		this.mLocationId = pLocationId;
	}
	
	/**
	 * Instantiates a new TRU cart inventory info.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pQuantity the quantity
	 */
	public TRUCartInventoryInfo(String pCatalogRefId, long pQuantity) {
		this.mCatalogRefId = pCatalogRefId;
		this.mQuantity = pQuantity;
	}
	
	/**
	 * Gets the location id.
	 *
	 * @return the location id
	 */
	public String getLocationId() {
		return mLocationId;
	}
	
	/**
	 * Sets the location id.
	 *
	 * @param pLocationId the new location id
	 */
	public void setLocationId(String pLocationId) {
		this.mLocationId = pLocationId;
	}
	
	/**
	 * Gets the catalog ref id.
	 *
	 * @return the catalog ref id
	 */
	public String getCatalogRefId() {
		return mCatalogRefId;
	}
	
	/**
	 * Sets the catalog ref id.
	 *
	 * @param pCatalogRefId the new catalog ref id
	 */
	public void setCatalogRefId(String pCatalogRefId) {
		this.mCatalogRefId = pCatalogRefId;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param pQuantity the new quantity
	 */
	public void setQuantity(long pQuantity) {
		this.mQuantity = pQuantity;
	}
	
}
