package com.tru.commerce.inventory;

import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

import com.tangosol.io.pof.PortableException;
import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import com.tangosol.net.RequestTimeoutException;
import com.tangosol.net.messaging.ConnectionException;
import com.tangosol.util.processor.NumberIncrementor;
import com.tru.orcl.coh.entity.ItemQuantity;
import com.tru.orcl.coh.entity.ItemQuantityKey;

/**
 The Class TRUCoherenceCacheAdapter will be used to
 * query and update the coherence cache.

 * @version 1.0
 * @author PA
 * 
 *
 */
public class TRUCoherenceCacheAdapter extends GenericService{


	/** The  use coherence. */
	boolean mUseCoherence;
	
	/** The Enable member listener. */
	private boolean mEnableMemberListener;
	
	/**
	 * Checks if is enable member listener.
	 *
	 * @return the enableMemberListener
	 */
	public boolean isEnableMemberListener() {
		return mEnableMemberListener;
	}

	/**
	 * Sets the enable member listener.
	 *
	 * @param pEnableMemberListener the enableMemberListener to set
	 */
	public void setEnableMemberListener(boolean pEnableMemberListener) {
		mEnableMemberListener = pEnableMemberListener;
	}

	/** The item qantity cache. */
	private static NamedCache ITEM_QANTITY_CACHE = null;
	
	/** The Member listener event logger. */
	private TRUMemberListenerEventLogger mMemberListenerEventLogger;
	
	/**
	 * Gets the member listener event logger.
	 *
	 * @return the member listener event logger
	 */
	public TRUMemberListenerEventLogger getMemberListenerEventLogger() {
		return mMemberListenerEventLogger;
	}

	/**
	 * Sets the member listener event logger.
	 *
	 * @param pMemberListenerEventLogger the new member listener event logger
	 */
	public void setMemberListenerEventLogger(
			TRUMemberListenerEventLogger pMemberListenerEventLogger) {
		mMemberListenerEventLogger = pMemberListenerEventLogger;
	}
	
	/**
	 * Checks if is use coherence.
	 *
	 * @return true, if is use coherence
	 */
	public boolean isUseCoherence() {
		return mUseCoherence;
	}
	
	/**
	 * Sets the use coherence.
	 *
	 * @param pUseCoherence the new use coherence
	 */
	public void setUseCoherence(boolean pUseCoherence) {
		this.mUseCoherence = pUseCoherence;
	}
	
	/**
	 * Overriden to initialize the item cache.
	 * 
	 * @throws ServiceException 
	 * @see atg.nucleus.GenericService#doStartService()
	 */
	public void doStartService() throws ServiceException{
		super.doStartService();
		initializeItemQuantityCache();
	}

	/**
	 * Initialize item quantity cache.
	 */
	private void initializeItemQuantityCache() {
		try {
			ITEM_QANTITY_CACHE = CacheFactory.getCache(TRUInventoryConstants.ITEM_QANTITY);
			if(isEnableMemberListener()){
				ITEM_QANTITY_CACHE.getCacheService().addMemberListener(getMemberListenerEventLogger());
			}
		} catch (RequestTimeoutException e) {
			if (isLoggingError()) {
				logError(
						"RequestTimeoutException in @Class::TRUCoherenceCacheAdapter::@method::initializeItemQuantityCache()",
						e);
			}
		} catch (ConnectionException connectionExc) {
			if (isLoggingError()) {
				logError(
						"ConnectionException in @Class::TRUCoherenceCacheAdapter::@method::initializeItemQuantityCache()",
						connectionExc);
			}
		} catch (PortableException portableExc) {
			if (isLoggingError()) {
				logError(
						"PortableException in @Class::TRUCoherenceCacheAdapter::@method::initializeItemQuantityCache()",
						portableExc);
			}
		}
	}
	
	/**
	 * Gets the item quantity.
	 *
	 * @param pKey the key
	 * @return the item quantity
	 */
	public ItemQuantity getItemQuantity(ItemQuantityKey pKey) {

		if (!isUseCoherence()) {
			return null;
		} else if (ITEM_QANTITY_CACHE == null) {
			initializeItemQuantityCache();
		}
		return (ItemQuantity) ITEM_QANTITY_CACHE.get(pKey);
	}

	/**
	 * Gets the batch item quantity.
	 *
	 * @param pKeys the keys
	 * @return the batch item quantity
	 */
	public Map<ItemQuantityKey, ItemQuantity> getBatchItemQuantity(
			List<ItemQuantityKey> pKeys) {
		if (!isUseCoherence()) {
			return null;
		} else if (ITEM_QANTITY_CACHE == null) {
			initializeItemQuantityCache();
		}
		return ITEM_QANTITY_CACHE.getAll(pKeys);
	}

	/**
	 * Sets the item quantity.
	 *
	 * @param pKey the key
	 * @param pQuantity the quantity
	 */
	public void setItemQuantity(ItemQuantityKey pKey, long pQuantity) {
		if (!isUseCoherence()) {
			return;
		} else if (ITEM_QANTITY_CACHE == null) {
			initializeItemQuantityCache();
		}
		ITEM_QANTITY_CACHE.invoke(pKey, new NumberIncrementor(TRUInventoryConstants.ATC_QUANITY,
				Long.valueOf(-pQuantity), true));
	}

	/**
	 * Sets the item status.
	 *
	 * @param pKey the key
	 * @param pStatus the status
	 */
	public void setItemStatus(ItemQuantityKey pKey, int pStatus) {
		if (!isUseCoherence()) {
			return;
		} else if (ITEM_QANTITY_CACHE == null) {
			initializeItemQuantityCache();
		}
		ITEM_QANTITY_CACHE.invoke(pKey,new NumberIncrementor(TRUInventoryConstants.ATC_STATUS,
				 Integer.valueOf(-pStatus), true));
	}
}