/*
 * 
 */
package com.tru.commerce.inventory;

import atg.nucleus.GenericService;


/**
 * The Class TRUCommercePropertyManager.
 * @author PA.
 * @version 1.0.
 */
public class TRUInventoryConfiguration extends GenericService {

	/** The Enable coherence override property name. */
	private String mCoherenceOverridePropertyName;
	
	/** The Item name property name. */
	private String mItemNamePropertyName;
	
	/** The Facility name property name. */
	private String mFacilityNamePropertyName;
	
	/** The Atc quantity property name. */
	private String mAtcQuantityPropertyName;
	
	/** The Atc status property name. */
	private String mAtcStatusPropertyName;
	
	/** The Online string. */
	private String mOnlineString;
	
	/**
	 * Gets the online string.
	 *
	 * @return the online string
	 */
	public String getOnlineString() {
		return mOnlineString;
	}

	/**
	 * Sets the online string.
	 *
	 * @param pOnlineString the new online string
	 */
	public void setOnlineString(String pOnlineString) {
		mOnlineString = pOnlineString;
	}
	
	/**
	 * Gets the coherence override property name.
	 *
	 * @return the coherence override property name
	 */
	public String getCoherenceOverridePropertyName() {
		return mCoherenceOverridePropertyName;
	}

	/**
	 * Sets the coherence override property name.
	 *
	 * @param pCoherenceOverridePropertyName the new coherence override property name
	 */
	public void setCoherenceOverridePropertyName(
			String pCoherenceOverridePropertyName) {
		mCoherenceOverridePropertyName = pCoherenceOverridePropertyName;
	}

	/**
	 * Gets the item name property name.
	 *
	 * @return the item name property name
	 */
	public String getItemNamePropertyName() {
		return mItemNamePropertyName;
	}

	/**
	 * Sets the item name property name.
	 *
	 * @param pItemNamePropertyName the new item name property name
	 */
	public void setItemNamePropertyName(String pItemNamePropertyName) {
		mItemNamePropertyName = pItemNamePropertyName;
	}

	/**
	 * Gets the facility name property name.
	 *
	 * @return the facility name property name
	 */
	public String getFacilityNamePropertyName() {
		return mFacilityNamePropertyName;
	}

	/**
	 * Sets the facility name property name.
	 *
	 * @param pFacilityNamePropertyName the new facility name property name
	 */
	public void setFacilityNamePropertyName(String pFacilityNamePropertyName) {
		mFacilityNamePropertyName = pFacilityNamePropertyName;
	}

	/**
	 * Gets the atc quantity property name.
	 *
	 * @return the atc quantity property name
	 */
	public String getAtcQuantityPropertyName() {
		return mAtcQuantityPropertyName;
	}

	/**
	 * Sets the atc quantity property name.
	 *
	 * @param pAtcQuantityPropertyName the new atc quantity property name
	 */
	public void setAtcQuantityPropertyName(String pAtcQuantityPropertyName) {
		mAtcQuantityPropertyName = pAtcQuantityPropertyName;
	}

	/**
	 * Gets the atc status property name.
	 *
	 * @return the atc status property name
	 */
	public String getAtcStatusPropertyName() {
		return mAtcStatusPropertyName;
	}

	/**
	 * Sets the atc status property name.
	 *
	 * @param pAtcStatusPropertyName the new atc status property name
	 */
	public void setAtcStatusPropertyName(String pAtcStatusPropertyName) {
		mAtcStatusPropertyName = pAtcStatusPropertyName;
	}
}
