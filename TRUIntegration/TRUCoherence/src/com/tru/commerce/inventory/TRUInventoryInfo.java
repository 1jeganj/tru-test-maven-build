package com.tru.commerce.inventory;

/**
 * The Class TRUInventoryInfo will create 
 * inventoruInfo objects.
 */
public class TRUInventoryInfo {
	
	/** SKU id. */
	String mCatalogRefId;
	
	/** Available quantity. */
	long mQuantity;
	
	/**  Facility Id. */
	String mLocationId;
	
	/**  Original location Id. */
	String mOriginalLocationId;
	
	/** The Atc status. */
	int mAtcStatus;
	
	/**
	 * Instantiates a new TRU inventory info.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pQuantity the quantity
	 * @param pLocationId the location id
	 */
	public TRUInventoryInfo(String pCatalogRefId, long pQuantity, String pLocationId) {
		this.mCatalogRefId = pCatalogRefId;
		this.mQuantity = pQuantity;
		this.mLocationId = pLocationId;
	}
	
	/**
	 * Instantiates a new TRU inventory info.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pQuantity the quantity
	 * @param pLocationId the location id
	 * @param pOriginalLocationId the original location id
	 */
	public TRUInventoryInfo(String pCatalogRefId, long pQuantity, String pLocationId, String pOriginalLocationId) {
		this.mCatalogRefId = pCatalogRefId;
		this.mQuantity = pQuantity;
		this.mLocationId = pLocationId;
		this.mOriginalLocationId = pOriginalLocationId;
	}
	
	/**
	 * Instantiates a new TRU inventory info.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pQuantity the quantity
	 * @param pLocationId the location id
	 * @param pOriginalLocationId the original location id
	 * @param pAtcStatus the atc status
	 */
	public TRUInventoryInfo(String pCatalogRefId, long pQuantity, String pLocationId, String pOriginalLocationId, int pAtcStatus) {
		this.mCatalogRefId = pCatalogRefId;
		this.mQuantity = pQuantity;
		this.mLocationId = pLocationId;
		this.mOriginalLocationId = pOriginalLocationId;
		this.mAtcStatus = pAtcStatus;
	}
	
	/**
	 * Gets the catalog ref id.
	 *
	 * @return the catalog ref id
	 */
	public String getCatalogRefId() {
		return mCatalogRefId;
	}
	
	/**
	 * Sets the catalog ref id.
	 *
	 * @param pCatalogRefId the new catalog ref id
	 */
	public void setCatalogRefId(String pCatalogRefId) {
		this.mCatalogRefId = pCatalogRefId;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param pQuantity the new quantity
	 */
	public void setQuantity(long pQuantity) {
		this.mQuantity = pQuantity;
	}
	
	/**
	 * Gets the location id.
	 *
	 * @return the location id
	 */
	public String getLocationId() {
		return mLocationId;
	}
	
	/**
	 * Sets the location id.
	 *
	 * @param pLocationId the new location id
	 */
	public void setLocationId(String pLocationId) {
		this.mLocationId = pLocationId;
	}

	/**
	 * Gets the original location id.
	 *
	 * @return the original location id
	 */
	public String getOriginalLocationId() {
		return mOriginalLocationId;
	}

	/**
	 * Sets the original location id.
	 *
	 * @param pOriginalLocationId the new original location id
	 */
	public void setOriginalLocationId(String pOriginalLocationId) {
		this.mOriginalLocationId = pOriginalLocationId;
	}

	/**
	 * @return the mAtcStatus
	 */
	public int getAtcStatus() {
		return mAtcStatus;
	}

	/**
	 * @param pAtcStatus the mAtcStatus to set
	 */
	public void setAtcStatus(int pAtcStatus) {
		mAtcStatus = pAtcStatus;
	}
	

}
