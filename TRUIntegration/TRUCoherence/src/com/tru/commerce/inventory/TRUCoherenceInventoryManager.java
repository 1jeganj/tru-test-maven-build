/**
 * Custom inventory manager to query Coherence Cache
 */
package com.tru.commerce.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import atg.commerce.inventory.AbstractInventoryManagerImpl;
import atg.commerce.inventory.InventoryException;
import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tangosol.io.pof.PortableException;
import com.tangosol.net.RequestTimeoutException;
import com.tangosol.net.messaging.ConnectionException;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.messaging.TRUCoherenceFlagCacheFlushSource;
import com.tru.orcl.coh.entity.ItemQuantity;
import com.tru.orcl.coh.entity.ItemQuantityKey;

/**
 * The Class TRUCoherenceInventoryManager.
 * @version 1.0
 * @author Oracle
 */
public class TRUCoherenceInventoryManager extends AbstractInventoryManagerImpl {

	/** The  store out of stock list. */
	private List<String> mOutOfStockListStoreIds;
	
	/** The out of stock list store. */
	private List<String> mOutOfStockListStore;
	
	/** The coherence cache adapter. */
	TRUCoherenceCacheAdapter mCoherenceCacheAdapter;
	

	/** Holds The  out of stock list. */
	private List<String> mOutOfStockListOnline;
	
	/** The Inventory repository. */
	private Repository mInventoryRepository;
	
	/** The Property manager. */
	private TRUInventoryConfiguration mPropertyManager;
	
	/** The m store config repository. */
	private Repository mStoreConfigRepository;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/** The Coherence flag cache flush source. */
	private TRUCoherenceFlagCacheFlushSource mCoherenceFlagCacheFlushSource;
	
	/**
	 * @return the coherenceFlagCacheFlushSource
	 */
	public TRUCoherenceFlagCacheFlushSource getCoherenceFlagCacheFlushSource() {
		return mCoherenceFlagCacheFlushSource;
	}

	/**
	 * @param pCoherenceFlagCacheFlushSource the coherenceFlagCacheFlushSource to set
	 */
	public void setCoherenceFlagCacheFlushSource(TRUCoherenceFlagCacheFlushSource pCoherenceFlagCacheFlushSource) {
		mCoherenceFlagCacheFlushSource = pCoherenceFlagCacheFlushSource;
	}
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * Gets the store config repository.
	 *
	 * @return the store config repository
	 */
	public Repository getStoreConfigRepository() {
		return mStoreConfigRepository;
	}


	/**
	 * Sets the store config repository.
	 *
	 * @param pStoreConfigRepository the new store config repository
	 */
	public void setStoreConfigRepository(Repository pStoreConfigRepository) {
		mStoreConfigRepository = pStoreConfigRepository;
	}

	/**
	 * Gets the property manager.
	 *
	 * @return the property manager
	 */
	public TRUInventoryConfiguration getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the new property manager
	 */
	public void setPropertyManager(TRUInventoryConfiguration pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * Gets the inventory repository.
	 *
	 * @return the inventory repository
	 */
	public Repository getInventoryRepository() {
		return mInventoryRepository;
	}

	/**
	 * Sets the inventory repository.
	 *
	 * @param pInventoryRepository the new inventory repository
	 */
	public void setInventoryRepository(Repository pInventoryRepository) {
		mInventoryRepository = pInventoryRepository;
	}

	/**
	 * Gets the out of stock list store ids.
	 *
	 * @return the out of stock list store ids
	 */
	public List<String> getOutOfStockListStoreIds() {
		return mOutOfStockListStoreIds;
	}

	/**
	 * Sets the out of stock list store ids.
	 *
	 * @param pOutOfStockListStoreIds the new out of stock list store ids
	 */
	public void setOutOfStockListStoreIds(List<String> pOutOfStockListStoreIds) {
		this.mOutOfStockListStoreIds = pOutOfStockListStoreIds;
	}


	/**
	 * Gets the out of stock list store.
	 *
	 * @return the out of stock list store
	 */
	public List<String> getOutOfStockListStore() {
		return mOutOfStockListStore;
	}

	/**
	 * Sets the out of stock list store.
	 *
	 * @param pOutOfStockListStore the new out of stock list store
	 */
	public void setOutOfStockListStore(List<String> pOutOfStockListStore) {
		this.mOutOfStockListStore = pOutOfStockListStore;
	}

	/**
	 * Gets the out of stock list.
	 *
	 * @return the out of stock list
	 */
	public List<String> getOutOfStockListOnline() {
		return mOutOfStockListOnline;
	}

	/**
	 * Sets the out of stock list.
	 *
	 * @param pOutOfStockList the new out of stock list
	 */
	public void setOutOfStockListOnline(List<String> pOutOfStockList) {
		this.mOutOfStockListOnline = pOutOfStockList;
	}

	/**
	 * Gets the coherence cache adapter.
	 *
	 * @return the coherence cache adapter
	 */
	public TRUCoherenceCacheAdapter getCoherenceCacheAdapter() {
		return mCoherenceCacheAdapter;
	}

	/**
	 * Sets the coherence cache adapter.
	 *
	 * @param pCoherenceCacheAdapter the new coherence cache adapter
	 */
	public void setCoherenceCacheAdapter(
			TRUCoherenceCacheAdapter pCoherenceCacheAdapter) {
		this.mCoherenceCacheAdapter = pCoherenceCacheAdapter;
	}

	/**
	 * Checks if is test mode.
	 *
	 * @return true, if is test mode
	 */
	private boolean isTestMode() {
		return false;
	}

	/* (non-Javadoc)
	 * @see atg.commerce.inventory.AbstractInventoryManagerImpl#purchase(java.lang.String, long)
	 */
	@Override
	/**
	 * 
	 * Description:	Actually purchases a product. Inventory is reduced in the Coherence.
	 *Specified By:	purchase in interface InventoryManager
	 *@param	 catalogRefId - the id of the item involved
	 *			 pHowMany - inventory quantity needed
	 *@return	One of the following:
	 *INVENTORY_STATUS_SUCCEED 1
	 *INVENTORY_STATUS_FAIL -1
	 *INVENTORY_STATUS_INSUFFICIENT_SUPPLY -2
	 *INVENTORY_STATUS_ITEM_NOT_FOUND -3
	 *@exception	InventoryException - if there was a problem during the purchase process.
	 */
	public int purchase(String pCatalogRefId, long pHowMany)
			throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.PURCHASE_METHOD_ONLINE);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::purchase() : START");
			vlogDebug("SKU ID : {0}, How Many: {1} ",pCatalogRefId,pHowMany);
		}
		
		if (pCatalogRefId == null) {
			if (isLoggingDebug()) {
				vlogDebug("Catalog Ref Id null");
			}
			return INVENTORY_STATUS_FAIL;
		}

		if (isLoggingDebug()) {
			vlogDebug("Inside purchase: " + pHowMany + " of sku " + pCatalogRefId);
		}

		/* test mode always return success */
		if (isTestMode() || isEnableCoherenceOverride()){
			return INVENTORY_STATUS_SUCCEED;
		}
		/* check quantity */
		if (pHowMany < TRUInventoryConstants.MIN_INVENTORY) {
			if (isLoggingDebug()){
				vlogDebug("Invalid amount specified for purchase: " + pHowMany +
						 " of sku " + pCatalogRefId);
			}
			return INVENTORY_STATUS_FAIL;
		}

		ItemQuantityKey itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
				getPropertyManager().getOnlineString());
		boolean isException = false;
		ItemQuantity	itemQuantity =null;
		try {
			/* get status first */
			getIntegrationInfoLogger().logIntegrationEntered(
					getIntegrationInfoLogger().getServiceNameMap().get(TRUInventoryConstants.COHERENCE),
					TRUInventoryConstants.PURCHASE_METHOD_ONLINE, null, null);
			itemQuantity = getCoherenceCacheAdapter().getItemQuantity(itemQuantityKey);
		} catch (RequestTimeoutException e) {
			isException = true;
			if (isLoggingError()) {
				logError(
						"RequestTimeoutException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", e);
			}
		} catch (ConnectionException connectionExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"ConnectionException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", connectionExc);
			}
		} catch (PortableException portableExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"PortableException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", portableExc);
			}
		}
		if(isException){
			return INVENTORY_STATUS_SUCCEED;
		}

		if (itemQuantity == null) {
			return INVENTORY_STATUS_ITEM_NOT_FOUND;
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.PURCHASE_METHOD_ONLINE);
		}
		return validatePurchase(itemQuantityKey, pHowMany, itemQuantity);

	}

	/**
	 * Validate purchase.
	 *
	 * @param pItemQuantityKey the item quantity key
	 * @param pHowMany the how many
	 * @param pItemQuantity the item quantity
	 * @return the int
	 */
	private int validatePurchase(ItemQuantityKey pItemQuantityKey, long pHowMany,
			ItemQuantity pItemQuantity) {
		if (pItemQuantity.getAtcStatus() != TRUInventoryConstants.COHERENCE_IN_STOCK) {
			if (isLoggingDebug()) {
				vlogDebug("Item was not IN_STOCK");
			}
			return INVENTORY_STATUS_FAIL;
		}

		/* Next get current inventory level from Coherence */
		long currentLevel = pItemQuantity.getAtcQuantity();
		long level = currentLevel - pHowMany;
		int status=TRUInventoryConstants.DEFAULT_STATUS;
		if (level<= TRUInventoryConstants.INVENTORY_LEVEL_ZERO ){
			status=TRUInventoryConstants.STATUS_DECREMENT_VALUE;
			getCoherenceCacheAdapter().setItemStatus(pItemQuantityKey, status);
		}
		if (level < TRUInventoryConstants.INVENTORY_LEVEL_ZERO) {
			if (isLoggingDebug()){
				vlogDebug("Insufficient supply for decrement by " + pHowMany);
			}
			return INVENTORY_STATUS_INSUFFICIENT_SUPPLY;
		}

		getCoherenceCacheAdapter().setItemQuantity(pItemQuantityKey, pHowMany);
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::purchase() : END");
		}

		return INVENTORY_STATUS_SUCCEED;
	}


	
	@Override
	/**
	 *Description:	Actually purchases a product. Inventory is reduced in the Coherence.
	 *Specified By:	purchase in interface InventoryManager
	 *@param	catalogRefId - the id of the item involved
	 *			pHowMany - inventory quantity needed
	 *locationId - the location of the inventory
	 *@return	One of the following:
	 *INVENTORY_STATUS_SUCCEED 1
	 *INVENTORY_STATUS_FAIL -1
	 *INVENTORY_STATUS_INSUFFICIENT_SUPPLY -2
	 *INVENTORY_STATUS_ITEM_NOT_FOUND -3
	 *@exception	InventoryException - if there was a problem during the purchase process.
	 */
	public int purchase(String pCatalogRefId, long pHowMany, String pLocationId)
			throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.PURCHASE_METHOD_STORE);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::purchase() : START");
			vlogDebug("SKU ID : {0}, How Many: {1} , Location ID :{2}",pCatalogRefId,pHowMany,pLocationId);
		}

		if (pCatalogRefId == null) {
			if (isLoggingDebug()) {
				vlogDebug("Catalog Ref Id null");
			}
			return INVENTORY_STATUS_FAIL;
		}

		if (isLoggingDebug()) {
			vlogDebug("Inside purchase: " + pHowMany + " of sku " + pCatalogRefId +
					 " at " + pLocationId);
		}

		if (isTestMode() || isEnableCoherenceOverride()){
			return INVENTORY_STATUS_SUCCEED;
		}
		if (pHowMany < TRUInventoryConstants.MIN_INVENTORY) {
			if (isLoggingDebug()){
				vlogDebug("Invalid amount specified for purchase: " + pHowMany +
						 " of sku " + pCatalogRefId + " at " + pLocationId);
			}
			return INVENTORY_STATUS_FAIL;
		}
		ItemQuantityKey itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
				pLocationId);
		boolean isException = false;
		ItemQuantity	itemQuantity =null;
		try {
			/* get status first */
			getIntegrationInfoLogger().logIntegrationEntered(
					getIntegrationInfoLogger().getServiceNameMap().get(TRUInventoryConstants.COHERENCE),
					TRUInventoryConstants.PURCHASE_METHOD_STORE, null, null);
			itemQuantity = getCoherenceCacheAdapter().getItemQuantity(itemQuantityKey);
		} catch (RequestTimeoutException e) {
			isException = true;
			if (isLoggingError()) {
				logError(
						"RequestTimeoutException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", e);
			}
		} catch (ConnectionException connectionExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"ConnectionException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", connectionExc);
			}
		} catch (PortableException portableExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"PortableException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", portableExc);
			}
		}
		if(isException){
			return INVENTORY_STATUS_SUCCEED;
		}
		if (itemQuantity == null) {
			return INVENTORY_STATUS_ITEM_NOT_FOUND;
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.PURCHASE_METHOD_STORE);
		}
		return validatePurchase(itemQuantityKey, pHowMany, itemQuantity);
	}

	/**
	 * Description: Return the availability status of a given item.
	 *
	 * @param pCatalogRefId - The id of the item involved.
	 * @return AVAILABILITY_STATUS_IN_STOCK 1000
	 * AVAILABILITY_STATUS_OUT_OF_STOCK 1001
	 * @throws InventoryException - if there was a problem determining the status of the
	 * item
	 */
	@Override
	public int queryAvailabilityStatus(String pCatalogRefId)
			throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_ONLINE);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryAvailabilityStatus() : START");
			vlogDebug("SKU ID : {0}",pCatalogRefId);
		}

		if (pCatalogRefId == null) {
			throw new InventoryException(TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_ONLINE_EXCEPTION);
		}
		if (isLoggingDebug()) {
			vlogDebug("Inside queryAvailabilityStatus: " + pCatalogRefId);
		}

		if (isTestMode()){
			
			//added for OUTOFSTOCK item list
			if (getOutOfStockListOnline()!= null && !getOutOfStockListOnline().isEmpty() && getOutOfStockListOnline().contains(pCatalogRefId)){
				if (isLoggingDebug()) {
					vlogDebug("Inventory Item stuatus is OUTOFSTOCK from outofstock list");
				}
				return AVAILABILITY_STATUS_OUT_OF_STOCK;
			}
			//added for OUTOFSTOCK item list
			
			return AVAILABILITY_STATUS_IN_STOCK;
		}
		ItemQuantityKey itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
				getPropertyManager().getOnlineString());
		ItemQuantity itemQuantity = null;
		if(isEnableCoherenceOverride()){
			itemQuantity = getItemQuantityFromRepository(itemQuantityKey);
		}else{
			itemQuantity = getItemQuantityFromCoherence(itemQuantityKey, TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_ONLINE);
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_ONLINE);
		}
		if (itemQuantity == null) {
			if (isLoggingDebug()) {
				vlogDebug("Inventory Item stuatus is DISCONTINUED. returning OUTOFSTOCK");
			}
			return AVAILABILITY_STATUS_OUT_OF_STOCK;
		}
		int availabilityStatus = itemQuantity.getAtcStatus();
		

		return checkAvailability(pCatalogRefId, availabilityStatus);

	}
	
	/**
	 * Gets the item quantity from coherence.
	 *
	 * @param pItemQuantityKey the item quantity key
	 * @return the item quantity from coherence
	 */
	private ItemQuantity getItemQuantityFromCoherence(ItemQuantityKey pItemQuantityKey, String pCoherenceMethod) {
		ItemQuantity itemQuantity = null;
		boolean isException = false;
		try {
			if(getCoherenceCacheAdapter() != null){
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(TRUInventoryConstants.COHERENCE), pCoherenceMethod, null, null);
				itemQuantity = getCoherenceCacheAdapter().getItemQuantity(pItemQuantityKey);
			}else{
				alertLogger(TRUInventoryConstants.DEFAULT_ERROR_MESSAGE);
				return null;
			}
		} catch (RequestTimeoutException e) {
			isException = true;
			if (isLoggingError()) {
				logError(
						"RequestTimeoutException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", e);
			}
			alertLogger(TRUInventoryConstants.DEFAULT_ERROR_MESSAGE);
		} catch (ConnectionException connectionExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"ConnectionException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", connectionExc);
			}
			alertLogger(TRUInventoryConstants.DEFAULT_ERROR_MESSAGE);
		} catch (PortableException portableExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"PortableException in @Class::TRUCoherenceInventoryManager::@method::getItemQuantityFromCoherence()", portableExc);
			}
			alertLogger(portableExc.getMessage());
		}finally{
			if(isException){
				itemQuantity = getItemQuantityFromRepository(pItemQuantityKey);
			}else{
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(TRUInventoryConstants.COHERENCE), pCoherenceMethod, null, null);	
			}
		}
		return itemQuantity;
	}


	/**
	 * Gets the item quantity from repository.
	 *
	 * @param pItemQuantityKey the item quantity key
	 * @return the item quantity from repository
	 */
	private ItemQuantity getItemQuantityFromRepository(ItemQuantityKey pItemQuantityKey) {
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: getItemQuantityFromRepository: START");
		}
		ItemQuantity repositoryItemQuantity = queryInventoryRepository(pItemQuantityKey);
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: getItemQuantityFromRepository: END");
		}
		return repositoryItemQuantity;
	}

	/**
	 * Check availability.
	 *
	 * @param pCatalogRefId the catalog ref id
	 * @param pAvailabilityStatus the availability status
	 * @return the int
	 * @throws InventoryException the inventory exception
	 */
	private int checkAvailability(String pCatalogRefId, int pAvailabilityStatus)
			throws InventoryException {
		if (pAvailabilityStatus > 0) {

			if (pAvailabilityStatus == TRUInventoryConstants.COHERENCE_OUT_OF_STOCK) {
				if (isLoggingDebug()) {
					vlogDebug("Item was OUT OF STOCK");
				}
				return AVAILABILITY_STATUS_OUT_OF_STOCK;
			}

			if (isLoggingDebug()) {
				logDebug("@Class::TRUCoherenceInventoryManager::@method::queryAvailabilityStatus() : END");
			}

			return AVAILABILITY_STATUS_IN_STOCK;
		}
		throw new InventoryException(TRUInventoryConstants.QUERY_AVAILABILITY_STATUS + pCatalogRefId +
				 TRUInventoryConstants.LEFT_BRACES + pAvailabilityStatus +TRUInventoryConstants.EQUAL_BACKWARD_SLASH + pAvailabilityStatus +
				 TRUInventoryConstants.BACKWARD_SLASH);
	}

	/**
	 * Description: Return the availability status of a given item for a store
	 * location.
	 *
	 * @param pCatalogRefId            - The id of the item involved. locationId - the location of
	 * the inventory
	 * @param pLocationId the location id
	 * @return AVAILABILITY_STATUS_IN_STOCK 1000
	 * AVAILABILITY_STATUS_OUT_OF_STOCK 1001
	 * @throws InventoryException                - if there was a problem determining the status of the
	 * item
	 */
	@Override
	public int queryAvailabilityStatus(String pCatalogRefId, String pLocationId)
			throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryAvailabilityStatus() : START");
			vlogDebug("SKU ID : {0}, Location ID :{1}",pCatalogRefId,pLocationId);
		}

		if (pCatalogRefId == null || pLocationId == null) {
			throw new InventoryException(TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE_EXCEPTION);
		}

		if (isLoggingDebug()) {
			vlogDebug("Inside queryAvailabilityStatus: " + pCatalogRefId +
					 "at location " + pLocationId);
		}

		if (isTestMode()){
			
			//added for OUTOFSTOCK item list
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStoreIds()!=null && !getOutOfStockListStoreIds().isEmpty() && getOutOfStockListStore().contains(pCatalogRefId)){
				for(String outOfStockStoreId: getOutOfStockListStoreIds()){
					if(pLocationId.equalsIgnoreCase(outOfStockStoreId)){
						if (isLoggingDebug()) {
							vlogDebug("Inventory Item stuatus is OUTOFSTOCK from outofstock list");
						}
						return AVAILABILITY_STATUS_OUT_OF_STOCK;
					}
				}
			}
			//added for OUTOFSTOCK item list
			
			return AVAILABILITY_STATUS_IN_STOCK;
		}
		ItemQuantityKey itemQuantityKey = null;
		if(StringUtils.isBlank(pLocationId)){
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
					getPropertyManager().getOnlineString());
		}else{
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
					pLocationId);
		}
		ItemQuantity itemQuantity = null;
		if(isEnableCoherenceOverride()){
			itemQuantity = getItemQuantityFromRepository(itemQuantityKey);
		}else{
			itemQuantity = getItemQuantityFromCoherence(itemQuantityKey,TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}
		if (itemQuantity == null) {
			
			if (isLoggingDebug()) {
				vlogDebug("Inventory Item stuatus is DISCONTINUED.Hence returning OUTOFSTOCK");
			}
			return AVAILABILITY_STATUS_OUT_OF_STOCK;
		}
		int  availabilityStatus = itemQuantity.getAtcStatus();
		
		return checkAvailability(pCatalogRefId, availabilityStatus);

	}

	/**
	 * Description: Return the availability status of a given item for a store
	 * location.
	 *
	 * @param pCatalogRefId            - The id of the item involved. locationId - the location of
	 * the inventory
	 * @param pLocationId the location id
	 * @return AVAILABILITY_STATUS_IN_STOCK 1000
	 * AVAILABILITY_STATUS_OUT_OF_STOCK 1001
	 * @throws InventoryException                - if there was a problem determining the status of the
	 * item
	 */
	public TRUInventoryInfo queryInventoryInfo(String pCatalogRefId, String pLocationId)
			throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryAvailabilityStatus() : START");
			vlogDebug("SKU ID : {0}, Location ID :{1}",pCatalogRefId,pLocationId);
		}

		if (pCatalogRefId == null || pLocationId == null) {
			throw new InventoryException(TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE_EXCEPTION);
		}

		if (isLoggingDebug()) {
			vlogDebug("Inside queryAvailabilityStatus: " + pCatalogRefId +
					 "at location " + pLocationId);
		}
		if (isTestMode()){
			//added for OUTOFSTOCK item list
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStoreIds()!=null && !getOutOfStockListStoreIds().isEmpty() && getOutOfStockListStore().contains(pCatalogRefId)){
				for(String outOfStockStoreId: getOutOfStockListStoreIds()){
					if(pLocationId.equalsIgnoreCase(outOfStockStoreId)){
						if (isLoggingDebug()) {
							vlogDebug("Inventory Item stuatus is OUTOFSTOCK from outofstock list");
						}
						return new TRUInventoryInfo(pCatalogRefId, TRUInventoryConstants.DEFAULT_STOCK_LEVEL, pLocationId);
					}
				}
			}
			return new TRUInventoryInfo(pCatalogRefId, TRUInventoryConstants.TEST_MODE_STOCK_LEVEL, pLocationId);
		}
		ItemQuantityKey itemQuantityKey = null;
		if(StringUtils.isBlank(pLocationId)){
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
					getPropertyManager().getOnlineString());
		}else{
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
					pLocationId);
		}
		ItemQuantity itemQuantity = null;
		if(isEnableCoherenceOverride()){
			itemQuantity = getItemQuantityFromRepository(itemQuantityKey);
		}else{
			itemQuantity = getItemQuantityFromCoherence(itemQuantityKey, TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_AVAILABILITY_STATUS_STORE);
		}
		if (itemQuantity == null) {
			
			if (isLoggingDebug()) {
				vlogDebug("Inventory Item stuatus is DISCONTINUED.Hence returning OUTOFSTOCK");
			}
			
			return new TRUInventoryInfo(pCatalogRefId, TRUInventoryConstants.DEFAULT_STOCK_LEVEL, pLocationId);
		}
		return new TRUInventoryInfo(pCatalogRefId, itemQuantity.getAtcQuantity(), pLocationId,null,itemQuantity.getAtcStatus());

	}
	/**
	 * Description: Return how many of a given item is in the system.
	 *
	 * @param pCatalogRefId - The id of the item involved.
	 * @return TEST_MODE_STOCK_LEVEL DEFAULT_STOCK_LEVEL Actual stock level.
	 * @throws InventoryException - if there was a problem determining the number.
	 */
	@Override
	public long queryStockLevel(String pCatalogRefId) throws InventoryException {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_STOCK_LEVEL_ONLINE);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryStockLevel() : START");
			vlogDebug("SKU ID : {0}",pCatalogRefId);
		}

		if (pCatalogRefId == null) {
			throw new InventoryException(TRUInventoryConstants.QUERY_STOCK_LEVEL_ONLINE_EXCEPTION);
		}

		if (isTestMode()){
			
			//added for OUTOFSTOCK item list
			if (getOutOfStockListOnline()!= null && !getOutOfStockListOnline().isEmpty() && getOutOfStockListOnline().contains(pCatalogRefId)){
				if (isLoggingDebug()) {
					vlogDebug("Inventory Item stuatus is OUTOFSTOCK from outofstock list");
				}
				return TRUInventoryConstants.DEFAULT_STOCK_LEVEL;
			}
			//added for OUTOFSTOCK item list
			return TRUInventoryConstants.TEST_MODE_STOCK_LEVEL;
		}
		if (isLoggingDebug()) {
			vlogDebug("Inside queryStockLevel: " + pCatalogRefId);
		}

		long level;

		ItemQuantityKey itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
				getPropertyManager().getOnlineString());
		ItemQuantity itemQuantity = null;
		if(isEnableCoherenceOverride()){
			itemQuantity = getItemQuantityFromRepository(itemQuantityKey);
		}
		else{
			itemQuantity = getItemQuantityFromCoherence(itemQuantityKey,TRUInventoryConstants.QUERY_STOCK_LEVEL_ONLINE);
		}

		if (itemQuantity == null) {
			/* return default stock level -1 */
			level = TRUInventoryConstants.DEFAULT_STOCK_LEVEL;
		} else {
			level = itemQuantity.getAtcQuantity();
		}
		if (isLoggingDebug()) {
			vlogDebug("stockLevel = " + level);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryStockLevel() : END");
		}
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_STOCK_LEVEL_ONLINE);
		}
		return level;

	}

	/**
	 * Description: Return how many of a given item is in the system.
	 *
	 * @param pCatalogRefId            - The id of the item involved. locationId - the location of
	 * the inventory
	 * @param pLocationId the location id
	 * @return TEST_MODE_STOCK_LEVEL DEFAULT_STOCK_LEVEL Actual stock level
	 * @throws InventoryException                - if there was a problem determining the number
	 */
	@Override
	public long queryStockLevel(String pCatalogRefId, String pLocationId)
			throws InventoryException {
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_STOCK_LEVEL_STORE);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryStockLevel() : START");
			vlogDebug("SKU ID : {0} , Location ID : {1}",pCatalogRefId,pLocationId);
		}

		if (pCatalogRefId == null || pLocationId == null) {
			throw new InventoryException(TRUInventoryConstants.QUERY_STOCK_LEVEL_STORE_EXCEPTION);
		}

		if (isTestMode()){
			//added for OUTOFSTOCK item list
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStoreIds()!=null && !getOutOfStockListStoreIds().isEmpty() && getOutOfStockListStore().contains(pCatalogRefId)){
				for(String outOfStockStoreId: getOutOfStockListStoreIds()){
					if(pLocationId.equalsIgnoreCase(outOfStockStoreId)){
						if (isLoggingDebug()) {
							vlogDebug("Inventory Item stuatus is OUTOFSTOCK from outofstock list");
						}
						return TRUInventoryConstants.DEFAULT_STOCK_LEVEL;
					}
				}
			}
			//added for OUTOFSTOCK item list
			return TRUInventoryConstants.DEFAULT_STORE_STOCK_LEVEL;
		}
		if (isLoggingDebug()) {
			vlogDebug("Inside queryStockLevel: " + pCatalogRefId + " at " +
					 pLocationId);
		}

		long level;
		ItemQuantityKey itemQuantityKey = null;
		if(StringUtils.isBlank(pLocationId)){
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId,
					getPropertyManager().getOnlineString());
		}else{
			itemQuantityKey = new ItemQuantityKey(pCatalogRefId, pLocationId);
		}
		ItemQuantity itemQuantity = null;
		if(isEnableCoherenceOverride()){
			itemQuantity = getItemQuantityFromRepository(itemQuantityKey);
		}
		else{
			itemQuantity = getItemQuantityFromCoherence(itemQuantityKey, TRUInventoryConstants.QUERY_STOCK_LEVEL_STORE);
		}

		if (itemQuantity == null) {
			level = TRUInventoryConstants.DEFAULT_STOCK_LEVEL;
		}
		else{
			level = itemQuantity.getAtcQuantity();
		}

		if (isLoggingDebug()) {
			vlogDebug("stockLevel = " + level);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryStockLevel() : END");
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_STOCK_LEVEL_STORE);
		}
		return level;

	}
	/**
     * Description: Get list of in-stock ship-to-home items with quantity from a
     * given SKU ID list.
     * 
      * @param pCatalogRefIds
     *            - the ids of the items involved
     * @return A list of TRUInventoryInfo objects that contains SKU id and
     *         quantity.
     */

	public Map<String, TRUInventoryInfo> queryBulkS2HStockLevelMap(List<String> pCatalogRefIds) {

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_REGISTRY_STOCK_LEVEL);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: queryBulkS2HStockLevelMap: START");
			vlogDebug("SKU ID'S : {0}", pCatalogRefIds.toString());
		}

		Map<String, TRUInventoryInfo> truInventoryInfoMap = new HashMap<String, TRUInventoryInfo>();

		List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

		if (isTestMode()) {
			// added for OUTOFSTOCK item list
			if (getOutOfStockListOnline() != null && !getOutOfStockListOnline().isEmpty()) {
				for (String id : getOutOfStockListOnline()) {
					if (pCatalogRefIds.contains(id)) {
						truInventoryInfoMap.put(id, new TRUInventoryInfo(id, TRUInventoryConstants.DEFAULT_STOCK_LEVEL,
								getPropertyManager().getOnlineString()));
						pCatalogRefIds.remove(id);
					}
				}
			}
			// added for OUTOFSTOCK item list
			for (String id : pCatalogRefIds) {
				truInventoryInfoMap.put(id, new TRUInventoryInfo(id, TRUInventoryConstants.TEST_MODE_STOCK_LEVEL,
						getPropertyManager().getOnlineString()));
			}
			return truInventoryInfoMap;
		}

		for (String id : pCatalogRefIds) {
			itemQuantityKeyList.add(new ItemQuantityKey(id, getPropertyManager().getOnlineString()));
		}
		Map<ItemQuantityKey, ItemQuantity> map = null;
		if (isEnableCoherenceOverride()) {
			map = getBatchItemQuantityFromRepository(itemQuantityKeyList);
		}else{
			map =  getBatchItemQuantityFromCoherence(itemQuantityKeyList); 
		}

		if (map != null) {

			for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : map.entrySet()) {
				truInventoryInfoMap.put(entry.getKey().getItemName(), new TRUInventoryInfo(
						entry.getKey().getItemName(), entry.getValue().getAtcQuantity(), null,null,entry.getValue().getAtcStatus()));
			}

			if (isLoggingDebug()) {
				logDebug("@Class::TRUCoherenceInventoryManager::@method:: queryBulkS2HStockLevelMap: END");
			}

		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_REGISTRY_STOCK_LEVEL);
		}
		return truInventoryInfoMap;

	}


	/**
	 * Gets the batch item quantity from coherence.
	 *
	 * @param pItemQuantityKeyList the item quantity key list
	 * @return the batch item quantity from coherence
	 */
	private Map<ItemQuantityKey, ItemQuantity> getBatchItemQuantityFromCoherence(
			List<ItemQuantityKey> pItemQuantityKeyList) {
		
		Map<ItemQuantityKey, ItemQuantity> resultMap = null;
		boolean isException = false;
		try {
			if(getCoherenceCacheAdapter() != null){
				resultMap = getCoherenceCacheAdapter().getBatchItemQuantity(pItemQuantityKeyList);
			}else{
				return null;
			}
			
		}catch (RequestTimeoutException e) {
			isException = true;
			if (isLoggingError()) {
				logError(
						"RequestTimeoutException in @Class::TRUCoherenceInventoryManager::@method::getBatchItemQuantityFromCoherence()", e);
			}
		} catch (ConnectionException connectionExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"ConnectionException in @Class::TRUCoherenceInventoryManager::@method::getBatchItemQuantityFromCoherence()", connectionExc);
			}
		} catch (PortableException portableExc){
			isException = true;
			if (isLoggingError()) {
				logError(
						"PortableException in @Class::TRUCoherenceInventoryManager::@method::getBatchItemQuantityFromCoherence()", portableExc);
			}
		}finally{
			if(isException){
				resultMap = getBatchItemQuantityFromRepository(pItemQuantityKeyList);
			}
		}
		return resultMap;
	}
	
	/**
	 * Gets the batch item quantity from repository.
	 *
	 * @param pItemQuantityKeyList the item quantity key list
	 * @return the batch item quantity from repository
	 */
	private Map<ItemQuantityKey, ItemQuantity> getBatchItemQuantityFromRepository(
			List<ItemQuantityKey> pItemQuantityKeyList) {
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: getBatchItemQuantityFromRepository: START");
		}
		Map<ItemQuantityKey, ItemQuantity> resultMap = new HashMap<ItemQuantityKey, ItemQuantity>(); 
		for (ItemQuantityKey itemQuantityKey : pItemQuantityKeyList) {
			ItemQuantity repositoryItemQuantity = queryInventoryRepository(itemQuantityKey);
			if(repositoryItemQuantity != null){
				resultMap.put(itemQuantityKey, repositoryItemQuantity);
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: getBatchItemQuantityFromRepository: END");
		}
		return resultMap;
	}

	/**
	 * Query bulk stock level of particular store for given SKU ID list.
	 *
	 * @param pCatalogRefIds the catalog ref ids
	 * @param pLocationId the p location id
	 * @param pWareHouseId the ware house id
	 * @return the map
	 */
	public Map<String, TRUInventoryInfo> queryBulkStoreStockLevelMap(List<String> pCatalogRefIds, String pLocationId,
			String pWareHouseId) {

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_REGISTRY_STOCK_LEVEL);
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: queryBulkStoreStockLevelMap: START");
			vlogDebug("SKU ID'S : {0}", pCatalogRefIds.toString());
		}
		String locationId = pLocationId;
		if(StringUtils.isBlank(locationId)){
			locationId = getPropertyManager().getOnlineString();
		}
		Map<String, TRUInventoryInfo> truInventoryInfoMap = new HashMap<String, TRUInventoryInfo>();

		List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

		if (isTestMode()) {
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStoreIds()!=null && !getOutOfStockListStoreIds().isEmpty() ){
				for (String id : getOutOfStockListStore()) {
					if (pCatalogRefIds.contains(id)) {
						for(String outOfStockStoreId: getOutOfStockListStoreIds()){
							if(pLocationId.equalsIgnoreCase(outOfStockStoreId)){
								truInventoryInfoMap.put(outOfStockStoreId, new TRUInventoryInfo(id, TRUInventoryConstants.DEFAULT_STOCK_LEVEL,
										locationId));
							}
						}
						pCatalogRefIds.remove(id);
					}
				}
			}
			// added for OUTOFSTOCK item list
			for (String id : pCatalogRefIds) {
				truInventoryInfoMap.put(id, new TRUInventoryInfo(id, TRUInventoryConstants.TEST_MODE_STOCK_LEVEL,
						locationId));
			}
			return truInventoryInfoMap;
		}
		for (String id : pCatalogRefIds) {
			itemQuantityKeyList.add(new ItemQuantityKey(id, locationId));
		}
		Map<ItemQuantityKey, ItemQuantity> inventoryResponseMap = null;
		if (isEnableCoherenceOverride()) {
			inventoryResponseMap = getBatchItemQuantityFromRepository(itemQuantityKeyList);
		}else{
			inventoryResponseMap =  getBatchItemQuantityFromCoherence(itemQuantityKeyList); 
		}
		if (inventoryResponseMap != null) {

			for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : inventoryResponseMap.entrySet()) {
				ItemQuantity iq = entry.getValue();
				if (iq.getAtcStatus() != TRUInventoryConstants.COHERENCE_OUT_OF_STOCK
						&& iq.getAtcQuantity() > TRUInventoryConstants.INVENTORY_LEVEL_ZERO) {
					truInventoryInfoMap.put(entry.getKey().getItemName(), new TRUInventoryInfo(entry.getKey()
							.getItemName(), entry.getValue().getAtcQuantity(), locationId,null,entry.getValue().getAtcStatus()));
				}
			}
		}
		// Check for warehouse inventory if we do not have
		if (truInventoryInfoMap.size() < pCatalogRefIds.size()) {
			itemQuantityKeyList.clear();
			if (inventoryResponseMap != null) {
				inventoryResponseMap.clear();
			}
			String wareHouseId = pWareHouseId;
			if(StringUtils.isBlank(wareHouseId)){
				wareHouseId = getPropertyManager().getOnlineString();
			}
			for (String id : pCatalogRefIds) {
				if (!truInventoryInfoMap.containsKey(id)) {
					itemQuantityKeyList.add(new ItemQuantityKey(id, wareHouseId));
				}
			}
			inventoryResponseMap = getBatchItemQuantityFromCoherence(itemQuantityKeyList); 

			if (inventoryResponseMap != null) {

				for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : inventoryResponseMap.entrySet()) {
					truInventoryInfoMap.put(entry.getKey().getItemName(), new TRUInventoryInfo(entry.getKey()
							.getItemName(), entry.getValue().getAtcQuantity(), wareHouseId,null,entry.getValue().getAtcStatus()));
				}
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method:: queryBulkStoreStockLevelMap: END");
			vlogDebug("SKU ID'S : {0}", pCatalogRefIds.toString());
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_REGISTRY_STOCK_LEVEL);
		}

		return truInventoryInfoMap;

	}


	/**
	 * Description: Get list of in-stock ship-to-home items with quantity from a
	 * given SKU ID list.
	 * 
	 * @param pCatalogRefIds
	 *            - the ids of the items involved
	 * @return A list of TRUInventoryInfo objects that contains SKU id and
	 *         quantity.
	 */

	public List<TRUInventoryInfo> queryBulkS2HStockLevel(
			List<String> pCatalogRefIds) {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_STOCK_LEVEL);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryBulkS2HStockLevel() : START");
			vlogDebug("SKU ID'S : {0}",pCatalogRefIds.toString());
		}

		List<TRUInventoryInfo> truInventoryInfoList = new ArrayList<TRUInventoryInfo>();

		List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

		if (isTestMode()) {
			// added for OUTOFSTOCK item list
			if (getOutOfStockListOnline() != null && !getOutOfStockListOnline().isEmpty()) {
				for (String id : getOutOfStockListOnline()) {
					if (pCatalogRefIds.contains(id)) {
						truInventoryInfoList.add(new TRUInventoryInfo(id, TRUInventoryConstants.DEFAULT_STOCK_LEVEL, null));
						pCatalogRefIds.remove(id);
					}
				}
			}
			// added for OUTOFSTOCK item list
			for (String id : pCatalogRefIds) {
				truInventoryInfoList.add(new TRUInventoryInfo(id, TRUInventoryConstants.TEST_MODE_STOCK_LEVEL, null));
			}
			return truInventoryInfoList;
		}
		
		for (String id : pCatalogRefIds) {
			itemQuantityKeyList.add(new ItemQuantityKey(id, getPropertyManager().getOnlineString()));
		}
		
		Map<ItemQuantityKey, ItemQuantity> map = null;
		if (isEnableCoherenceOverride()) {
			map = getBatchItemQuantityFromRepository(itemQuantityKeyList);
		}else{
			map =  getBatchItemQuantityFromCoherence(itemQuantityKeyList); 
		}


		if (map != null) {

			for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : map
					.entrySet()) {
				truInventoryInfoList
						.add(new TRUInventoryInfo(entry.getKey().getItemName(),
								entry.getValue().getAtcQuantity(), getPropertyManager().getOnlineString(),null,entry.getValue().getAtcStatus()));
			}

			if (isLoggingDebug()) {
				logDebug("@Class::TRUCoherenceInventoryManager::@method::queryBulkS2HStockLevel() : END");
			}

		}
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_S2H_STOCK_LEVEL);
		}
		return truInventoryInfoList;

	}

	/**
	 * Description: Get a response with id, quantity, location for all in-stock
	 * items.
	 *
	 * @param pInventoryInfos            the ids of the items involved Returns: A list of SKU Ids,
	 *            which are available with the requested quantity.
	 * @return the list
	 */

	public List<TRUInventoryInfo> validateBulkStockLevel(
			List<TRUInventoryInfo> pInventoryInfos) {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_STOCK_LEVEL);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::validateBulkStockLevel() : START");
			vlogDebug("Inventory Info'S : {0}",pInventoryInfos.toString());
		}

		List<TRUInventoryInfo> response = new ArrayList<TRUInventoryInfo>();
		List<TRUInventoryInfo> outOfStockList = new ArrayList<TRUInventoryInfo>();

		if (pInventoryInfos != null) {

			if (isTestMode()) {
				//added for OUTOFSTOCK item list
				for (TRUInventoryInfo info : pInventoryInfos) {
					if (info.getLocationId() != null && getOutOfStockListStore() != null
							&& !getOutOfStockListStore().isEmpty()
							&& getOutOfStockListStore().contains(info.getCatalogRefId())
							&& getOutOfStockListStoreIds() != null && !getOutOfStockListStoreIds().isEmpty()
							&& getOutOfStockListStoreIds().contains(info.getLocationId())) {
						response.add(new TRUInventoryInfo(info
								.getCatalogRefId(),
								TRUInventoryConstants.DEFAULT_STOCK_LEVEL, info
										.getLocationId(), info
										.getOriginalLocationId()));
						outOfStockList.add(info);
					} else if (info.getLocationId() == null && getOutOfStockListOnline() != null
							&& !getOutOfStockListOnline().isEmpty()
							&& getOutOfStockListOnline().contains(info.getCatalogRefId())) {
						response.add(new TRUInventoryInfo(info
								.getCatalogRefId(),
								TRUInventoryConstants.DEFAULT_STOCK_LEVEL, info
										.getLocationId(), info
										.getOriginalLocationId()));
						outOfStockList.add(info);
					}
				}
				pInventoryInfos.removeAll(outOfStockList);
				//added for OUTOFSTOCK item list
				
				for (TRUInventoryInfo info : pInventoryInfos) {
					response.add(new TRUInventoryInfo(info.getCatalogRefId(),
							info.getQuantity(), info.getLocationId(), info.getOriginalLocationId()));
				}
				if (isLoggingDebug()) {
					vlogDebug("response length"+response.size());
					vlogDebug("response  value"+response.toString());
				}
				return response;
			}
			List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

			for (TRUInventoryInfo info : pInventoryInfos) {
				if(info.getLocationId() != null){
					itemQuantityKeyList.add(new ItemQuantityKey(info
							.getCatalogRefId(), info.getLocationId()));
				}else{
					itemQuantityKeyList.add(new ItemQuantityKey(info
							.getCatalogRefId(), getPropertyManager().getOnlineString()));
				}
			}
			if (itemQuantityKeyList.size() !=TRUInventoryConstants.DEFAULT_STATUS && !itemQuantityKeyList.isEmpty()) {
				
				Map<ItemQuantityKey, ItemQuantity> resultMap = null;
				if (isEnableCoherenceOverride()) {
					resultMap = getBatchItemQuantityFromRepository(itemQuantityKeyList);
				}else{
					resultMap =  getBatchItemQuantityFromCoherence(itemQuantityKeyList); 
				}
				
				if(resultMap !=null){
					for (Entry<ItemQuantityKey, ItemQuantity> entry : resultMap
							.entrySet()) {

						ItemQuantity iq = entry.getValue();
						//sending facilityName as null for online inventory 
						if(entry.getKey().getFacilityName() != null && entry.getKey().getFacilityName().equalsIgnoreCase(getPropertyManager().getOnlineString())){
							response.add(new TRUInventoryInfo(entry.getKey()
									.getItemName(), iq.getAtcQuantity(), null,null,entry.getValue().getAtcStatus()));
						}else{
							response.add(new TRUInventoryInfo(entry.getKey()
									.getItemName(), iq.getAtcQuantity(), entry.getKey().getFacilityName(),null,entry.getValue().getAtcStatus()));
						
						}
					}
				}

				if (isLoggingDebug()) {
					logDebug("@Class::TRUCoherenceInventoryManager::@method::validateBulkStockLevel() : END");
					vlogDebug("response length"+response.size());
				}

			}	
		}

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.QUERY_BULK_STOCK_LEVEL);
		}
	
		return response;

	}

	/**
	 * Query inventory repository.
	 *
	 * @param pItemQtyKey the item qty key
	 * @return the list
	 */
	private ItemQuantity queryInventoryRepository(ItemQuantityKey pItemQtyKey) {
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryInventoryRepository() : START");
			logDebug("Item Name is: "+pItemQtyKey.getItemName());
			logDebug("Facility Name is: "+pItemQtyKey.getFacilityName());
		}
		long atcQuantity = TRUInventoryConstants.INVENTORY_LEVEL_ZERO;
		ItemQuantity repoItemQty = null;
		try {
				RepositoryItem[] inventoryItems = null;
				RepositoryItemDescriptor inventoryAvailability = getInventoryRepository().getItemDescriptor(TRUInventoryConstants.INVENTORY_AVAILABILITY);
				RepositoryView repositoryView = inventoryAvailability.getRepositoryView();
				QueryBuilder queryBuilder = repositoryView.getQueryBuilder();
				QueryExpression itemNameProperty = queryBuilder.createPropertyQueryExpression(getPropertyManager().getItemNamePropertyName());
				QueryExpression itemNameValue = queryBuilder.createConstantQueryExpression(pItemQtyKey.getItemName());
				QueryExpression facilityNameProperty = queryBuilder.createPropertyQueryExpression(getPropertyManager().getFacilityNamePropertyName());
				QueryExpression facilityNameValue = queryBuilder.createConstantQueryExpression(pItemQtyKey.getFacilityName());
				Query itemNameQuery = queryBuilder.createComparisonQuery(itemNameProperty, itemNameValue, QueryBuilder.EQUALS);
				Query facilityNameQuery = queryBuilder.createComparisonQuery(facilityNameProperty, facilityNameValue, QueryBuilder.EQUALS);
				Query[] queries = { itemNameQuery, facilityNameQuery };
				Query andQuery = queryBuilder.createAndQuery(queries);
				inventoryItems = repositoryView.executeQuery(andQuery);
				if (inventoryItems != null&& inventoryItems.length > 0) {
					repoItemQty = new ItemQuantity();
					for (RepositoryItem intentoryItem : inventoryItems) {
						atcQuantity = (long) intentoryItem.getPropertyValue(getPropertyManager().getAtcQuantityPropertyName());
						int atcStatus = (int) intentoryItem.getPropertyValue(getPropertyManager().getAtcStatusPropertyName());
						if(isLoggingDebug()){
							logDebug("Atc Quantity is: "+atcQuantity);
							logDebug("Atc Status is: "+atcStatus);
						}
						repoItemQty.setAtcQuantity(atcQuantity);
						repoItemQty.setAtcStatus(atcStatus);
					}
				}
		} catch (RepositoryException re) {
			if(isLoggingError()){
				logError("RepositoryException while getting inventory items",re);
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUCoherenceInventoryManager::@method::queryInventoryRepository() : END");
		}
		return repoItemQty;
	}

	
	/**
	 * Return valid storeIds.
	 *
	 * @param pItemQuantityKeyList the item quantity key list
	 * @return the list
	 */
	private List<String> validMultiStore(List<ItemQuantityKey> pItemQuantityKeyList) {
		
		List<String> validItems = new ArrayList<String>();
		
		Map<ItemQuantityKey, ItemQuantity> facilityResultMap = null;
		if (isEnableCoherenceOverride()) {
			facilityResultMap = getBatchItemQuantityFromRepository(pItemQuantityKeyList);
		}else{
			facilityResultMap =  getBatchItemQuantityFromCoherence(pItemQuantityKeyList); 
		}

		if (facilityResultMap != null) {
			for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : facilityResultMap
					.entrySet()) {
				ItemQuantity itemQuantity = entry.getValue();
				if (itemQuantity.getAtcStatus() != TRUInventoryConstants.COHERENCE_OUT_OF_STOCK) {
					validItems.add(entry.getKey().getFacilityName());
				}

			}
		}
		return validItems;
	}

	/**
	 * Description: Validate a list of locations for SKU availability.
	 *
	 * @param pCatalogRefId            - the id of the item involved locationIds - the list of
	 *            location IDs to validate
	 * @param pLocationIds the location ids
	 * @return A list of location Ids in which the item is IN STOCK
	 */

	public List<String> validateMultiStoreInventory(String pCatalogRefId,
			List<String> pLocationIds) {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.VALIDATE_MULTI_STORE_INVENTORY);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::validateMultiStoreInventory() : START");
			vlogDebug("Location Id's : {0}",pLocationIds.toString());
		}
		List<String> validItems = new ArrayList<String>();
		/* SKU available at all locations in test mode */ 
		if (isTestMode()){
			//added  OUTOFSTOCK item list
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStore()!=null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStore().contains(pCatalogRefId)){
				for(String outOfStockStoreId: getOutOfStockListStoreIds()){
					if(pLocationIds.contains(outOfStockStoreId)){
						pLocationIds.remove(outOfStockStoreId);
					}
				}
				return pLocationIds;
			}
			//added of OUTOFSTOCK item list
			return pLocationIds;
		}
		List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

		for (String locationId : pLocationIds) {
			itemQuantityKeyList.add(new ItemQuantityKey(pCatalogRefId, locationId));
		}

		validItems = validMultiStore(itemQuantityKeyList);
		
		if (isLoggingDebug()) {
			vlogDebug("Valid Stores = " + validItems.toString());
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::validateMultiStoreInventory() : END");
		}
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.VALIDATE_MULTI_STORE_INVENTORY);
		}
		
		return validItems;

	}

	/**
	 * Description: Validate a list of locations for SKU availability.
	 *
	 * @param pCatalogRefId            - the id of the item involved locationIds - the list of
	 *            location IDs to validate
	 * @param pLocationIds the location ids
	 * @return A list of location Ids in which the item is IN STOCK
	 */

	public Map<String,TRUInventoryInfo> validateMultiStoreInventoryForStoreLocator(String pCatalogRefId,
			List<String> pLocationIds) {
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(this.getClass().getName(),
					TRUInventoryConstants.VALIDATE_MULTI_STORE_INVENTORY);
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::validateMultiStoreInventoryForStoreLocator() : START");
			vlogDebug("Location Id's : {0}",pLocationIds.toString());
		}
		List<String> validItems = new ArrayList<String>();
		Map<String,TRUInventoryInfo> response = new HashMap<String,TRUInventoryInfo>();
		/* SKU available at all locations in test mode */ 
		if (isTestMode()){
			//added  OUTOFSTOCK item list
			if (getOutOfStockListStore()!= null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStore()!=null && !getOutOfStockListStore().isEmpty() && getOutOfStockListStore().contains(pCatalogRefId)){
				for(String outOfStockStoreId: getOutOfStockListStoreIds()){
					if(pLocationIds.contains(outOfStockStoreId)){
						response.put(outOfStockStoreId, new TRUInventoryInfo(pCatalogRefId, TRUInventoryConstants.DEFAULT_STOCK_LEVEL , outOfStockStoreId));
						pLocationIds.remove(outOfStockStoreId);
					}
				}
				for (String locationId : pLocationIds) {
					response.put(locationId, new TRUInventoryInfo(pCatalogRefId, TRUInventoryConstants.TEST_MODE_STOCK_LEVEL , locationId));
				}
			}
			//added of OUTOFSTOCK item list
			return response;
		}
		List<ItemQuantityKey> itemQuantityKeyList = new ArrayList<ItemQuantityKey>();

		for (String locationId : pLocationIds) {
			itemQuantityKeyList.add(new ItemQuantityKey(pCatalogRefId, locationId));
		}
		Map<ItemQuantityKey, ItemQuantity> facilityResultMap = null;
		if (isEnableCoherenceOverride()) {
			facilityResultMap = getBatchItemQuantityFromRepository(itemQuantityKeyList);
		}else{
			facilityResultMap =  getBatchItemQuantityFromCoherence(itemQuantityKeyList); 
		}
		if (facilityResultMap != null) {
			for (Map.Entry<ItemQuantityKey, ItemQuantity> entry : facilityResultMap
					.entrySet()) {
				ItemQuantity itemQuantity = entry.getValue();
				if (itemQuantity.getAtcStatus() != TRUInventoryConstants.COHERENCE_OUT_OF_STOCK) {
					response.put(entry.getKey().getFacilityName(), new TRUInventoryInfo(entry.getKey()
							.getItemName(), itemQuantity.getAtcQuantity(), entry.getKey().getFacilityName(),null,entry.getValue().getAtcStatus()));
				}

			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Valid Stores = " + validItems.toString());
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCoherenceInventoryManager::@method::validateMultiStoreInventoryForStoreLocator() : END");
		}
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(this.getClass().getName(),
					TRUInventoryConstants.VALIDATE_MULTI_STORE_INVENTORY);
		}
		
		return response;

	}
	
	/**
	 * Checks if is enable coherence override.
	 *
	 * @return true, if is enable coherence override
	 */
	public boolean isEnableCoherenceOverride() {
		Boolean enableCoherenceOverride = false;
		try {
			RepositoryItem[] invConfigs = getInventoryConfigItems();
			  if(invConfigs != null && invConfigs.length > TRUInventoryConstants.INT_ZERO){
				enableCoherenceOverride = (Boolean) invConfigs[TRUInventoryConstants.INT_ZERO]
						.getPropertyValue(getPropertyManager()
								.getCoherenceOverridePropertyName());
			  }
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("RepositoryException while checking invenotryConfig",e);
			}
		}
		  
		return enableCoherenceOverride;
	}
	/**
	 * Get inventory config items
	 * @return
	 * @throws RepositoryException
	 */

	private RepositoryItem[] getInventoryConfigItems() throws RepositoryException {
		RepositoryView view;
		  RepositoryItem [] invConfigs;
		  view = getStoreConfigRepository().getView(TRUInventoryConstants.INVENTORY_CONFIG);
		  QueryBuilder builder = view.getQueryBuilder();
		  Query profanityWordsQuery = builder.createUnconstrainedQuery();
		  invConfigs = view.executeQuery(profanityWordsQuery);
		return invConfigs;
	}
	
	/**
	 * Enable coherence override.
	 */
	public void enableCoherenceOverride() {
		MutableRepositoryItem item;
		try {
			RepositoryItem[] invConfigs = getInventoryConfigItems();
			  if(invConfigs != null && invConfigs.length > TRUInventoryConstants.INT_ZERO){
				  item = (MutableRepositoryItem) invConfigs[TRUInventoryConstants.INT_ZERO];
				  if(item != null){
					  item.setPropertyValue(getPropertyManager().getCoherenceOverridePropertyName(), true);
					  getCoherenceFlagCacheFlushSource().sendRepositoryItemToflushCache();
				  }
			  }
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("RepositoryException while checking enableCoherenceOverride",e);
			}
		}
	}
	
	/**
	 * Disable coherence override.
	 */
	public void disableCoherenceOverride() {
		MutableRepositoryItem item;
		try {
			RepositoryItem[] invConfigs = getInventoryConfigItems();
			  if(invConfigs != null && invConfigs.length > TRUInventoryConstants.INT_ZERO){
				  item = (MutableRepositoryItem) invConfigs[TRUInventoryConstants.INT_ZERO];
				  if(item != null){
					  item.setPropertyValue(getPropertyManager().getCoherenceOverridePropertyName(), false);
					  getCoherenceFlagCacheFlushSource().sendRepositoryItemToflushCache();
				  }
			  }
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("RepositoryException while checking disableCoherenceOverride",e);
			}
		}
	}
	
	/**
	 * atg.commerce.inventory.AbstractInventoryManagerImpl#purchase unimplemented method
	 *
	 * @param pArg0 the arg0
	 * @param pArg1 the arg1
	 * @return int
	 * @throws InventoryException the inventory exception
	 */
	@Override
	public int purchase(String pArg0, double pArg1) throws InventoryException {
		return 0;
	}
	
	/**
	 * atg.commerce.inventory.AbstractInventoryManagerImpl#purchase unimplemented method
	 *
	 * @param pArg0 the arg0
	 * @param pArg1 the arg1
	 * @param pArg2 the arg2
	 * @return int
	 * @throws InventoryException the inventory exception
	 */
	@Override
	public int purchase(String pArg0, double pArg1, String pArg2)
			throws InventoryException {
		return 0;
	}
	
	/**
	 * Alert logger.
	 *
	 * @param pErrorMessage the error message
	 */
	private void alertLogger(String pErrorMessage){
		String errorMessage = TRUInventoryConstants.DEFAULT_ERROR_MESSAGE;
		Map<String, String> extra = new ConcurrentHashMap<String, String>();	
		extra.put(TRUInventoryConstants.STR_RESPONSE_CODE,TRUInventoryConstants.DEFAULT_RESPONSE_CODE );	
		extra.put(TRUInventoryConstants.STR_PAGE, TRUInventoryConstants.STR_CART_PAGE);
		if (StringUtils.isNotBlank(pErrorMessage)) {
			errorMessage = pErrorMessage;
		}
		extra.put(TRUInventoryConstants.STR_ERROR, errorMessage);
		getAlertLogger().logFailure(TRUInventoryConstants.OPERATION_NAME,TRUInventoryConstants.TASK_NAME,extra);
	}
}