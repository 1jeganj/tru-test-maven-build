package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;
import atg.repository.ItemDescriptorImpl;
import atg.repository.Repository;
import atg.repository.RepositoryException;

import com.tru.commerce.inventory.TRUInventoryConstants;

/**
 *This Sink is to receive the message to flush cache of coherence override flag.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCoherenceFlagCacheFlushSink extends GenericService implements
		MessageSink {
	
	/** The m store config repository. */
	private Repository mStoreConfigRepository;
	
	/**
	 * @return the storeConfigRepository
	 */
	public Repository getStoreConfigRepository() {
		return mStoreConfigRepository;
	}


	/**
	 * @param pStoreConfigRepository the storeConfigRepository to set
	 */
	public void setStoreConfigRepository(Repository pStoreConfigRepository) {
		mStoreConfigRepository = pStoreConfigRepository;
	}


	/**
	 * This is to receive the message.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCoherenceFlagCacheFlushSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		String coherenceFlagCacheFlushMessage = null;
		if(objMessage != null && objMessage.getObject() instanceof String){
			coherenceFlagCacheFlushMessage =  (String) objMessage.getObject();
			if(TRUInventoryConstants.COHERENCE_FLAG_CACHE_FLUSH.equalsIgnoreCase(coherenceFlagCacheFlushMessage)){
				try {
					ItemDescriptorImpl inventoryConfig = (ItemDescriptorImpl)getStoreConfigRepository().getItemDescriptor(TRUInventoryConstants.INVENTORY_CONFIG);
					inventoryConfig.invalidateItemCache();
				} catch (RepositoryException e) {
					if(isLoggingError()){
						logError("RepositoryException in TRUCoherenceFlagCacheFlushSink",e);
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCoherenceFlagCacheFlushSink method : receiveMessage");
		}
	}
}
