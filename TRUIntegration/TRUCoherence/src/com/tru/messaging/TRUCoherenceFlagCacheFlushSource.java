package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.commerce.inventory.TRUInventoryConstants;

/**
 *
 * @author PA
 * @version 1.0
 */
public class TRUCoherenceFlagCacheFlushSource extends GenericService implements
		MessageSource {

	/**
	 * mPortName.
	 */
	private String mPortName;

	/**
	 * Started.
	 */
	private boolean mStarted;

	/**
	 * mMessageSourceContext.
	 */
	private MessageSourceContext mMessageSourceContext;

	/**
	 * This method is to send the master import message.
	 *
	 */
	public void sendRepositoryItemToflushCache(){
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCoherenceFlagCacheFlushSource method : sendRepositoryItemToflushCache");
		}
		ObjectMessage objectMessage = null;
		//Create object message
		try {
			objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
			//Set Object message
			objectMessage.setObject(TRUInventoryConstants.COHERENCE_FLAG_CACHE_FLUSH);
			if (isLoggingDebug()) {
				logDebug("objectMessage in sendRepositoryItemToflushCache() Method...." + objectMessage);
			}
			//Send Coherence flag cache flush
			getMessageSourceContext().sendMessage(getPortName(), objectMessage);
		} catch (JMSException e) {
			if(isLoggingError()){
				vlogError("JMSException: While creating object message and execption details are : {0}", e);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCoherenceFlagCacheFlushSource method : sendRepositoryItemToflushCache");
		}
	}

	/**
	 * MessageSourceContext.
	 * @param pMessageSourceContext - MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pMessageSourceContext) {
		mMessageSourceContext = pMessageSourceContext;
	}

	/**
	 * startMessageSource.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCoherenceFlagCacheFlushSource method : startMessageSource");
		}
		mStarted = true;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCoherenceFlagCacheFlushSource method : startMessageSource");
		}
	}

	/**
	 * stopMessageSource.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCoherenceFlagCacheFlushSource method : stopMessageSource");
		}
		mStarted = false;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCoherenceFlagCacheFlushSource method : stopMessageSource");
		}
	}

	/**
	 * This method returns the started value.
	 *
	 * @return the started
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method sets the started with parameter value pStarted.
	 *
	 * @param pStarted the started to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * This method returns the context value.
	 *
	 * @return the context
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mMessageSourceContext;
	}

	/**
	 * This method returns the portName value.
	 *
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * This method sets the portName with parameter value pPortName.
	 *
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

}