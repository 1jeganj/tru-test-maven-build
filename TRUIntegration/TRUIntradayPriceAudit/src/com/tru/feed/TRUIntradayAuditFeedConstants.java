package com.tru.feed;

/**
 * The Class TRUIntradayAuditFeedConstants hold constants related to intraday
 * price audit.
 */
public class TRUIntradayAuditFeedConstants {

	/** The Constant NUMBER_ONE. */
	public static final int INTEGER_NUMBER_ONE = 1;

	/** The Constant PRICE. */
	public static final String PRICE = "price";

	/** The Constant START_HEADER. */
	public static final String START_HEADER = "START";

	/** The Constant NEW_LINE_STRING. */
	public static final String NEW_LINE_STRING = "\n";

	/** The Constant PIPE_DELIMITER. */
	public static final String PIPE_DELIMITER = "|";

	/** The Constant END_TRAILER. */
	public static final String END_TRAILER = "END";

	/** The Constant JMS_MESSAGE. */
	public static final String JMS_MESSAGE = "JMSMessage";
	
	/** The Constant COMMA_STRING. */
	public static final String COMMA_STRING = ",";
	
	/** The Constant ATG_COMMERCE_PRICE_LISTS. */
	public static final String ATG_COMMERCE_PRICE_LISTS = "/atg/commerce/pricing/priceLists/PriceLists";

}
