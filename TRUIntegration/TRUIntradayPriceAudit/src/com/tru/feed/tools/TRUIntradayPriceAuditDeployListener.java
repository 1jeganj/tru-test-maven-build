package com.tru.feed.tools;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.jms.JMSException;

import atg.core.util.StringUtils;
import atg.deployment.common.event.DeploymentEvent;
import atg.deployment.common.event.DeploymentEventListener;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectHome;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.versionmanager.AssetVersion;
import atg.versionmanager.exceptions.VersionException;

import com.tru.feed.TRUIntradayAuditFeedConstants;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.merchandising.constants.TRUMerchConstants;
import com.tru.messaging.TRUIntradayPriceMessageSource;

/**
 * This class is used to extend GenericService class to capture emergency and
 * special events price updates
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUIntradayPriceAuditDeployListener extends GenericService implements DeploymentEventListener {

	/** The Site name. */
	private String mSiteName;

	/** The Enabled. */
	private boolean mEnabled;

	/** The Intraday price message source. */
	private TRUIntradayPriceMessageSource mIntradayPriceMessageSource;

	/** The Price properties. */
	private TRUPriceFeedRepositoryProperties mPriceProperties;

	/** The mRegularPriceFeedName. */
	private String mRegularPriceFeedName;

	/**
	 * This method will invalidate the product catalog cache.
	 * 
	 * @param pDeploymentEvent
	 *            of type DeploymentEvent
	 */
	@Override
	public void deploymentEvent(DeploymentEvent pDeploymentEvent) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUIntradayPriceAuditListener.deploymentEvent() method");
		}

		if (isEnabled() && pDeploymentEvent != null && DeploymentEvent.DEPLOYMENT_COMPLETE == pDeploymentEvent.getNewState()
				&& getSiteName().equalsIgnoreCase(pDeploymentEvent.getTarget())) {

			Set affectedRepositories = pDeploymentEvent.getAffectedRepositories();
			if (affectedRepositories != null && affectedRepositories.contains(TRUIntradayAuditFeedConstants.ATG_COMMERCE_PRICE_LISTS)) {
				Set<RepositoryItem> repositoryItemsUpdated = getUpdatedItems(pDeploymentEvent);
				processRepoToFetchSkus(repositoryItemsUpdated);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUIntradayPriceAuditListener.deploymentEvent() method");
		}
	}

	/**
	 * Process repo to fetch skus.
	 *
	 * @param pRepositoryItemsUpdated the repository items updated
	 */
	private void processRepoToFetchSkus(Set<RepositoryItem> pRepositoryItemsUpdated) {
		if (isLoggingDebug()) {
			logDebug("repositoryItemsUpdated in deployment:" + pRepositoryItemsUpdated);
		}
		if (pRepositoryItemsUpdated != null && !pRepositoryItemsUpdated.isEmpty()) {
			try {
				Set<String> skuIds = new HashSet<String>();
				for (RepositoryItem repositoryItem : pRepositoryItemsUpdated) {
					String skuId = (String) repositoryItem.getPropertyValue(getPriceProperties().getSkuId());
					skuIds.add(skuId);
				}
				if (skuIds != null && !skuIds.isEmpty()) {
					vlogDebug("Before Sending to local messaging queue");
					mIntradayPriceMessageSource.sendMessage(skuIds);
					vlogDebug("After Sending to local messaging queue");
				}

			} catch (JMSException exc) {
				vlogError("JMS Exception while posting message to local queue item ", exc);
			}
		}
	}

	/**
	 * Gets the updated items.
	 * 
	 * @param pDeploymentEvent
	 *            the deployment event
	 * @return the updated items
	 */
	private Set<RepositoryItem> getUpdatedItems(DeploymentEvent pDeploymentEvent) {
		vlogDebug("Start of TRUIntradayPriceAuditListener.getProjectName() method");

		String[] projects = null;
		ProjectHome projectHome = null;
		String projectName = null;
		Set workSpaceAssets = null;
		Iterator<AssetVersion> workspaceAssetsIterator = null;
		RepositoryItem repositoryItem = null;
		Set<RepositoryItem> repositoryItemsUpdated = new LinkedHashSet<RepositoryItem>();
		try {
			projects = pDeploymentEvent.getDeploymentProjectIDs();
			projectHome = ProjectConstants.getPersistentHomes().getProjectHome();
			if (projects != null && projects.length >= TRUMerchConstants.INTEGER_NUMBER_ONE && projectHome != null) {
				Project project = projectHome.findById(projects[0]);
				if (project != null) {
					workSpaceAssets = project.getAssets();
					projectName = project.getDisplayName();
					vlogDebug("TRUIntradayPriceAuditListener.getProjectName() projectName : {0}" , projectName);
				}
				if (workSpaceAssets != null && !projectName.contains(mRegularPriceFeedName)) {
					workspaceAssetsIterator = (Iterator<AssetVersion>) workSpaceAssets.iterator();
				}
				if (workspaceAssetsIterator != null) {
					while (workspaceAssetsIterator.hasNext()) {
						AssetVersion assetVersion = workspaceAssetsIterator.next();
						try {
							if (assetVersion != null) {
								repositoryItem = assetVersion.getRepositoryItem();
								reposWithExtendedPrice(repositoryItem, repositoryItemsUpdated);
							}
						} catch (RepositoryException repoExc) {
							vlogError("Repository Exception while accessing item ", repositoryItem, repoExc);
						} catch (VersionException verExc) {
							vlogError("Version Exception while accessing item ", repositoryItem, verExc);
						}
					}
				}

			}
		} catch (EJBException e) {
			if (isLoggingError()) {
				logError(e);
			}
		} catch (FinderException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUIntradayPriceAuditListener.getProjectName() method");
		}
		return repositoryItemsUpdated;
	}

	/**
	 * Repos with extended price.
	 *
	 * @param pRepositoryItem the repository item
	 * @param pRepositoryItemsUpdated the repository items updated
	 * @throws RepositoryException the repository exception
	 */
	private void reposWithExtendedPrice(RepositoryItem pRepositoryItem, Set<RepositoryItem> pRepositoryItemsUpdated) throws RepositoryException {
		if (pRepositoryItem != null) {
			String itemDescriptorName = pRepositoryItem.getItemDescriptor().getItemDescriptorName();
			if (StringUtils.isNotEmpty(itemDescriptorName) && itemDescriptorName.equalsIgnoreCase(TRUIntradayAuditFeedConstants.PRICE)) {
				RepositoryItem item = (RepositoryItem) pRepositoryItem.getPropertyValue(getPriceProperties().getPriceListPropertyName());
				if (item != null) {
					vlogDebug("TRUIntradayPriceAuditListener.getProjectName() item id : {0} " , item.getRepositoryId());
					if (item.getPropertyValue(getPriceProperties().getBasePriceListPropertyName()) != null) {
						pRepositoryItemsUpdated.add(pRepositoryItem);
					}
				}
			}
		}
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * @return the intradayPriceMessageSource
	 */
	public TRUIntradayPriceMessageSource getIntradayPriceMessageSource() {
		return mIntradayPriceMessageSource;
	}

	/**
	 * @param pIntradayPriceMessageSource
	 *            the intradayPriceMessageSource to set
	 */
	public void setIntradayPriceMessageSource(TRUIntradayPriceMessageSource pIntradayPriceMessageSource) {
		mIntradayPriceMessageSource = pIntradayPriceMessageSource;
	}

	/**
	 * @return the priceProperties
	 */
	public TRUPriceFeedRepositoryProperties getPriceProperties() {
		return mPriceProperties;
	}

	/**
	 * @param pPriceProperties
	 *            the priceProperties to set
	 */
	public void setPriceProperties(TRUPriceFeedRepositoryProperties pPriceProperties) {
		mPriceProperties = pPriceProperties;
	}

	/**
	 * @return the mSiteName
	 */
	public String getSiteName() {
		return mSiteName;
	}

	/**
	 * @param pSiteName
	 *            the mSiteName to set
	 */
	public void setSiteName(String pSiteName) {
		mSiteName = pSiteName;
	}

	/**
	 * @return the mRegularPriceFeedName
	 */
	public String getRegularPriceFeedName() {
		return mRegularPriceFeedName;
	}

	/**
	 * @param pRegularPriceFeedName
	 *            the mRegularPriceFeedName to set
	 */
	public void setRegularPriceFeedName(String pRegularPriceFeedName) {
		this.mRegularPriceFeedName = pRegularPriceFeedName;
	}

}
