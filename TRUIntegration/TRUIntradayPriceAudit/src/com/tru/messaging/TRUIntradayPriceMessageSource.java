package com.tru.messaging;

import java.util.Set;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.feed.TRUIntradayAuditFeedConstants;

/**
 * This class is used to generate messages from auditFeedMap and post it to the
 * local sql DMS queue.
 */
public class TRUIntradayPriceMessageSource extends GenericService implements MessageSource {

	/** The Context. */
	private MessageSourceContext mContext;

	/** The Started. */
	boolean mStarted = false;

	/** The Port name. */
	private String mPortName;

	/**
	 * Frame message from auditFeedMap and Send message to local queue.
	 * 
	 * @param pSkuIds
	 *            - pSkuIds
	 * @throws JMSException
	 *             the JMS exception
	 */
	public void sendMessage(Set<String> pSkuIds) throws JMSException {

		vlogDebug("START: TRUIntradayPriceMessageSource sendMessage() ");

		if (pSkuIds != null) {
			StringBuilder stringBuffer = new StringBuilder();
			if (getMessageSourceContext() != null) {
				TextMessage msg = getMessageSourceContext().createTextMessage(getPortName());
				msg.setJMSType(TRUIntradayAuditFeedConstants.JMS_MESSAGE);
				vlogDebug("TRUIntradayPriceMessageSource skuIds size : {0} skuId info :{1}" , pSkuIds.size() , pSkuIds);
				int counter = 0;
				for (String skuId : pSkuIds) {
					counter++;
					stringBuffer.append(skuId);
					if (pSkuIds.size() > TRUIntradayAuditFeedConstants.INTEGER_NUMBER_ONE && pSkuIds.size() != counter) {
						stringBuffer.append(TRUIntradayAuditFeedConstants.COMMA_STRING);
					}
				}
				msg.setText(stringBuffer.toString());
				vlogDebug("Message in sendMessage() Method....{0}" , msg.getText());
				getMessageSourceContext().sendMessage(getPortName(), msg);
			}
		}
		vlogDebug("END: TRUIntradayPriceMessageSource sendMessage()");
	}

	/**
	 * This method startMessageSource to log.
	 */
	@Override
	public void startMessageSource() {
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}

	/**
	 * This method stopMessageSource to log.
	 */
	@Override
	public void stopMessageSource() {
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * @param pStarted
	 *            the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * @param pPortName
	 *            the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * @param pContext
	 *            the mContext to set.
	 */
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}

	/**
	 * 
	 * @return mContext.
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mContext;
	}

}
