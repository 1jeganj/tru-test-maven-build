drop table tru_custom_price;

CREATE TABLE tru_custom_price (
	price_id 		varchar2(254)	NOT NULL REFERENCES dcs_price(price_id),
	sale_price 		number(19, 7)	NOT NULL,
	country_code 		varchar2(3)	NOT NULL,
	market_code 		varchar2(3)	NOT NULL,
	store_number 		number(5)	NOT NULL,
	PRIMARY KEY(price_id)
);

CREATE INDEX tru_custom_price_price_idx ON tru_custom_price(price_id);