CREATE TABLE parade_paypal_pament_group
(
	payer_id VARCHAR(40),
	payPal_token VARCHAR(40),
	payPal_corelation_id VARCHAR(40),
	payPal_transaction_id VARCHAR(50),
	payment_group_id VARCHAR(40) NOT NULL,
	PRIMARY KEY (payment_group_id)
) ;

CREATE TABLE parade_billing_address
(
	payment_group_id VARCHAR(40) NOT NULL,
    prefix VARCHAR(40),
	first_name VARCHAR(40),
	middle_name VARCHAR(10),
	last_name VARCHAR(40),
	suffix VARCHAR(40),
	address_1 VARCHAR(40),
	address_2 VARCHAR(40),
	address_3 VARCHAR(40),
	city VARCHAR(40),
	state VARCHAR(40),
    county VARCHAR(40),
	postal_code VARCHAR(40),
	country VARCHAR(40),
	phone_number VARCHAR(40),
	email VARCHAR(40),
	PRIMARY KEY (payment_group_id)
) ;