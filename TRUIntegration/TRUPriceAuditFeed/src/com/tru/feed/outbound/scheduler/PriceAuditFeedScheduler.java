package com.tru.feed.outbound.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feed.outbound.tools.FeedExportInvoker;

/**
 * This class is PriceAuditFeedScheduler
 * @author PA.
 * 
 */
public class PriceAuditFeedScheduler extends SingletonSchedulableService {
	
	/** Property to hold mExportJobInvoker */
	
	private FeedExportInvoker mExportJobInvoker;
	
	/** Property to hold mEnable */
	
	private boolean mEnable;
	
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * 
	 * @return the Export Job
	 */

	public FeedExportInvoker getExportJobInvoker() {
		return mExportJobInvoker;
	}

	/**
	 * 
	 * @param pExportJobInvoker the Job Invoker
	 * 
	 */
	public void setExportJobInvoker(FeedExportInvoker pExportJobInvoker) {
		mExportJobInvoker = pExportJobInvoker;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: PriceAuditFeed.doScheduledTask()");
		}
        if(isEnable()){
           doPriceAuditJob();	
        }
		if (isLoggingDebug()) {
			logDebug("Exit From :: PriceAuditFeed.doScheduledTask()");
		}
	}
	/**
	 * @return
	 */
	public void doPriceAuditJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: PriceAuditFeed.doPriceAuditJob()");
		}
		getExportJobInvoker().doExportFeedJob();
		if (isLoggingDebug()) {
			logDebug("Exit from :: PriceAuditFeed.doPriceAuditJob()");
		}
	}
}
