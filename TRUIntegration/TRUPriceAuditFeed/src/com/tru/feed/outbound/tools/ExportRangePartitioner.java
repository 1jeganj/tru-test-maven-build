/*
 *  This class is used to implement multi-threading concept
 */
package com.tru.feed.outbound.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;


/**
 * The Class RangePartitioner.
 */
public class ExportRangePartitioner extends GenericService implements Partitioner {
	
	/** The Feed context. */
	private FeedContext mFeedContext;
	
	/**
	 * Gets the feed context.
	 *
	 * @return the feed context
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		this.mFeedContext = pFeedContext;
	}

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public int getRange() {
	List<RepositoryItem> lBaseFeedProcessVOList=(List<RepositoryItem>)getFeedContext().getData(TRUPriceAuditFeedConstants.SKU_ITEMS);
		return lBaseFeedProcessVOList.size();
	}

	/**
	 * This method is used to create the partition/thread based upon the grid-size
	 * @param pGridSize - GridSize
	 * @return Map<String, ExecutionContext>
	 */
	@Override
	public Map<String, ExecutionContext> partition(int pGridSize) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ExportRangePartitioner.partition()");
		}
		int temp = pGridSize;
		
		Map<String, Range> individualThreadRange = new HashMap<String, Range>();
		Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();
		if (((getRange()) / temp) == TRUPriceAuditFeedConstants.NUMBER_ZERO) {

			temp = TRUPriceAuditFeedConstants.NUMBER_ONE;
		}
		List<String> entityList = new ArrayList<String>();
		entityList.add(TRUPriceAuditFeedConstants.EXPORT_FEED);
		for (int start = TRUPriceAuditFeedConstants.NUMBER_ONE; start <= temp; start++) {

			ExecutionContext value = new ExecutionContext();
			
			for (String EntityName : entityList) {
				int entitySize;
				
					entitySize = getRange();
				
				Long individualRange = Long.valueOf((entitySize) / temp);

				Range CurrentEntityRange = null;
				Long fromId = null;
				Long toId = null;
				Long remainder = null;

				if (start == TRUPriceAuditFeedConstants.NUMBER_ONE) {
					fromId = Long.valueOf(TRUPriceAuditFeedConstants.NUMBER_ZERO);
					if (individualRange == TRUPriceAuditFeedConstants.NUMBER_ZERO) {
						toId =Long.valueOf(TRUPriceAuditFeedConstants.NUMBER_ZERO);
					} else {
						toId = individualRange;
					}

				} else {
					CurrentEntityRange = individualThreadRange.get(EntityName);
					fromId =Long.valueOf(CurrentEntityRange.getFrom());
					toId = Long.valueOf(CurrentEntityRange.getTo());

					if (individualRange == TRUPriceAuditFeedConstants.NUMBER_ZERO) {
						fromId =Long.valueOf(TRUPriceAuditFeedConstants.NUMBER_ZERO);
						toId = Long.valueOf(TRUPriceAuditFeedConstants.NUMBER_ZERO);
					} else {
						fromId = toId;
						toId += individualRange;
					}
				}
				remainder = Long.valueOf((entitySize) % (temp));
				if (start == temp) {
					toId = toId + remainder;

				}
				individualThreadRange.put(EntityName,
						new Range(fromId.intValue(), toId.intValue()));
			}
			Map<String, Range> lThreadRange = new HashMap<String, Range>();
			lThreadRange.putAll(individualThreadRange);
			value.put(TRUPriceAuditFeedConstants.RANGE_MAP, lThreadRange);
			result.put(TRUPriceAuditFeedConstants.PARTITION + start, value);
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: ExportRangePartitioner.partition()");
		}
		return result;
	}

}
