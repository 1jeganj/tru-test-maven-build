
package com.tru.feed.outbound.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import atg.nucleus.GenericService;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.feed.outbound.vo.PriceAuditFeedVO;

/**
 * The Class PriceAuditItemWriterToMemory.
 * @author PA.
 * @version 1.0.
 */
public class PriceAuditItemWriterToMemory extends GenericService implements ItemWriter<PriceAuditFeedVO>,ItemStream {

	/** The m feed context. */
	private FeedContext mFeedContext;
	
	/**  The property m productVOs.*/
	private List<PriceAuditFeedVO> mPriceAuditFeedVOs;
	
	
	/**
	 * Gets the feed context.
	 * 
	 * @return the mFeedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 * 
	 * @param pFeedContext
	 *            the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * Save data.
	 * 
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	protected void saveData(String pCongifKey, Object pConfigValue) {
		getFeedContext().setData(pCongifKey, pConfigValue);

	}

	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	protected Object retriveData(String pDataMapKey) {

		return getFeedContext().getData(pDataMapKey);

	}


	/**
	 *   The write method add the pVOItems and set it to the list of productVos.
	 *   @param pVOItems - VOItems
	 */
	@Override
	public void write(List<? extends PriceAuditFeedVO> pVOItems) {
		if (isLoggingDebug()) {
			logDebug("Entered Into PriceAuditItemWriterToMemory:write() method");
			if (pVOItems != null) {
				logDebug("PriceAuditItemWriterToMemory write() : PriceAuditVO Size :  " + pVOItems.size());
			}
		}
		if (pVOItems != null) {
			Iterator<? extends PriceAuditFeedVO> auditFeedVOs = pVOItems.iterator();
			if (auditFeedVOs != null) {
				while (auditFeedVOs.hasNext()) {
					addPriceAuditVos(auditFeedVOs);
				}
			}
		}
		if (mPriceAuditFeedVOs != null) {
			int voSize = mPriceAuditFeedVOs.size();
			if (isLoggingDebug()) {
				logDebug("Product Vo's List Size :: --->" + voSize);
			}
		}
	}

	/**
	 * Adds the price audit vos.
	 *
	 * @param pAuditFeedVOs the audit feed v os
	 */
	private void addPriceAuditVos(Iterator<? extends PriceAuditFeedVO> pAuditFeedVOs) {
		List<? extends PriceAuditFeedVO> feedVOs = (List<? extends PriceAuditFeedVO>) pAuditFeedVOs.next();
		if (feedVOs != null) {
			for (PriceAuditFeedVO priceAuditFeedVO : feedVOs) {
				if (mPriceAuditFeedVOs != null && priceAuditFeedVO != null) {
					mPriceAuditFeedVOs.add(priceAuditFeedVO);
				}
			}
		}
	}

	/**
	 *   The close method holds the listProductVos object and set it to the context level.
	 *   @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public void close() throws ItemStreamException {
		if(getFeedContext()!=null){
		getFeedContext().setData(TRUPriceAuditFeedConstants.EXPORT_LIST, mPriceAuditFeedVOs);
		}else{
			if(isLoggingDebug()){
				logDebug("FeedContext is getting null in ExportFeedItemWriterToMemory:close()");
			}
		}
	}

/**
 *   The open method initializes the list of productVOs.
 *   @param pArg0 - Arg0
 *   @throws ItemStreamException - ItemStreamException
 */
	@Override
	public void open(ExecutionContext pArg0) throws ItemStreamException {
		mPriceAuditFeedVOs=new ArrayList<PriceAuditFeedVO>();
	}

	/**
	 *   The update method needs to override but does not have any business logic.
	 *   @param pArg0 - Arg0
	 *   @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public void update(ExecutionContext pArg0) throws ItemStreamException {
		if(isLoggingDebug()){
			logDebug("Entered Into ExportFeedItemWriterToMemory:update() method");
		}
	}

	
}
