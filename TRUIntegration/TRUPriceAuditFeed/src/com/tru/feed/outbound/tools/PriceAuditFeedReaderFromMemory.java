package com.tru.feed.outbound.tools;

import java.util.Iterator;
import java.util.List;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.feed.outbound.vo.PriceAuditFeedVO;

/**
 * The Class PriceAuditFeedReaderFromMemory.
 */
public class PriceAuditFeedReaderFromMemory extends AbstractFeedItemReaderFromMemory {
	/**
	 * The m iterator.
	 */
	Iterator<? extends PriceAuditFeedVO> mIterator = null;
	/**
	 * Next.
	 * 
	 * @return the base feed process vo
	 */
	protected PriceAuditFeedVO next()
	{
		if (mIterator.hasNext()) {
				return (PriceAuditFeedVO)mIterator.next();
			} 
		return null;
	}
	/**
	 * (non-Javadoc).
	 *
	 * @see com.tru.feed.outbound.tools.AbstractFeedItemReader#doOpen()
	 */
	@SuppressWarnings("unchecked")
	public  void  doOpen()  {
		synchronized(this) {
			List<PriceAuditFeedVO> priceAuditVOsList=(List<PriceAuditFeedVO>)getFeedContext().getData(TRUPriceAuditFeedConstants.EXPORT_LIST);
			if (isLoggingDebug()) {
				logDebug("PriceAuditFeedReaderFromMemory doOpen() : Final List size of priceauditVOs : " + priceAuditVOsList.size());
			}
			mIterator = priceAuditVOsList.iterator();
		}
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @return the base feed process vo
	 * @see com.tru.feed.outbound.tools.AbstractFeedItemReader#doRead()
	 */
	@Override
	protected PriceAuditFeedVO doRead() {
		return next();
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @return the base feed process vo
	 * @see com.tru.feed.outbound.tools.AbstractFeedItemReader#read()
	 */
	@Override
	public PriceAuditFeedVO read(){
		return next();
	}

}
