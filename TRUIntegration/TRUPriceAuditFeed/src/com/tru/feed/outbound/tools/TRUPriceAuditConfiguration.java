package com.tru.feed.outbound.tools;

import atg.nucleus.GenericService;

/**
 * The Class TRUPriceAuditConfiguration.
 * @author PA.
 * @version 1.0.
 */
public class TRUPriceAuditConfiguration extends GenericService{
	/**
	 * property to hold mAppendTRU.
	 */
	private String mAppendTRU;
	/**
	 * property to hold mAppendUSAPriceAudit.
	 */
	private String mAppendUSAPriceAudit;
	/**
	 * property to hold mMaxRecords.
	 */
	private String mMaxRecords;
	/**
	 * property to hold mBadDirectory.
	 */
	private String mBadDirectory;
	/**
	 * property to hold mEnvName.
	 */
	private String mEnvName;
	/**
	 * property to hold mHeaderDateFormat.
	 */
	private String mHeaderDateFormat;
	/**
	 * property to hold mOutputDirectory.
	 */
	private String mOutputDirectory;
	/**
	 * property to hold mFileNameDateFormat.
	 */
	private String mFileNameDateFormat;
	/**
	 * property to hold mLocationId.
	 */
	private String mLocationId;
	
	/** The Esb destination prefix. */
	private String mEsbDestinationPrefix;
	
	/** The Esb directory. */
	private String mEsbDirectory;
	
	/** The Transfer to esb folder. */
	private boolean mTransferToEsbFolder;

	/**
	 * @return the mLocationId
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * Sets the location id.
	 *
	 * @param pLocationId the new location id
	 */
	public void setLocationId(String pLocationId) {
		mLocationId = pLocationId;
	}

	/**
	 * @return the mFileNameDateFormat
	 */
	public String getFileNameDateFormat() {
		return mFileNameDateFormat;
	}

	/**
	 * Sets the file name date format.
	 *
	 * @param pFileNameDateFormat the new file name date format
	 */
	public void setFileNameDateFormat(String pFileNameDateFormat) {
		mFileNameDateFormat = pFileNameDateFormat;
	}

	/**
	 * @return the mOutputDirectory
	 */
	public String getOutputDirectory() {
		return mOutputDirectory;
	}

	/**
	 * Sets the output directory.
	 *
	 * @param pOutputDirectory the new output directory
	 */
	public void setOutputDirectory(String pOutputDirectory) {
		mOutputDirectory = pOutputDirectory;
	}

	/**
	 * @return the mHeaderDateFormat
	 */
	public String getHeaderDateFormat() {
		return mHeaderDateFormat;
	}

	/**
	 * Sets the header date format.
	 *
	 * @param pHeaderDateFormat the new header date format
	 */
	public void setHeaderDateFormat(String pHeaderDateFormat) {
		mHeaderDateFormat = pHeaderDateFormat;
	}

	/**
	 * @return the mEnvName
	 */
	public String getEnvName() {
		return mEnvName;
	}

	/**
	 * Sets the env name.
	 *
	 * @param pEnvName the new env name
	 */
	public void setEnvName(String pEnvName) {
		mEnvName = pEnvName;
	}

	/**
	 * @return the mBadDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}

	/**
	 * Sets the bad directory.
	 *
	 * @param pBadDirectory the new bad directory
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
	}

	/**
	 * @return the mMaxRecords
	 */
	public String getMaxRecords() {
		return mMaxRecords;
	}

	/**
	 * Sets the max records.
	 *
	 * @param pMaxRecords the new max records
	 */
	public void setMaxRecords(String pMaxRecords) {
		mMaxRecords = pMaxRecords;
	}

	/**
	 * Gets the esd destination prefix.
	 * 
	 * @return the esd destination prefix
	 */
	public String getEsbDestinationPrefix() {
		return mEsbDestinationPrefix;
	}

	/**
	 * Sets the esb destination prefix.
	 * 
	 * @param pEsbDestinationPrefix
	 *            the new esb destination prefix
	 */
	public void setEsbDestinationPrefix(String pEsbDestinationPrefix) {
		mEsbDestinationPrefix = pEsbDestinationPrefix;
	}

	/**
	 * Gets the esb directory.
	 * 
	 * @return the esb directory
	 */
	public String getEsbDirectory() {
		return mEsbDirectory;
	}

	/**
	 * Sets the esb directory.
	 * 
	 * @param pEsbDirectory
	 *            the new esb directory
	 */
	public void setEsbDirectory(String pEsbDirectory) {
		mEsbDirectory = pEsbDirectory;
	}

	/**
	 * @return the transferToEsbFolder
	 */
	public boolean isTransferToEsbFolder() {
		return mTransferToEsbFolder;
	}

	/**
	 * Sets the transfer to esb folder.
	 *
	 * @param pTransferToEsbFolder            the transferToEsbFolder to set
	 */
	public void setTransferToEsbFolder(boolean pTransferToEsbFolder) {
		mTransferToEsbFolder = pTransferToEsbFolder;
	}

	/**
	 * @return the mAppendTRU
	 */
	public String getAppendTRU() {
		return mAppendTRU;
	}

	/**
	 * Sets the append tru.
	 *
	 * @param pAppendTRU the mAppendTRU to set
	 */
	public void setAppendTRU(String pAppendTRU) {
		mAppendTRU = pAppendTRU;
	}

	/**
	 * @return the mAppendUSAPriceAudit
	 */
	public String getAppendUSAPriceAudit() {
		return mAppendUSAPriceAudit;
	}

	/**
	 * Sets the append usa price audit.
	 *
	 * @param pAppendUSAPriceAudit the mAppendUSAPriceAudit to set
	 */
	public void setAppendUSAPriceAudit(String pAppendUSAPriceAudit) {
		mAppendUSAPriceAudit = pAppendUSAPriceAudit;
	}

}
