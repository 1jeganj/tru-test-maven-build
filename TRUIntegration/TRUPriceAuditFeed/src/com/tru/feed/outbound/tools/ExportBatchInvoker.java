package com.tru.feed.outbound.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.converter.JobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.ExitCodeMapper;
import org.springframework.batch.core.launch.support.JvmSystemExiter;
import org.springframework.batch.core.launch.support.SimpleJvmExitCodeMapper;
import org.springframework.batch.core.launch.support.SystemExiter;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import atg.nucleus.GenericService;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;


/**
 * <p>
 * Basic launcher for starting jobs from the command line. In general, it is
 * assumed that this launcher will primarily be used to start a job via a script
 * from an Enterprise Scheduler. Therefore, exit codes are mapped to integers so
 * that schedulers can use the returned values to determine the next course of
 * action. The returned values can also be useful to operations teams in
 * determining what should happen upon failure. For example, a returned code of
 * 5 might mean that some resource wasn't available and the job should be
 * restarted. However, a code of 10 might mean that something critical has
 * happened and the issue should be escalated.
 * </p>
 * 
 * <p>
 * With any launch of a batch job within Spring Batch, a Spring context
 * containing the {@link Job} and some execution context has to be created. This
 * command line launcher can be used to load the job and its context from a
 * single location. All dependencies of the launcher will then be satisfied by
 * autowiring by type from the combined application context. Default values are
 * provided for all fields except the {@link JobLauncher} and {@link JobLocator}
 * . Therefore, if autowiring fails to set it (it should be noted that
 * dependency checking is disabled because most of the fields have default
 * values and thus don't require dependencies to be fulfilled via autowiring)
 * then an exception will be thrown. It should also be noted that even if an
 * exception is thrown by this class, it will be mapped to an integer and
 * returned.
 * </p>
 * 
 * <p>
 * Notice a property is available to set the {@link SystemExiter}. This class is
 * used to exit from the main method, rather than calling exit method of System
 * directly. This is because unit testing a class calling exit method of System is
 * impossible without kicking off the test within a new JVM, which it is
 * possible to do, however it is a complex solution, much more so than
 * strategizing the exiter.
 * </p>
 * 
 * <p>
 * The arguments to this class are as follows:
 * </p>
 * 
 * <code>
 * jobPath <options> jobName (jobParameters)*
 * </code>
 * 
 * <p>
 * The command line options are as follows
 * <ul>
 * <li>jobPath: the xml application context containing a {@link Job}
 * <li>-restart: (optional) to restart the last failed execution</li>
 * <li>-next: (optional) to start the next in a sequence according to the
 * {@link JobParametersIncrementer} in the {@link Job}</li>
 * <li>jobName: the bean id of the job.
 * <li>jobParameters: 0 to many parameters that will be used to launch a job.
 * </ul>
 * </p>
 * 
 * <p>
 * If the <code>-next</code> option is used the parameters on the command line
 * (if any) are appended to those retrieved from the incrementer, overriding any
 * with the same key.
 * </p>
 * 
 * <p>
 * The combined application context must contain only one instance of
 * {@link JobLauncher}. The job parameters passed in to the command line will be
 * converted to {@link Properties} by assuming that each individual element is
 * one parameter that is separated by an equals sign. For example,
 * "vendor.id=290232". Below is an example arguments list: "
 * 
 * <p>
 * <code>
 * java org.springframework.batch.execution.bootstrap.support.ATGSpringBatchInvoker testJob.xml 
 * testJob schedule.date=2008/01/24 vendor.id=3902483920 
 * </code>
 * </p>
 * 
 * <p>
 * Once arguments have been successfully parsed, autowiring will be used to set
 * various dependencies. The {@JobLauncher} for example, will be
 * loaded this way. If none is contained in the bean factory (it searches by
 * type) then a {@link BeanDefinitionStoreException} will be thrown. The same
 * exception will also be thrown if there is more than one present. Assuming the
 * JobLauncher has been set correctly, the jobName argument will be used to
 * obtain an actual {@link Job}. If a {@link JobLocator} has been set, then it
 * will be used, if not the beanFactory will be asked, using the jobName as the
 * bean id.
 * </p>
 * 
 * 
 * @author PA
 * @since 1.0
 */
public class ExportBatchInvoker extends GenericService {
	// Package private for unit test
		/** The system exiter. */
	private static SystemExiter mSystemExiter = new JvmSystemExiter();

	/** The message. */
	private static String mMessage = TRUPriceAuditFeedConstants.EMPTY;
	/** The Constant LOGGER. */
	private static final Log LOGGER = LogFactory.getLog(ExportBatchInvoker.class);

	/** The exit code mapper. */
	private ExitCodeMapper mExitCodeMapper = new SimpleJvmExitCodeMapper();

	/** The mLauncher. */
	private JobLauncher mLauncher;

	/** The job locator. */
	private JobLocator mJobLocator;
	/** The m context. */
	private ConfigurableApplicationContext mContext;

	/** The job explorer. */
	private JobExplorer mJobExplorer;

	/**
	 * Gets the context.
	 * 
	 * @return the context
	 */
	public ConfigurableApplicationContext getContext() {
		return mContext;
	}

	/**
	 * Sets the context.
	 * 
	 * @param pContext
	 *            the new context
	 */
	public void setContext(ConfigurableApplicationContext pContext) {
		mContext = pContext;
	}

	/**
	 * Injection setter for the {@link JobLauncher}.
	 * 
	 * @param pLauncher
	 *            the new launcher
	 */
	public void setLauncher(JobLauncher pLauncher) {
		this.mLauncher = pLauncher;
	}

	/**
	 * Injection setter for {@link mJobExplorer}.
	 * 
	 * @param pJobExplorer
	 *            the new job explorer
	 */
	public void setJobExplorer(JobExplorer pJobExplorer) {
		this.mJobExplorer = pJobExplorer;
	}

	/**
	 * Injection setter for the {@link ExitCodeMapper}.
	 * 
	 * @param pExitCodeMapper
	 *            the new exit code mapper
	 */
	public void setExitCodeMapper(ExitCodeMapper pExitCodeMapper) {
		this.mExitCodeMapper = pExitCodeMapper;
	}

	/**
	 * Static setter for the {@link SystemExiter} so it can be adjusted before
	 * dependency injection. Typically overridden by
	 * 
	 * @param pSystemExiter
	 *            the system exiter {@link #setSystemExiter(SystemExiter)}.
	 */
	public static void presetSystemExiter(SystemExiter pSystemExiter) {
		ExportBatchInvoker.mSystemExiter = pSystemExiter;
	}

	/**
	 * Retrieve the error message set by an instance of.
	 * 
	 * @return the error message {@link ExportBatchInvoker} as it exits. Empty if the
	 *         last job launched was successful.
	 */
	public static String getErrorMessage() {
		return mMessage;
	}

	/**
	 * Injection setter for the {@link SystemExiter}.
	 * 
	 * @param pSystemExiter
	 *            the new system exiter
	 */
	public void setSystemExiter(SystemExiter pSystemExiter) {
		ExportBatchInvoker.mSystemExiter = pSystemExiter;
	}


	/**
	 * Delegate to the exiter to (possibly) exit the VM gracefully.
	 * 
	 * @param pStatus
	 *            the status
	 */
	public void exit(int pStatus) {
		mSystemExiter.exit(pStatus);
	}

	/**
	 * Sets the job locator.
	 * 
	 * @param pJobLocator
	 *            the new job locator {@link JobLocator} to find a job to run.
	 */
	public void setJobLocator(JobLocator pJobLocator) {
		this.mJobLocator = pJobLocator;
	}

	/**
	 * Start a job by obtaining a combined classpath using the job launcher and
	 * job paths. If a JobLocator has been set, then use it to obtain an actual
	 * job, if not ask the context for it.
	 * 
	 * @param pJobName
	 *            the job name
	 * @param pOpts
	 *            the opts
	 * @return the int
	 */
	public int start(String pJobName, Set<String> pOpts) {
        LOGGER.info(TRUPriceAuditFeedConstants.LOGGER_START);
		try {
			Assert.state(mLauncher != null, TRUPriceAuditFeedConstants.NO_JOBLAUNCHER_PROVIDED_MSG);
			if (pOpts.contains(TRUPriceAuditFeedConstants.RESTART)|| pOpts.contains(TRUPriceAuditFeedConstants.NEXT)) {
				Assert.state(mJobExplorer != null,
						TRUPriceAuditFeedConstants.NO_JOBEXPLORER_PROVIDED_MSG);
			}

			Job job;
			if (mJobLocator != null) {
				job = mJobLocator.getJob(pJobName);
			} else {
				job = (Job) getContext().getBean(pJobName);
			}
			
			Calendar lCDateTime = Calendar.getInstance();
			JobParameters jobParameters = 
					  new JobParametersBuilder().addLong(pJobName,lCDateTime.getTimeInMillis()).toJobParameters();
			LOGGER.info(TRUPriceAuditFeedConstants.JOB_NAME + pJobName);
			LOGGER.info(TRUPriceAuditFeedConstants.JOB + job);
			LOGGER.info(TRUPriceAuditFeedConstants.JOB_PARAMETERS + jobParameters);			
			JobExecution jobExecution = mLauncher.run(job, jobParameters);
			LOGGER.info(TRUPriceAuditFeedConstants.LOGGER_END);
			return mExitCodeMapper.intValue(jobExecution.getExitStatus()
					.getExitCode());

		} catch (Exception e) {
			LOGGER.error(TRUPriceAuditFeedConstants.JOB_TERMINATED_ERR_MSG, e);
			if(isLoggingError()){
				logError(TRUPriceAuditFeedConstants.JOB_TERMINATED_ERR_MSG, e);
			}
			ExportBatchInvoker.mMessage = TRUPriceAuditFeedConstants.JOB_TERMINATED_ERR_MSG;
			LOGGER.info(TRUPriceAuditFeedConstants.LOGGER_END_EXCEPTION);
			return mExitCodeMapper.intValue(ExitStatus.FAILED.getExitCode());
		} finally {
			if (mContext != null) {
				mContext.close();
			}
		}
	}

	/**
	 * Load context.
	 * 
	 * @param pJobPath
	 *            the job path
	 */
	public void loadContext(String[] pJobPath) {
		if(isLoggingDebug()){
        	logDebug("Entered into ExportBatchInvoker:: loadContext");
        }
		setContext(new ClassPathXmlApplicationContext(pJobPath));
		getContext().getAutowireCapableBeanFactory().autowireBeanProperties(
				this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
		if(isLoggingDebug()){
        	logDebug("Exit From ExportBatchInvoker:: loadContext");
        }
	}

	/**
	 * Launch a batch job using a {@link ExportBatchInvoker}. Creates a new Spring
	 * context for the job execution, and uses a common parent for all such
	 * contexts. No exception are thrown from this method, rather exceptions are
	 * logged and an integer returned through the exit status in a
	 * {@link JvmSystemExiter} (which can be overridden by defining one in the
	 * Spring context).<br/>
	 * Parameters can be provided in the form key=value, and will be converted
	 * using the injected {@link JobParametersConverter}.
	 * 
	 * @param pArgs
	 *            the args
	 */

	public void startJob(String[] pArgs) {
		if(isLoggingDebug()){
        	logDebug("Entered into ExportBatchInvoker:: startJob");
        }
		Set<String> lOpts = new HashSet<String>();
		List<String> lParams = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		lParams.add(cal.getTime().toString());

		int count = 0;

		String lJobName = null;

		for (String arg : pArgs) {
			if (arg.startsWith(TRUPriceAuditFeedConstants.HYPHEN)) {
				lOpts.add(arg);
			} else {
				if (count == 0) {
					lJobName = arg;
				} else {
					lParams.add(arg);
				}
				count++;
			}
		}

		if (lJobName == null) {

			LOGGER.error(TRUPriceAuditFeedConstants.JOB_FAIL_NO_PARAM_JOB_NAME);
			ExportBatchInvoker.mMessage = TRUPriceAuditFeedConstants.JOB_FAIL_NO_PARAM_JOB_NAME;

		}

		String[] lParameters = lParams.toArray(new String[lParams.size()]);

		start(lJobName, lOpts);
		if(isLoggingDebug()){
        	logDebug("Exit From ExportBatchInvoker:: startJob");
        }
	}
}