package com.tru.feed.outbound.tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSARepository;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.logging.TRUAlertLogger;

/**
 * This class retreive the productIds after firing the query for Rod002 and Kiosk Feed.
 * 
 * @author PA.
 * @version 1.0.
 * 
 */
public class ExportMultisiteFeedRetriveEntityIdTask extends AbstractTasklet {

	/** The entity result set extractor. */
	private EntityResultSetExtractor mEntityResultSetExtractor = new EntityResultSetExtractor();

	/**
	 * Gets the entity result set extractor.
	 *
	 * @return the entityResultSetExtractor.
	 */
	public EntityResultSetExtractor getEntityResultSetExtractor() {
		return mEntityResultSetExtractor;
	}

	/**
	 * Sets the entity result set extractor.
	 *
	 * @param pEntityResultSetExtractor the entityResultSetExtractor to set.
	 */
	public void setEntityResultSetExtractor(EntityResultSetExtractor pEntityResultSetExtractor) {
		mEntityResultSetExtractor = pEntityResultSetExtractor;
	}

	/** Property to hold mExportFixedChildProductsSql. */
	private String mExportActiveSkusSql;

	/**
	 * Property to hold mTransactionManager.
	 */
	private TransactionManager mTransactionManager;

	/**
	 * Property to hold mFeedContext.
	 */
	private FeedContext mFeedContext;

	/**
	 * Gets the feed context.
	 *
	 * @return the feedContext.
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext the feedContext to set.
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * Gets the transaction manager.
	 *
	 * @return the transactionManager
	 */

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * Sets the transaction manager.
	 *
	 * @param pTransactionManager the transactionManager to set.
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/** The m data source name. */
	private String mDataSourceName;

	/** Property to hold mCatalogRepository. */
	private Repository mCatalogRepository;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	/**
	 * Gets the catalog repository.
	 *
	 * @return the catalogRepository
	 */
	public Repository getCatalogRepository() {
		return mCatalogRepository;
	}

	/**
	 * Sets the catalog repository.
	 *
	 * @param pCatalogRepository the catalogRepository to set.
	 */
	public void setCatalogRepository(Repository pCatalogRepository) {
		mCatalogRepository = pCatalogRepository;
	}

	/**
	 * Gets the data source name.
	 * 
	 * @return the mDataSource.
	 */
	public String getDataSourceName() {
		return mDataSourceName;
	}

	/**
	 * Sets the data source name.
	 * 
	 * @param pDataSourceName the new data source name.
	 */
	public void setDataSourceName(String pDataSourceName) {
		mDataSourceName = pDataSourceName;
	}

	/**
	 * This method retrieve the productIds from query and get the productItems by calling catalog repository and set the productItems in the context to export the feed data.
	 * 
	 * 
	 */
	@Override
	protected void doExecute() {
		if (isLoggingDebug()) {
			Calendar cal = Calendar.getInstance();
			logDebug("Entered into ExportFeedRetriveEntityIdTask:doExecute() : " + cal.getTime().toString());
		}

		String startDate = new SimpleDateFormat(TRUPriceAuditFeedConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUPriceAuditFeedConstants.PRICE_AUDIT_STARTED);
		List<String> activeSkus = querySkusInRepo();
		RepositoryItem[] skuItems = null;
		Repository catalogRepo = (Repository) getCatalogRepository();
		List<RepositoryItem> repositoryItemList = new ArrayList<RepositoryItem>();
		try {
			skuItems = fetchSKUItems(activeSkus, catalogRepo);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("Getting Exception", e);
			}
		}
		if (skuItems != null) {
			if (isLoggingDebug()) {
				logDebug("Size of skuItems: " + skuItems.length);
			}
			repositoryItemList = Arrays.asList(skuItems);
		}
		if (repositoryItemList != null) {
			vlogDebug("Size of Final skuItems: " + repositoryItemList.size());
		}
		getFeedContext().setData(TRUPriceAuditFeedConstants.SKU_ITEMS, repositoryItemList);
		if (isLoggingDebug()) {
			logDebug("Exit From ::  ExportFeedRetriveEntityIdTask:doExecute()");
		}
	}

	/**
	 * Query products in repo.
	 * 
	 * @return the list.
	 */
	private List<String> querySkusInRepo() {
		Connection connection = null;
		Statement statement = null;
		List<String> activeSkus = new ArrayList<String>();
		ResultSet rs = null;
		try {
			connection = ((GSARepository) getCatalogRepository()).getConnection();
			statement = connection.createStatement();
			String query = getExportActiveSkusSql();
			rs = statement.executeQuery(query);
			if (rs != null) {
				while (rs.next()) {
					activeSkus.add(rs.getString(TRUPriceAuditFeedConstants.INTEGER_NUMBER_ONE));
				}
			}
		} catch (SQLException e) {
			if (isLoggingError()) {
				logError("SQLException occured while fetching the skus", e);
			}
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				if (isLoggingError()) {
					logError("SQLException occured while closing the connection", e);
				}
			}
		}
		return activeSkus;
	}

	/**
	 * Fetch product items.
	 *
	 * @param pActiveSkuIds the active sku ids
	 * @param pCatalogRepo            the catalog repo
	 * @return the repository item[]
	 * @throws RepositoryException             the repository exception
	 */
	private RepositoryItem[] fetchSKUItems(List<String> pActiveSkuIds, Repository pCatalogRepo) throws RepositoryException {
		RepositoryItem[] skuItems = null;
		if (pActiveSkuIds != null) {
			skuItems = pCatalogRepo.getItems(pActiveSkuIds.toArray(new String[pActiveSkuIds.size()]), TRUPriceAuditFeedConstants.SKU);
			if (isLoggingDebug()) {
				logDebug("SKU ID List Size :" + pActiveSkuIds.size());
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("SKU ID List Size  :" + pActiveSkuIds);
			}
		}
		return skuItems;
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceAuditFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceAuditFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceAuditFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceAuditFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUPriceAuditFeedConstants.PRICE_AUDIT, TRUPriceAuditFeedConstants.FEED_STATUS, null, extenstions);
	}

	
	/**
	 * Gets the export active skus sql.
	 *
	 * @return the export active skus sql
	 */
	public String getExportActiveSkusSql() {
		return mExportActiveSkusSql;
	}

	/**
	 * Sets the export fixed child products sql.
	 *
	 * @param pExportActiveSkusSql the new export active skus sql
	 */
	public void setExportActiveSkusSql(String pExportActiveSkusSql) {
		this.mExportActiveSkusSql = pExportActiveSkusSql;
	}
	
	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

}
