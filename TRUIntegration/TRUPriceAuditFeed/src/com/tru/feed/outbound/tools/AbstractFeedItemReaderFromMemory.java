package com.tru.feed.outbound.tools;

import java.util.Map;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import atg.nucleus.GenericService;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.feed.outbound.vo.PriceAuditFeedVO;

/**
 * AbstractFeedItemReaderFromMemory is an abstract class which will extend GenericService.
 * This class contains all the abstract methods and overridden by class .
 *
 * @author PA
 * @version 1.0
 */
public abstract class AbstractFeedItemReaderFromMemory extends GenericService implements
		ItemReader<PriceAuditFeedVO>, ItemStream {
	 
 	/** The Individual thread range. */
 	private Map<String,Range> mIndividualThreadRange;
	
	/** The Exec context. */
	private ExecutionContext mExecContext;
	
	/** The mphase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;
	
	/** The m feed context. */
	private FeedContext mFeedContext;

	/**
	 * Gets the exec context.
	 *
	 * @return the exec context
	 */
	public ExecutionContext getExecContext() {
		return mExecContext;
	}

	/**
	 * Sets the exec context.
	 *
	 * @param pExecContext the new exec context
	 */
	public void setExecContext(ExecutionContext pExecContext) {
		mExecContext = pExecContext;
	}

	/**
	 * Gets the feed context.
	 *
	 * @return the mFeedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}
	
	/**
	 * Gets the step name.
	 *
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Sets the step name.
	 *
	 * @param pStepName the new step name
	 */
	public void setStepName(String pStepName) {
		mStepName = pStepName;
	}

	/**
	 * Gets the phase name.
	 * 
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Sets the phase name.
	 * 
	 * @param pPhaseName
	 *            the new phase name
	 */
	public void setPhaseName(String pPhaseName) {
		mPhaseName = pPhaseName;
	}


	/**
	 * Save data.
	 * 
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	protected void saveData(String pCongifKey, Object pConfigValue) {
		getFeedContext().setData(pCongifKey, pConfigValue);

	}
	/**
	 * Do open.
	 * 
	 */
	protected void doOpen()  {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFeedItemReaderFromMemory:doOpen() method");
		}
	}

	/**
	 * Do close.
	 */
	protected void doClose()  {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFeedItemReaderFromMemory:doClose() method");
		}
	}

	/**
	 * Do read.
	 * 
	 * @return the base feed process vo
	 * 
	 */
	protected abstract PriceAuditFeedVO doRead();


	/**
	 * close.
	 * 
	 * @see org.springframework.batch.item.ItemStream#close()
	 * @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public void close() throws ItemStreamException {
			doClose();
	}

	/**
	 * open.
	 * 
	 * @see
	 * org.springframework.batch.item.ItemStream#open(org.springframework.batch
	 * .item.ExecutionContext)
	 * @param pExecContext - ExecContext
	 * @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public void  open(ExecutionContext pExecContext) throws ItemStreamException {
		synchronized(this) {
			setExecContext(pExecContext);
			mIndividualThreadRange=(Map<String, Range>) pExecContext.get(TRUPriceAuditFeedConstants.RANGE_MAP);
			doOpen();
		}
	}

	/**
	 * update.
	 * 
	 * @see
	 * org.springframework.batch.item.ItemStream#update(org.springframework.
	 * batch.item.ExecutionContext)
	 * @param pArg0 - Arg0
	 * @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public void update(ExecutionContext pArg0) throws ItemStreamException {
		doUpdate();
	}

	/**
	 * Do close.
	 * 
	 */
	protected void doUpdate() {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFeedItemReaderFromMemory:doUpdate() method");
		}
	}

	/**
	 * Retrive data.
	 * 
	 * @param pDataMapKey - the data map key
	 * @return the object
	 */
	protected Object retriveData(String pDataMapKey) {

		return getFeedContext().getData(pDataMapKey);

	}

	/**
	 * read.
	 * 
	 * @see org.springframework.batch.item.ItemReader#read()
	 * @return priceAuditFeedVO
	 */
	@Override
	public PriceAuditFeedVO read(){
		PriceAuditFeedVO priceAuditFeedVO = null;
		priceAuditFeedVO = doRead();
		return priceAuditFeedVO;
	}

	/**
	 * Gets the start index.
	 *
	 * @return the start index
	 */
	public int getStartIndex()
	{
		synchronized(this) {
			return getExecContext().getInt(TRUPriceAuditFeedConstants.FROM,0);
		}
	}
	
	/**
	 * Gets the last index.
	 *
	 * @param pDefaultSize the default size
	 * @return the last index
	 */
	public int getLastIndex(int pDefaultSize)
	{
		synchronized(this) {
			return getExecContext().getInt(TRUPriceAuditFeedConstants.TO,pDefaultSize);
		}
	}
	
	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public Map<String,Range> getRange()
	{
		synchronized(this) {
			return mIndividualThreadRange;
		}
	}
}
