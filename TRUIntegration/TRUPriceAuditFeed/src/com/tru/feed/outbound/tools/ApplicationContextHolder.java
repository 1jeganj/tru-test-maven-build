package com.tru.feed.outbound.tools;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ApplicationContextHolder. 
 * 
 * <p>Service class for application context holder </p>
 *
 * @author PA 
 * @version 1.0
 * 
 */
public class ApplicationContextHolder implements ApplicationContextAware {
	
	/** The m application context. */
	private static ApplicationContext mApplicationContext;
	
	/**
	 * Gets the application context.
	 *
	 * @return the application context
	 */
	public	static ApplicationContext getApplicationContext() {
		return mApplicationContext;
	}
	
	/** (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 * @param pApplicationContext -  ApplicationContext parameter
	 * @throws BeansException -BeansException

	 */
	@Override
	public void setApplicationContext(ApplicationContext pApplicationContext)
			throws BeansException {
		
		this.mApplicationContext=pApplicationContext;
	}
	
	/**
	 * Gets the bean.
	 *
	 * @param pBeanName the bean name
	 * @return the bean
	 */
	public static Object getBean(String pBeanName){
		return getApplicationContext().getBean(pBeanName);
	}

	
}
