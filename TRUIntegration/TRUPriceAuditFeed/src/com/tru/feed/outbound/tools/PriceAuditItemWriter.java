package com.tru.feed.outbound.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.feed.outbound.email.PAFEmailConfig;
import com.tru.feed.outbound.vo.PriceAuditFeedVO;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class PriceAuditItemWriter is used to write price details into priceauditVO.
 * @version 1.0.
 */
public class PriceAuditItemWriter extends GenericService implements ItemWriter<PriceAuditFeedVO>, ItemStream {
	/**
	 * property to hold the delimiter.
	 */
	private static final CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
	/**
	 * property to hold bad mBadFilename.
	 */
	/**
	 * property to hold mHeaderSequence.
	 */
	private String mHeaderSequence;
	/**
	 * property to hold mSalePriceSum.
	 */
	private Double mSalePriceSum;
	/**
	 * property to hold PAFEmailConfig.
	 */
	private PAFEmailConfig mEmailConfig;
	/**
	 * property to hold successRecordCount.
	 */
	private int mSuccessRecordCount;
	/**
	 * property to hold badRecordCount.
	 */
	private int mBadRecordCount;
	/**
	 * property to hold mPriceLists.
	 */
	private Repository mPriceLists;
	/**
	 * property to hold mFileName.
	 */
	private String mFileName;
	/**
	 * property to hold mSelectSequenceNumberSQL.
	 */
	private String mSelectSequenceNumberSQL;
	/**
	 * property to hold mInsertSequenceNumberSQL.
	 */
	private String mInsertSequenceNumberSQL;
	/**
	 * property to hold mFetchSequenceNumberSQL.
	 */
	private String mFetchSequenceNumberSQL;
	/**
	 * property to hold mSetSequenceNumberSQL1.
	 */
	private String mSetSequenceNumberSQL1;
	/**
	 * property to hold mSetSequenceNumberSQL2.
	 */
	private String mSetSequenceNumberSQL2;
	/**
	 * mWriter for holding mFileOutputStream, for bad records.
	 */
	/**
	 * property to hold mTruPriceAuditConfig.
	 */
	private TRUPriceAuditConfiguration mTruPriceAuditConfig;
	/**
	 * property to hold mFeedContext.
	 */
	private FeedContext mFeedContext;

	/**
	 * property to hold bad mEsbFileName.
	 */
	private String mEsbFileName;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;

	/**
	 * @return the feedContext.
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * @param pFeedContext
	 *            the feedContext to set
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * write method which export the data.
	 * 
	 * @param pPriceAuditList PriceAuditList
	 * @throws IOException IOException
	 */
	public void write(List<? extends PriceAuditFeedVO> pPriceAuditList) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into :: PriceAuditItemWriter:write()");
		}
		synchronized(this) {
			export(pPriceAuditList);
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: PriceAuditItemWriter:write()");
		}
	}

	/**
	 * write method which export data into the csv file.
	 * 
	 * @param pPriceAuditList
	 *            the PriceAuditList
	 * @throws IOException
	 *             the Exception
	 */
	public void export(List<? extends PriceAuditFeedVO> pPriceAuditList) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:export(List<? extends PriceAuditFeedVO> pPriceAuditList)");
		}
		int maxRecords = Integer.parseInt(getTruPriceAuditConfig().getMaxRecords());

		List<PriceAuditFeedVO> priceAuditList = new ArrayList<PriceAuditFeedVO>();
		List<PriceAuditFeedVO> priceAuditFailureList = new ArrayList<PriceAuditFeedVO>();
		setBadRecordCount(TRUPriceAuditFeedConstants.NUMBER_ZERO);
		for (PriceAuditFeedVO pafvo : pPriceAuditList) {
			if (StringUtils.isNotBlank(pafvo.getProductId()) && StringUtils.isNotBlank(pafvo.getSkuId())) {
				priceAuditList.add(pafvo);
			} else {
				priceAuditFailureList.add(pafvo);
			}
		}
		if (pPriceAuditList != null) {
			vlogDebug("PriceAuditItemWriter:export() MaxRecords size : {0} ", maxRecords);
		}
		if (isLoggingDebug()) {
			logDebug("PriceAuditItemWriter:export(List<? extends PriceAuditFeedVO> pPriceAuditList) ");
		}
		vlogInfo("PriceAuditItemWriter:export() Price Audit List size : {0} ", pPriceAuditList.size());
		vlogInfo("PriceAuditItemWriter:export() Price Audit List size with ProductId and SkuId : {0} ", priceAuditList.size());
		writeToFile(priceAuditList, priceAuditFailureList);
		if (isLoggingDebug()) {
			logDebug("ExitFrom PriceAuditItemWriter:export(List<? extends PriceAuditFeedVO> pPriceAuditList)");
		}
	}

	/**
	 * Write the price Audit details into success and bad files.
	 *
	 * @param pPafList            the paf list
	 * @param pBadPriceAuditList the bad price audit list
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	public void writeToFile(List<PriceAuditFeedVO> pPafList, List<PriceAuditFeedVO> pBadPriceAuditList) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:writeToFile()");
		}
		boolean writeSuccess = true;
		int splitSize = 0;
		String finalFileName = null;
		Map<String, Object> exceptionMap = new HashMap<String, Object>();
		final String[] nameMapping = new String[] { TRUPriceAuditFeedConstants.PRODUCT_ID, TRUPriceAuditFeedConstants.SKU_ID,
				TRUPriceAuditFeedConstants.SKN_ID, TRUPriceAuditFeedConstants.LISTPRICE, TRUPriceAuditFeedConstants.SALEPRICE,
				TRUPriceAuditFeedConstants.BACK_ORDER_CODE, TRUPriceAuditFeedConstants.WEB_DISPLAY_FLAG,
				TRUPriceAuditFeedConstants.SUPRESS_IN_SEARCH, TRUPriceAuditFeedConstants.HIDDEN_FLAG_FOR_BEAN, TRUPriceAuditFeedConstants.LOCALE,
				TRUPriceAuditFeedConstants.COUNTRY_CDE, TRUPriceAuditFeedConstants.MARKET_CDE, TRUPriceAuditFeedConstants.STORE_NUMBER, };
		try {
			// success record writing
			if (isLoggingDebug()) {
				logDebug("PriceAuditItemWriter:writeToFile() Writing success records");
			}
				finalFileName = getSplitFileName(splitSize);
				splitSize = populateSuccessFile(splitSize, nameMapping, pPafList, finalFileName);
			// bad record writing
				if(pBadPriceAuditList != null && !pBadPriceAuditList.isEmpty()) {
					setBadRecordCount(pBadPriceAuditList.size());
				}
		} catch (FileNotFoundException e) {
			writeSuccess = false;
			exceptionMap.put(TRUPriceAuditFeedConstants.EXCEPTION, e.getMessage());
			if (isLoggingError()) {
				logError("FileNotFoundException occured while open in PriceAuditItemWriter", e);
			}
		}
		sendEmailAfterWrite(writeSuccess, exceptionMap, pBadPriceAuditList);
		if (isLoggingDebug()) {
			logDebug("Exit From PriceAuditItemWriter:writeToFile()");
		}
	}

	/**
	 * Populate success file with price audit details, records are populated.
	 * based on max records configured.
	 *
	 * @param pSplitSize the split size
	 * @param pNameMapping the name mapping
	 * @param pPafvolist the pafvolist
	 * @param pFinalFile the final file
	 * @return the int
	 */
	private int populateSuccessFile(int pSplitSize, final String[] pNameMapping, List<PriceAuditFeedVO> pPafvolist, String pFinalFile) {
		int newSplitSize = pSplitSize;
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:populateSuccessFile()");
			if (pPafvolist != null) {
				logDebug("PriceAuditItemWriter:populateSuccessFile() pafvolist size : " + pPafvolist.size());
			}
		}
		try {
			File outFile = createNewFile(pFinalFile);
			FileOutputStream fileOutputStream = new FileOutputStream(outFile);
			Writer writer = new OutputStreamWriter(fileOutputStream);
			if (isLoggingDebug()) {
				logDebug("OutputStreamWriter is created Successfully");
			}
			ICsvBeanWriter beanWriter = new CsvBeanWriter(writer, PriceAuditItemWriter.PIPE_DELIMITED);
			fetchDetailsToPopulate(pNameMapping, pPafvolist, pFinalFile, beanWriter);
			newSplitSize++;
			flushBeanWriter(beanWriter);
			if (isLoggingDebug()) {
				logDebug("Exit From PriceAuditItemWriter:populateSuccessFile()" + newSplitSize);
			}
			if (getTruPriceAuditConfig().isTransferToEsbFolder()) {
				moveSuccessToEsbLocation(outFile);
			}
		} catch (FileNotFoundException fileNotFoundExcep) {
			if (isLoggingError()) {
				logError("FileNotFound occured while writing file populateSuccessFile	", fileNotFoundExcep);
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError("IOException occured after writing file populateSuccessFile	", ioe);
			}
		}
		return newSplitSize;
	}

	/**
	 * Flush bean writer.
	 * @param pBeanWriter - BeanWriter
	 */
	private void flushBeanWriter(ICsvBeanWriter pBeanWriter) {
		try {
			if (pBeanWriter != null) {
				pBeanWriter.flush();
				pBeanWriter.close();
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError("IOException occured after writing file	", ioe);
			}
		}
	}

	/**
	 * Fetch details to populate.
	 * 
	 * @param pNameMapping
	 *            the name mapping
	 * @param pPafvolist
	 *            the pafvolist
	 * @param pFinalFile
	 *            the final file
	 * @param pBeanWriter - BeanWriter
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void fetchDetailsToPopulate(final String[] pNameMapping, List<PriceAuditFeedVO> pPafvolist, String pFinalFile,
			ICsvBeanWriter pBeanWriter) throws IOException {
		//write to header
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(getTruPriceAuditConfig().getHeaderDateFormat(), Locale.getDefault());
		String headerDate = sdf.format(cal.getTime());
		final String[] header = new String[] { TRUPriceAuditFeedConstants.HEADER, headerDate,
				leftPadding(getHeaderSequence(), TRUPriceAuditFeedConstants.HEARDER_PADDING), };
		final CellProcessor[] processors = getProcessors();
		pBeanWriter.writeHeader(header);
		// write the beans data
		int recordCount = TRUPriceAuditFeedConstants.INTEGER_NUMBER_TWO;
		double salePriceSum = TRUPriceAuditFeedConstants.NUMBER_ZERO_WITH_DECIMAL;
		for (PriceAuditFeedVO priceAuditVO : pPafvolist) {
			if (StringUtils.isNotBlank(priceAuditVO.getSalePrice())) {
			salePriceSum = salePriceSum + Double.parseDouble(priceAuditVO.getSalePrice());
			}
			pBeanWriter.write(priceAuditVO, pNameMapping, processors);
			recordCount++;
		}
		if (isLoggingDebug()) {
			logDebug("PriceAuditItemWriter:mBeanWriter Success File name : " + pFinalFile);
			logDebug("PriceAuditItemWriter:mBeanWriter Success Record count : " + recordCount);
		}
		int successCount = recordCount - TRUPriceAuditFeedConstants.INTEGER_NUMBER_TWO;
		setSuccessRecordCount(successCount);
		setSalePriceSum(salePriceSum);
		DecimalFormat decimalFormat = new DecimalFormat(TRUPriceAuditFeedConstants.NUMBER_ZERO_WITH_TWO_DECIMALS);
		String formattedSalePrice = decimalFormat.format(salePriceSum);
		String finalSalePrice = formattedSalePrice.replace(TRUPriceAuditFeedConstants.DOT_STRING, TRUPriceAuditFeedConstants.EMPTY);

		// write the trailer
		final String[] trailer = new String[] { TRUPriceAuditFeedConstants.TRAILER,
				leftPadding(Integer.toString(recordCount), TRUPriceAuditFeedConstants.RECORDS_PADDING),
				leftPadding(finalSalePrice, TRUPriceAuditFeedConstants.SALE_PRICE_SUM_PADDING), };
		pBeanWriter.writeHeader(trailer);
	}

	/**
	 * Creates the new file.
	 *
	 * @param pFinalFile the final file
	 * @return the file
	 */
	private File createNewFile(String pFinalFile) {
		File outFile = new File(pFinalFile);
		if (!(outFile.exists())) {
			try {
				outFile.createNewFile();
			} catch (IOException e) {
				if (isLoggingError()) {
					logError("Getting Exception", e);
				}
			}
		}
		return outFile;
	}

	/**
	 * Move success to esb location.
	 * 
	 * @param pOutFile
	 *            the out file
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void moveSuccessToEsbLocation(File pOutFile) throws FileNotFoundException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:moveSuccessToEsbFile() outFile: " + pOutFile);
		}
		File esbDirectory = new File(getTruPriceAuditConfig().getEsbDirectory());
		if (!esbDirectory.exists()) {
			throw new FileNotFoundException();
		}
		String esbFinalFile = getTruPriceAuditConfig().getEsbDirectory() + getEsbFileName();
		File esbOutFile = new File(esbFinalFile);
		if (!(esbOutFile.exists())) {
			esbOutFile.createNewFile();
		}
		if (isLoggingDebug()) {
			logDebug("PriceAuditItemWriter:moveSuccessToEsbFile() : esbOutFile:	" + esbOutFile);
		}
		copyFileOperation(pOutFile, esbOutFile);
		if (isLoggingDebug()) {
			logDebug("Exit from PriceAuditItemWriter:moveSuccessToEsbFile() ");
		}
	}

	/**
	 * Copy file operation from source to esb location.
	 * 
	 * @param pOutFile
	 *            the out file
	 * @param pEsbOutFile
	 *            the esb out file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void copyFileOperation(File pOutFile, File pEsbOutFile) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:copyFileOperation()");
		}
		FileChannel sourceChannel = null;
		FileChannel esbChannel = null;
		try {
			sourceChannel = new FileInputStream(pOutFile).getChannel();
			esbChannel = new FileOutputStream(pEsbOutFile).getChannel();
			esbChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} catch (IOException ioe) {
			vlogError("copyFileOperation : IOException occured while copying file	" , ioe);
			throw new FileNotFoundException();
		} finally {
			sourceChannel.close();
			esbChannel.close();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from PriceAuditItemWriter:copyFileOperation()");
		}
	}

	/**
	 * Populate bad file with price audit bad records.
	 * 
	 * @param pBadPriceAuditList
	 *            the bad price audit list
	 * @param pNameMapping
	 *            the name mapping
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
		// write bad records

	/**
	 * This method is used for applying the constraint on the feilds to populate.
	 * 
	 * @return processors
	 */
	private static CellProcessor[] getProcessors() {
		final CellProcessor[] processors = new CellProcessor[] { new Optional(), // ProductId
				new NotNull(), // SkuId

	/**
	 * This method is used for applying the constraint on the feilds to populate.
	 * 
	 * @return processors
	 */
				new Optional(), // SKN
				new Optional(), // ListPrice
				new Optional(), // Saleprice
				new Optional(), // Backorder Code
				new Optional(), // Web Display Flag
				new Optional(), // Supress In Search
				new Optional(), // Hidden Flag
				new Optional(), // Locale
				new Optional(), // Country Code
				new Optional(), // Market Code
				new Optional(), // Store Number
		};
		return processors;
	}

	/**
	 * This update method does not have any business logic but have to override.
	 * 
	 * @param pExecutionContext
	 *            the ExecutionContext
	 * @throws ItemStreamException
	 *             the exception
	 */
	@Override
	public void update(ExecutionContext pExecutionContext) throws ItemStreamException {
		if (isLoggingDebug()) {
			logDebug("Entered into PriceAuditItemWriter:update()");
		}
	}

	/**
	 * This method is used to open the initial level tags.
	 * 
	 * @param pExecutionContext
	 *            the ExecutionContext
	 * @throws ItemStreamException
	 *             the exception
	 */
	@Override
	public void open(ExecutionContext pExecutionContext) throws ItemStreamException {
		synchronized(this) {
			if (isLoggingDebug()) {
				logDebug("Entered into :: PriceAuditItemWriter:open()");
			}
			if (isLoggingDebug()) {
				logDebug("Exit From :: PriceAuditItemWriter:open()");
			}
		}
	}
	
	/**
	 * Gets the split file name.
	 * 
	 * @param pSplitFile
	 *            the splitFile
	 * @return finalFile
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public String getSplitFileName(int pSplitFile) throws FileNotFoundException {
		if (isLoggingDebug()) {
			logDebug("Entered into :: PriceAuditItemWriter:getSplitFileName()");
		}
		String finalFile = TRUPriceAuditFeedConstants.EMPTY;
		StringBuffer fileSequence = new StringBuffer();
		StringBuffer fileSequenceFileName = new StringBuffer();
		fileSequence.append(TRUPriceAuditFeedConstants.FILE_SEQUENCE_FILE_NAME).append(TRUPriceAuditFeedConstants.FILE_TYPE);
		fileSequenceFileName.append(getTruPriceAuditConfig().getOutputDirectory()).append(fileSequence);
		File outFileSequence = null;
		int sequenceNumber = TRUPriceAuditFeedConstants.NUMBER_ONE;
		if (pSplitFile == 0) {
			String esbDestPrefix = getTruPriceAuditConfig().getEsbDestinationPrefix();
			String appendTRU = getTruPriceAuditConfig().getAppendTRU();
			String appendUSAPriceAudit = getTruPriceAuditConfig().getAppendUSAPriceAudit();
			StringBuffer fileNameFinal = new StringBuffer();
			String fileName = null;
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(getTruPriceAuditConfig().getFileNameDateFormat(), Locale.getDefault());
			String fileNameDate = sdf.format(cal.getTime());
			fileNameFinal.append(esbDestPrefix).append(appendTRU).append(getTruPriceAuditConfig().getEnvName())
					.append(TRUPriceAuditFeedConstants.UNDERSCORE).append(fileNameDate).append(appendUSAPriceAudit);
			fileName = fileNameFinal.toString();
			if (StringUtils.isNotBlank(getTruPriceAuditConfig().getOutputDirectory()) && (StringUtils.isNotBlank(fileName))
					&& (StringUtils.isNotBlank(fileSequenceFileName.toString()))) {
				File directory = new File(getTruPriceAuditConfig().getOutputDirectory());
				if (!directory.exists()) {
					throw new FileNotFoundException();
				}
				outFileSequence = new File(fileSequenceFileName.toString());
				Scanner scanner = null;
				BufferedWriter bw = null;
				if (outFileSequence.exists()) {
					try {
						scanner = new Scanner(outFileSequence);
						int[] tall = new int[TRUPriceAuditFeedConstants.INTEGER_NUMBER_HUNDRED];
						int i = 0;
						while (scanner.hasNextInt()) {
							tall[i++] = scanner.nextInt();
						}
						sequenceNumber = tall[0];
						scanner.close();
						sequenceNumber = sequenceNumber + TRUPriceAuditFeedConstants.NUMBER_ONE;
						bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFileSequence)));
						bw.write(Integer.toString(sequenceNumber));
						bw.close();
					} catch (NumberFormatException ne) {
						if (isLoggingError()) {
							logError("Getting Exception", ne);
						}
					} catch (IOException e) {
						if (isLoggingError()) {
							logError("Getting Exception", e);
						}
					} finally {
						try {
							if (scanner != null) {
								scanner.close();
							}
							if (bw != null) {
								bw.close();
							}
						} catch (IOException ioe) {
							if (isLoggingError()) {
								logError("Getting Exception", ioe);
							}
						}
					}
				} else {
					Writer w = null;
					try {
						outFileSequence.createNewFile();
						FileOutputStream fos = new FileOutputStream(outFileSequence);
						OutputStreamWriter osw = new OutputStreamWriter(fos);
						w = new BufferedWriter(osw);
						w.write(Integer.toString(sequenceNumber));
						w.close();
					} catch (IOException e) {
						if (isLoggingError()) {
							logError("Getting Exception", e);
						}
					} finally {
						try {
							if (w != null) {
								w.close();
							}
						} catch (IOException ioe) {
							if (isLoggingError()) {
								logError("Getting Exception", ioe);
							}
						}
					}
				}
				setFileName(fileName);
				StringBuffer fileNameBuffer = new StringBuffer();
				fileNameBuffer.append(fileName);
				fileNameBuffer.append(leftPadding(Integer.toString(sequenceNumber), TRUPriceAuditFeedConstants.HEARDER_PADDING));
				fileNameBuffer.append(TRUPriceAuditFeedConstants.FILE_TYPE);
				fileName=fileNameBuffer.toString();
				finalFile = getTruPriceAuditConfig().getOutputDirectory() + fileName;
				setEsbFileName(fileName);
				setHeaderSequence(Integer.toString(sequenceNumber));
				if (isLoggingDebug()) {
					logDebug("Splitsize zero : FileName:	" + finalFile);
				}
			}
		} else {
			if (StringUtils.isNotBlank(getTruPriceAuditConfig().getOutputDirectory()) && (StringUtils.isNotBlank(getFileName()))
					&& (StringUtils.isNotBlank(fileSequenceFileName.toString()))) {
				File directory = new File(getTruPriceAuditConfig().getOutputDirectory());
				if (!directory.exists()) {
					throw new FileNotFoundException();
				}
				outFileSequence = new File(fileSequenceFileName.toString());
				Scanner scanner = null;
				BufferedWriter bw = null;
				if (outFileSequence.exists()) {
					try {
						scanner = new Scanner(outFileSequence);
						int[] tall = new int[TRUPriceAuditFeedConstants.INTEGER_NUMBER_HUNDRED];
						int i = 0;
						while (scanner.hasNextInt()) {
							tall[i++] = scanner.nextInt();
						}
						sequenceNumber = tall[0];
						scanner.close();
						sequenceNumber = sequenceNumber + TRUPriceAuditFeedConstants.NUMBER_ONE;
						bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFileSequence)));
						bw.write(Integer.toString(sequenceNumber));
						bw.close();
					} catch (NumberFormatException ne) {
						if (isLoggingError()) {
							logError("Getting Exception", ne);
						}
					} catch (IOException e) {
						if (isLoggingError()) {
							logError("Getting Exception", e);
						}
					} finally {
						try {
							if (scanner != null) {
								scanner.close();
							}
							if (bw != null) {
								bw.close();
							}
						} catch (IOException ioe) {
							if (isLoggingError()) {
								logError("Getting Exception", ioe);
							}
						}
					}
				}
				finalFile = getTruPriceAuditConfig().getOutputDirectory() + getFileName()
						+ leftPadding(Integer.toString(sequenceNumber), TRUPriceAuditFeedConstants.HEARDER_PADDING)
						+ TRUPriceAuditFeedConstants.FILE_TYPE;
				setHeaderSequence(Integer.toString(sequenceNumber));
				setEsbFileName(getFileName() + leftPadding(Integer.toString(sequenceNumber), TRUPriceAuditFeedConstants.HEARDER_PADDING)
						+ TRUPriceAuditFeedConstants.FILE_TYPE);
				if (isLoggingDebug()) {
					logDebug("Splitsize :FileName:	" + finalFile);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: PriceAuditItemWriter:getSplitFileName()");
		}
		return finalFile;
	}

	/**
	 * This method used to close the start tags which opened in open method and.
	 * also perform the task to uplaod feed export file to the ftp location.
	 * 
	 * @throws ItemStreamException
	 *             the exception
	 */

	@Override
	public void close() throws ItemStreamException {
		synchronized (this) {
			if (isLoggingDebug()) {
				logDebug("Entered into :: PriceAuditItemWriter:close()");
			}
		}
	}

	/**
	 * This method send email after file upload on the basic of flag return.
	 * upload success or failed.
	 * 
	 * @param pWriteStatus
	 *            - boolean success/fail
	 * @param pExceptionMap
	 *            - Exception map
	 * @param pBadPriceAuditList 
	 */
	public void sendEmailAfterWrite(boolean pWriteStatus, Map<String, Object> pExceptionMap, List<PriceAuditFeedVO> pBadPriceAuditList) {
		if (isLoggingDebug()) {
			logDebug("START:: PriceAuditItemWriter.sendEmailAfter()");
		}
		Integer successRecordCount = getSuccessRecordCount();
		Integer badRecordCount = getBadRecordCount();
		String startDate = new SimpleDateFormat(TRUPriceAuditFeedConstants.FEED_DATE_FORMAT).format(new Date());
		File[] attachmentFile = null;
		Map<String, Object> templateParameter = new HashMap<String, Object>();
		TemplateEmailInfoImpl pafFeedEmailInfo = getEmailConfig().getPafTemplateEmailInfo();
		TemplateEmailSender emailSender = getEmailConfig().getTemplateEmailSender();
		pafFeedEmailInfo.setTemplateURL(getEmailConfig().getPafTemplateURL());
		pafFeedEmailInfo.setSiteId(getEmailConfig().getSiteId());
		if (pWriteStatus) {
			frameSuccessandErrorMails(successRecordCount, badRecordCount, templateParameter, startDate);
			if (pBadPriceAuditList != null && !pBadPriceAuditList.isEmpty()) {
				StringBuilder errorFileName = new StringBuilder();
				errorFileName.append(TRUPriceAuditFeedConstants.ERROR_FILE).append(getEsbFileName());
				attachmentFile = new File[FeedConstants.NUMBER_ONE];
				attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pBadPriceAuditList, errorFileName.toString());
			}
		} else {
			String subject = getEmailConfig().getPriceAuditFailureSub();
			String content = getEmailConfig().getInvalidOutFolderContent();
			String[] args = new String[TRUPriceAuditFeedConstants.INTEGER_NUMBER_TWO];
			args[TRUPriceAuditFeedConstants.INTEGER_NUMBER_ZERO] = getEmailConfig().getEnv();
			Date currentDate = new Date();
			DateFormat dateFormat = new SimpleDateFormat(TRUPriceAuditFeedConstants.SUB_DATE_FORMAT);
			args[TRUPriceAuditFeedConstants.INTEGER_NUMBER_ONE] = dateFormat.format(currentDate);
			String formattedSubject = String.format(subject, args);
			
			templateParameter.put(TRUPriceAuditFeedConstants.FEED_STATUS, TRUPriceAuditFeedConstants.FEED_FAILED);
			templateParameter.put(TRUPriceAuditFeedConstants.MESSAGE_SUBJECT, formattedSubject);
			templateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
			templateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getEmailConfig().getOSHostName());
			String lException = getEmailConfig().getInvalidOutFolderDetailExcep();
			if (lException != null) {
				templateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, lException);
			}
			logFailedMessage(startDate, TRUPriceAuditFeedConstants.PRICE_AUDIT_ERROR);
		}
		templateParameter.put(TRUPriceAuditFeedConstants.SITE_ID, getEmailConfig().getSiteId());
		clearEmailInfo(pafFeedEmailInfo);
		if (pafFeedEmailInfo.getTemplateParameters() != null) {
			pafFeedEmailInfo.getTemplateParameters().putAll(templateParameter);
		} else {
			pafFeedEmailInfo.setTemplateParameters(templateParameter);
		}
		pafFeedEmailInfo.setMessageSubject((String) templateParameter.get(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
		if (attachmentFile != null) {
			pafFeedEmailInfo.setMessageAttachments(attachmentFile);
		}
		List<String> recipientList = getEmailConfig().getPafReceiverEmailId();
		try {
			emailSender.sendEmailMessage(pafFeedEmailInfo, recipientList);
		} catch (TemplateEmailException e) {
			if (isLoggingError()) {
				logError(TRUPriceAuditFeedConstants.TEMPLATE_EXCEPTION, e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: PriceAuditItemWriter.sendEmailAfterWrite()");
		}
	}

	/**
	 * Clear email info.
	 * 
	 * @param pEmailInfo
	 *            the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: PriceAuditItemWriter : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: PriceAuditItemWriter : @Method: clearEmailInfo()");
	}

	/**
	 * Frame success and error mails.
	 * 
	 * @param pSuccessRecordCount
	 *            the success record count
	 * @param pBadRecordCount
	 *            the bad record count
	 * @param pTemplateParameter
	 *            the template parameter
	 * @param startDate 
	 */
	private void frameSuccessandErrorMails(Integer pSuccessRecordCount, Integer pBadRecordCount, Map<String, Object> pTemplateParameter, String startDate) {
		if (isLoggingDebug()) {
			logDebug("Begin:@Class: PriceAuditItemWriter : @Method: frameSuccessandErrorMails()");
		}
		String subject = null;

		if (pBadRecordCount == TRUPriceAuditFeedConstants.NUMBER_ZERO) {
			subject = getEmailConfig().getPriceAuditSuccessSub();
		} else {
			subject = getEmailConfig().getPriceAuditErrorSub();
		}
		String[] args = new String[TRUPriceAuditFeedConstants.INTEGER_NUMBER_TWO];
		args[TRUPriceAuditFeedConstants.INTEGER_NUMBER_ZERO] = getEmailConfig().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceAuditFeedConstants.SUB_DATE_FORMAT);
		args[TRUPriceAuditFeedConstants.INTEGER_NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.FEED_STATUS, TRUPriceAuditFeedConstants.FEED_SUCCESS);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.MESSAGE_SUBJECT, formattedSubject);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, formattedSubject);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pSuccessRecordCount + pBadRecordCount);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pBadRecordCount);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		pTemplateParameter.put(TRUPriceAuditFeedConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getEmailConfig().getOSHostName());
		if (isLoggingDebug()) {
			logDebug("End:@Class: PriceAuditItemWriter : @Method: frameSuccessandErrorMails()");
		}
		logSuccessMessage(startDate, TRUPriceAuditFeedConstants.PRICE_AUDIT_SUCCESS);
	}

	/**
	 * Creates the attachment file. This method used to create attachment file.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @return the file
	 */
	public File createAttachmentFile(List<PriceAuditFeedVO> pInvalidRecords, String pErrorFileName) {
		if (isLoggingDebug()) {
			logDebug("Begin:@Class: PriceAuditItemWriter : @Method: createAttachmentFile()");
		}
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;

		File errorFolder = new File(getTruPriceAuditConfig().getBadDirectory());
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}
		File attachment = new File(errorFolder, attachmentFileName(pErrorFileName));
		try {
			outputStream = new FileOutputStream(attachment);
			workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(TRUPriceAuditFeedConstants.XLS_SHEET);
			int rowCount = FeedConstants.NUMBER_ZERO;
			HSSFRow rowhead = sheet.createRow(rowCount);
			HSSFCellStyle rowCellStyle = workbook.createCellStyle();
			rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			rowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getEmailConfig().getFileName());
			rowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getEmailConfig().getRecordNumber());
			rowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getEmailConfig().getTimeStamp());
			rowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getEmailConfig().getAttachmentItemIdLabel());
			rowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(getEmailConfig().getAttachmentExceptionReasonLabel());

			rowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(rowCellStyle);

			rowCount = FeedConstants.NUMBER_ONE;
			HSSFCellStyle dataCellStyle = workbook.createCellStyle();
			dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			for (PriceAuditFeedVO badPriceAuditVO : pInvalidRecords) {
				HSSFRow dataRow = sheet.createRow(rowCount);
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(new HSSFRichTextString(pErrorFileName));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUPriceAuditFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(badPriceAuditVO.getSkuId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(getEmailConfig().getProductIdMissing()));

				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				rowCount++;
			}
			if (null != workbook) {
				workbook.write(outputStream);
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError("Error in : @class PriceAuditItemWriter method createAttachmentFile()", ioe);
			}
		} finally {
			if (null != workbook) {
				workbook = null;
			}
			try {
				if (null != outputStream) {
					outputStream.close();
				}
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class PriceAuditItemWriter method createAttachmentFile()", ioe);
				}
			}

		}
		if (isLoggingDebug()) {
			logDebug("End:@Class: PriceAuditItemWriter : @Method: createAttachmentFile()");
		}
		return attachment;
	}

	/**
	 * Attachement file name. This method used to construct the attached file
	 * name
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachmentFileName(String pFileName) {
		if (isLoggingDebug()) {
			logDebug("Begin:@Class: PriceAuditItemWriter : @Method: attachementFileName()");
		}
		String newFileName = pFileName.substring(FeedConstants.NUMBER_ZERO, pFileName.indexOf(FeedConstants.DOT) + FeedConstants.NUMBER_ONE);
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = stringBuffer.append(newFileName).append(TRUPriceAuditFeedConstants.XLS);
		if (isLoggingDebug()) {
			logDebug("End:@Class: PriceAuditItemWriter : @Method: attachementFileName()");
		}
		return stringBuffer.toString();
	}

	/**
	 * This method will pad zeros for the provided string.
	 * 
	 * @param pPaddingString
	 *            - The string to be left padded with zeros
	 * @param pPadLength
	 *            - Return pPadLength after padding
	 * @return string
	 */
	public String leftPadding(String pPaddingString, int pPadLength) {
		StringBuilder sb = new StringBuilder();
		for (int toPrepend = pPadLength - pPaddingString.length(); toPrepend > 0; toPrepend--) {
			sb.append(TRUPriceAuditFeedConstants.NUMBER_ZERO);
		}
		sb.append(pPaddingString);
		return sb.toString();
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUPriceAuditFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceAuditFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceAuditFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceAuditFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUPriceAuditFeedConstants.PRICE_AUDIT, TRUPriceAuditFeedConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceAuditFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceAuditFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceAuditFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceAuditFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUPriceAuditFeedConstants.PRICE_AUDIT, TRUPriceAuditFeedConstants.FEED_STATUS, null, extenstions);
	}

	/**
	 * @return the mTruPriceAuditConfig
	 */
	public TRUPriceAuditConfiguration getTruPriceAuditConfig() {
		return mTruPriceAuditConfig;
	}

	/**
	 * @param pTruPriceAuditConfig
	 *            the mTruPriceAuditConfig to set
	 */
	public void setTruPriceAuditConfig(TRUPriceAuditConfiguration pTruPriceAuditConfig) {
		this.mTruPriceAuditConfig = pTruPriceAuditConfig;
	}

	/**
	 * @return the mInsertSequenceNumberSQL
	 */
	public String getInsertSequenceNumberSQL() {
		return mInsertSequenceNumberSQL;
	}

	/**
	 * @param pInsertSequenceNumberSQL
	 *            the mInsertSequenceNumberSQL to set
	 */
	public void setInsertSequenceNumberSQL(String pInsertSequenceNumberSQL) {
		this.mInsertSequenceNumberSQL = pInsertSequenceNumberSQL;
	}

	/**
	 * @return the mFetchSequenceNumberSQL
	 */
	public String getFetchSequenceNumberSQL() {
		return mFetchSequenceNumberSQL;
	}

	/**
	 * @param pFetchSequenceNumberSQL
	 *            the mFetchSequenceNumberSQL to set
	 */
	public void setFetchSequenceNumberSQL(String pFetchSequenceNumberSQL) {
		this.mFetchSequenceNumberSQL = pFetchSequenceNumberSQL;
	}

	/**
	 * @return the mSetSequenceNumberSQL1
	 */
	public String getSetSequenceNumberSQL1() {
		return mSetSequenceNumberSQL1;
	}

	/**
	 * @param pSetSequenceNumberSQL1
	 *            the mSetSequenceNumberSQL1 to set
	 */
	public void setSetSequenceNumberSQL1(String pSetSequenceNumberSQL1) {
		this.mSetSequenceNumberSQL1 = pSetSequenceNumberSQL1;
	}

	/**
	 * @return the mSetSequenceNumberSQL2
	 */
	public String getSetSequenceNumberSQL2() {
		return mSetSequenceNumberSQL2;
	}

	/**
	 * @param pSetSequenceNumberSQL2
	 *            the mSetSequenceNumberSQL2 to set
	 */
	public void setSetSequenceNumberSQL2(String pSetSequenceNumberSQL2) {
		this.mSetSequenceNumberSQL2 = pSetSequenceNumberSQL2;
	}

	/**
	 * @return the mSelectSequenceNumberSQL
	 */
	public String getSelectSequenceNumberSQL() {
		return mSelectSequenceNumberSQL;
	}

	/**
	 * @param pSelectSequenceNumberSQL
	 *            the mSelectSequenceNumberSQL to set
	 */
	public void setSelectSequenceNumberSQL(String pSelectSequenceNumberSQL) {
		this.mSelectSequenceNumberSQL = pSelectSequenceNumberSQL;
	}

	/**
	 * @return the mFileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * @param pFileName
	 *            the mFileName to set
	 */
	public void setFileName(String pFileName) {
		this.mFileName = pFileName;
	}

	/**
	 * @return the mPriceLists
	 */
	public Repository getPriceLists() {
		return mPriceLists;
	}

	/**
	 * @param pPriceLists
	 *            the mPriceLists to set
	 */
	public void setPriceLists(Repository pPriceLists) {
		this.mPriceLists = pPriceLists;
	}

	/**
	 * @return the mSuccessRecordCount
	 */
	public int getSuccessRecordCount() {
		return mSuccessRecordCount;
	}

	/**
	 * @param pSuccessRecordCount
	 *            the mSuccessRecordCount to set
	 */
	public void setSuccessRecordCount(int pSuccessRecordCount) {
		this.mSuccessRecordCount = pSuccessRecordCount;
	}

	/**
	 * @return the mBadRecordCount
	 */

	/**
	 * @param pBadRecordCount
	 *            the mBadRecordCount to set
	 */

	/**
	 * @return the mEmailConfig
	 */
	public PAFEmailConfig getEmailConfig() {
		return mEmailConfig;
	}

	/**
	 * @param pEmailConfig
	 *            the mEmailConfig to set
	 */
	public void setEmailConfig(PAFEmailConfig pEmailConfig) {
		this.mEmailConfig = pEmailConfig;
	}

	/**
	 * @return the mSalePriceSum
	 */
	public Double getSalePriceSum() {
		return mSalePriceSum;
	}

	/**
	 * @param pSalePriceSum
	 *            the mSalePriceSum to set
	 */
	public void setSalePriceSum(Double pSalePriceSum) {
		this.mSalePriceSum = pSalePriceSum;
	}

	/**
	 * @return the mHeaderSequence
	 */
	public String getHeaderSequence() {
		return mHeaderSequence;
	}

	/**
	 * @param pHeaderSequence
	 *            the mHeaderSequence to set
	 */
	public void setHeaderSequence(String pHeaderSequence) {
		this.mHeaderSequence = pHeaderSequence;
	}

	/**
	 * @return the mBadFileName
	 */

	/**
	 * @param pBadFileName
	 *            the mBadFileName to set
	 */

	/**
	 * Gets the esb file name.
	 * 
	 * @return the esb file name
	 */
	public String getEsbFileName() {
		return mEsbFileName;
	}

	/**
	 * Sets the esb file name.
	 * 
	 * @param pEsbFileName
	 *            the new esb file name
	 */
	public void setEsbFileName(String pEsbFileName) {
		mEsbFileName = pEsbFileName;
	}

	/**
	 * @return the mBadRecordCount
	 */
	public int getBadRecordCount() {
		return mBadRecordCount;
	}

	/**
	 * @param pBadRecordCount
	 *            the mBadRecordCount to set
	 */
	public void setBadRecordCount(int pBadRecordCount) {
		this.mBadRecordCount = pBadRecordCount;
	}
	
	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}