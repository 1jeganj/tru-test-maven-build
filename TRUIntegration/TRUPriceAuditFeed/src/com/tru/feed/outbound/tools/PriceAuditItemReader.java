package com.tru.feed.outbound.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.adapter.gsa.ChangeAwareSet;
import atg.commerce.catalog.custom.CatalogProperties;
import atg.core.util.StringUtils;
import atg.multisite.RepositorySiteStorageAdapter;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.pricing.TRUPriceProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.feed.outbound.TRUPriceAuditFeedConstants;
import com.tru.feed.outbound.vo.PriceAuditFeedVO;

/**
 * The Class PriceAuditItemReader.
 * @author PA.
 * @version 1.0.
 */
public class PriceAuditItemReader extends AbstractFeedItemReader {
	/** The Unique sub list. */
	private ThreadLocal<List<RepositoryItem>> mUniqueSubList = new ThreadLocal<List<RepositoryItem>>();
	/** The Thread index. */
	private ThreadLocal<Integer> mThreadIndex = new ThreadLocal<Integer>();

	/** The m pricing tools. */
	private TRUPricingTools mPricingTools;

	/** The m catalog properties. */
	private CatalogProperties mCatalogProperties;

	/** The m repository site storage adapter. */
	private RepositorySiteStorageAdapter mRepositorySiteStorageAdapter;
	
	/**
	 * property to hold mTruPriceAuditConfig.
	 */
	private TRUPriceAuditConfiguration mTruPriceAuditConfig;

	/**
	 * This method gets the productItems from the context and based on the thread range, it partition the list.
	 * doOpen method started.
	 */
	public void doOpen() {
		if (isLoggingDebug()) {
			logDebug("Entered into ProductItemReader:doOpen()");
		}
		synchronized(this) {
			mThreadIndex.set(Integer.valueOf(TRUPriceAuditFeedConstants.NUMBER_ZERO));
			if (isLoggingDebug()) {
				Calendar cal = Calendar.getInstance();
				logDebug(cal.getTime().toString());
			}
			List<RepositoryItem> repositoryItemList = null;
			repositoryItemList = (List<RepositoryItem>) getFeedContext().getData(TRUPriceAuditFeedConstants.SKU_ITEMS);
			List<RepositoryItem> baseFeedProcessVOList = new ArrayList<RepositoryItem>();
			List<String> lEntityList = new ArrayList<String>();
			lEntityList.add(TRUPriceAuditFeedConstants.EXPORT_FEED);
			Map<String, Range> lRange = getRange();
			for (String currentEntity : lEntityList) {
				Range lCurrentEntityRange = lRange.get(currentEntity);
				if (repositoryItemList != null && !repositoryItemList.isEmpty() && lCurrentEntityRange.getTo() != 0) {
					if (isLoggingDebug()) {
						logDebug(" currentEntity:: " + currentEntity + " lCurrentEntityRange.getFrom():: " + 
								lCurrentEntityRange.getFrom()+ " lCurrentEntityRange.getTo(): " + lCurrentEntityRange.getTo());
					}
					baseFeedProcessVOList.addAll(repositoryItemList.subList(lCurrentEntityRange.getFrom(), lCurrentEntityRange.getTo()));
				}
			}
			mUniqueSubList.set(baseFeedProcessVOList);
			if (isLoggingDebug()) {
				logDebug("Exit From ProductItemReader:doOpen()");
			}
		}
	}

	/**
	 * This method returns the PriceAuditFeedVO having all the required properties which need to export for PriceAuditFeed.
	 * 
	 * 
	 * @return PriceAuditFeedVO - PriceAuditFeedVO
	 */
	@Override
	protected List<PriceAuditFeedVO> doRead() {
		return next();
	}

	/**
	 * This method returns the PriceAuditFeedVO by calling the repositoryItem of
	 * product, price and feed.
	 * 
	 * @return the base price audit feed process vo
	 */
	protected  List<PriceAuditFeedVO> next() {
		if (isLoggingDebug()) {
			logDebug("Entered into ProductItemReader:next()");
		}
		int lIndex = mThreadIndex.get().intValue();
		List<RepositoryItem> lBaseFeedProcessVOList = mUniqueSubList.get();
		if (lBaseFeedProcessVOList != null && 0 <= lIndex && lIndex < lBaseFeedProcessVOList.size()) {
			mThreadIndex.set(Integer.valueOf(lIndex + TRUPriceAuditFeedConstants.NUMBER_ONE));
			List<PriceAuditFeedVO> priceAuditFeedVOs = populatePriceAuditVO(lBaseFeedProcessVOList.get(lIndex));
			if (priceAuditFeedVOs != null) {
				vlogDebug("ProductItemReader:next() : PriceAuditFeedVO count : {0}" , priceAuditFeedVOs.size());
			}
			return priceAuditFeedVOs;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from ProductItemReader:next()");
		}
		return null;
	}

	/**
	 * This method returns the PriceAuditFeedVO with all the properties
	 * extracted from DB and set.
	 *
	 * @param pSKUItem the SKU item
	 * @return PriceAuditFeedVO
	 */
	private  List<PriceAuditFeedVO> populatePriceAuditVO(RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("Entered into ProductItemReader:populatePriceAuditVO()" );
		}
		List<PriceAuditFeedVO> priceAuditFeedVOs = new ArrayList<PriceAuditFeedVO>();
		
		if (isLoggingDebug()) {
			logDebug("skuItem :"+pSKUItem);
		}
				
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
				PriceAuditFeedVO priceAuditFeedVO = null;
		String skuId = pSKUItem.getRepositoryId();
		
		String skn = null;
		String productId = null;
		ChangeAwareSet sites = null;
		RepositoryItem productItem = null;
		String hiddenFlag = TRUPriceAuditFeedConstants.SUPER_DISPLAY_FLAG;
		boolean hiddenFlagCheck = true;
				String onlinePID = TRUPriceAuditFeedConstants.EMPTY;
		String skuSubType = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuType());
				if (skuSubType != null && !skuSubType.equalsIgnoreCase(TRUPriceAuditFeedConstants.NON_MERCH_SKU)) {
			onlinePID = (String) pSKUItem.getPropertyValue(catalogProperties.getOnlinePID());
				}
		String originalParentProd = (String) pSKUItem.getPropertyValue(catalogProperties.getOriginalParentProduct());
		Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) pSKUItem.getPropertyValue(catalogProperties.getParentProducts());
		if (parentProducts != null && !parentProducts.isEmpty()) {
			productItem = parentProducts.iterator().next();
			if (productItem != null) {
				productId = productItem.getRepositoryId();
				if (productItem.getPropertyValue(catalogProperties.getSitesPropertyName()) != null) {
					sites = (ChangeAwareSet) productItem.getPropertyValue(getCatalogProperties().getSitesPropertyName());
				}
				if (productItem.getPropertyValue(catalogProperties.getSuperDisplayFlag()) != null) {
					hiddenFlagCheck = (Boolean) productItem.getPropertyValue(catalogProperties.getSuperDisplayFlag());
					if (!hiddenFlagCheck) {
						hiddenFlag = TRUPriceAuditFeedConstants.SUPER_DISPLAY_FLAG_N;
					}
				}
			}
		}
		priceAuditFeedVO = new PriceAuditFeedVO();
		priceAuditFeedVO.setSkn(originalParentProd);
		priceAuditFeedVO.setProductId(onlinePID);
		priceAuditFeedVO.setSkuId(skuId);
		priceAuditFeedVO.setHiddenFlag(hiddenFlag);
		try {
				Site site = null;
				if (sites != null && !sites.isEmpty()) {
					site = fetchSiteRepository(sites);
				}

				if (site != null) {
					populatedPriceDetailsIntoVO(priceAuditFeedVO, productId, skuId, site);
				}
			
				
					// get First Inventory Received Date
			populateInventoryFlagsIntoVO(pSKUItem, priceAuditFeedVO, catalogProperties, skuSubType);
					
				} catch (RepositoryException re) {
					if (isLoggingError()) {
						logError("RepositoryException occured in  PriceAuditItemReader.populatePriceAuditVO() with exception : {0}", re);
					}
				}
				// get hidden flag

				if (priceAuditFeedVO != null) {
			vlogDebug(skuId + "  " + priceAuditFeedVO.getProductId() + "  " + skn + "  " + priceAuditFeedVO.getListPrice() + "  "
					+ priceAuditFeedVO.getSalePrice() + "  " + priceAuditFeedVO.getWebDisplayFlag() + "  " + priceAuditFeedVO.getSupressInSearch()
					+ "  " + hiddenFlag + "  " + priceAuditFeedVO.getLocale() + "  " + priceAuditFeedVO.getCountryCode() + "  "
					+ priceAuditFeedVO.getMarketCode() + "  " + priceAuditFeedVO.getStoreNumber());
				}
				priceAuditFeedVOs.add(priceAuditFeedVO);
		return priceAuditFeedVOs;
	}

	/**
	 * Fetch site repository.
	 *
	 * @param pSites the sites
	 * @return the site
	 */
	private Site fetchSiteRepository(ChangeAwareSet pSites) {
		Site site = null;
		String siteValue = null;
		Iterator it = pSites.iterator();
		while (it.hasNext()) {
			siteValue = (String) it.next();
			if (null != siteValue) {
				break;
			}
		}
		if (null != siteValue) {
			try {
				site = getRepositorySiteStorageAdapter().getSite(siteValue);
			} catch (SiteContextException exp) {
				if (isLoggingError()) {
					logError("SiteContextException occured in  PriceAuditItemReader.populatePriceAuditVO() with exception : ", exp);
				}
			}
		}
		return site;
	}

	/**
	 * Populate inventory flags into vo.
	 *
	 * @param pSku the sku
	 * @param pPriceAuditFeedVO the price audit feed vo
	 * @param pCatalogProperties the catalog properties
	 * @param pSkuSubType the sku sub type
	 * @throws RepositoryException the repository exception
	 */
	private void populateInventoryFlagsIntoVO(RepositoryItem pSku, PriceAuditFeedVO pPriceAuditFeedVO,
			TRUCatalogProperties pCatalogProperties, String pSkuSubType) throws RepositoryException {
		char backOrderStatus = TRUPriceAuditFeedConstants.N;
		char webDisplayFlag = TRUPriceAuditFeedConstants.N;
		char supressInSearch = TRUPriceAuditFeedConstants.N;
		if (pSku != null && pSkuSubType != null && !pSkuSubType.equalsIgnoreCase(TRUPriceAuditFeedConstants.NON_MERCH_SKU)) {
			if (StringUtils.isNotBlank((String) pSku.getPropertyValue(pCatalogProperties.getBackOrderStatus()))) {
				backOrderStatus = ((String) pSku.getPropertyValue(pCatalogProperties.getBackOrderStatus())).charAt(0);
			}
			if (pSku.getPropertyValue(pCatalogProperties.getWebDisplayFlag()) != null && 
					(Boolean) pSku.getPropertyValue(pCatalogProperties.getWebDisplayFlag())) {
				webDisplayFlag = TRUPriceAuditFeedConstants.A;
			}
			if (pSku.getPropertyValue(pCatalogProperties.getSupressInSearch()) != null && 
					(Boolean) pSku.getPropertyValue(pCatalogProperties.getSupressInSearch())) {
				supressInSearch = TRUPriceAuditFeedConstants.Y;
			}
		}
		pPriceAuditFeedVO.setBackOrderCode(backOrderStatus);
		pPriceAuditFeedVO.setWebDisplayFlag(webDisplayFlag);
		pPriceAuditFeedVO.setSupressInSearch(supressInSearch);
	}

	/**
	 * Populated price details into vo.
	 *
	 * @param pPriceAuditFeedVO the price audit feed vo
	 * @param pProductId the product id
	 * @param pSkuId the sku id
	 * @param pSite the site
	 */
	private void populatedPriceDetailsIntoVO(PriceAuditFeedVO pPriceAuditFeedVO, String pProductId, String pSkuId, Site pSite) {
		TRUPriceProperties priceProperties = null;
		String countryCode = null;
		String marketCode = null;
		String storeNumber = null;
		String locale = null;
		double listPrice = TRUPriceAuditFeedConstants.NUMBER_ZERO_WITH_DECIMAL;
		double salePrice = TRUPriceAuditFeedConstants.NUMBER_ZERO_WITH_DECIMAL;
		RepositoryItem priceList = null;
		RepositoryItem listPriceItem = null;
		RepositoryItem salePriceItem = null;
		
		
		priceProperties = getPricingTools().getPriceProperties();
		if (getPricingTools().getPriceForSKU(pSkuId, pProductId, pSite, true, false) != null) {
			listPriceItem = getPricingTools().getPriceForSKU(pSkuId, pProductId, pSite, true, false);
		}
		if (getPricingTools().getPriceForSKU(pSkuId, pProductId, pSite, false, true) != null) {
			salePriceItem = getPricingTools().getPriceForSKU(pSkuId, pProductId, pSite, false, true);
		}
		priceList = (RepositoryItem) pSite.getPropertyValue(getPricingTools().getDefaultPriceListPropertyName());
		if (priceList != null) {
			locale = (String) priceList.getPropertyValue(priceProperties.getLocalePropetryName());
		}
		if (listPriceItem != null) {
			listPrice = (Double) getPricingTools().getValueForProperty(priceProperties.getListPricePropertyName(), listPriceItem);
			countryCode = (String) listPriceItem.getPropertyValue(priceProperties.getCountryCodePropertyName());
			marketCode = (String) listPriceItem.getPropertyValue(priceProperties.getMarketCodePropertyName());
			storeNumber = (String) listPriceItem.getPropertyValue(priceProperties.getStoreNumberPropertyName());
		}
		if (salePriceItem != null) {
			salePrice = (Double) getPricingTools().getValueForProperty(priceProperties.getListPricePropertyName(), salePriceItem);
		}
		pPriceAuditFeedVO.setCountryCode(countryCode);
		pPriceAuditFeedVO.setMarketCode(marketCode);
		pPriceAuditFeedVO.setStoreNumber(storeNumber);
		pPriceAuditFeedVO.setLocale(locale);
		if (listPrice != TRUPriceAuditFeedConstants.NUMBER_ZERO) {
			pPriceAuditFeedVO.setListPrice(Double.toString(listPrice));
		}
		if (salePrice != TRUPriceAuditFeedConstants.NUMBER_ZERO) {
			pPriceAuditFeedVO.setSalePrice(Double.toString(salePrice));
		}
	}

	/**
	 * @return the mCatalogProperties
	 */
	public CatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the new catalog properties
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * @return the mPricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * Sets the pricing tools.
	 *
	 * @param pPricingTools the new pricing tools
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}

	/**
	 * @return the mRepositorySiteStorageAdapter
	 */
	public RepositorySiteStorageAdapter getRepositorySiteStorageAdapter() {
		return mRepositorySiteStorageAdapter;
	}

	/**
	 * Sets the repository site storage adapter.
	 *
	 * @param pRepositorySiteStorageAdapter the new repository site storage adapter
	 */
	public void setRepositorySiteStorageAdapter(RepositorySiteStorageAdapter pRepositorySiteStorageAdapter) {
		this.mRepositorySiteStorageAdapter = pRepositorySiteStorageAdapter;
	}

	/**
	 * @return the mTruPriceAuditConfig
	 */
	public TRUPriceAuditConfiguration getTruPriceAuditConfig() {
		return mTruPriceAuditConfig;
	}

	/**
	 * Sets the tru price audit config.
	 *
	 * @param pTruPriceAuditConfig the new tru price audit config
	 */
	public void setTruPriceAuditConfig(TRUPriceAuditConfiguration pTruPriceAuditConfig) {
		this.mTruPriceAuditConfig = pTruPriceAuditConfig;
	}
}
