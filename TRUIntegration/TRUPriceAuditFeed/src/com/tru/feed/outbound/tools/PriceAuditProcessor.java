package com.tru.feed.outbound.tools;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tru.feed.outbound.vo.PriceAuditFeedVO;



/**
 * PriceAuditProcessor class.
 * @author PA
 * @version 1.0.
 *
 */
@Component("itemProcessor")
@Scope(value = "step")
public class PriceAuditProcessor implements ItemProcessor<PriceAuditFeedVO, PriceAuditFeedVO> {


	/** The mThreadName. */
	@Value("#{stepExecutionContext[name]}")
	private String mThreadName;
	/**
	 * @param pItem - Item
	 * @return PriceAuditFeedVO 
	 */
	@Override
	public  PriceAuditFeedVO process(PriceAuditFeedVO pItem){
		synchronized(this) {
			return pItem;
		}
	}

	/**
	 * 
	 * @return the Thread
	 */
	public String getThreadName() {
		return mThreadName;
	}

	/**
	 * 
	 * @param pThreadName the thread
	 */
	public void setThreadName(String pThreadName) {
		mThreadName = pThreadName;
	}


}
