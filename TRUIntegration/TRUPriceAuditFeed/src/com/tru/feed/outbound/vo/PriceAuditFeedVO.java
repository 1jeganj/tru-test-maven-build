package com.tru.feed.outbound.vo;

/**
 * The Class PriceAuditFeedVO.
 * @author PA.
 * @version 1.0.
 */
public class PriceAuditFeedVO {
	/** Unique ATG Product ID. */
	private String mProductId;
	/** Reference point to the sku. */
	private String mSkuId;
	/** SKN Reference. */
	private String mSkn;
	/** property to hold listPrice. */
	private String mListPrice;
	/** property to hold salePrice. */
	private String mSalePrice;
	/** property to hold back order code. */
	private char mBackOrderCode;
	/** property to hold web display flag. */
	private char mWebDisplayFlag;
	/**property to hold suppress_in_search. */
	private char mSupressInSearch;
	/** property to hold BCC hidden flag. */
	private String mHiddenFlag;
	/** Locale of the price list to be created.*/
	private String mLocale;
	/** Three digit ISO Country code. */
	private String mCountryCode;
	/** Three digit market code. */
	private String mMarketCode;
	/** Facility/Location code for every storefront. */
	private String mStoreNumber;
	/**
	 * @return the mProductId.
	 */
	public String getProductId() {
		return mProductId;
	}
	/**
	 * @param pProductId the mProductId to set.
	 */
	public void setProductId(String pProductId) {
		this.mProductId = pProductId;
	}
	/**
	 * @return the mSkuId.
	 */
	public String getSkuId() {
		return mSkuId;
	}
	/**
	 * @param pSkuId the mSkuId to set.
	 */
	public void setSkuId(String pSkuId) {
		this.mSkuId = pSkuId;
	}
	/**
	 * @return the mBackOrderCode.
	 */
	public char getBackOrderCode() {
		return mBackOrderCode;
	}
	/**
	 * @param pBackOrderCode the mBackOrderCode to set.
	 */
	public void setBackOrderCode(char pBackOrderCode) {
		this.mBackOrderCode = pBackOrderCode;
	}
	/**
	 * @return the mWebDisplayFlag.
	 */
	public char getWebDisplayFlag() {
		return mWebDisplayFlag;
	}
	/**
	 * @param pWebDisplayFlag the mWebDisplayFlag to set.
	 */
	public void setWebDisplayFlag(char pWebDisplayFlag) {
		this.mWebDisplayFlag = pWebDisplayFlag;
	}
	/**
	 * @return the mLocale.
	 */
	public String getLocale() {
		return mLocale;
	}
	/**
	 * @param pLocale the mLocale to set.
	 */
	public void setLocale(String pLocale) {
		this.mLocale = pLocale;
	}
	/**
	 * @return the mCountryCode.
	 */
	public String getCountryCode() {
		return mCountryCode;
	}
	/**
	 * @param pCountryCode the mCountryCode to set.
	 */
	public void setCountryCode(String pCountryCode) {
		this.mCountryCode = pCountryCode;
	}
	/**
	 * @return the mMarketCode.
	 */
	public String getMarketCode() {
		return mMarketCode;
	}
	/**
	 * @param pMarketCode the mMarketCode to set
	 */
	public void setMarketCode(String pMarketCode) {
		this.mMarketCode = pMarketCode;
	}
	/**
	 * @return the mHiddenFlag
	 */
	public String getHiddenFlag() {
		return mHiddenFlag;
	}
	/**
	 * @param pHiddenFlag the mHiddenFlag to set
	 */
	public void setHiddenFlag(String pHiddenFlag) {
		this.mHiddenFlag = pHiddenFlag;
	}
	/**
	 * @return the mSkn
	 */
	public String getSkn() {
		return mSkn;
	}
	/**
	 * @param pSkn the mSkn to set
	 */
	public void setSkn(String pSkn) {
		this.mSkn = pSkn;
	}
	/**
	 * @return the mListPrice
	 */
	public String getListPrice() {
		return mListPrice;
	}
	/**
	 * @param pListPrice the mListPrice to set
	 */
	public void setListPrice(String pListPrice) {
		this.mListPrice = pListPrice;
	}
	/**
	 * @return the mSalePrice
	 */
	public String getSalePrice() {
		return mSalePrice;
	}
	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(String pSalePrice) {
		this.mSalePrice = pSalePrice;
	}
	/**
	 * @return the mStoreNumber
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}
	/**
	 * @param pStoreNumber the mStoreNumber to set
	 */
	public void setStoreNumber(String pStoreNumber) {
		this.mStoreNumber = pStoreNumber;
	}
	/**
	 * @return the mSupressInSearch
	 */
	public char getSupressInSearch() {
		return mSupressInSearch;
	}
	/**
	 * @param pSupressInSearch the mSupressInSearch to set
	 */
	public void setSupressInSearch(char pSupressInSearch) {
		this.mSupressInSearch = pSupressInSearch;
	}
}
