package com.tru.feed.outbound;

/**
 * The Class TRUPriceAuditFeedConstants.
 * @author PA
 * @version 1.0
 */
public class TRUPriceAuditFeedConstants {
	/**
	 * The constant to hold productItems.
	 */
	public static final String SKU_ITEMS="skuItems";
	
	/**
	 * The constant to hold NUMBER_ZERO.
	 */
	public static final int NUMBER_ZERO = 0;
	
	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;
	
	/**
	 * The Constant to hold exportFeed.
	 */
	public static final String EXPORT_FEED = "exportFeed";
	
	/**
	 * The Constant to hold RANGE_MAP.
	 */
	public static final String RANGE_MAP = "RANGE_MAP";
	
	/**
	 * The Constant to hold partition.
	 */
	public static final String PARTITION = "partition";
	
	/**
	 * The Constant to hold COMA_SEPERATOR.
	 */
	public static final String COMA_SEPERATOR=","; 
	
	/**
	 * The Constant to hold EMPTY.
	 */
	public static final String EMPTY = "";
	/**
	 * Constant to hold NO_JOBLAUNCHER_PROVIDED_MSG.
	 */
	public static final String NO_JOBLAUNCHER_PROVIDED_MSG = "A JobLauncher must be provided.  Please add one to the configuration.";
	/**
	 * Constant to hold NO_JOBEXPLORER_PROVIDED_MSG.
	 */
	public static final String NO_JOBEXPLORER_PROVIDED_MSG = 
			"A JobExplorer must be provided for a restart or start next operation.  Please add one to the configuration.";
	/**
	 * Constant to hold RESTART.
	 */
	public static final String RESTART = "-restart";
	/**
	 * Constant to hold NEXT.
	 */
	public static final String NEXT = "-next";
	/**
	 * Constant to hold JOB_TERMINATED_ERR_MSG.
	 */
	public static final String JOB_TERMINATED_ERR_MSG = "Job Terminated in error: ";
	/**
	 * Constant to hold HYPHEN.
	 */
	public static final String HYPHEN = "-";
	/**
	 * Constant to hold JOB_FAIL_NO_PARAM_JOB_NAME.
	 */
	public static final String JOB_FAIL_NO_PARAM_JOB_NAME = "At least 1 argument required: JobName.";
	/**
	 * Constatnt to hold siteId.
	 */
	public static final String SITE_ID="siteId";
	/**
	 * Constant to hold id for siteIds.
	 */
	public static final String SITE_IDS="siteIds";
	/**
	 * Constant to hold id for siteIdFileNameMap.
	 */
	public static final String SITE_ID_FILENAME_MAP="siteIdFileNameMap";
	/**
	 * Constant to hold id for SITE_CONF_ID.
	 */
	public static final String SITE_CONF_ID = "id";
	/**
	 * Constant to hold id for site production url for configuration.
	 */
	public static final String SITE_CONF_PROD_URL = "production_url";
	/**
	 * Constant to hold id for site production url for configuration.
	 */
	public static final String SITE_ID_PRODURL_MAP = "siteIdprodURLMap";	
	/**
	 * Constant to hold id for salePrice for configuration.
	 */
	public static final String SALE_PRICE_LIST = "sale_pricelist_id";
	/**
	 * Constant to hold id for listPrice for configuration.
	 */
	public static final String LIST_PRICE_LIST = "list_pricelist_id";
	/**
	 * Constant to hold id for salePriceMap for configuration.
	 */
	public static final String SALE_PRICE_MAP = "salePriceMap";
	/**
	 * Constant to hold id for listPriceMap for configuration.
	 */
	public static final String LIST_PRICE_MAP = "listPriceMap";
	/**
	 * Constant to hold id for siteIdFileNameMap.
	 */
	public static final String SITE_ID_FTP_HOME_DIR_MAP="siteIdFTPHomeDirectoryMap";
	/**
	 * Constant to hold id for siteIds.
	 */
	public static final String FTP_SITE_IDS="siteIds";
	/**
	 * Constant to hold PhaseName.
	 */
	public static final String PHASE_NAME = "PhaseName";
	/**
	 * Constant to hold priceAuditFileName.
	 */
	public static final String PRICE_AUDIT_FEED_FILENAME = "priceAuditFileName";
	/**
	 * Constant to hold priceAuditHomeDirectory.
	 */
	public static final String PRICE_AUDIT_FEED_FTP_HOME_DIR="priceAuditHomeDirectory";
	/**
	 * Constant to hold APOSTROPHE.
	 */
	public static final String APOSTROPHE="'";
	/** The Constant INTERGER_NUMBER_ONE. */
	public static final int INTEGER_NUMBER_ONE = 1;
	/** The Constant for PRODUCT. */
	public static final String SKU = "sku";
	/** The Constant for TO .*/
	public static final String TO = "to";
	/** The Constant for FROM .*/
	public static final String FROM = "from";
	/** The Constant for EXPORT_LIST. */
	public static final String EXPORT_LIST="exportList";
	/** The Constant for SITE_CONFIGURATION .*/
	public static final String SITE_CONFIGURATION="siteConfiguration";
	/**
	 * Constant to hold backOrderStatus.
	 */
	public static final String BACK_ORDER_STATUS = "backOrderStatus";
	/**
	 * Constant to hold N.
	 */
	public static final char DEFAULT_BACK_ORDER_CODE = 'N';
	/**
	 * Constant to hold BLANK_CHARACTER.
	 */
	public static final char BLANK_CHARACTER = ' ';
	/**
	 * Constant to hold webDisplayFlag.
	 */
	public static final String WEB_DISPLAY_FLAG = "webDisplayFlag";
	/**
	 * Constant to hold A.
	 */
	public static final char A = 'A';
	/**
	 * Constant to hold N.
	 */
	public static final char N = 'N';
	/**
	 * Constant to hold Y.
	 */
	public static final char Y = 'Y';
	/**
	 * Constant to hold default SUPER_DISPLAY_FLAG.
	 */
	public static final String SUPER_DISPLAY_FLAG = "Y";
	/**
	 * Constant to hold N SUPER_DISPLAY_FLAG.
	 */
	public static final String SUPER_DISPLAY_FLAG_N = "N";
	/**
	 * CONSTANT TO HOLD supressInSearch.
	 */
	public static final String SUPRESS_IN_SEARCH = "supressInSearch"; 
	/**
	 * Constant to hold inventory.
	 */
	public static final String INVENTORY="inventory";
	/**
	 * Constant to hold INVENTRY_CATALOGREFID.
	 */
	public static final String INVENTRY_CATALOGREFID = "catalogRefId";
	/**
	 * Constant to hold creationDate.
	 */
	public static final String CREATION_DATE = "creationDate";
	/**
	 * Constant to hold locationId.
	 */
	public static final String LOCATION_ID = "locationId";
	/**
	 * Constant to hold TRU.
	 */
	public static final String TRU = "TRU";
	/**
	 * Constant to hold File Type CSV.
	 */
	public static final String FILE_TYPE = ".txt";
	/**
	 * Constant to hold SKN.
	 */
	public static final String SKN = "rusItemNumber";
	/**
	 * Constant to hold Hidden Flag.
	 */
	public static final String HIDDEN_FLAG = "superDisplayFlag";
	/**
	 * Constant to hold SEQUENCE_NUMBER.
	 */
	public static final String SEQUENCE_NUMBER = "sequence_number";
	/**
	 * Constant to hold EXCEPTION.
	 */
	public static final String EXCEPTION = "exception";
	/**
	 * Constant to hold write status.
	 */
	public static final String FEED_STATUS = "feedStatus";
	/**
	 * Constant to hold success.
	 */
	public static final String FEED_SUCCESS = "success";
	/**
	 * Constant to hold NO_OF_RECORD.
	 */
	public static final String NO_OF_RECORD = "noOfRecords"; 
	/**
	 * Constant to hold FEED_FAILED.
	 */
	public static final String FEED_FAILED = "failure";
	/**
	 * Constant to hold FILE_SEQUENCE_FILE_NAME.
	 */
	public static final String FILE_SEQUENCE_FILE_NAME = "FileSequenceFileName";
	/**
	 * Constant to hold HEADER.
	 */
	public static final String HEADER = "HEADER";
	/**
	 * Constant to hold productId.
	 */
	public static final String PRODUCT_ID = "productId";
	/**
	 * Constant to hold skuId.
	 */
	public static final String SKU_ID="skuId";
	/**
	 * Constant to hold skn.
	 */
	public static final String SKN_ID = "skn";
	/**
	 * Constant to hold listPrice.
	 */
	public static final String LISTPRICE = "listPrice";
	/** 
	 * Constant to hold salePrice.
	 */
	public static final String SALEPRICE = "salePrice";
	/**
	 * Constant to hold firstInventoryReceivedDate.
	 */
	public static final String FIRST_INVENTORY_RECEIVED_DATE = "firstInventoryReceivedDate";
	/**
	 * Constant to hold backOrderCode.
	 */
	public static final String BACK_ORDER_CODE = "backOrderCode";
	/**
	 * Constant to hold hiddenFlag.
	 */
	public static final String HIDDEN_FLAG_FOR_BEAN = "hiddenFlag";
	/**
	 * Constant to hold locale.
	 */
	public static final String LOCALE = "locale";
	/**
	 * Constant to hold countryCode.
	 */
	public static final String COUNTRY_CDE = "countryCode";
	/**
	 * Constant to hold marketCode.
	 */
	public static final String MARKET_CDE = "marketCode";
	/**
	 * Constant to hold storeNumber.
	 */
	public static final String STORE_NUMBER = "storeNumber";
	/**
	 * Constant to hold TRAILER.
	 */
	public static final String TRAILER = "TRAILER";
	/**
	 * Constant to hold header and sequence padding.
	 */
	public static final int HEARDER_PADDING = 6;
	/**
	 * Constant to hold RECORDS_PADDING.
	 */
	public static final int RECORDS_PADDING = 10;
	/**
	 * Constant to hold SALE_PRICE_SUM_PADDING.
	 */
	public static final int SALE_PRICE_SUM_PADDING = 15;
	/**
	 * Constant to hold messageSubject.
	 */
	public static final String MESSAGE_SUBJECT = "messageSubject";
	/**
	 * Constant to hold Underscore.
	 */
	public static final String UNDERSCORE = "_";
	/**
	 * Constant to hold US_PRICE_AUDIT.
	 */
	public static final String US_PRICE_AUDIT = "_US_PRICE_AUDIT_";
	/**
	 * Constant to hold onlinePID.
	 */
	public static final String ONLINE_PID = "onlinePID";
	/**
	 * Constant to hold SUB_TYPE.
	 */
	public static final String SUB_TYPE = "type";
	/**
	 * Constant to hold NON_MERCH_SKU.
	 */
	public static final String NON_MERCH_SKU = "nonMerchSKU";
	/**
	 * Constant to hold CHANNEL_AVAILABILITY.
	 */
	public static final String CHANNEL_AVAILABILITY = "channelAvailability";
	/**
	 * Constant to hold ONLINE_ONLY.
	 */
	public static final String ONLINE_ONLY = "Online Only";
	/**
	 * Constant to hold BOTH_STORE_ONLINE.
	 */
	public static final String BOTH_STORE_ONLINE = "N/A";
	/**
	 * Constant to hold NUMBER_ZERO_WITH_TWO_DECIMALS.
	 */
	public static final String NUMBER_ZERO_WITH_TWO_DECIMALS = "0.00";
	/**
	 * Constant to hold DOT_STRING.
	 */
	public static final String DOT_STRING = ".";
	/**
	 * Constant to hold INTEGER_NUMBER_TWO.
	 */
	public static final int INTEGER_NUMBER_TWO = 2;
	/**
	 * Constant to hold INTEGER_NUMBER_ZERO.
	 */
	public static final int INTEGER_NUMBER_ZERO = 0;
	/**
	 * Constant to hold NUMBER_ZERO_WITH_DECIMAL.
	 */
	public static final double NUMBER_ZERO_WITH_DECIMAL = 0.0;
	/**
	 * Constant to hold INTEGER_NUMBER_HUNDRED.
	 */
	public static final int INTEGER_NUMBER_HUNDRED = 100;
	/**
	 * Constant to hold SHIP_TO_STORE_ELIGIBLE.
	 */
	public static final String SHIP_TO_STORE_ELIGIBLE = "shipToStoreEeligible";
	/**
	 * Constant to hold SHIP_FROM_STORE_ELIGIBLE.
	 */
	public static final String SHIP_FROM_STORE_ELIGIBLE = "shipFromStoreEligibleLov";
	/**
	 * Constant to hold ITEM_IN_STORE_PICKUP.
	 */
	public static final String ITEM_IN_STORE_PICKUP = "itemInStorePickUp";
	/**
	 * Constant to hold STRING_Y.
	 */
	public static final String STRING_Y = "Y";
	
	/** The Constant TIME_FORMAT. */
	public static final String TIME_FORMAT = "YYYY-MM-dd";
	
	/** The Constant STRING_ZERO. */
	public static final String STRING_ZERO = "0";
	
	/** The Constant COLLECTION_PRODUCT. */
	public static final String COLLECTION_PRODUCT = "collectionProduct";
	
	/** The Constant TYPE. */
	public static final String TYPE = "type";
	
	/** The Constant TEMPLATE_EXCEPTION. */
	public static final String TEMPLATE_EXCEPTION = "Template Exception Thrown : ";

	/** The Constant AUDIT_FAILURE_MSG. */
	public static final String AUDIT_FAILURE_MSG = "TRU ATG Web Store - USA - %1$s - Price Audit Export Processing Failed - %2$s";

	/** The Constant SUB_DATE_FORMAT. */
	public static final String SUB_DATE_FORMAT = "MMddyyyy-HH:mm:ss";

	/** The Constant TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT = "totalRecordCount";

	/** The Constant TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT = "successRecordCount";

	/** The Constant TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT = "totalFailedRecordCount";

	/** The Constant TEMPLATE_PARAMETER_DETAIL_EXCEPTION. */
	public static final String TEMPLATE_PARAMETER_DETAIL_EXCEPTION = "detailException";

	/** The Constant TEMPLATE_PARAMETER_MESSAGE_CONTENT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_CONTENT = "messageContent";

	/** The Constant TEMPLATE_PARAMETER_OS_HOST_NAME. */
	public static final String TEMPLATE_PARAMETER_OS_HOST_NAME = "OSHostName";

	/** The Constant XLS_SHEET. */
	public static final String XLS_SHEET = "sheet";

	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "MMddyyyy'-'HH:mm:ss";

	/** The Constant XLS. */
	public static final String XLS = "xls";

	/** The Constant ERROR_FILE. */
	public static final String ERROR_FILE = "Error_";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_SUBJECT = "messageSubject";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant PRICE_AUDIT. */
	public static final String PRICE_AUDIT ="Price Audit";
	
	/** The Constant PRICE_AUDIT. */
	public static final String PRICE_AUDIT_STARTED ="Price Audit export started";
	
	/** The Constant PRICE_AUDIT_SUCCESS. */
	public static final String PRICE_AUDIT_SUCCESS = "Price Audit export successful";
	
	/** The Constant PRICE_AUDIT_SUCCESS. */
	public static final String PRICE_AUDIT_ERROR = "Price Audit export failed";
	
	/** The Constant LOGGER_START. */
	public static final String LOGGER_START = "Entered into ExportBatchInvoker:: start";

	/** The Constant JOB_NAME. */
	public static final String JOB_NAME = "Job name : ";

	/** The Constant JOB. */
	public static final String JOB = "Job : ";

	/** The Constant JOB_PARAMETERS. */
	public static final String JOB_PARAMETERS = "Job parameters : ";

	/** The Constant LOGGER_END. */
	public static final String LOGGER_END = "Exit from ExportBatchInvoker:: end";

	/** The Constant LOGGER_END_EXCEPTION. */
	public static final String LOGGER_END_EXCEPTION = "Exit from ExportBatchInvoker exception :: end";

}
