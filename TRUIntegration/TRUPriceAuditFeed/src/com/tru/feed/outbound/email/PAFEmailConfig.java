package com.tru.feed.outbound.email;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

/**
 * The Class PAFEmailConfig.
 * @version 1.0
 * @author Professional Access.
 */
public class PAFEmailConfig extends GenericService{
	/**
	 * property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * holds mPafReceiverEmailId.
	 */
	private List<String> mPafReceiverEmailId;
	/**
	 * property to hold mTemplateEmailSender.
	 */
	private TemplateEmailSender mTemplateEmailSender;
	/**
	 * property to hold mPafCodeTemplateURL.
	 */
	private String mPafTemplateURL;
	/**
	 * property to hold mPafTemplateURL.
	 */
	private TemplateEmailInfoImpl mPafTemplateEmailInfo;
	/**
	 * property to hold mFailureTemplateEmailInfo.
	 */
	private String mFailureTemplateEmailInfo;
	/**
	 * property to hold mSuccessTemplateEmailInfo.
	 */
	private String mSuccessTemplateEmailInfo;

	/** The m env. */
	private String mEnv;

	/** The m price audit success sub. */
	private String mPriceAuditSuccessSub;

	/** The m price audit error sub. */
	private String mPriceAuditErrorSub;

	/** The m price audit failure sub. */
	private String mPriceAuditFailureSub;

	/** The m price audit error detail excep. */
	private String mPriceAuditErrorDetailExcep;

	/** The m file name. */
	private String mFileName;

	/** The m record number. */
	private String mRecordNumber;

	/** The m time stamp. */
	private String mTimeStamp;

	/** The m attachment item id label. */
	private String mAttachmentItemIdLabel;

	/** The m attachment exception reason label. */
	private String mAttachmentExceptionReasonLabel;

	/** The m product id missing. */
	private String mProductIdMissing;

	/** The OS host name. */
	private String mOSHostName;

	/** The m invalid output folder path. */
	private String mInvalidOutFolderContent;

	/** The m invalid out folder detail excep. */
	private String mInvalidOutFolderDetailExcep;

	/**
	 * @return the mFailureTemplateEmailInfo.
	 */
	public String getFailureTemplateEmailInfo() {
		return mFailureTemplateEmailInfo;
	}
	/**
	 * @param pFailureTemplateEmailInfo the mFailureTemplateEmailInfo to set
	 */
	public void setFailureTemplateEmailInfo(String pFailureTemplateEmailInfo) {
		this.mFailureTemplateEmailInfo = pFailureTemplateEmailInfo;
	}
	/**
	 * @return the mSuccessTemplateEmailInfo.
	 */
	public String getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}
	/**
	 * @param pSuccessTemplateEmailInfo the mSuccessTemplateEmailInfo to set.
	 */
	public void setSuccessTemplateEmailInfo(String pSuccessTemplateEmailInfo) {
		this.mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}
	/**
	 * @return the mPafTemplateEmailInfo.
	 */
	public TemplateEmailInfoImpl getPafTemplateEmailInfo() {
		return mPafTemplateEmailInfo;
	}
	/**
	 * @param pPafTemplateEmailInfo the mPafTemplateEmailInfo to set.
	 */
	public void setPafTemplateEmailInfo(TemplateEmailInfoImpl pPafTemplateEmailInfo) {
		this.mPafTemplateEmailInfo = pPafTemplateEmailInfo;
	}
	/**
	 * @return the mTemplateEmailSender.
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}
	/**
	 * @param pTemplateEmailSender the mTemplateEmailSender to set.
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		this.mTemplateEmailSender = pTemplateEmailSender;
	}
	/**
	 * @return the mPafTemplateURL.
	 */
	public String getPafTemplateURL() {
		return mPafTemplateURL;
	}
	/**
	 * @param pPafTemplateURL the mPafTemplateURL to set.
	 */
	public void setPafTemplateURL(String pPafTemplateURL) {
		this.mPafTemplateURL = pPafTemplateURL;
	}
	/**
	 * @return the mPafReceiverEmailId.
	 */
	public List<String> getPafReceiverEmailId() {
		return mPafReceiverEmailId;
	}
	/**
	 * @param pPafReceiverEmailId the mPafReceiverEmailId to set.
	 */
	public void setPafReceiverEmailId(List<String> pPafReceiverEmailId) {
		this.mPafReceiverEmailId = pPafReceiverEmailId;
	}
	/**
	 * @return the mSiteId.
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the mSiteId to set.
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}

	/**
	 * Gets the env.
	 * 
	 * @return the env
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * Sets the env.
	 * 
	 * @param pEnv
	 *            the env to set
	 */
	public void setEnv(String pEnv) {
		mEnv = pEnv;
	}

	/**
	 * Gets the price audit success sub.
	 * 
	 * @return the price audit success sub
	 */
	public String getPriceAuditSuccessSub() {
		return mPriceAuditSuccessSub;
	}

	/**
	 * Sets the price audit success sub.
	 * 
	 * @param pPriceAuditSuccessSub
	 *            the new price audit success sub
	 */
	public void setPriceAuditSuccessSub(String pPriceAuditSuccessSub) {
		this.mPriceAuditSuccessSub = pPriceAuditSuccessSub;
	}

	/**
	 * Gets the price audit error sub.
	 * 
	 * @return the price audit error sub
	 */
	public String getPriceAuditErrorSub() {
		return mPriceAuditErrorSub;
	}

	/**
	 * Sets the price audit error sub.
	 * 
	 * @param pPriceAuditErrorSub
	 *            the new price audit error sub
	 */
	public void setPriceAuditErrorSub(String pPriceAuditErrorSub) {
		this.mPriceAuditErrorSub = pPriceAuditErrorSub;
	}

	/**
	 * Gets the price audit failure sub.
	 * 
	 * @return the price audit failure sub
	 */
	public String getPriceAuditFailureSub() {
		return mPriceAuditFailureSub;
	}

	/**
	 * Sets the price audit failure sub.
	 * 
	 * @param pPriceAuditFailureSub
	 *            the new price audit failure sub
	 */
	public void setPriceAuditFailureSub(String pPriceAuditFailureSub) {
		this.mPriceAuditFailureSub = pPriceAuditFailureSub;
	}

	/**
	 * Gets the price audit error detail excep.
	 * 
	 * @return the price audit error detail excep
	 */
	public String getPriceAuditErrorDetailExcep() {
		return mPriceAuditErrorDetailExcep;
	}

	/**
	 * Sets the price audit error detail excep.
	 * 
	 * @param pPriceAuditErrorDetailExcep
	 *            the new price audit error detail excep
	 */
	public void setPriceAuditErrorDetailExcep(String pPriceAuditErrorDetailExcep) {
		this.mPriceAuditErrorDetailExcep = pPriceAuditErrorDetailExcep;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param pFileName
	 *            the new file name
	 */
	public void setFileName(String pFileName) {
		this.mFileName = pFileName;
	}

	/**
	 * Gets the record number.
	 * 
	 * @return the record number
	 */
	public String getRecordNumber() {
		return mRecordNumber;
	}

	/**
	 * Sets the record number.
	 * 
	 * @param pRecordNumber
	 *            the new record number
	 */
	public void setRecordNumber(String pRecordNumber) {
		this.mRecordNumber = pRecordNumber;
	}

	/**
	 * Gets the time stamp.
	 * 
	 * @return the time stamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets the time stamp.
	 * 
	 * @param pTimeStamp
	 *            the new time stamp
	 */
	public void setTimeStamp(String pTimeStamp) {
		this.mTimeStamp = pTimeStamp;
	}

	/**
	 * Gets the attachment item id label.
	 * 
	 * @return the attachment item id label
	 */
	public String getAttachmentItemIdLabel() {
		return mAttachmentItemIdLabel;
	}

	/**
	 * Sets the attachment item id label.
	 * 
	 * @param pAttachmentItemIdLabel
	 *            the new attachment item id label
	 */
	public void setAttachmentItemIdLabel(String pAttachmentItemIdLabel) {
		this.mAttachmentItemIdLabel = pAttachmentItemIdLabel;
	}

	/**
	 * Gets the attachment exception reason label.
	 * 
	 * @return the attachment exception reason label
	 */
	public String getAttachmentExceptionReasonLabel() {
		return mAttachmentExceptionReasonLabel;
	}

	/**
	 * Sets the attachment exception reason label.
	 * 
	 * @param pAttachmentExceptionReasonLabel
	 *            the new attachment exception reason label
	 */
	public void setAttachmentExceptionReasonLabel(String pAttachmentExceptionReasonLabel) {
		this.mAttachmentExceptionReasonLabel = pAttachmentExceptionReasonLabel;
	}

	/**
	 * Gets the product id missing.
	 * 
	 * @return the product id missing
	 */
	public String getProductIdMissing() {
		return mProductIdMissing;
	}

	/**
	 * Sets the product id missing.
	 * 
	 * @param pProductIdMissing
	 *            the new product id missing
	 */
	public void setProductIdMissing(String pProductIdMissing) {
		this.mProductIdMissing = pProductIdMissing;
	}

	/**
	 * Gets the OS host name.
	 * 
	 * @return the OS host name
	 */
	public String getOSHostName() {
		return mOSHostName;
	}

	/**
	 * Sets the OS host name.
	 * 
	 * @param pOSHostName
	 *            the new OS host name
	 */
	public void setOSHostName(String pOSHostName) {
		mOSHostName = pOSHostName;
	}

	/**
	 * Gets the invalid out folder content.
	 * 
	 * @return the invalid out folder content
	 */
	public String getInvalidOutFolderContent() {
		return mInvalidOutFolderContent;
	}

	/**
	 * Sets the invalid out folder content.
	 * 
	 * @param pInvalidOutFolderContent
	 *            the new invalid out folder content
	 */
	public void setInvalidOutFolderContent(String pInvalidOutFolderContent) {
		this.mInvalidOutFolderContent = pInvalidOutFolderContent;
	}

	/**
	 * Gets the invalid out folder detail excep.
	 * 
	 * @return the invalid out folder detail excep
	 */
	public String getInvalidOutFolderDetailExcep() {
		return mInvalidOutFolderDetailExcep;
	}

	/**
	 * Sets the invalid out folder detail excep.
	 * 
	 * @param pInvalidOutFolderDetailExcep
	 *            the new invalid out folder detail excep
	 */
	public void setInvalidOutFolderDetailExcep(String pInvalidOutFolderDetailExcep) {
		this.mInvalidOutFolderDetailExcep = pInvalidOutFolderDetailExcep;
	}
}
