package com.tru.reportemail;



import java.util.HashMap;
import java.util.Map;

import javax.transaction.TransactionManager;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.common.TRUConstants;
/**
 * @version 1.0
 * @author PA
 * This Class is used to trigger email when exception occurs.
 * 
 */
public class ReportEmailSender extends GenericService{

	/** Property to hold mTemplateEmailSender .*/
	
	private TemplateEmailSender mTemplateEmailSender;
	
	/** Property to hold mReportEmailConfiguration .*/
	
	private ReportEmailConfiguration mReportEmailConfiguration;
	/**
	 * @return the ReportEmailConfiguration
	 */
	public ReportEmailConfiguration getReportEmailConfiguration() {
		return mReportEmailConfiguration;
	}
	/**
	 * @param pReportEmailConfiguration the ReportEmailConfiguration to set
	 */
	public void setReportEmailConfiguration(
			ReportEmailConfiguration pReportEmailConfiguration) {
		mReportEmailConfiguration = pReportEmailConfiguration;
	}
	private TransactionManager mTransactionManager;
	/**
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}
	/**
	 * @param pTemplateEmailSender the templateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	/**
	 * @param pTransactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	
	/**
	 * <b>This method will send an email to recipient if the generation of report
	 * is failed.</b><br>
	 * .
	 * 
	 * @param pMessage - Message.
	 * @param pReportType - ReportType.
	 */
	public void sendReportFailureEmail(String pMessage , String pReportType) {
		if(isLoggingDebug()){
			logDebug("ReportEmailSender :: sendReportFailureEmail() :: START");
		}
		try {
			Map<String,Object> mapObject=new HashMap<String,Object>();
			mapObject.put(TRUConstants.MESSAGE_VALUE, pMessage);
			mapObject.put(TRUConstants.REPORT_TYPE, pReportType);
			
			TemplateEmailInfoImpl emailInfo =getReportEmailConfiguration().getReportFailureEmailInfo();
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(mapObject);
				} else {
					emailInfo.setTemplateParameters(mapObject);
				}
			
			getTemplateEmailSender().sendEmailMessage(emailInfo, getReportEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
		} 
		if(isLoggingDebug()){
			logDebug("ReportEmailSender :: sendReportFailureEmail() :: END");
		}
	}
	
	/**
	 * <b>This method will send an email to recipient if the generation of report
	 * is success.</b><br>
	 * .
	 * 
	 * @param pMessage - Message.
	 * @param pReportType - ReportType.
	 */
	public void sendReportSuccessEmail(String pMessage ,String pReportType) {
		if(isLoggingDebug()){
			logDebug("ReportEmailSender :: sendReportSuccessEmail() :: START");
		}
		try {
			Map<String,Object> mapObject=new HashMap<String,Object>();
			mapObject.put(TRUConstants.MESSAGE_VALUE, pMessage);
			mapObject.put(TRUConstants.REPORT_TYPE, pReportType);
			
			TemplateEmailInfoImpl emailInfo =getReportEmailConfiguration().getReportSuccessEmailInfo();
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(mapObject);
				} else {
					emailInfo.setTemplateParameters(mapObject);
				}
			
			getTemplateEmailSender().sendEmailMessage(emailInfo, getReportEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
		} 
		if(isLoggingDebug()){
			logDebug("ReportEmailSender :: sendReportSuccessEmail() :: END");
		}
	}
	
}
