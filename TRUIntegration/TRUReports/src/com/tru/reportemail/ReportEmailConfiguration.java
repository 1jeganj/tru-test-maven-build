package com.tru.reportemail;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class ReportEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class ReportEmailConfiguration extends GenericService {

	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mReportFailureEmailInfo;

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mReportSuccessEmailInfo;
	
	/** Property to hold Recipients. */
	private List<String> mRecipients;
	
	/**
	 * @return the reportSuccessEmailInfo
	 */
	public TemplateEmailInfoImpl getReportSuccessEmailInfo() {
		return mReportSuccessEmailInfo;
	}

	/**
	 * @param pReportSuccessEmailInfo the reportSuccessEmailInfo to set
	 */
	public void setReportSuccessEmailInfo(
			TemplateEmailInfoImpl pReportSuccessEmailInfo) {
		mReportSuccessEmailInfo = pReportSuccessEmailInfo;
	}

	/**
	 * @return the ReportFailureEmailInfo
	 */
	public TemplateEmailInfoImpl getReportFailureEmailInfo() {
		return mReportFailureEmailInfo;
	}

	/**
	 * @param pReportFailureEmailInfo the reportFailureEmailInfo to set
	 */
	public void setReportFailureEmailInfo(
			TemplateEmailInfoImpl pReportFailureEmailInfo) {
		mReportFailureEmailInfo = pReportFailureEmailInfo;
	}

	/**
	 * Gets the recipients.
	 *
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 *
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}
}
