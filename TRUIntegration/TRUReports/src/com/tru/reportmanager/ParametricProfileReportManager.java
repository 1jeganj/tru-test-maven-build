package com.tru.reportmanager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.email.conf.TRUParametericCSVEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.parametric.TRUParametricCSVTools;
import com.tru.endeca.vo.ParametricProfileVO;
import com.tru.reporttools.ParametricProfileWriter;


/**
 * The Class contains logic to generate ParametricProfile feed file .
 * @author PA
 * @version 1.0
 * 
 */
public class ParametricProfileReportManager extends GenericService {

	/** property to hold mTRUParametricCSVTools. */
	private TRUParametricCSVTools mTRUParametricCSVTools;

	/**property to hold mParametricProfileWriter. */
	private ParametricProfileWriter mParametricProfileWriter;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	/** The m ParametricCSV email configuration. */
	private TRUParametericCSVEmailConfiguration mParametericCSVEmailConfiguration;

	/** The m enabled. */
	public boolean mEnabled;

	/**
	 * This method generates report for ParametricFeedGenerator.
	 */
	public void generateReport() {
		boolean isCheck = true;
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametricProfileReportManager.generateReport()");
		}
		if(isEnabled()) {
			// Get data for Parametric report.
			List<ParametricProfileVO> parametricProfileList = getTRUParametricCSVTools().getParametricCsvData();

			// Write data to CSV file.
			if (parametricProfileList != null) {
				isCheck = getParametricProfileWriter().doParametricProfileWrite(parametricProfileList);
			}
		}
		
		if(isCheck) {
			getTRUEmailServiceUtils().sendSiteMapSuccessEmail(TRUEndecaConstants.PARAMETRIC_CSV,setParametericCSVSucessEmailTemplate());
		}
	

		if (isLoggingDebug()) {
			logDebug("Exit from :: ParametricProfileReportManager.generateReport()");
		}
	}
	
	
	
	/**
	 * Sets the parametric CSV success email template.
	 *
	 * @return the map
	 */
	private Map<String, Object> setParametericCSVSucessEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: ParametericReportSheduler.setParametericCSVSucessEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		StringBuilder sb=new StringBuilder(getParametericCSVEmailConfiguration().getSuccessParametericCSVEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUCC, getParametericCSVEmailConfiguration().getSuccParametericCSVMessage());
		if (isLoggingDebug()) {
			logDebug("END:: ParametericReportSheduler.setParametericCSVSucessEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	
	/**
	 *
	 * @return mTRUParametricCSVTools.
	 */
	public TRUParametricCSVTools getTRUParametricCSVTools() {
		return mTRUParametricCSVTools;
	}

	/**
	 *
	 * @param pTRUParametricCSVTools - the TRUParametricCSVTools to set.
	 */
	public void setTRUParametricCSVTools(TRUParametricCSVTools pTRUParametricCSVTools) {
		this.mTRUParametricCSVTools = pTRUParametricCSVTools;
	}
	/**
	 * @return mParametricProfileWriter.
	 */
	public ParametricProfileWriter getParametricProfileWriter() {
		return mParametricProfileWriter;
	}

	/**
	 * @param pParametricProfileWriter - the ParametricProfileWriter to set.
	 */
	public void setParametricProfileWriter(ParametricProfileWriter pParametricProfileWriter) {
		this.mParametricProfileWriter = pParametricProfileWriter;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		this.mEnabled = pEnabled;
	}
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	
	/**
	 * @return the ParametericCSVEmailConfiguration
	 */
	public TRUParametericCSVEmailConfiguration getParametericCSVEmailConfiguration() {
		return mParametericCSVEmailConfiguration;
	}

	/**
	 * @param pParametericCSVEmailConfiguration the ParametericCSVEmailConfiguration to set
	 */
	public void setParametericCSVEmailConfiguration(
			TRUParametericCSVEmailConfiguration pParametericCSVEmailConfiguration) {
		mParametericCSVEmailConfiguration = pParametericCSVEmailConfiguration;
	}
	
	
}
