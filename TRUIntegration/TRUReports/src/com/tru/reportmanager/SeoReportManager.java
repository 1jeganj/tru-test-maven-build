package com.tru.reportmanager;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import atg.nucleus.GenericService;

import com.tru.reportemail.ReportEmailSender;
import com.tru.reporttools.TRUReportConstants;
/**
 * @version 1.0
 * @author PA
 * The Class SeoReportManager.
 * 
 */
public class SeoReportManager extends GenericService {
	/**
	 * property to hold mExtension.
	 */
	private String mExtension;
	/**
	 * property to hold mFileNameEnd.
	 */
	private String mFileNameEnd;
	/**
	 * property to hold mFileNameDateFormat.
	 */
	private String mFileNameDateFormat;
	/**
	 * property to hold mEnv.
	 */
	private String mEnv;
	/**
	 * property to hold mSeoAppend.
	 */
	private String mSeoAppend;

	/**
	 * property to hold mOutputDirectory.
	 */
	private String mOutputDirectory;

	/**
	 * property to hold mDatasource.
	 */
	private String mDatasource;
	/**
	 * property to hold mStoredProcedure.
	 */
	private String mStoredProcedure;
	/**
	 * property to hold mReportType.
	 */
	private String mReportType;
	/**
	 * property to hold mReportEmailSender.
	 */
	private ReportEmailSender mReportEmailSender;
	/**
	 * property to hold mErrorCodes.
	 */
	private Map<String,String> mErrorCodes;
	/**
	 * @return the errorCodes
	 */
	public Map<String, String> getErrorCodes() {
		return mErrorCodes;
	}

	/**
	 * @param pErrorCodes the errorCodes to set
	 */
	public void setErrorCodes(Map<String, String> pErrorCodes) {
		mErrorCodes = pErrorCodes;
	}

	/**
	 * @return the ReportEmailSender
	 */
	public ReportEmailSender getReportEmailSender() {
		return mReportEmailSender;
	}

	/**
	 * @param pReportEmailSender the ReportEmailSender to set
	 */
	public void setReportEmailSender(
			ReportEmailSender pReportEmailSender) {
		mReportEmailSender = pReportEmailSender;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return mReportType;
	}

	/**
	 * @param pReportType the reportType to set
	 */
	public void setReportType(String pReportType) {
		mReportType = pReportType;
	}

	/**
	 * @return the mFileNameEnd.
	 */
	public String getFileNameEnd() {
		return mFileNameEnd;
	}

	/**
	 * @param pFileNameEnd
	 *            the FileNameEnd to set.
	 */
	public void setFileNameEnd(String pFileNameEnd) {
		this.mFileNameEnd = pFileNameEnd;
	}

	/**
	 * @return the mFileNameDateFormat.
	 */
	public String getFileNameDateFormat() {
		return mFileNameDateFormat;
	}

	/**
	 * @param pFileNameDateFormat
	 *            the FileNameDateFormat to set.
	 */
	public void setFileNameDateFormat(String pFileNameDateFormat) {
		this.mFileNameDateFormat = pFileNameDateFormat;
	}

	/**
	 * @return the mEnv.
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * @param pEnv
	 *            the Env to set.
	 */
	public void setEnv(String pEnv) {
		this.mEnv = pEnv;
	}

	/**
	 * @return the mSeoAppend.
	 */
	public String getSeoAppend() {
		return mSeoAppend;
	}

	/**
	 * @param pSeoAppend
	 *            the SeoAppend to set.
	 */
	public void setSeoAppend(String pSeoAppend) {
		this.mSeoAppend = pSeoAppend;
	}

	/**
	 * @return the mOutputDirectory.
	 */
	public String getOutputDirectory() {
		return mOutputDirectory;
	}

	/**
	 * @param pOutputDirectory
	 *            the OutputDirectory to set.
	 */
	public void setOutputDirectory(String pOutputDirectory) {
		this.mOutputDirectory = pOutputDirectory;
	}

	/**
	 * @return the mDatasource.
	 */
	public String getDatasource() {
		return mDatasource;
	}

	/**
	 * @param pDatasource
	 *            the Datasource to set.
	 */
	public void setDatasource(String pDatasource) {
		this.mDatasource = pDatasource;
	}

	/**
	 * @return the mStoredProcedure.
	 */
	public String getStoredProcedure() {
		return mStoredProcedure;
	}

	/**
	 * @param pStoredProcedure
	 *            the StoredProcedure to set.
	 */
	public void setStoredProcedure(String pStoredProcedure) {
		this.mStoredProcedure = pStoredProcedure;
	}
	
	/**
	 * @return the mExtension.
	 */
	public String getExtension() {
		return mExtension;
	}

	/**
	 * @param pExtension
	 *            the Extension to set.
	 */
	public void setExtension(String pExtension) {
		this.mExtension = pExtension;
	}

	/**
	 * This method used to generate reports for seoReports using store procedure.
	 */
	public void generateSeoReports() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: SeoReportManager.generateSeoReports()");
		}
		Connection con = null;
		try {

			Context ctx = new javax.naming.InitialContext();
			DataSource ds = (DataSource) ctx.lookup(getDatasource());
			con = ds.getConnection();
			CallableStatement callableStatement = con
					.prepareCall(getStoredProcedure());
			String fileName = getFileName();
			callableStatement.setString(TRUReportConstants.INTERGER_ONE,
					fileName);
			callableStatement.setString(TRUReportConstants.INTERGER_TWO,
					getOutputDirectory());
			callableStatement.execute();

			mReportEmailSender.sendReportSuccessEmail(mErrorCodes.get(TRUReportConstants.DEFAULT) , getReportType());
			
		} catch (SQLException exs) {
			mReportEmailSender.sendReportFailureEmail(mErrorCodes.get(TRUReportConstants.SQL_EXCEPTION), getReportType());
			if (isLoggingError()) {
				logError("SQLException while executing stored procedure: {0}",
						exs);
			}
		} catch (NamingException en) {
			// TODO Auto-generated catch block
			mReportEmailSender.sendReportFailureEmail(mErrorCodes.get(TRUReportConstants.NAMING_EXCEPTION), getReportType());
			if (isLoggingError()) {
				logError("NamingException while executing stored procedure: {0}",
						en);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: SeoReportManager.generateSeoReports()");
		}

	}
   /**
    * This method is used to generate feed file name.
    * 
    * @return fileName.
    * 
    *  @throws NamingException if there is an issue
    */
	public String getFileName() throws NamingException{
		String fileName = null;
		if (null == getEnv() || null == getSeoAppend()
				|| null == getFileNameEnd() || null == getFileNameDateFormat()
				|| null == getExtension()) {
			throw new NamingException();
		}
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(getFileNameDateFormat());
		String fileNameDate = sdf.format(cal.getTime());
		fileName = getEnv() + getSeoAppend() + TRUReportConstants.UNDERSCORE+
				 fileNameDate + getFileNameEnd() + getExtension();
		
		return fileName;

	}

}
