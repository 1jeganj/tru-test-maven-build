package com.tru.reportmanager;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.sql.DataSource;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feed.FeedFileSequenceProperties;
import com.tru.reportemail.ReportEmailSender;
import com.tru.reporttools.TRUReportConstants;
/**
 * @author PA
 * @version 1.0
 * The Class is used to generate Product Fulfillment Report configure feed file name and invoking stored procedure to generate feed file.  
 * 
 */
public class ProductFulfillmentReportManager extends GenericService {
	
	/** Property to hold ReportFileDir */
	private String mEnvAppend;

	/**  Property to hold mTrusAppend. */
	private String mTrusAppend;
	
	/** The Sequence append. */
	private String mSeqAppend;
	
	/** The Usa append. */
	private String mUsaAppend;
	
	/** Property to hold SimpleDateFormat */
	private String mSimpleDateFormat;
	
	/** Property to hold FileNameDateFormat */
	private String mFileNameDateFormat;

	
	/** Property to hold ProductFulfillmentAppend */
	private String mProductFulfillmentAppend;
	
	/** Property to hold mDatasource. */
	private DataSource mDatasource;
	
	/** Property to hold mOutputDirectory. */
	private String mOutputDirectory;
	
	/** The Inv stored procedure. */
	private String mInvStoredProcedure;
	
	/** Property to hold mReportEmailSender.*/
	private ReportEmailSender mReportEmailSender;

	/** Property to hold mErrorCodes.*/
	private Map<String,String> mErrorCodes;
	
	/** Property to hold mReportType.*/
	private String mReportType;

	/** Property to hold TRUFeedFileSequenceRepository*/
	private Repository mFeedFileSequenceRepository;
	
	/** The Feed repository properties. */
	private FeedFileSequenceProperties mFeedRepositoryProperties;
	
	/**
	 * Gets the feed repository properties.
	 *
	 * @return the feed repository properties
	 */
	public FeedFileSequenceProperties getFeedRepositoryProperties() {
		return mFeedRepositoryProperties;
	}

	/**
	 * Sets the feed repository properties.
	 *
	 * @param pFeedRepositoryProperties the new feed repository properties
	 */
	public void setFeedRepositoryProperties(FeedFileSequenceProperties pFeedRepositoryProperties) {
		mFeedRepositoryProperties = pFeedRepositoryProperties;
	}

	/**
	 * Gets the feed file sequence repository.
	 *
	 * @return the feed file sequence repository
	 */
	public Repository getFeedFileSequenceRepository() {
		return mFeedFileSequenceRepository;
	}

	/**
	 * Sets the feed file sequence repository.
	 *
	 * @param pFeedFileSequenceRepository the new feed file sequence repository
	 */
	public void setFeedFileSequenceRepository(Repository pFeedFileSequenceRepository) {
		mFeedFileSequenceRepository = pFeedFileSequenceRepository;
	}
	
	/**
	 * Gets the report type.
	 *
	 * @return the report type
	 */
	public String getReportType() {
		return mReportType;
	}

	/**
	 * Sets the report type.
	 *
	 * @param pReportType the new report type
	 */
	public void setReportType(String pReportType) {
		mReportType = pReportType;
	}

	/**
	 * @return the errorCodes
	 */
	public Map<String, String> getErrorCodes() {
		return mErrorCodes;
	}

	/**
	 * @param pErrorCodes the errorCodes to set
	 */
	public void setErrorCodes(Map<String, String> pErrorCodes) {
		mErrorCodes = pErrorCodes;
	}
	
	/**
	 * Gets the report email sender.
	 *
	 * @return the report email sender
	 */
	public ReportEmailSender getReportEmailSender() {
		return mReportEmailSender;
	}

	/**
	 * Sets the report email sender.
	 *
	 * @param pReportEmailSender the new report email sender
	 */
	public void setReportEmailSender(ReportEmailSender pReportEmailSender) {
		mReportEmailSender = pReportEmailSender;
	}

	/**
	 * Gets the datasource.
	 *
	 * @return the datasource
	 */
	public DataSource getDatasource() {
		return mDatasource;
	}

	/**
	 * Sets the datasource.
	 *
	 * @param pDatasource the new datasource
	 */
	public void setDatasource(DataSource pDatasource) {
		mDatasource = pDatasource;
	}

	/**
	 * Gets the inv stored procedure.
	 *
	 * @return the inv stored procedure
	 */
	public String getInvStoredProcedure() {
		return mInvStoredProcedure;
	}

	/**
	 * Sets the inv stored procedure.
	 *
	 * @param pInvStoredProcedure the new inv stored procedure
	 */
	public void setInvStoredProcedure(String pInvStoredProcedure) {
		mInvStoredProcedure = pInvStoredProcedure;
	}

	/**
	 * @return the mOutputDirectory.
	 */
	public String getOutputDirectory() {
		return mOutputDirectory;
	}

	/**
	 * @param pOutputDirectory
	 *            the OutputDirectory to set.
	 */
	public void setOutputDirectory(String pOutputDirectory) {
		this.mOutputDirectory = pOutputDirectory;
	}
	
	/**
	 * Gets the simple date format.
	 *
	 * @return the simple date format
	 */
	public String getSimpleDateFormat() {
		return mSimpleDateFormat;
	}

	/**
	 * Sets the simple date format.
	 *
	 * @param pSimpleDateFormat the simple date format
	 */
	public void setSimpleDateFormat(String pSimpleDateFormat) {
		mSimpleDateFormat = pSimpleDateFormat;
	}

	/**
	 * Gets the env append.
	 *
	 * @return the env append
	 */
	public String getEnvAppend() {
		return mEnvAppend;
	}

	/**
	 * Sets the env append.
	 *
	 * @param pEnvAppend the env append
	 */
	public void setEnvAppend(String pEnvAppend) {
		mEnvAppend = pEnvAppend;
	}

	/**
	 * Gets the trus append.
	 *
	 * @return the trus append
	 */
	public String getTrusAppend() {
		return mTrusAppend;
	}

	/**
	 * Sets the trus append.
	 *
	 * @param pTrusAppend the trus append
	 */
	public void setTrusAppend(String pTrusAppend) {
		mTrusAppend = pTrusAppend;
	}

	/**
	 * Gets the seq append.
	 *
	 * @return the seq append
	 */
	public String getSeqAppend() {
		return mSeqAppend;
	}

	/**
	 * Sets the seq append.
	 *
	 * @param pSeqAppend the seq append
	 */
	public void setSeqAppend(String pSeqAppend) {
		mSeqAppend = pSeqAppend;
	}

	/**
	 * Gets the usa append.
	 *
	 * @return the usa append
	 */
	public String getUsaAppend() {
		return mUsaAppend;
	}

	/**
	 * Sets the usa append.
	 *
	 * @param pUsaAppend the usa append
	 */
	public void setUsaAppend(String pUsaAppend) {
		mUsaAppend = pUsaAppend;
	}
	
	/**
	 * Gets the product fulfillment append.
	 *
	 * @return the product fulfillment append
	 */
	public String getProductFulfillmentAppend() {
		return mProductFulfillmentAppend;
	}

	/**
	 * Sets the product fulfillment append.
	 *
	 * @param pProductFulfillmentAppend the product fulfillment append
	 */
	public void setProductFulfillmentAppend(String pProductFulfillmentAppend) {
		mProductFulfillmentAppend = pProductFulfillmentAppend;
	}

	/**
	 * Gets the file name date format.
	 *
	 * @return the file name date format
	 */
	public String getFileNameDateFormat() {
		return mFileNameDateFormat;
	}

	/**
	 * Sets the file name date format.
	 *
	 * @param pFileNameDateFormat the file name date format
	 */
	public void setFileNameDateFormat(String pFileNameDateFormat) {
		mFileNameDateFormat = pFileNameDateFormat;
	}

	/**
	 * Generate inventory fulfillment reports.
	 */
	public void generateInventoryFulfillmentReports() {
		
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProductFulfillmentReportManager.generateInventoryFulfillmentReports()");
		}
		String seqNumber = getFileSequenceNumber();
		String fileName  = getFileNameForFulfillmentReport(seqNumber);
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(TRUReportConstants.INV_REPO_GENERAT,fileName); 
		}
		
		vlogDebug("Product Fulfillment Report file name :{0}",fileName);
		
		executeStoredProcedure(fileName,getInvStoredProcedure(),seqNumber);
				
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(TRUReportConstants.INV_REPO_GENERAT,fileName);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: ProductFulfillmentReportManager.generateInventoryFulfillmentReports()");
		}
	}
	
	/**
	 * Execute stored procedure.
	 *
	 * @param pFileName the file name
	 * @param pStoredProcedure the stored procedure
	 * @param pSEQ - Sequence number
	 * @return true, if successful
	 */
	private boolean executeStoredProcedure(String pFileName,String pStoredProcedure,String pSEQ){
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProductFulfillmentReportManager.executeStoredProcedure()");
			logDebug("FileName-->" + pFileName);
			logDebug("Sequence Number-->" + pSEQ);
		}
		SimpleDateFormat sdf = new SimpleDateFormat(getSimpleDateFormat());
		String date = sdf.format(new Date());
		Connection con = null;
		try {
			con = getDatasource().getConnection();
			CallableStatement callableStatement = con
					.prepareCall(pStoredProcedure);

			callableStatement.setString(TRUReportConstants.INTERGER_ONE,pFileName);

			callableStatement.setString(TRUReportConstants.INTERGER_TWO,getOutputDirectory());

			callableStatement.setString(TRUReportConstants.INTERGER_THREE, date);

			callableStatement.setString(TRUReportConstants.INTERGER_FOUR,pSEQ);

			callableStatement.execute();
			
			mReportEmailSender.sendReportSuccessEmail(mErrorCodes.get(TRUReportConstants.DEFAULT) , getReportType());
			
			if (isLoggingDebug()) {
				logDebug("Exit from :: ProductFulfillmentReportManager.executeStoredProcedure()");
			}
			return true;
			
		} catch (SQLException exs) {
			if (isLoggingError()) {
				logError("SQLException while executing stored procedure: {0}",exs);
			}
			mReportEmailSender.sendReportFailureEmail(mErrorCodes.get(TRUReportConstants.SQL_EXCEPTION), getReportType());
			return false;
		}
		
	}
	
	
	/**
	 * Gets the file name for fulfillment report.
	 *
	 * @param pSeqNumber the seq number
	 * @return the file name for fulfillment report
	 */
	private String getFileNameForFulfillmentReport(String pSeqNumber) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProductFulfillmentReportManager.getFileNameForFulfillmentReport()");
		}
		StringBuilder sb = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(getFileNameDateFormat());
		String fileNameDate = sdf.format(cal.getTime());
		sb.append(getEnvAppend());
		sb.append(getProductFulfillmentAppend());
		sb.append(TRUReportConstants.UNDERSCORE);
		sb.append(fileNameDate);
		sb.append(getUsaAppend());
		sb.append(TRUReportConstants.UNDERSCORE);
		sb.append(getTrusAppend());
		sb.append(TRUReportConstants.UNDERSCORE);
		sb.append(pSeqNumber);
		if (isLoggingDebug()) {
			logDebug("Exit from :: ProductFulfillmentReportManager.getFileNameForFulfillmentReport()");
		}
		return sb.toString();
		
	}
	
	
	/**
	 * Gets the file sequence number.
	 *
	 * @return the file sequence number
	 */
	private String getFileSequenceNumber() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProductFulfillmentReportManager.getFileSequenceNumber()");
		}
		int sequenceNumber = TRUReportConstants.INTERGER_ONE;
		try {
			
			RepositoryItem item = getFeedFileSequenceRepository().getItem(getFeedRepositoryProperties().getFeedfileProdFulfilmentSequenceReportId(),getFeedRepositoryProperties().getFeedfileSequenceItemDescriptor());
			MutableRepository mutRep = (MutableRepository)getFeedFileSequenceRepository();
			if(item == null){
				MutableRepositoryItem mutItem = mutRep.createItem(getFeedRepositoryProperties().getFeedfileProdFulfilmentSequenceReportId(), getFeedRepositoryProperties().getFeedfileSequenceItemDescriptor());
				mutItem.setPropertyValue(getFeedRepositoryProperties().getSeqProcessedPropertyName(), sequenceNumber);
				mutRep.addItem(mutItem);
			}else{
			Integer seqVal = (Integer)item.getPropertyValue(getFeedRepositoryProperties().getSeqProcessedPropertyName());
			sequenceNumber =seqVal + TRUReportConstants.INTERGER_ONE;
			MutableRepositoryItem mutItem = (MutableRepositoryItem)item;
			mutItem.setPropertyValue(getFeedRepositoryProperties().getSeqProcessedPropertyName(), sequenceNumber);
			mutRep.updateItem(mutItem);
			}
			
		} catch (RepositoryException e1) {
			if (isLoggingError()) {
				logError("RepositoryException while processing feed file sequence",e1);
			}
		}

		String seqStr = Integer.toString(sequenceNumber);
		StringBuilder sb = new StringBuilder();
		for (int i = (TRUReportConstants.HEARDER_PADDING - seqStr.length()); i > TRUReportConstants.INTERGER_ZERO; i--) {
			sb.append(TRUReportConstants.INTERGER_ZERO);
		}
		sb.append(seqStr);
		if (isLoggingDebug()) {
			logDebug("Exit from :: ProductFulfillmentReportManager.getFileSequenceNumber()");
		}
		return sb.toString();
	}
}
