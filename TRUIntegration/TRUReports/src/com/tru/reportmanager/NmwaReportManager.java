package com.tru.reportmanager;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import atg.nucleus.GenericService;

import com.tru.reportemail.ReportEmailSender;
import com.tru.reporttools.TRUReportConstants;
/**
 * @author PA
 * @version 1.0
 * The Class is used to configure feed file name and invoking stored procedure to generate feed file.  
 * 
 */
public class NmwaReportManager extends GenericService {
	/**
	 * property to hold mOutputDirectory.
	 */
	private String mOutputDirectory;
	/**
	 * property to hold mStartMonth.
	 */
	private String mStartMonth;
	/**
	 * property to hold mEndMonth.
	 */
	private String mEndMonth;
	/**
	 * property to hold mDatasource.
	 */
	private String mDatasource;
	/**
	 * property to hold mStoredProcedure.
	 */
	private String mStoredProcedure;
	/**
	 * property to hold mFileNameDateFormat.
	 */
	private String mFileNameDateFormat;
	/**
	 * property to hold mEnv.
	 */
	private String mEnv;
	/**
	 * property to hold mSeoAppend.
	 */
	private String mNmwaAppend;
	/**
	 * property to hold mFileNameEnd.
	 */
	private String mFileNameEnd;
	/**
	 * property to hold mReportType.
	 */
	private String mReportType;
	/**
	 * property to hold mReportEmailSender.
	 */
	private ReportEmailSender mReportEmailSender;
	/**
	 * property to hold mErrorCodes.
	 */
	private Map<String,String> mErrorCodes;

	/**
	 * @return the errorCodes
	 */
	public Map<String, String> getErrorCodes() {
		return mErrorCodes;
	}

	/**
	 * @param pErrorCodes the errorCodes to set
	 */
	public void setErrorCodes(Map<String, String> pErrorCodes) {
		mErrorCodes = pErrorCodes;
	}

	/**
	 * @return the mOutputDirectory.
	 */
	public String getOutputDirectory() {
		return mOutputDirectory;
	}

	/**
	 * @param pOutputDirectory
	 *            the OutputDirectory to set.
	 */
	public void setOutputDirectory(String pOutputDirectory) {
		this.mOutputDirectory = pOutputDirectory;
	}
	/**
	 * @return the mStartMonth.
	 */
	public String getStartMonth() {
		return mStartMonth;
	}

	/**
	 * @param pStartMonth
	 *            the StartMonth to set.
	 */
	public void setStartMonth(String pStartMonth) {
		this.mStartMonth = pStartMonth;
	}

	/**
	 * @return the mEndMonth.
	 */
	public String getEndMonth() {
		return mEndMonth;
	}

	/**
	 * @param pEndMonth
	 *            the EndMonth to set.
	 */
	public void setEndMonth(String pEndMonth) {
		this.mEndMonth = pEndMonth;
	}

	/**
	 * @return the mDatasource.
	 */
	public String getDatasource() {
		return mDatasource;
	}

	/**
	 * @param pDatasource
	 *            the Datasource to set.
	 */
	public void setDatasource(String pDatasource) {
		this.mDatasource = pDatasource;
	}

	/**
	 * @return the mStoredProcedure.
	 */
	public String getStoredProcedure() {
		return mStoredProcedure;
	}

	/**
	 * @param pStoredProcedure
	 *            the StoredProcedure to set.
	 */
	public void setStoredProcedure(String pStoredProcedure) {
		this.mStoredProcedure = pStoredProcedure;
	}
	/**
	 * @return the mFileNameDateFormat.
	 */
	public String getFileNameDateFormat() {
		return mFileNameDateFormat;
	}

	/**
	 * @param pFileNameDateFormat
	 *            the FileNameDateFormat to set.
	 */
	public void setFileNameDateFormat(String pFileNameDateFormat) {
		this.mFileNameDateFormat = pFileNameDateFormat;
	}
	/**
	 * @return the mEnv.
	 */
	public String getEnv() {
		return mEnv;
	}
	/**
	 * @param pEnv
	 *            the Env to set.
	 */
	public void setEnv(String pEnv) {
		this.mEnv = pEnv;
	}
	
	/**
	 * @return the mFileNameEnd.
	 */
	public String getFileNameEnd() {
		return mFileNameEnd;
	}

	/**
	 * @param pFileNameEnd - the FileNameEnd to set.
	 *            
	 */
	public void setFileNameEnd(String pFileNameEnd) {
		this.mFileNameEnd = pFileNameEnd;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return mReportType;
	}

	/**
	 * @param pReportType the reportType to set
	 */
	public void setReportType(String pReportType) {
		mReportType = pReportType;
	}
	
	/**
	 * This method used to generate reports for nmwa using store procedure.
	 */
	public void generateNMWAReports() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAReportsSheduler.generateNMWAReports()");

		}
		Connection con = null;

		try {
			Context ctx = new javax.naming.InitialContext();
			DataSource ds = (DataSource) ctx.lookup(getDatasource());

			con = ds.getConnection();
			CallableStatement callableStatement = con
					.prepareCall(getStoredProcedure());

			callableStatement.setString(TRUReportConstants.INTERGER_ONE,
					getFileName());

			callableStatement.setString(TRUReportConstants.INTERGER_TWO,
					getOutputDirectory());

			callableStatement.setString(
					TRUReportConstants.INTERGER_THREE, getStartMonth());

			callableStatement.setString(
					TRUReportConstants.INTERGER_FOUR, getEndMonth());

			callableStatement.execute();
			
			mReportEmailSender.sendReportSuccessEmail( mErrorCodes.get(TRUReportConstants.DEFAULT), getReportType());
			
		} catch (SQLException exs) {
			 mReportEmailSender.sendReportFailureEmail(mErrorCodes.get(TRUReportConstants.SQL_EXCEPTION) , getReportType());
			if (isLoggingError()) {
				logError("SQLException while executing stored procedure: {0}",
						exs);
			}
		} catch (NamingException en) {
			// TODO Auto-generated catch block
			mReportEmailSender.sendReportFailureEmail(mErrorCodes.get(TRUReportConstants.NAMING_EXCEPTION) , getReportType());
			if (isLoggingError()) {
				logError("NamingException while executing stored procedure: {0}",
						en);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: NmwaReportManager.generateNMWAReports()");
		}
	}

	   /**
	    * This method is used to generate feed file name.
	    * 
	    * @return fileName.
	    * 
	    *  @throws NamingException if there is an issue
	    */
		public String getFileName() throws NamingException{
			String fileName = null;
		if (null == getEnv() || null == getNmwaAppend()
				|| null == getFileNameEnd() || null == getFileNameDateFormat()) {
			throw new NamingException();
		}
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(getFileNameDateFormat());
			String fileNameDate = sdf.format(cal.getTime());
			fileName = getEnv() + getNmwaAppend() + TRUReportConstants.UNDERSCORE
					+ fileNameDate + getFileNameEnd();
			return fileName;

		}

	/**
	 * @return the mNmwaAppend.
	 */
	public String getNmwaAppend() {
		return mNmwaAppend;
	}

	/**
	 * @param pNmwaAppend the NmwaAppend to set.
	 */
	public void setNmwaAppend(String pNmwaAppend) {
		this.mNmwaAppend = pNmwaAppend;
	}
	
	/**
	 * @return the mReportEmailSender.
	 */
	public ReportEmailSender getReportEmailSender() {
		return mReportEmailSender;
	}

	/**
	 * @param pReportEmailSender
	 *            the ReportEmailSender to set.
	 */
	public void setReportEmailSender(ReportEmailSender pReportEmailSender) {
		this.mReportEmailSender = pReportEmailSender;
	}
}
