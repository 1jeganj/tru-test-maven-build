package com.tru.reportscheduler;



import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.reportmanager.InventoryVisibilityReportManager;
/**
 * @version 1.0
 * @author PA
 * This Class is used to trigger scheduler to generate NMWAReport.
 * 
 */
public class InvVisibilityReportSheduler extends SingletonSchedulableService{
	
	/** Property to hold mEnable .*/
	
	private boolean mEnable;
	
	/**
	 * Property to hold mReportManager.
	 */
	private  InventoryVisibilityReportManager mReportManager;
		/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler - Scheduler.
	 * @param pScheduledJob - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: InvVisibilityReportSheduler.doScheduledTask()");
		}
        if(isEnable()){
        	doInvReportJob();	
        }
		if (isLoggingDebug()) {
			logDebug("Exit From :: InVisibilityReportSheduler.doScheduledTask()");
		}
	}
	/**
	 * This method invokes generateNMWAReport() method.
	 */
	public void doInvReportJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: InvVisibilityReportSheduler.doInvReportJob()");
		}
		getReportManager().generateInventoryVisibilityReports();
		
		if (isLoggingDebug()) {
			logDebug("Exit from :: InvVisibilityReportSheduler.doInvReportJob()");
		}
	}

	/**
	 * Gets the report manager.
	 *
	 * @return the report manager
	 */
	public InventoryVisibilityReportManager getReportManager() {
		return mReportManager;
	}

	/**
	 * Sets the report manager.
	 *
	 * @param pReportManager the new report manager
	 */
	public void setReportManager(InventoryVisibilityReportManager pReportManager) {
		mReportManager = pReportManager;
	}
   
	
}
