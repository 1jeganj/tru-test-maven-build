package com.tru.reportscheduler;


import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.reportmanager.ProductFulfillmentReportManager;


/**
 * @author PA
 * @version 1.0
 * The Class is used to generate inventory report by invoking stored procedure to generate the file.  
 * 
 */
public class ProdFulfillmentReportSheduler extends SingletonSchedulableService{
	
	/** Property to hold mEnable .*/
	
	private boolean mEnable;
	
	/**
	 * Property to hold mReportManager.
	 */
	private  ProductFulfillmentReportManager mReportManager;
		/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler - Scheduler.
	 * @param pScheduledJob - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProdFulfillmentReportSheduler.doScheduledTask()");
		}
        if(isEnable()){
        	doInvReportJob();	
        }
		if (isLoggingDebug()) {
			logDebug("Exit From :: ProdFulfillmentReportSheduler.doScheduledTask()");
		}
	}
	/**
	 * This method invokes generateNMWAReport() method.
	 */
	public void doInvReportJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: ProdFulfillmentReportSheduler.doInvReportJob()");
		}
		getReportManager().generateInventoryFulfillmentReports();
		
		if (isLoggingDebug()) {
			logDebug("Exit from :: ProdFulfillmentReportSheduler.doInvReportJob()");
		}
	}

	/**
	 * Gets the report manager.
	 *
	 * @return the report manager
	 */
	public ProductFulfillmentReportManager getReportManager() {
		return mReportManager;
	}

	/**
	 * Sets the report manager.
	 *
	 * @param pReportManager the new report manager
	 */
	public void setReportManager(ProductFulfillmentReportManager pReportManager) {
		mReportManager = pReportManager;
	}

}
