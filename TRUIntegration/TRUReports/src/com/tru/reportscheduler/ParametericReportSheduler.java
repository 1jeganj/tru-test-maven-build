package com.tru.reportscheduler;


import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.reportmanager.ParametricProfileReportManager;

/**
 * @version 1.0
 * @author PA
 * This Class is used to trigger scheduler to generate ParametericReport.
 */
public class ParametericReportSheduler extends SingletonSchedulableService {

	/** Property to hold mEnable. */
	private boolean mEnable;

	/**
	 * Property to hold mParametricProfileReportManager.
	 */
	private ParametricProfileReportManager mParametricProfileReportManager;
	

	/**
	 * 
	 * @return mParametricProfileReportManager.
	 */
	public ParametricProfileReportManager getParametricProfileReportManager() {
		return mParametricProfileReportManager;
	}

	/**
	 * @param pParametricProfileReportManager -  the ParametricProfileReportManager to set.
	 */
	public void setParametricProfileReportManager(ParametricProfileReportManager pParametricProfileReportManager) {
		this.mParametricProfileReportManager = pParametricProfileReportManager;
	}

	/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable -  the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler.
	 * @param pScheduledJob
	 *            - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametericReportsSheduler.doScheduledTask()");
		}
		if (isEnable()) {
			doParametricProfileReportJob();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: ParametericReportsSheduler.doScheduledTask()");
		}
	}

	/**
	 * This method invokes ParametricProfileReport Report.
	 */
	public void doParametricProfileReportJob() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametericReportsSheduler.doParametricProfileReportJob()");
		}

		getParametricProfileReportManager().generateReport();

		if (isLoggingDebug()) {
			logDebug("Exit from :: ParametericReportsSheduler.doParametricProfileReportJob()");
		}
	}


}
