package com.tru.reportscheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.reportmanager.SeoReportManager;

/**
 * @author PA
 * @version 1.0
 * The Class SEOReportsSheduler.
 * This Class is used to trigger scheduler to generate SEOReports.
 */
public class SEOReportsSheduler extends SingletonSchedulableService {

	/** Property to hold mEnable. */

	private boolean mEnable;
	/*
	 * Property to hold mSeoReportManager.
	 */
	private SeoReportManager mSeoReportManager;

	/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable - the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the mSeoReportManager.
	 */
	public SeoReportManager getSeoReportManager() {
		return mSeoReportManager;
	}

	/**
	 * @param pSeoReportManager - the SeoReportManager to set.
	 */
	public void setSeoReportManager(SeoReportManager pSeoReportManager) {
		this.mSeoReportManager = pSeoReportManager;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler - Scheduler.
	 * @param pScheduledJob  - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: SEOReportsSheduler.doScheduledTask()");
		}
		if (isEnable()) {
			doSeoReportJob();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: SEOReportsSheduler.doScheduledTask()");
		}
	}

	/***
	 * This method invokes generateSeoReports().
	 */
	public void doSeoReportJob() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: SEOReportsSheduler.doSeoReportJob()");
		}
		getSeoReportManager().generateSeoReports();

		if (isLoggingDebug()) {
			logDebug("Exit from :: SEOReportsSheduler.doSeoReportJob()");
		}
	}
}
