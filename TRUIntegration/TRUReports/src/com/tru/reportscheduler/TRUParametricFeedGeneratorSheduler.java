package com.tru.reportscheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.email.conf.TRUParametericFeedEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.index.TRUParametricFeedGenerator;

/**
 * @author PA
 * @version 1.0
 * The Class TRUParametricFeedGeneratorSheduler.
 * This Class is used to trigger scheduler to generate ParametricFeedGenerator.
 */
public class TRUParametricFeedGeneratorSheduler  extends SingletonSchedulableService {

	/** 
	 * Property to hold mEnable. 
	 */
	private boolean mEnable;

	/**
	 * property to hold mParametricFeedGenerator.
	 */
	private TRUParametricFeedGenerator mParametricFeedGenerator;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	
	/** The m Parametricfeed email configuration. */
	private TRUParametericFeedEmailConfiguration mParametericFeedEmailConfiguration;
	
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	

	/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable -  the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the ParametricFeedGenerator.
	 */
	public TRUParametricFeedGenerator getParametricFeedGenerator() {
		return mParametricFeedGenerator;
	}

	/**
	 * 
	 * @param pParametricFeedGenerator the enable to set.
	 */
	public void setParametricFeedGenerator(TRUParametricFeedGenerator pParametricFeedGenerator) {
		this.mParametricFeedGenerator = pParametricFeedGenerator;
	}
	
	/**
	 * @return the ParametericFeedEmailConfiguration
	 */
	public TRUParametericFeedEmailConfiguration getParametericFeedEmailConfiguration() {
		return mParametericFeedEmailConfiguration;
	}

	/**
	 * @param pParametericFeedEmailConfiguration the ParametericFeedEmailConfiguration to set
	 */
	public void setParametericFeedEmailConfiguration(
			TRUParametericFeedEmailConfiguration pParametericFeedEmailConfiguration) {
		mParametericFeedEmailConfiguration = pParametericFeedEmailConfiguration;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler.
	 * @param pScheduledJob
	 *            - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametericReportsSheduler.doScheduledTask()");
		}
		if (isEnable()) {
			doParametricProfileReportJob();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: ParametericReportsSheduler.doScheduledTask()");
		}
	}

	/**
	 * This method invokes ParametricProfileReport Report.
	 */
	public void doParametricProfileReportJob() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametericReportsSheduler.doParametricProfileReportJob()");
		}

		if(getParametricFeedGenerator() != null) {
			getParametricFeedGenerator().processFacets();
			getTRUEmailServiceUtils().sendSiteMapSuccessEmail(TRUEndecaConstants.PARAMETRIC_FEED,setParametericFeedSucessEmailTemplate());
		}

		if (isLoggingDebug()) {
			logDebug("Exit from :: ParametericReportsSheduler.doParametricProfileReportJob()");
		}
	}
	
	/**
	 * Sets theparametric feed success email template.
	 *
	 * @return the map
	 */
	private Map<String, Object> setParametericFeedSucessEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricFeedGeneratorSheduler.setParametericFeedSucessEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		StringBuilder sb=new StringBuilder(getParametericFeedEmailConfiguration().getSuccessParametericFeedEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUCC, getParametericFeedEmailConfiguration().getSuccParametericFeedMessage());
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricFeedGeneratorSheduler.setParametericFeedSucessEmailTemplate() method.. ");
		}
		return templateParameters;
	}

}
