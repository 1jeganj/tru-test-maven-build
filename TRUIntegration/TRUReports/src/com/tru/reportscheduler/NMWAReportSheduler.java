package com.tru.reportscheduler;



import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.reportmanager.NmwaReportManager;
/**
 * @version 1.0
 * @author PA
 * This Class is used to trigger scheduler to generate NMWAReport.
 * 
 */
public class NMWAReportSheduler extends SingletonSchedulableService{
	
	/** Property to hold mEnable .*/
	
	private boolean mEnable;
	
	/**
	 * Property to hold mNmwReportManager.
	 */
	private  NmwaReportManager mNmwReportManager;
		/**
	 * @return the enable.
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set.
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time.</b><br>
	 * .
	 * 
	 * @param pScheduler - Scheduler.
	 * @param pScheduledJob - ScheduledJob.
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAReportsSheduler.doScheduledTask()");
		}
        if(isEnable()){
           doNMWAReportJob();	
        }
		if (isLoggingDebug()) {
			logDebug("Exit From :: NMWAReportsSheduler.doScheduledTask()");
		}
	}
	/**
	 * This method invokes generateNMWAReport() method.
	 */
	public void doNMWAReportJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAReportsSheduler.doNMWAEmailJob()");
		}
		getNmwReportManager().generateNMWAReports();
		
		if (isLoggingDebug()) {
			logDebug("Exit from :: NMWAReportsSheduler.doNMWAEmailJob()");
		}
	}
   /**
   * 
   * @return mNmwReportManager.
   */
	public NmwaReportManager getNmwReportManager() {
		return mNmwReportManager;
	}
    /**
     * 
     * @param pNmwReportManager the NmwReportManager to set.
     */
	public void setNmwReportManager(NmwaReportManager pNmwReportManager) {
		this.mNmwReportManager = pNmwReportManager;
	}
}
