package com.tru.reporttools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.email.conf.TRUParametericCSVEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.vo.ParametricProfileVO;


/**
 * The Class ParametricProfileWriter.
 *
 * @author PA.
 * The Class ParametricProfileWriter.
 * @version 1.0
 */
public class ParametricProfileWriter extends GenericService{
	/**
	 * property to hold the delimiter.
	 */
	private static final CsvPreference PIPE_DELIMITED = new CsvPreference.Builder('"', '|', "\n").build();
	/**
	 * FileOutputStream is meant for writing streams of raw bytes Property to
	 * hold mFileOutputStream, for success records.
	 */
	private FileOutputStream mFileOutputStream;
	/**
	 * mWriter for holding mFileOutputStream, for success records.
	 */
	private Writer mWriter;
	/**
	 * property to hold mBeanWriter, for success records.
	 */
	private ICsvBeanWriter mBeanWriter;
	/**
	 * property to hold mOutputDirectory.
	 */
	private String mOutputDirectory;
	/**
	 * property to hold mFileNameEnd.
	 */
	private String mFileNameEnd;
	/**
	 * property to hold mFileNameDateFormat.
	 */
	private String mFileNameDateFormat;
	/**
	 * property to hold mHeaderDateFormat.
	 */
	private String mHeaderDateFormat;
	
	/** The m file extension. */
	private String mFileExtension;

	/**
	 * property to hold mEnv.
	 */
	private String mEnv;
	/**
	 * property to hold mParametricProfileAppend.
	 */
	private String mParametricProfileAppend;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	/** The m ParametricCSV email configuration. */
	private TRUParametericCSVEmailConfiguration mParametericCSVEmailConfiguration;
	

	/***
	 * This method is used for writing parametric profile feed file.
	 * 
	 * @param pParametricProfileList the ParametricProfileList
	 * @return errorMailSent -- the boolean value
	 * 
	 */
	public boolean doParametricProfileWrite(List<ParametricProfileVO> pParametricProfileList) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: ParametericReportsSheduler.doParametricProfileWrite()");
		}
		boolean errorMailSent = false;
		String fileName = getFileName();
		if (StringUtils.isNotBlank(getOutputDirectory()) && (StringUtils.isNotBlank(fileName))) {
			String finalFile = getOutputDirectory()+fileName;
			try {
				File outFile = new File(finalFile);
				if (!(outFile.exists())) {
					outFile.createNewFile();
				}
				mFileOutputStream = new FileOutputStream(outFile);
				mWriter = new OutputStreamWriter(mFileOutputStream);
				mBeanWriter = new CsvBeanWriter(mWriter, ParametricProfileWriter.PIPE_DELIMITED);
				if (isLoggingDebug()) {
					logDebug("OutputStreamWriter is created Successfully");
				}
				//Write header

				final String[] header = new String[] { TRUReportConstants.BREADCRUMB_HEADER,TRUReportConstants.PAGE_NAME_DISPLAY_TXT_HEADER,
						TRUReportConstants.CATALOG_STATUS_CD_HEADER,TRUReportConstants.PARAMETRIC_PROFILE_NAV_TXT_HEADER,TRUReportConstants.CATEGORY_ID_HEADER };
				final CellProcessor[] processors = getProcessors();
				mBeanWriter.writeHeader(header);

				final String[] nameMapping = new String[] {TRUReportConstants.BREADCRUMB,TRUReportConstants.PAGE_NAME_DISPLAY_TXT,TRUReportConstants.CATALOG_STATUS_CD,
						TRUReportConstants.PARAMETRIC_PROFILE_NAV_TXT,TRUReportConstants.CATEGORY_ID};

				for(ParametricProfileVO pvo : pParametricProfileList) {
					mBeanWriter.write(pvo, nameMapping, processors);
				}

				if (mBeanWriter != null) {
					mBeanWriter.flush();
					mBeanWriter.close();
				}

			} catch (FileNotFoundException fileNotFoundExcep) {
				if (isLoggingError()) {
					vlogError(fileNotFoundExcep, "FileNotFound occured in ParametricProfileWriter.doParametricProfileWrite() Method");
				}
				
				errorMailSent = true;
				getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_CSV,
						setParametricCSVFailureEmailTemplate(getParametericCSVEmailConfiguration().getFailureExceptionMessage(),fileNotFoundExcep));
			} catch (IOException ioException) {
				if (isLoggingError()) {
					vlogError(ioException, "IOException occured in ParametricProfileWriter.doParametricProfileWrite() Method");
				}
				errorMailSent = true;
				getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_CSV,
						setParametricCSVFailureEmailTemplate(getParametericCSVEmailConfiguration().getFailureExceptionMessage(),ioException));
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit From :: ParametericReportsSheduler.doParametricProfileWrite()");
		}
		return !errorMailSent;
	}
	
	/**
	 * This method is used to generate feed file name .
	 * 
	 * @return fileName.
	 */
	String getFileName() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: ParametericReportsSheduler.getFileName() Method");
		}
		String fileName = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(getFileNameDateFormat());
		String fileNameDate = sdf.format(cal.getTime());
		String fileExtension = getFileExtension();
		if(StringUtils.isEmpty(fileExtension)){
			return fileName;
		}
		fileName = getEnv() + getParametricProfileAppend() + TRUReportConstants.UNDERSCORE+fileNameDate + TRUReportConstants.UNDERSCORE + getFileNameEnd() + fileExtension;
		if (isLoggingDebug()) {
			logDebug("END:: ParametericReportsSheduler.getFileName() Method");
		}
		return fileName;
	}

	/**
	 * This method is used for applying the constraint on the feilds to populate.
	 * 
	 * @return processors.
	 */
	private static CellProcessor[] getProcessors() {
		final CellProcessor[] processors = new CellProcessor[] { 
				new Optional(), // seoTitle
				new Optional(), // seoTitleOverride
				new Optional(), // seoDescription
				new Optional(), // seoDescriptionOverride
				new NotNull(), // keywords
		};
		return processors;
	}
	
	/**
	 * Sets the Parametric CSV failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setParametricCSVFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricCSVTools.setParametricCSVFailureEmailTemplate method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getParametericCSVEmailConfiguration().getFailureParametericCSVEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricCSVTools.setParametricCSVFailureEmailTemplate method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Gets the stack trace.
	 *
	 * @param pThrowable the throwable
	 * @return the stack trace
	 */
	private String getStackTrace(Throwable pThrowable) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricCSVTools.getStackTrace() method.. ");
		}
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    pThrowable.printStackTrace(printWriter); 
	    if(result.toString().length() > TRUEndecaConstants.INT_THOUSAND){
	    	if (isLoggingDebug()) {
				logDebug("END:: TRUParametricCSVTools.getStackTrace() method.. ");
			}
	    	return result.toString().substring(TRUEndecaConstants.INT_ZERO, TRUEndecaConstants.INT_THOUSAND);
	    }
	    if (isLoggingDebug()) {
			logDebug("END:: TRUParametricCSVTools.getStackTrace() method.. ");
		}
	    return result.toString();
	  }
	

	

	/**
	 * Gets the env.
	 *
	 * @return the mEnv.
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * Sets the env.
	 *
	 * @param pEnv the Env to set.
	 */
	public void setEnv(String pEnv) {
		this.mEnv = pEnv;
	}

	/**
	 * Gets the parametric profile append.
	 *
	 * @return the mParametricProfileAppend.
	 */
	public String getParametricProfileAppend() {
		return mParametricProfileAppend;
	}

	/**
	 * Sets the parametric profile append.
	 *
	 * @param pParametricProfileAppend the ParametricProfileAppend to set.
	 */
	public void setParametricProfileAppend(String pParametricProfileAppend) {
		this.mParametricProfileAppend = pParametricProfileAppend;
	}

	/**
	 * Gets the file name date format.
	 *
	 * @return the mFileNameDateFormat.
	 */
	public String getFileNameDateFormat() {
		return mFileNameDateFormat;
	}

	/**
	 * Sets the file name date format.
	 *
	 * @param pFileNameDateFormat the FileNameDateFormat to set.
	 */
	public void setFileNameDateFormat(String pFileNameDateFormat) {
		this.mFileNameDateFormat = pFileNameDateFormat;
	}

	/**
	 * Gets the file name end.
	 *
	 * @return the mFileNameEnd.
	 */
	public String getFileNameEnd() {
		return mFileNameEnd;
	}

	/**
	 * Sets the file name end.
	 *
	 * @param pFileNameEnd the mFileNameEnd to set.
	 */
	public void setFileNameEnd(String pFileNameEnd) {
		this.mFileNameEnd = pFileNameEnd;
	}

	/**
	 * Gets the output directory.
	 *
	 * @return the mOutputDirectory.
	 */
	public String getOutputDirectory() {
		return mOutputDirectory;
	}

	/**
	 * Sets the output directory.
	 *
	 * @param pOutputDirectory the OutputDirectory to set.
	 */
	public void setOutputDirectory(String pOutputDirectory) {
		this.mOutputDirectory = pOutputDirectory;
	}
	
	/**
	 * Sets the file Extension.
	 *
	 * @param pFileExtension the FileExtension to set.
	 */
	public void setFileExtension(String pFileExtension) {
		this.mFileExtension = pFileExtension;
	}

	/**
	 * Gets the file Extension.
	 *
	 * @return  mFileExtension.
	 */
	public String getFileExtension() {
		return mFileExtension;
	}
	
	/**
	 * Gets the header date format.
	 *
	 * @return  mHeaderDateFormat.
	 */
	public String getHeaderDateFormat() {
		return mHeaderDateFormat;
	}
	/**
	 * Sets the header date format.
	 *
	 * @param pHeaderDateFormat the HeaderDateFormat to set.
	 */
	public void setHeaderDateFormat(String pHeaderDateFormat) {
		this.mHeaderDateFormat = pHeaderDateFormat;
	}
	/**
	 * This method will pad zeros for the provided string.
	 * 
	 * @param pPaddingString
	 *            - The string to be left padded with zeros.
	 * @param pPadLength
	 *            - Return pPadLength after padding.
	 * @return string.
	 */
	public String leftPadding(String pPaddingString, int pPadLength) {
		StringBuilder sb = new StringBuilder();
		for (int toPrepend = pPadLength - pPaddingString.length(); toPrepend > 0; toPrepend--) {
			sb.append(TRUReportConstants.INTERGER_ZERO);
		}
		sb.append(pPaddingString);
		return sb.toString();
	}
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	
	/**
	 * @return the ParametericCSVEmailConfiguration
	 */
	public TRUParametericCSVEmailConfiguration getParametericCSVEmailConfiguration() {
		return mParametericCSVEmailConfiguration;
	}

	/**
	 * @param pParametericCSVEmailConfiguration the ParametericCSVEmailConfiguration to set
	 */
	public void setParametericCSVEmailConfiguration(
			TRUParametericCSVEmailConfiguration pParametericCSVEmailConfiguration) {
		mParametericCSVEmailConfiguration = pParametericCSVEmailConfiguration;
	}


}
