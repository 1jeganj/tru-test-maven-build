package com.tru.reporttools;
/**
 * This Class is used for the TRUReport Constants
 */
public class TRUReportConstants {
	/** The Constant INTERGER_ONE. */
	public static final int INTERGER_ONE = 1;
	/** The Constant INTERGER_TWO. */
	public static final int INTERGER_TWO = 2;
	/** The Constant INTERGER_THREE. */
	public static final int INTERGER_THREE = 3;
	/** The Constant INTERGER_FOUR. */
	public static final int INTERGER_FOUR = 4;

	/** The Constant for PRODUCT. */
	public static final String PRODUCT = "product";

	/** The Constant for CATEGORY. */
	public static final String CLASSIFICATION = "classification";

	/** The Constant for seoTitle. */
	public static final String SEO_TITLE = "seoTitle";

	/** The Constant for seoDescription. */
	public static final String SEO_DESCRIPTION = "seoDescription";

	/** The Constant for seoH1. */
	public static final String SEOH1 = "seoH1";

	/** The Constant for seoTagOverride. */
	public static final String SEO_TAG_OVERRIDE = "seoTagOverride";

	/** The Constant for category name. */
	public static final String DISPLAY_NAME = "displayName";

	/** The Constant for category description. */
	public static final String DESCRIPTION = "description";

	/** The Constant for HYPEN. */
	public static final String HYPEN = " - ";

	/** The Constant for NAME. */
	public static final String NAME = "name";

	/** The Constant for UNDERSCORE. */
	public static final String UNDERSCORE = "_";

	/** The Constant to hold File Type CSV. */
	public static final String FILE_TYPE = ".csv";

	/** The Constant to hold SEOTITLE. */
	public static final String SEOTITLE = "seoTitle";

	/** The Constant to hold SEOTITLE_OVERRIDE. */
	public static final String SEOTITLE_OVERRIDE = "seoTitleOverride";

	/** The Constant to hold SEODESCRIPTION. */
	public static final String SEODESCRIPTION = "seoDescription";

	/** The Constant to hold SEODESCRIPTION_OVERRIDE. */
	public static final String SEODESCRIPTION_OVERRIDE = "seoDescriptionOverride";

	/** The Constant to hold KEYWORDS. */
	public static final String KEYWORDS = "metaKeyWords";

	/** The Constant for BREADCRUMB. */
	public static final String BREADCRUMB = "breadCrumb";

	/** The Constant for PAGE_NAME_DISPLAY_TXT. */
	public static final String PAGE_NAME_DISPLAY_TXT = "pageNameDisplayTxt";

	/** The Constant for CATALOG_STATUS_CD. */
	public static final String CATALOG_STATUS_CD = "catalogStatusCD";

	/** The Constant for PARAMETRIC_PROFILE_NAV_TXT. */
	public static final String PARAMETRIC_PROFILE_NAV_TXT = "parametricProfileNavTxt";

	/** The Constant for CATEGORY_ID. */
	public static final String CATEGORY_ID = "catalogId";

	/** Constant to hold HEADER. */
	public static final String HEADER = "HEADER";

	/** Constant to hold header and sequence padding. */
	public static final int HEARDER_PADDING = 6;

	/** The constant to hold INTERGER_ZERO. */
	public static final int INTERGER_ZERO = 0;

	/** Constant to hold TRAILER. */
	public static final String TRAILER = "TRAILER";

	/** Constant to hold RECORDS_PADDING. */
	public static final int RECORDS_PADDING = 10;

	/** Constant to hold SALE_PRICE_SUM_PADDING. */
	public static final int SUM_PADDING = 15;

	/** Constant to hold ANSCESTORS. */
	public static final String ANSCESTORS = "ancestors";

	/** The Constant to hold MAINPRODUCT. */
	public static final String MAINPRODUCT = "MainProduct";

	/** The Constant to hold TRUPRODUCTDETAILPAGE. */
	public static final String TRUPRODUCTDETAILPAGE = "TRUProductDetailPage";

	/** The Constant to hold LABEL. */
	public static final String LABEL = "label";

	/** The Constant to hold NAVIGATION. */
	public static final String NAVIGATION = "navigation";

	/** The Constant to hold TYPE. */
	public static final String TYPE = "@type";

	/** The Constant to hold CONTENT_CONST. */
	public static final String CONTENT_CONST = "contents";

	/** The Constant to hold GREATER_THAN_CONST. */
	public static final String GREATER_THAN_CONST = ">";
	/** The Constant to hold CONST_A. */
	public static final String CONST_A = "A";
	/** The Constant to hold PARAMETRIC_RPT_NUM. */
	public static final String PARAMETRIC_RPT_NUM = "PARAMETRIC_RPT_NUM";
	/** The Constant to hold BREADCRUMB_HEADER. */
	public static final String BREADCRUMB_HEADER = "BREADCRUMB";
	/** The Constant to hold PAGE_NAME_DISPLAY_TXT_HEADER. */
	public static final String PAGE_NAME_DISPLAY_TXT_HEADER = "PAGE_NAME_DISPLAY_TXT";

	/** The Constant to hold CATALOG_STATUS_CD_HEADER. */
	public static final String CATALOG_STATUS_CD_HEADER = "CATALOG_STATUS_CD";

	/** The Constant to hold PARAMETRIC_PROFILE_NAV_TXT_HEADER. */
	public static final String PARAMETRIC_PROFILE_NAV_TXT_HEADER = "PARAMETRIC_PROFILE_NAV_TXT";

	/** The Constant to hold CATEGORY_ID_HEADER. */
	public static final String CATEGORY_ID_HEADER = "CATEGORY_ID";

	/** The Constant to hold MAINHEADER. */
	public static final String MAINHEADER = "MainHeader";

	/** The Constant to hold REFINEMENTCRUMBS. */
	public static final String REFINEMENTCRUMBS = "refinementCrumbs";

	/** The Constant to hold MAINCONTENT. */
	public static final String MAINCONTENT = "MainContent";

	/** The Constant to hold PDPBREADCRUMBLIST. */
	public static final String PDPBREADCRUMBLIST = "PDPBreadCrumbList";

	/** The Constant to hold CATEGORYNAME. */
	public static final String CATEGORYNAME = "categoryName";
	
    public static final String SQL_EXCEPTION = "SQLException";
	
	public static final String NAMING_EXCEPTION = "NamingException";
	
	public static final String DEFAULT = "default";
	
	/** constants for SINGLE_PIPE. */
	public static final String SINGLE_PIPE = "|";
	
	/** constants for SINGLE_PIPE. */
	public static final String SINGLE_DOT = ".";
	
	/** The Constant TRUE_FLAG. */
	public static final String TRUE_FLAG = "true";
	
	/** The Constant INV_REPO_GENERAT. */
	public static final String INV_REPO_GENERAT ="Inventory Report Generation";
	
	/** The Constant REPO_SEQ. */
	public static final String REPO_SEQ= "000001";
	/**
	 * Constant to hold IVIS_FILE_SEQUENCE_FILE_NAME.
	 */
	public static final String IVIS_FILE_SEQUENCE_FILE_NAME = "VisibiltySequenceFile";
	/**
	 * Constant to hold IVIS_FILE_SEQUENCE_FILE_NAME.
	 */
	public static final String FULF_FILE_SEQUENCE_FILE_NAME = "FulfillmentSequenceFile";
	/**
	 * Constant to hold TEXT FILE TYPE.
	 */
	public static final String TEXT_FILE_TYPE = ".txt";

}
