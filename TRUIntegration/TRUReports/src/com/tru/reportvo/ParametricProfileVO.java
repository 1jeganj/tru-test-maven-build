package com.tru.reportvo;
/**
 * @version 1.0
 * @author PA
 * This Class is used for the ParametricProfileVO.
 * 
 */
public class ParametricProfileVO {
	
	/**
	 * property to hold mCatalogId.
	 */
	private String mCatalogId;
	/**
	 * property to hold mPageNameDisplayTxt.
	 */
	private String mPageNameDisplayTxt;
	/**
	 * property to hold mPageNameDisplayTxt.
	 */
	private String mCatalogStatusCD;
	/**
	 * property to hold mParametricProfileNavTxt.
	 */
	private String mParametricProfileNavTxt;
	/**
	 * property to hold mBreadCrumb.
	 */
	private String mBreadCrumb;
	/**
	 * @return the mCatalogId.
	 */
	public String getCatalogId() {
		return mCatalogId;
	}

	/**
	 * @param pCatalogId the CatalogId to set.
	 */
	public void setCatalogId(String pCatalogId) {
		this.mCatalogId = pCatalogId;
	}

	/**
	 * @return the mPageNameDisplayTxt.
	 */
	public String getPageNameDisplayTxt() {
		return mPageNameDisplayTxt;
	}

	/**
	 * @param pPageNameDisplayTxt the PageNameDisplayTxt to set.
	 */
	public void setPageNameDisplayTxt(String pPageNameDisplayTxt) {
		this.mPageNameDisplayTxt = pPageNameDisplayTxt;
	}

	/**
	 * @return the mCatalogStatusCD.
	 */
	public String getCatalogStatusCD() {
		return mCatalogStatusCD;
	}

	/**
	 * @param pCatalogStatusCD the CatalogStatusCD to set.
	 */
	public void setCatalogStatusCD(String pCatalogStatusCD) {
		this.mCatalogStatusCD = pCatalogStatusCD;
	}

	/**
	 * @return the mParametricProfileNavTxt.
	 */
	public String getParametricProfileNavTxt() {
		return mParametricProfileNavTxt;
	}

	/**
	 * @param pParametricProfileNavTxt the ParametricProfileNavTxt to set.
	 */
	public void setParametricProfileNavTxt(String pParametricProfileNavTxt) {
		this.mParametricProfileNavTxt = pParametricProfileNavTxt;
	}

	/**
	 * @return the mBreadCrumb.
	 */
	public String getBreadCrumb() {
		return mBreadCrumb;
	}

	/**
	 * @param pBreadCrumb the BreadCrumb to set.
	 */
	public void setBreadCrumb(String pBreadCrumb) {
		this.mBreadCrumb = pBreadCrumb;
	}
	
	

}
