package com.tru.feed;

/**
 * The FeedFileSequenceProperties class.
 *
 * @author PA
 *
 */
public class FeedFileSequenceProperties {
	
	/** The Feedfile sequence item descriptor. */
	private String mFeedfileSequenceItemDescriptor;
	
	/** The Seq processed property name. */
	private String mSeqProcessedPropertyName;
	
	/** The Feedfile inv sequence report value. */
	private String mFeedfileInvSequenceReportId;
	
	/** The Feedfile prod fulfilment sequence report id. */
	private String mFeedfileProdFulfilmentSequenceReportId;

	/**
	 * Gets the feedfile sequence item descriptor.
	 *
	 * @return the feedfile sequence item descriptor
	 */
	public String getFeedfileSequenceItemDescriptor() {
		return mFeedfileSequenceItemDescriptor;
	}

	/**
	 * Sets the feedfile sequence item descriptor.
	 *
	 * @param pFeedfileSequenceItemDescriptor the new feedfile sequence item descriptor
	 */
	public void setFeedfileSequenceItemDescriptor(String pFeedfileSequenceItemDescriptor) {
		mFeedfileSequenceItemDescriptor = pFeedfileSequenceItemDescriptor;
	}

	/**
	 * Gets the seq processed property name.
	 *
	 * @return the seq processed property name
	 */
	public String getSeqProcessedPropertyName() {
		return mSeqProcessedPropertyName;
	}

	/**
	 * Sets the seq processed property name.
	 *
	 * @param pSeqProcessedPropertyName the new seq processed property name
	 */
	public void setSeqProcessedPropertyName(String pSeqProcessedPropertyName) {
		mSeqProcessedPropertyName = pSeqProcessedPropertyName;
	}

	/**
	 * Gets the feedfile inv sequence report id.
	 *
	 * @return the feedfile inv sequence report id
	 */
	public String getFeedfileInvSequenceReportId() {
		return mFeedfileInvSequenceReportId;
	}

	/**
	 * Sets the feedfile inv sequence report id.
	 *
	 * @param pFeedfileInvSequenceReportId the new feedfile inv sequence report id
	 */
	public void setFeedfileInvSequenceReportId(String pFeedfileInvSequenceReportId) {
		mFeedfileInvSequenceReportId = pFeedfileInvSequenceReportId;
	}

	/**
	 * Gets the feedfile prod fulfilment sequence report id.
	 *
	 * @return the feedfile prod fulfilment sequence report id
	 */
	public String getFeedfileProdFulfilmentSequenceReportId() {
		return mFeedfileProdFulfilmentSequenceReportId;
	}

	/**
	 * Sets the feedfile prod fulfilment sequence report id.
	 *
	 * @param pFeedfileProdFulfilmentSequenceReportId the new feedfile prod fulfilment sequence report id
	 */
	public void setFeedfileProdFulfilmentSequenceReportId(String pFeedfileProdFulfilmentSequenceReportId) {
		mFeedfileProdFulfilmentSequenceReportId = pFeedfileProdFulfilmentSequenceReportId;
	}
	
	
}
