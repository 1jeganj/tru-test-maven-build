package com.tru.integrations.synchrony.bean;

/**
 * This class is used for the Synchrony Response SDP Bean
 * @version 1.0
 * @author PA
 * 
 */
public class SynchronySDPResponseBean {

	private String mApplicationDecision;
	private String mTempAcctNumber;
	private String mPassExpDate;
	private String mCVV;
	private String mDateTimeStamp;
	private String mSingleUseCouponCode;
	private String mOlsonRewardNumber;
	private String mPassMonth;
	private String mPassYear;

	/**
	 * Gets the applicationDecision
	 * 
	 * @return the applicationDecision.
	 */
	public String getApplicationDecision() {
		return mApplicationDecision;
	}

	/**
	 * @param pApplicationDecision - the applicationDecision to set.
	 */
	public void setApplicationDecision(String pApplicationDecision) {
		mApplicationDecision = pApplicationDecision;
	}

	/**
	 *  Gets the tempAcctNumber
	 * 
	 * @return the tempAcctNumber.
	 */
	public String getTempAcctNumber() {
		return mTempAcctNumber;
	}

	/**
	 * @param pTempAcctNumber - the tempAcctNumber to set.
	 *            
	 */
	public void setTempAcctNumber(String pTempAcctNumber) {
		mTempAcctNumber = pTempAcctNumber;
	}

	/**
	 * Gets the passExpDate
	 * 
	 * @return the passExpDate.
	 */
	public String getPassExpDate() {
		return mPassExpDate;
	}

	/**
	 * @param pPassExpDate -  the passExpDate to set.
	 *
	 */
	public void setPassExpDate(String pPassExpDate) {
		mPassExpDate = pPassExpDate;
	}

	/**
	 * Gets the cVV
	 * 
	 * @return the cVV.
	 */
	public String getCVV() {
		return mCVV;
	}

	/**
	 * @param pCVV - the cVV to set.
	 *            
	 */
	public void setCVV(String pCVV) {
		mCVV = pCVV;
	}

	/**
	 * Gets the dateTimeStamp
	 * 
	 * @return the dateTimeStamp.
	 */
	public String getDateTimeStamp() {
		return mDateTimeStamp;
	}

	/**
	 * @param pDateTimeStamp - the dateTimeStamp to set.
	 *            
	 */
	public void setDateTimeStamp(String pDateTimeStamp) {
		mDateTimeStamp = pDateTimeStamp;
	}

	/**
	 * Gets the singleUseCouponCode
	 * 
	 * @return the singleUseCouponCode.
	 */
	public String getSingleUseCouponCode() {
		return mSingleUseCouponCode;
	}

	/**
	 * @param pSingleUseCouponCode - the singleUseCouponCode to set.
	 *            
	 */
	public void setSingleUseCouponCode(String pSingleUseCouponCode) {
		mSingleUseCouponCode = pSingleUseCouponCode;
	}

	/**
	 * Gets the olsonRewardNumber
	 * 
	 * @return the olsonRewardNumber.
	 */
	public String getOlsonRewardNumber() {
		return mOlsonRewardNumber;
	}

	/**
	 * @param pOlsonRewardNumber - the olsonRewardNumber to set.
	 *            
	 */
	public void setOlsonRewardNumber(String pOlsonRewardNumber) {
		mOlsonRewardNumber = pOlsonRewardNumber;
	}

	/**
	 * Gets the passMonth
	 * 
	 * @return the passMonth.
	 */
	public String getPassMonth() {
		return mPassMonth;
	}

	/**
	 * @param pPassMonth - the passMonth to set.
	 *            
	 */
	public void setPassMonth(String pPassMonth) {
		mPassMonth = pPassMonth;
	}

	/**
	 * Gets the passYear
	 * 
	 * @return the passYear.
	 */
	public String getPassYear() {
		return mPassYear;
	}

	/**
	 * @param pPassYear - the passYear to set.
	 *            
	 */
	public void setPassYear(String pPassYear) {
		mPassYear = pPassYear;
	}

}
