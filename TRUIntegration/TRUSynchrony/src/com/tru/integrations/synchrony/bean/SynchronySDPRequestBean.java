package com.tru.integrations.synchrony.bean;

/**
 * This class is used for the Synchrony Request SDP Bean.
 * 
 * @author Sudheer Guduru
 * @version 1.0
 * 
 */
public class SynchronySDPRequestBean {

	private String mFirstName;
	private String mMiddleInit;
	private String mLastName;
	private String mHomeAddress;
	private String mHomeAddress2;
	private String mCity;
	private String mState;
	private String mZipCode;
	private String mIntialTransactionAmt;
	private String mEmail;
	private String mMemberNumber;
	private String mSaleAmomt;
	private String mMemberSinceDate;
	private String mPromoCodeAD;
	private String mPromoCodePOC;
	private String mRU;
	private String mDateTimeStamp;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * @param pFirstName
	 *            the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * @return the middleInit
	 */
	public String getMiddleInit() {
		return mMiddleInit;
	}

	/**
	 * @param pMiddleInit
	 *            the middleInit to set
	 */
	public void setMiddleInit(String pMiddleInit) {
		mMiddleInit = pMiddleInit;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return mLastName;
	}

	/**
	 * @param pLastName
	 *            the lastName to set
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}

	/**
	 * @return the homeAddress
	 */
	public String getHomeAddress() {
		return mHomeAddress;
	}

	/**
	 * @param pHomeAddress
	 *            the homeAddress to set
	 */
	public void setHomeAddress(String pHomeAddress) {
		mHomeAddress = pHomeAddress;
	}

	/**
	 * @return the homeAddress2
	 */
	public String getHomeAddress2() {
		return mHomeAddress2;
	}

	/**
	 * @param pHomeAddress2
	 *            the homeAddress2 to set
	 */
	public void setHomeAddress2(String pHomeAddress2) {
		mHomeAddress2 = pHomeAddress2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return mCity;
	}

	/**
	 * @param pCity
	 *            the city to set
	 */
	public void setCity(String pCity) {
		mCity = pCity;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return mState;
	}

	/**
	 * @param pState
	 *            the state to set
	 */
	public void setState(String pState) {
		mState = pState;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return mZipCode;
	}

	/**
	 * @param pZipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String pZipCode) {
		mZipCode = pZipCode;
	}

	/**
	 * @return the intialTransactionAmt
	 */
	public String getIntialTransactionAmt() {
		return mIntialTransactionAmt;
	}

	/**
	 * @param pIntialTransactionAmt
	 *            the intialTransactionAmt to set
	 */
	public void setIntialTransactionAmt(String pIntialTransactionAmt) {
		mIntialTransactionAmt = pIntialTransactionAmt;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param pEmail
	 *            the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * @return the memberNumber
	 */
	public String getMemberNumber() {
		return mMemberNumber;
	}

	/**
	 * @param pMemberNumber
	 *            the memberNumber to set
	 */
	public void setMemberNumber(String pMemberNumber) {
		mMemberNumber = pMemberNumber;
	}
	
	/**
	 * @return the saleAmomt
	 */
	public String getSaleAmomt() {
		return mSaleAmomt;
	}

	/**
	 * @param pSaleAmomt the saleAmomt to set
	 */
	public void setSaleAmomt(String pSaleAmomt) {
		mSaleAmomt = pSaleAmomt;
	}

	/**
	 * @return the memberSinceDate
	 */
	public String getMemberSinceDate() {
		return mMemberSinceDate;
	}

	/**
	 * @param pMemberSinceDate the memberSinceDate to set
	 */
	public void setMemberSinceDate(String pMemberSinceDate) {
		mMemberSinceDate = pMemberSinceDate;
	}

	/**
	 * @return the promoCodeAD
	 */
	public String getPromoCodeAD() {
		return mPromoCodeAD;
	}

	/**
	 * @param pPromoCodeAD
	 *            the promoCodeAD to set
	 */
	public void setPromoCodeAD(String pPromoCodeAD) {
		mPromoCodeAD = pPromoCodeAD;
	}

	/**
	 * @return the promoCodePOC
	 */
	public String getPromoCodePOC() {
		return mPromoCodePOC;
	}

	/**
	 * @param pPromoCodePOC
	 *            the promoCodePOC to set
	 */
	public void setPromoCodePOC(String pPromoCodePOC) {
		mPromoCodePOC = pPromoCodePOC;
	}

	/**
	 * @return the rU
	 */
	public String getRU() {
		return mRU;
	}

	/**
	 * @param pRU
	 *            the rU to set
	 */
	public void setRU(String pRU) {
		mRU = pRU;
	}

	/**
	 * @return the dateTimeStamp
	 */
	public String getDateTimeStamp() {
		return mDateTimeStamp;
	}

	/**
	 * @param pDateTimeStamp
	 *            the dateTimeStamp to set
	 */
	public void setDateTimeStamp(String pDateTimeStamp) {
		mDateTimeStamp = pDateTimeStamp;
	}

}
