package com.tru.integrations.synchrony.service;

import java.util.LinkedHashMap;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.constants.TRUConstants;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.bean.SynchronySDPRequestBean;
import com.tru.integrations.synchrony.bean.SynchronySDPResponseBean;
import com.tru.integrations.synchrony.constants.SynchronyConstants;
import com.tru.integrations.synchrony.util.AESCryptography;
import com.tru.integrations.synchrony.util.SynchronyConfiguration;
import com.tru.integrations.synchrony.util.SynchronyHelper;

/**
 * This class is used to prepare the URL for Synchrony.
 * 
 * @author Sudheer Guduru
 * @version 1.0
 */
public class Synchrony extends GenericService {
	
	

	private SynchronyConfiguration mSynchronyConfiguration;
	
	private SynchronyHelper mSynchronyHelper;
	
	private AESCryptography mAESCryptography;
	
	/**
	 * This method is used to prepare the SDP Request
	 * 
	 * @param pTruSynchronySDPBean
	 *            -TruSynchronySDPBean
	 * @return prepareRequestSDP
	 * 			  prepareRequestSDP
	 * @throws TRUIntegrationException
	 *             TRUIntegrationException
	 */
	private String prepareRequestSDP(SynchronySDPRequestBean pTruSynchronySDPBean) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareRequestSDP(SynchronySDPRequestBean) :: START");
		}
		getSynchronyHelper().validateRequestSDP(pTruSynchronySDPBean);
		Map<String, String> requestMap = new LinkedHashMap<String, String>();
		requestMap.put(SynchronyConstants.FIRSTNAME, pTruSynchronySDPBean.getFirstName());
		requestMap.put(SynchronyConstants.MIDDLEINIT, pTruSynchronySDPBean.getMiddleInit());
		requestMap.put(SynchronyConstants.LASTNAME, pTruSynchronySDPBean.getLastName());
		requestMap.put(SynchronyConstants.HOMEADDRESS, pTruSynchronySDPBean.getHomeAddress());
		requestMap.put(SynchronyConstants.HOMEADDRESS2, pTruSynchronySDPBean.getHomeAddress2());
		requestMap.put(SynchronyConstants.CITY, pTruSynchronySDPBean.getCity());
		requestMap.put(SynchronyConstants.STATE, pTruSynchronySDPBean.getState());
		requestMap.put(SynchronyConstants.ZIPCODE, pTruSynchronySDPBean.getZipCode());
		requestMap.put(SynchronyConstants.INITIAL_TRANSACTION_AMOUNT, pTruSynchronySDPBean.getIntialTransactionAmt());
		requestMap.put(SynchronyConstants.EMAIL, pTruSynchronySDPBean.getEmail());
		requestMap.put(SynchronyConstants.MEMBER_NUMBER, pTruSynchronySDPBean.getMemberNumber());
//		requestMap.put(SynchronyConstants.SALE_AMOUNT, pTruSynchronySDPBean.getSaleAmomt());
//		requestMap.put(SynchronyConstants.MEMBER_SINCE_DATE, pTruSynchronySDPBean.getMemberSinceDate());
		requestMap.put(SynchronyConstants.PROMOCODE_AD, pTruSynchronySDPBean.getPromoCodeAD());
		requestMap.put(SynchronyConstants.PROMOCODE_POC, pTruSynchronySDPBean.getPromoCodePOC());
		requestMap.put(SynchronyConstants.RU, pTruSynchronySDPBean.getRU());
		requestMap.put(SynchronyConstants.DATE_TIMESTAMP, pTruSynchronySDPBean.getDateTimeStamp());
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareRequestSDP(SynchronySDPRequestBean) :: END");
		}
		return prepareRequestSDP(requestMap);
	}
	
	/**
	 * This method is used to prepare the SDP Request.
	 * 
	 * @param pRequestMap
	 *            -RequestMap
	 * @return sdp
	 * 			  sdp
	 * @throws TRUIntegrationException
	 *             TRUIntegrationException
	 */
	private String prepareRequestSDP(Map<String, String> pRequestMap) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareRequestSDP(Map) :: START");
		}
		String sdp = null;
		StringBuilder sb = new StringBuilder();
		if(pRequestMap != null && !pRequestMap.isEmpty()) {
        	for(String key : pRequestMap.keySet()){
                sb = sb.append(key + SynchronyConstants.EQUAL + pRequestMap.get(key) + TRUConstants.PIPELINE);
        	}	
        }
		sdp = sb.substring(SynchronyConstants.ZERO_INDEX, sb.length() - SynchronyConstants.FIRST_INDEX);
		if(isLoggingDebug()){
			vlogDebug("Synchrony :: prepareRequestSDP(Map) :: SDP Before Encrption :: {0}", sdp);
		}
		sdp = getAESCryptography().encrypt(sdp, mSynchronyConfiguration.getSecurityKey());
		if (isLoggingDebug()) {
			vlogDebug("Synchrony :: prepareRequestSDP(Map) :: SDP After Encryption :: {0}", sdp);
			logDebug("Synchrony :: prepareRequestSDP(Map) :: END");
		}
		return sdp;
	}
	
	/**
	 * This method is used to prepare the Synchrony URL.
	 * 
	 * @param pRequestMap
	 *            -RequestMap
	 * @return url
	 * 			  url
	 * @throws TRUIntegrationException
	 *             TRUIntegrationException
	 */
	public String prepareSynchronyURL(Map<String, String> pRequestMap) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareSynchronyURL(Map) :: START");
		}
		String url = mSynchronyConfiguration.getURL() + SynchronyConstants.QUESTION_MARK +
				     SynchronyConstants.CHASH + SynchronyConstants.EQUAL + mSynchronyConfiguration.getCHash() + SynchronyConstants.AMPERSAND +
				     SynchronyConstants.SUBACTIONID + SynchronyConstants.EQUAL + mSynchronyConfiguration.getSubActionId() + SynchronyConstants.AMPERSAND +
			         SynchronyConstants.LANGID + SynchronyConstants.EQUAL + mSynchronyConfiguration.getLangId() + SynchronyConstants.AMPERSAND +
				     SynchronyConstants.SDP + SynchronyConstants.EQUAL + prepareRequestSDP(pRequestMap); 
		
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareSynchronyURL(Map) :: END");
		}
		return url;
	}
	
	/**
	 * This method is used to prepare the Synchrony URL.
	 * 
	 * @param pSynchronySDPBean
	 *            -SynchronySDPBean
	 * @return url
	 * 			  url
	 * @throws TRUIntegrationException
	 *             TRUIntegrationException
	 */
	public String prepareSynchronyURL(SynchronySDPRequestBean pSynchronySDPBean) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: prepareSynchronyURL(SynchronySDPRequestBean) :: START");
		}
		String cHash = mSynchronyConfiguration.getCHash();
		String subActionId = mSynchronyConfiguration.getSubActionId();
		String langId = mSynchronyConfiguration.getLangId();
		String sdp = prepareRequestSDP(pSynchronySDPBean);
		String url = mSynchronyConfiguration.getURL() + SynchronyConstants.QUESTION_MARK +
				SynchronyConstants.CHASH + SynchronyConstants.EQUAL + cHash + SynchronyConstants.AMPERSAND +
				SynchronyConstants.SUBACTIONID + SynchronyConstants.EQUAL + subActionId + SynchronyConstants.AMPERSAND +
				SynchronyConstants.LANGID + SynchronyConstants.EQUAL + langId + SynchronyConstants.AMPERSAND +
				SynchronyConstants.SDP + SynchronyConstants.EQUAL + sdp; 
		if (isLoggingDebug()) {
			vlogDebug("cHash: {0}, subActionId: {1}, langId: {2}, SDP: {3}", cHash, subActionId, langId, sdp);
			vlogDebug("Synchrony URL: {0}", url);
			logDebug("Synchrony :: prepareSynchronyURL(SynchronySDPRequestBean) :: END");
		}
		return url;
	}
	
	/**
	 * This method is used to parse the Synchrony SDP Response.
	 * 
	 * @param pResponseSDP
	 *            -ResponseSDP
	 * @return responseBean
	 * 			  responseBean
	 * @throws TRUIntegrationException
	 *             TRUIntegrationException
	 */
	public SynchronySDPResponseBean parseResponseSDP(String pResponseSDP) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: parseResponseSDP(String) :: START");
			vlogDebug("Synchrony :: parseResponseSDP :: SDP Before Decryption :: {0}", pResponseSDP);
		}
		SynchronySDPResponseBean responseBean = new SynchronySDPResponseBean();
		String responseSDP = getAESCryptography().decrypt(pResponseSDP, mSynchronyConfiguration.getSecurityKey());
		if (isLoggingDebug()) {
			vlogDebug("Synchrony :: parseResponseSDP :: SDP After Decryption :: {0}", responseSDP);
		}
		String[] keyValues = responseSDP.split(SynchronyConstants.SPILT);
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (int i = 0; i < keyValues.length; i++) {
			String[] keyValue = keyValues[i].split(SynchronyConstants.EQUAL);
			if(keyValue.length == SynchronyConstants.NUMERIC_TWO) {
				map.put(keyValue[SynchronyConstants.ZERO_INDEX], keyValue[SynchronyConstants.FIRST_INDEX]);
			}
		}
		responseBean = pareseResponseSDP(map);
		if (isLoggingDebug()) {
			logDebug("Synchrony :: parseResponseSDP(String) :: END");
		}
		return responseBean;
	}
	
	/**
	 * This method is used to prepare the Synchrony SDP Response Bean.
	 * 
	 * @param pResponseSDP
	 *            -ResponseSDP
	 * @return responseBean
	 * 			  responseBean
	 */
	public SynchronySDPResponseBean pareseResponseSDP(Map<String, String> pResponseSDP) {
		if (isLoggingDebug()) {
			logDebug("Synchrony :: parseResponseSDP(Map) :: START");
		}
		SynchronySDPResponseBean responseBean = new SynchronySDPResponseBean();
		responseBean.setApplicationDecision(pResponseSDP.get(SynchronyConstants.APPLICATION_DECISION));
		responseBean.setPassExpDate(pResponseSDP.get(SynchronyConstants.PASS_EXP_DATE));
		responseBean.setTempAcctNumber(pResponseSDP.get(SynchronyConstants.TEMP_ACCT_NUMBER));
		responseBean.setCVV(pResponseSDP.get(SynchronyConstants.CVV));
		responseBean.setSingleUseCouponCode(pResponseSDP.get(SynchronyConstants.SINGLE_USE_COUPON_CODE));
		responseBean.setOlsonRewardNumber(pResponseSDP.get(SynchronyConstants.OLSON_REWARD_NUMBER));
		responseBean.setDateTimeStamp(pResponseSDP.get(SynchronyConstants.DATE_TIMESTAMP));
		String expiryDate = responseBean.getPassExpDate();
		if(StringUtils.isNotBlank(expiryDate) && expiryDate.length() == SynchronyConstants._8) {
            responseBean.setPassMonth(expiryDate.substring(SynchronyConstants.FOUR_INDEX, SynchronyConstants.SIX_INDEX));
            responseBean.setPassYear(expiryDate.substring(SynchronyConstants.ZERO_INDEX, SynchronyConstants.FOUR_INDEX));
		}
		if (isLoggingDebug()) {
			logDebug("Synchrony :: parseResponseSDP(Map) :: END");
		}
		return responseBean;
	}

	/**
	 * @return the synchronyConfiguration.
	 */
	public SynchronyConfiguration getSynchronyConfiguration() {
		return mSynchronyConfiguration;
	}

	/**
	 * @param pSynchronyConfiguration the synchronyConfiguration to set
	 */
	public void setSynchronyConfiguration(SynchronyConfiguration pSynchronyConfiguration) {
		mSynchronyConfiguration = pSynchronyConfiguration;
	}
	
	/**
	 * @return the synchronyHelper
	 */
	public SynchronyHelper getSynchronyHelper() {
		return mSynchronyHelper;
	}

	/**
	 * @param pSynchronyHelper the synchronyHelper to set
	 */
	public void setSynchronyHelper(SynchronyHelper pSynchronyHelper) {
		mSynchronyHelper = pSynchronyHelper;
	}

	/**
	 * @return the aESCryptography
	 */
	public AESCryptography getAESCryptography() {
		return mAESCryptography;
	}

	/**
	 * @param pAESCryptography the aESCryptography to set
	 */
	public void setAESCryptography(AESCryptography pAESCryptography) {
		mAESCryptography = pAESCryptography;
	}
	
}
