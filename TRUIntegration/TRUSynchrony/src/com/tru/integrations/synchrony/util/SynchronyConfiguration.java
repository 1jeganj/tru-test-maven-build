package com.tru.integrations.synchrony.util;

import atg.nucleus.GenericService;

/**
 * This class configures values for Synchrony.
 * @author Sandeep Vishwakarma
 * @version 1.0
 *
 */
public class SynchronyConfiguration extends GenericService {
	
	private String mURL;
	
	private String mCHash;
	
	private String mSubActionId;
	
	private String mLangId;
	
	private String mSecurityKey;
	
	private String mTimeZome;
	
	private String mDateFormat;
	
	/**
	 * @return the URL
	 */
	public String getURL() {
		return mURL;
	}

	/**
	 * @param pURL the uRL to set
	 */
	public void setURL(String pURL) {
		mURL = pURL;
	}

	/**
	 * @return the cHash
	 */
	public String getCHash() {
		return mCHash;
	}

	/**
	 * @param pCHash the cHash to set
	 */
	public void setCHash(String pCHash) {
		mCHash = pCHash;
	}

	/**
	 * @return the subActionId
	 */
	public String getSubActionId() {
		return mSubActionId;
	}

	/**
	 * @param pSubActionId the subActionId to set
	 */
	public void setSubActionId(String pSubActionId) {
		mSubActionId = pSubActionId;
	}

	/**
	 * @return the langId
	 */
	public String getLangId() {
		return mLangId;
	}

	/**
	 * @param pLangId the langId to set
	 */
	public void setLangId(String pLangId) {
		mLangId = pLangId;
	}

	/**
	 * @return the securityKey
	 */
	public String getSecurityKey() {
		return mSecurityKey;
	}

	/**
	 * @param pSecurityKey the securityKey to set
	 */
	public void setSecurityKey(String pSecurityKey) {
		mSecurityKey = pSecurityKey;
	}

	/**
	 * @return the timeZome
	 */
	public String getTimeZome() {
		return mTimeZome;
	}

	/**
	 * @param pTimeZome the timeZome to set
	 */
	public void setTimeZome(String pTimeZome) {
		mTimeZome = pTimeZome;
	}

	/**
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return mDateFormat;
	}

	/**
	 * @param pDateFormat the dateFormat to set
	 */
	public void setDateFormat(String pDateFormat) {
		mDateFormat = pDateFormat;
	}
	
	
	
}
