package com.tru.integrations.synchrony.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import atg.nucleus.GenericService;

import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.bean.SynchronySDPRequestBean;
import com.tru.integrations.synchrony.constants.SynchronyConstants;

/**
 * This Class is a helper class for the Synchrony
 * 
 * @author Sudheer Guduru
 * @version 1.0
 *
 */
public class SynchronyHelper extends GenericService {
	
	/**
	 * This method is used to Validate the Request.
	 * @param pTruSynchronySDPBean of SynchronySDPRequestBean.
	 * @throws TRUIntegrationException  of TRUIntegrationException.
	 */
	public void validateRequestSDP(SynchronySDPRequestBean pTruSynchronySDPBean) throws TRUIntegrationException {
		validateLength(SynchronyConstants.FIRSTNAME, pTruSynchronySDPBean.getFirstName(), SynchronyConstants.FNAME_MAX_LENGTH);
		validateLength(SynchronyConstants.MIDDLEINIT, pTruSynchronySDPBean.getMiddleInit(), SynchronyConstants.MNAME_MAX_LENGTH);
		validateLength(SynchronyConstants.LASTNAME, pTruSynchronySDPBean.getLastName(), SynchronyConstants.LNAME_MAX_LENGTH);
		validateLength(SynchronyConstants.HOMEADDRESS, pTruSynchronySDPBean.getHomeAddress(), SynchronyConstants.HOME_ADDRESS_MAX_LENGTH);
		validateLength(SynchronyConstants.HOMEADDRESS2, pTruSynchronySDPBean.getHomeAddress2(), SynchronyConstants.HOME_ADDRESS2_MAX_LENGTH);
		validateLength(SynchronyConstants.CITY, pTruSynchronySDPBean.getCity(), SynchronyConstants.CITY_MAX_LENGTH);
		validateLength(SynchronyConstants.STATE, pTruSynchronySDPBean.getState(), SynchronyConstants.STATE_MAX_LENGTH);
		validateLength(SynchronyConstants.ZIPCODE, pTruSynchronySDPBean.getZipCode(), SynchronyConstants.ZIPCODE_MAX_LENGTH);
		validateLength(SynchronyConstants.INITIAL_TRANSACTION_AMOUNT, pTruSynchronySDPBean.getIntialTransactionAmt(), SynchronyConstants.ITA_MAX_LENGTH);
		validateLength(SynchronyConstants.EMAIL, pTruSynchronySDPBean.getEmail(), SynchronyConstants.EMAIL_MAX_LENGTH);
		validateLength(SynchronyConstants.MEMBER_NUMBER, pTruSynchronySDPBean.getMemberNumber(), SynchronyConstants.MEMBER_NO_MAX_LENGTH);
		checkNull(SynchronyConstants.PROMOCODE_AD , pTruSynchronySDPBean.getPromoCodeAD());
		checkNull(SynchronyConstants.PROMOCODE_POC , pTruSynchronySDPBean.getPromoCodePOC());
	}
	
	/**
	 * This method is used to Validate the Length of the Fields.
	 * @param pField of String
	 * @param pValue of String
	 * @param pLength of String
	 * @throws TRUIntegrationException of TRUIntegrationException.
	 */
	private void validateLength(String pField, String pValue, int pLength) throws TRUIntegrationException {
		if(pValue != null && pValue.length() > pLength){
			throw new TRUIntegrationException(SynchronyConstants.LENGTH_OF_THE + pField + SynchronyConstants.FIELD_NOT_MORE_THAN + pLength);
		}
	}
	
	/**
	 * This method is used for null check.
	 * @param pField of String
	 * @param pValue of String
	 * @throws TRUIntegrationException of TRUIntegrationException
	 */
	private void checkNull(String pField , String pValue) throws TRUIntegrationException {
		if (pValue != null && pValue.equalsIgnoreCase(null)) {
			throw new TRUIntegrationException(SynchronyConstants.VALUE_FOR + pField + SynchronyConstants.SHOULD_BE_NULL);
		}
	}
	
	/**
	 * This method is used to get time stamp in desired format for any zone in String.
	 * @param pZone of String
	 * @param pDateFormat of String
	 * @return parsedTimeStamp of String
	 */
	public String getZoneSpecificParsedTimeStamp(String pZone, String pDateFormat){
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat(pDateFormat,Locale.getDefault());
		TimeZone timeZone = TimeZone.getTimeZone(pZone);
		dateFormat.setTimeZone(timeZone);
		return dateFormat.format(date);
	}

}
