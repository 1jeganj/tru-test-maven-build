package com.tru.integrations.synchrony.util;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import atg.nucleus.GenericService;

import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.constants.SynchronyConstants;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;



/**
 * This class is used to Encrypt and Decrypt the data based on Synchrony Specification.
 * 
 * @author Sudheer Guduru
 * @version 1.0
 *
 */
public class AESCryptography extends GenericService {
	
	private static final String ALGORITHM = "AES";
	private static final String CHARSETNAME = "ISO-8859-1";
	
	/**
	 * This method is used to Encrypt the data based on Synchrony Specification.
	 * @param pMessage message need to be encrypt
	 * @param pSecurityKey security key
	 * @return encodedText encrypted text
	 * @throws TRUIntegrationException integration exception
	 */
	public String encrypt(String pMessage, String pSecurityKey) throws TRUIntegrationException {
		String encodedText = null;
		try {
			Cipher cipher = getCipher(Cipher.ENCRYPT_MODE, pSecurityKey);
			byte[] cipherText = cipher.doFinal(pMessage.getBytes());
			BASE64Encoder base64Encoder = new BASE64Encoder();
			encodedText = base64Encoder.encode(cipherText);
			encodedText = URLEncoder.encode(encodedText, CHARSETNAME);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException e) {
			if(isLoggingError()) {
				logError("Exception in AESCryptography.encrypt()", e);
			}
			throw new TRUIntegrationException(e);
		} catch (Exception e) {
			if(isLoggingError()) {
				logError("Exception in AESCryptography.encrypt()", e);
			}
			throw new TRUIntegrationException(e);
		}
		return encodedText;
	}
	
	/**
	 * This method is used to Decrypt the data based on Synchrony Specification.
	 * @param pMessage message need to be decode
	 * @param pSecurityKey security key
	 * @return decodedText decoded text
	 * @throws TRUIntegrationException integration exception
	 */
	public String decrypt(String pMessage, String pSecurityKey) throws TRUIntegrationException {
		String decodedText = null;
		try {
			Cipher cipher = getCipher(Cipher.DECRYPT_MODE, pSecurityKey);
			String decoded = URLDecoder.decode(pMessage, CHARSETNAME);
			BASE64Decoder base64Decoder = new BASE64Decoder();
			byte[] decodedByte = base64Decoder.decodeBuffer(decoded);
			byte[] cipherText = cipher.doFinal(decodedByte);
			decodedText = new String(cipherText);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException e) {
			if(isLoggingError()) {
				logError("Exception in AESCryptography.encrypt()", e);
			}
			throw new TRUIntegrationException(e);
		} catch (Exception e) {
			if(isLoggingError()) {
				logError("Exception in AESCryptography.encrypt()", e);
			}
			throw new TRUIntegrationException(e);
		}
		return decodedText;
	}

	/**
	 * This method is used to get the Cipher.
	 * @param pMode mode
	 * @param pSecurityKey security code
	 * @return cipher Cipher object
	 * @throws Exception Object of Exception class
	 * @throws NoSuchAlgorithmException Object of NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException Object of NoSuchPaddingException
	 * @throws InvalidKeyException Object of InvalidKeyException
	 */
	private Cipher getCipher(int pMode, String pSecurityKey) throws Exception,
			NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException {
		byte[] bytes = decodeSecurityKey(pSecurityKey);
		SecretKeySpec skeySpec = new SecretKeySpec(bytes, ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(pMode, skeySpec);
		return cipher;
	}
	
	
	/**
	 * This method decodes the Security Key.
	 * @param pSecurityKey security key
	 * @return result byte array 
	 * @throws Exception object of Exception
	 */
	private byte[] decodeSecurityKey(String pSecurityKey) throws Exception {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		try {
			int len = pSecurityKey.length();
			for (int i = SynchronyConstants.ZERO; i < len; i += SynchronyConstants.TWO) {
				char c1 = pSecurityKey.charAt(i);
				char c2 = SynchronyConstants.CHAR_ZERO;
				if ((i + SynchronyConstants.ONE) < len) {
					c2 = pSecurityKey.charAt(i + SynchronyConstants.ONE);
				}
				result.write((byte) ((getBase10(c1) * SynchronyConstants.SIXTEEN) + getBase10(c2)));
			}
		} catch (Exception e) {
			throw e;
		}

		return result.toByteArray();
	}
	
	/**
	 * Return the Base 10 value of character.
	 * @param pChar -plain character
	 * @return c -parsed character
	 */
	private int getBase10(char pChar) {
		return Integer.parseInt(String.valueOf(pChar), SynchronyConstants.SIXTEEN);
	}

}