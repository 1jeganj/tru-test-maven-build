package com.tru.integrations.synchrony.constants;

import com.tru.integrations.constants.TRUConstants;

/**
 * This Class is used for the Synchrony Constants.
 * 
 * @author SGuduru
 * @version 1.0
 *
 */
public class SynchronyConstants extends TRUConstants {

	public static final String FIRSTNAME = "0009";
	public static final String MIDDLEINIT = "0010";
	public static final String LASTNAME = "0012";
	public static final String HOMEADDRESS = "0014";
	public static final String HOMEADDRESS2 = "0015";
	public static final String CITY = "0016";
	public static final String STATE = "0017";
	public static final String ZIPCODE = "0018";
	public static final String INITIAL_TRANSACTION_AMOUNT = "ITA";
	public static final String EMAIL = "0569";
	public static final String MEMBER_NUMBER = "0274";
	public static final String SALE_AMOUNT = "0116";
	public static final String MEMBER_SINCE_DATE = "0734";
	public static final String PROMOCODE_AD = "AD";
	public static final String PROMOCODE_POC = "POC";
	public static final String RU ="RU";
	public static final String DATE_TIMESTAMP = "DT";
	
	public static final String APPLICATION_DECISION = "0365";
	public static final String TEMP_ACCT_NUMBER = "0038";
	public static final String PASS_EXP_DATE = "0049";
	public static final String CVV = "CVV";
	public static final String SINGLE_USE_COUPON_CODE = "SUCN";
	public static final String OLSON_REWARD_NUMBER = "0274";
	
	public static final String CHASH = "cHash";
	public static final String SUBACTIONID = "subActionId";
	public static final String LANGID = "langId";
	public static final String SDP = "SDP";
	
	public static final int FNAME_MAX_LENGTH = 16;
	public static final int MNAME_MAX_LENGTH = 1;
	public static final int LNAME_MAX_LENGTH = 20;
	public static final int HOME_ADDRESS_MAX_LENGTH = 35;
	public static final int HOME_ADDRESS2_MAX_LENGTH = 35;
	public static final int CITY_MAX_LENGTH = 23;
	public static final int STATE_MAX_LENGTH = 2;
	public static final int ZIPCODE_MAX_LENGTH = 5;
	public static final int ITA_MAX_LENGTH = 15;
	public static final int EMAIL_MAX_LENGTH = 50;
	public static final int MEMBER_NO_MAX_LENGTH = 13;
	
	public static final int ZERO_INDEX = 0;
	public static final int FIRST_INDEX = 1;
	public static final int FOUR_INDEX = 4;
	public static final int SIX_INDEX = 6;
	public static final String SPILT = "\\|";
	
	
	public static final String LENGTH_OF_THE = "Length of the ";
	public static final String FIELD_NOT_MORE_THAN = " Field is more than ";
	public static final String VALUE_FOR = "Value for ";
	public static final String SHOULD_BE_NULL = " should be null";
	
	public static final int _8 = 8;
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final char CHAR_ZERO = 0;
	
	public static final int SIXTEEN = 16;
	
}
