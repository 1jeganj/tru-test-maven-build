package com.tru.integrations.synchrony.test;

import java.util.LinkedHashMap;
import java.util.Map;

import atg.nucleus.GenericService;

import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.synchrony.bean.SynchronySDPRequestBean;
import com.tru.integrations.synchrony.bean.SynchronySDPResponseBean;
import com.tru.integrations.synchrony.constants.SynchronyConstants;
import com.tru.integrations.synchrony.service.Synchrony;
import com.tru.integrations.synchrony.util.SynchronyConfiguration;
import com.tru.integrations.synchrony.util.SynchronyHelper;

/**
 * This class is SynchronyTest
 * @author PA
 * 
 */
@SuppressWarnings("PMD")
public class SynchronyTest extends GenericService {
	
	private Synchrony mSynchrony;
	
	private SynchronyConfiguration mSynchronyConfiguration;
	
	private SynchronyHelper mSynchronyHelper;
	/**
	 * @param args args.
	 * @throws Exception Exception.
	 */
	public static void main(String[] args) throws Exception {

		String url = null;
		SynchronySDPResponseBean response = null;
		SynchronyTest test = new SynchronyTest();
		Synchrony truSynchrony = new Synchrony();
		SynchronyConfiguration sc = new SynchronyConfiguration();
		sc.setURL("https://uat.onlinecreditcenter6.com/eapplygen2/load.do");
		sc.setCHash("385211801");
		sc.setSubActionId("1000");
		sc.setLangId("en");
		truSynchrony.setSynchronyConfiguration(sc);
		url = truSynchrony.prepareSynchronyURL(test.createSDPMap());
		url = truSynchrony.prepareSynchronyURL(test.createSDPObject());
		response = truSynchrony.parseResponseSDP("2fcd5d2ff3be077203c66fcf8ec82a9bc71a64d70cb9a004d497ad17de3cffca81204285aaf7c41fb0e5e7055d8907ae56ceb3f4dc92713238dfbb1007b0207e45dede3b848ac13bd3e2f5db0ff98b665847bf02330f7039f463238f0e86f7363ef4d06c5c59dc626ece96ce70e62654f4aacd4bac5914a602ed7fd4da2b1401");
		response = truSynchrony.parseResponseSDP("3d03e109d945fa1e7720d17b9494552b837889eb865542e52c06695c005d33218920a9813f6539b467dfcb593718b3c5da914f293e661a00c2bb0b124ae1e5255a7e9ada32cf8a641f837d147edac10df66646b3d7f3c26352ac11fbdd1bddc43ef4d06c5c59dc626ece96ce70e62654f4aacd4bac5914a602ed7fd4da2b1401");

	}
	/**
	 * This method is for testing Synchrony Service.
	 * @throws TRUIntegrationException TRUIntegrationException.
	 */
	public void testSynchrony() throws TRUIntegrationException{
		if(isLoggingDebug()){
			logDebug("SynchronyTest :: testSynchrony() :: START");
		}
		String url = null;
		SynchronySDPResponseBean response = null;
		
		if(isLoggingDebug()){
			url = mSynchrony.prepareSynchronyURL(createSDPMap());
			vlogDebug("SDPMap URL :: {0}", url);
		}
		
		url = mSynchrony.prepareSynchronyURL(createSDPObject());
		if(isLoggingDebug()){
			vlogDebug("SDPObject URL :: {0}", url);
		}
		
//		response = mSynchrony.parseResponseSDP("2fcd5d2ff3be077203c66fcf8ec82a9bc71a64d70cb9a004d497ad17de3cffca81204285aaf7c41fb0e5e7055d8907ae56ceb3f4dc92713238dfbb1007b0207e45dede3b848ac13bd3e2f5db0ff98b665847bf02330f7039f463238f0e86f7363ef4d06c5c59dc626ece96ce70e62654f4aacd4bac5914a602ed7fd4da2b1401");
		response = mSynchrony.parseResponseSDP("sNz5TGUXa71lG8QCvjpC4u2ButEcq6KT64zrFSrapPLAEc6ey62TUPtwe8kNJegqjXz%2B64DcCGx%2B%0D%0AQx7HbH4UGnymxbQLobkKMPgCQhHrL1FwMmKTyBTTYHXQtEUPzXfeBz7feuFKFUoTGD%2B9AlF21XUv%0D%0AdUsT8aIuWHdm1FCnehY%3D");
		if(isLoggingDebug()){
			vlogDebug("response :: {0}", response.getApplicationDecision());
		}
		
//		response = mSynchrony.parseResponseSDP("3d03e109d945fa1e7720d17b9494552b837889eb865542e52c06695c005d33218920a9813f6539b467dfcb593718b3c5da914f293e661a00c2bb0b124ae1e5255a7e9ada32cf8a641f837d147edac10df66646b3d7f3c26352ac11fbdd1bddc43ef4d06c5c59dc626ece96ce70e62654f4aacd4bac5914a602ed7fd4da2b1401");
		response = mSynchrony.parseResponseSDP("OO2oWp9OnzB1kBenrUT6HxvoYskfUA6G1sJ277nOEX8xwYyaqHF0l5%2F2YXYagJtsZL0VVwVbmVva%0D%0A%2FRJu7mkqzMpvPMVoC5uoC0BrT0gj0VW9vA2MqF1LFpe7kFZ6Usu%2FBz7feuFKFUoTGD%2B9AlF21XUv%0D%0AdUsT8aIuWHdm1FCnehY%3D");
		if(isLoggingDebug()){
			vlogDebug("response :: {0}", response.getApplicationDecision());
			logDebug("SynchronyTest :: testSynchrony() :: END");
		}
	
	}
	
	/**
	 * This method creates a request object for Synchrony Service.
	 * @return SynchronySDPRequestBean object.
	 */
	public SynchronySDPRequestBean createSDPObject() {
		if(isLoggingDebug()){
			logDebug("SynchronyTest :: createSDPObject() :: START");
		}
		SynchronySDPRequestBean bean = new SynchronySDPRequestBean();
		bean.setFirstName("Joe");
		bean.setMiddleInit("A");
		bean.setLastName("Approval");
		bean.setHomeAddress("123%20Main%20Street");
		bean.setHomeAddress2("APT%203A");
		bean.setCity("Wayne");
		bean.setState("NJ");
		bean.setZipCode("11254");
		bean.setIntialTransactionAmt("00000000023394+");
		bean.setEmail("s.r@ge.com");
		bean.setMemberNumber("9874012937841");
//		bean.setSaleAmomt("3234.32");
//		bean.setMemberSinceDate("20130108");
		bean.setPromoCodeAD(null);
		bean.setPromoCodePOC(null);
		bean.setRU("www.google.com");
		bean.setDateTimeStamp(mSynchronyHelper.getZoneSpecificParsedTimeStamp(mSynchronyConfiguration.getTimeZome(), mSynchronyConfiguration.getDateFormat()));
		if(isLoggingDebug()){
			logDebug("SynchronyTest :: createSDPObject() :: END");
		}
		return bean;
	}
	
	/**
	 * This method creates a request object for Synchrony Service in the form of map object.
	 * @return Map<String,String>
	 */
	public Map<String, String> createSDPMap() {
		if(isLoggingDebug()){
			logDebug("SynchronyTest :: createSDPMap() :: START");
		}
		Map<String, String> requestMap = new LinkedHashMap<String, String>();
		requestMap.put(SynchronyConstants.FIRSTNAME, "Joe");
		requestMap.put(SynchronyConstants.MIDDLEINIT, "A");
		requestMap.put(SynchronyConstants.LASTNAME, "Approval");
		requestMap.put(SynchronyConstants.HOMEADDRESS, "123%20Main%20Street");
		requestMap.put(SynchronyConstants.HOMEADDRESS2, "APT%203A");
		requestMap.put(SynchronyConstants.CITY, "Wayne");
		requestMap.put(SynchronyConstants.STATE, "NJ");
		requestMap.put(SynchronyConstants.ZIPCODE, "11254");
		requestMap.put(SynchronyConstants.INITIAL_TRANSACTION_AMOUNT, "00000000023394+");
		requestMap.put(SynchronyConstants.EMAIL, "s.r@ge.com");
		requestMap.put(SynchronyConstants.MEMBER_NUMBER, "9874012937841");
//		requestMap.put(SynchronyConstants.SALE_AMOUNT, "3234.32");
//		requestMap.put(SynchronyConstants.MEMBER_SINCE_DATE, "20130108");
		requestMap.put(SynchronyConstants.PROMOCODE_AD, null);
		requestMap.put(SynchronyConstants.PROMOCODE_POC, null);
		requestMap.put(SynchronyConstants.RU, "www.toysrus.com/checkout.jsp?_flowExecutionKey=_cC9CE9FD7-7693-79F6-53BA-03B57C3D2AEB_k6F44C6B4-7B85-17E3-9CF6-4DCE68BA6D");
		requestMap.put(SynchronyConstants.DATE_TIMESTAMP, mSynchronyHelper.getZoneSpecificParsedTimeStamp(mSynchronyConfiguration.getTimeZome(), mSynchronyConfiguration.getDateFormat()));
		if(isLoggingDebug()){
			logDebug("SynchronyTest :: createSDPMap() :: END");
		}
		return requestMap;
	}

	/**
	 * @return the synchrony
	 */
	public Synchrony getSynchrony() {
		return mSynchrony;
	}

	/**
	 * @param pSynchrony the synchrony to set
	 */
	public void setSynchrony(Synchrony pSynchrony) {
		mSynchrony = pSynchrony;
	}
	/**
	 * @return the synchronyConfiguration
	 */
	public SynchronyConfiguration getSynchronyConfiguration() {
		return mSynchronyConfiguration;
	}
	/**
	 * @param pSynchronyConfiguration the synchronyConfiguration to set
	 */
	public void setSynchronyConfiguration(
			SynchronyConfiguration pSynchronyConfiguration) {
		mSynchronyConfiguration = pSynchronyConfiguration;
	}
	/**
	 * @return the synchronyHelper
	 */
	public SynchronyHelper getSynchronyHelper() {
		return mSynchronyHelper;
	}
	/**
	 * @param pSynchronyHelper the synchronyHelper to set
	 */
	public void setSynchronyHelper(SynchronyHelper pSynchronyHelper) {
		mSynchronyHelper = pSynchronyHelper;
	}
	
	
	
}
