<%@ include file="/includes/prelude.jspf"%>
<%@ tag language="java"%>
<%@ attribute name="header"%>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}" />
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Toolkit</title>

		<!-- toolkit styles -->
		<link rel="stylesheet" href="css/toolkit.css">
		<link rel="stylesheet" href="css/components.css">
		<link rel="stylesheet" href="css/structures.css">
		<link rel="stylesheet" href="css/templates.css">
		<link rel="stylesheet" href="css/tRus_custom.css">
		<!-- /toolkit styles -->

		<!-- LOADING GOOGLE MAPS-->
		<script type="text/javascript">
			window.google = window.google || {};
			google.maps = google.maps || {};
			(function() {
		
				function getScript(src) {
					document.write('<' + 'script src="' + src + '"><' + '/script>');
				}
		
				var modules = google.maps.modules = {};
				google.maps.__gjsload__ = function(name, text) {
					modules[name] = text;
				};
		
				google.maps.Load = function(apiLoad) {
					delete google.maps.Load;
					apiLoad(
							[
									0.009999999776482582,
									[
											[
													[
															"http://mt0.googleapis.com/vt?lyrs=m@292000000\u0026src=api\u0026hl=en-US\u0026",
															"http://mt1.googleapis.com/vt?lyrs=m@292000000\u0026src=api\u0026hl=en-US\u0026" ],
													null,
													null,
													null,
													null,
													"m@292000000",
													[
															"https://mts0.google.com/vt?lyrs=m@292000000\u0026src=api\u0026hl=en-US\u0026",
															"https://mts1.google.com/vt?lyrs=m@292000000\u0026src=api\u0026hl=en-US\u0026" ] ],
											[
													[
															"http://khm0.googleapis.com/kh?v=166\u0026hl=en-US\u0026",
															"http://khm1.googleapis.com/kh?v=166\u0026hl=en-US\u0026" ],
													null,
													null,
													null,
													1,
													"166",
													[
															"https://khms0.google.com/kh?v=166\u0026hl=en-US\u0026",
															"https://khms1.google.com/kh?v=166\u0026hl=en-US\u0026" ] ],
											[
													[
															"http://mt0.googleapis.com/vt?lyrs=h@292000000\u0026src=api\u0026hl=en-US\u0026",
															"http://mt1.googleapis.com/vt?lyrs=h@292000000\u0026src=api\u0026hl=en-US\u0026" ],
													null,
													null,
													null,
													null,
													"h@292000000",
													[
															"https://mts0.google.com/vt?lyrs=h@292000000\u0026src=api\u0026hl=en-US\u0026",
															"https://mts1.google.com/vt?lyrs=h@292000000\u0026src=api\u0026hl=en-US\u0026" ] ],
											[
													[
															"http://mt0.googleapis.com/vt?lyrs=t@132,r@292000000\u0026src=api\u0026hl=en-US\u0026",
															"http://mt1.googleapis.com/vt?lyrs=t@132,r@292000000\u0026src=api\u0026hl=en-US\u0026" ],
													null,
													null,
													null,
													null,
													"t@132,r@292000000",
													[
															"https://mts0.google.com/vt?lyrs=t@132,r@292000000\u0026src=api\u0026hl=en-US\u0026",
															"https://mts1.google.com/vt?lyrs=t@132,r@292000000\u0026src=api\u0026hl=en-US\u0026" ] ],
											null,
											null,
											[ [ "http://cbk0.googleapis.com/cbk?",
													"http://cbk1.googleapis.com/cbk?" ] ],
											[
													[
															"http://khm0.googleapis.com/kh?v=84\u0026hl=en-US\u0026",
															"http://khm1.googleapis.com/kh?v=84\u0026hl=en-US\u0026" ],
													null,
													null,
													null,
													null,
													"84",
													[
															"https://khms0.google.com/kh?v=84\u0026hl=en-US\u0026",
															"https://khms1.google.com/kh?v=84\u0026hl=en-US\u0026" ] ],
											[ [
													"http://mt0.googleapis.com/mapslt?hl=en-US\u0026",
													"http://mt1.googleapis.com/mapslt?hl=en-US\u0026" ] ],
											[ [
													"http://mt0.googleapis.com/mapslt/ft?hl=en-US\u0026",
													"http://mt1.googleapis.com/mapslt/ft?hl=en-US\u0026" ] ],
											[ [
													"http://mt0.googleapis.com/vt?hl=en-US\u0026",
													"http://mt1.googleapis.com/vt?hl=en-US\u0026" ] ],
											[ [
													"http://mt0.googleapis.com/mapslt/loom?hl=en-US\u0026",
													"http://mt1.googleapis.com/mapslt/loom?hl=en-US\u0026" ] ],
											[ [
													"https://mts0.googleapis.com/mapslt?hl=en-US\u0026",
													"https://mts1.googleapis.com/mapslt?hl=en-US\u0026" ] ],
											[ [
													"https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026",
													"https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026" ] ],
											[ [
													"https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026",
													"https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026" ] ] ],
									[ "en-US", "US", null, 0, null, null,
											"http://maps.gstatic.com/mapfiles/",
											"http://csi.gstatic.com",
											"https://maps.googleapis.com",
											"http://maps.googleapis.com", null,
											"https://maps.google.com",
											"https://csi.gstatic.com",
											"http://maps.gstatic.com/maps-api-v3/api/images/" ],
									[
											"http://maps.gstatic.com/maps-api-v3/api/js/19/10",
											"3.19.10" ],
									[ 1110885197 ],
									1,
									null,
									null,
									null,
									null,
									null,
									"",
									null,
									null,
									0,
									"http://khm.googleapis.com/mz?v=166\u0026",
									null,
									"https://earthbuilder.googleapis.com",
									"https://earthbuilder.googleapis.com",
									null,
									"http://mt.googleapis.com/vt/icon",
									[
											[ "http://mt0.googleapis.com/vt",
													"http://mt1.googleapis.com/vt" ],
											[ "https://mts0.googleapis.com/vt",
													"https://mts1.googleapis.com/vt" ],
											null,
											null,
											null,
											null,
											null,
											null,
											null,
											null,
											null,
											null,
											[ "https://mts0.google.com/vt",
													"https://mts1.google.com/vt" ],
											"/maps/vt", 292000000, 132 ],
									2,
									500,
									[
											null,
											"http://g0.gstatic.com/landmark/tour",
											"http://g0.gstatic.com/landmark/config",
											"",
											"http://www.google.com/maps/preview/log204",
											"",
											"http://static.panoramio.com.storage.googleapis.com/photos/",
											[ "http://geo0.ggpht.com/cbk",
													"http://geo1.ggpht.com/cbk",
													"http://geo2.ggpht.com/cbk",
													"http://geo3.ggpht.com/cbk" ] ],
									[
											"https://www.google.com/maps/api/js/master?pb=!1m2!1u19!2s10!2sen-US!3sUS!4s19/10",
											"https://www.google.com/maps/api/js/widget?pb=!1m2!1u19!2s10!2sen-US" ],
									null, 0, 0 ], loadScriptTime);
				};
				var loadScriptTime = (new Date).getTime();
				getScript("http://maps.gstatic.com/maps-api-v3/api/js/19/10/main.js");
			})();
		</script>
		
		<script src="javascript/main.js"></script>
		<!-- END LOADING GOOGLE MAPS-->

		<script type="text/javascript" charset="UTF-8"
			src="javascript/common.js"></script>
		<script type="text/javascript" charset="UTF-8" src="javascript/util.js"></script>
		<script type="text/javascript" charset="UTF-8" src="javascript/stats.js"></script>
	</head>

	<body class="">
		<div class="container-fluid home-template min-width">
			<c:if test="${not empty content.headerContent}">
				<div class="row row-no-padding">
					<div class="col-md-12 col-no-padding">
						<div class="fixed-nav">
							<c:forEach var="element" items="${content.headerContent}">
								<dsp:renderContentItem contentItem="${element}" />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
			
			
			
			
			
			

			<jsp:doBody />

			<div id="emailSignUpModal" class="modal fade" tabindex="-1"
				role="dialog" aria-labelledby="basicModal" aria-hidden="true"
				style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content sharp-border email-sign-up-overlay">
						<span data-dismiss="modal" class="sprite-icon-x-close"></span>
						<header>Signup Today</header>
						<div class="email-sign-up-form">
							<p>Receive special offers, sales alerts and coupons.</p>
							<div class="row email-input-group">
								<p>email address</p>
								<div>
									<input type="text">
									<button id="submitEmail">submit</button>
								</div>
								<a
									href="http://cloud.toysrus.resource.com/redesign/template-home-main.html#">privacy
									policy</a>
							</div>
							<div class="email-checkbox-group">
								<span class="checkbox-sm-off"></span><span class="">Don't
									show me this message again.</span>
							</div>
						</div>
						<div class="email-sign-up-thanks">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</div>
			</div>






			<dsp:include page="/common/mainFooter.jsp" />
			<div class="oo_feedback_float">
				<img src="/tru/images/oo_float_icon.gif" />
				<div class="olUp" tabindex="0">Feedback</div>
				<div class="olOver">
					<span>Click here to<br>rate this page
					</span>
				</div>
			</div>


			<script type="text/javascript" charset="UTF-8"
				src="javascript/vendor.js"></script>
			<script type="text/javascript" charset="UTF-8"
				src="javascript/toolkit.js"></script>
			<script type="text/javascript" charset="UTF-8"
				src="javascript/jquery.validate.min.js"></script>
			<script type="text/javascript" charset="UTF-8"
				src="javascript/pwstrength.js"></script>
			<script type="text/javascript" charset="UTF-8"
				src="javascript/tRus_custom.js"></script>
		</div>

	</body>
</html>