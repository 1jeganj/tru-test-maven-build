<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<style>
	.textFont {
		font-family: "Times New Roman", Georgia, Serif;
		font-size: 13px;
	}
	
	.bold {
		font-weight: bold;
	}
	
	.red {
		color: red;
	}
	</style>
	
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="priority"/>
		<dsp:oparam name="Priority2">
			<p><dsp:valueof param="messageContent" /></p>
			<br />
			<li><b>Priority : </b><span style="color:red"> <dsp:valueof param="priority" /></span><br/></li>
			<li><b>OS Host Name : </span></b> <dsp:valueof param="OSHostName"/><br/></li>
			<li><b>Detail Exception :</b> <dsp:valueof param="detailException" /><br/></li>	
			<br />
			<br />
		</dsp:oparam>
		<dsp:oparam name="sequenceMiss">
			<p><dsp:valueof param="messageContent" /></p>
			<br />
			<li><b>Priority : </b><span style="color:red"> Priority2 </span><br/></li>
			<li><b>OS Host Name : </span></b> <dsp:valueof param="OSHostName"/><br/></li>
			<li><b>Detail Exception :</b> <dsp:valueof param="detailException" /><br/></li>	
			<br />
			<br />
		</dsp:oparam>
		<dsp:oparam name="fileInvalid">
			<p><dsp:valueof param="messageContent" /></p>
			<br />
			<li><b>Priority : </b><span style="color:red"> Priority2 </span><br/></li>
			<li><b>OS Host Name : </span></b> <dsp:valueof param="OSHostName"/><br/></li>
			<li><b>Detail Exception :</b> <dsp:valueof param="detailException" /><br/></li>	
			<br />
			<br />
		</dsp:oparam>
		<dsp:oparam name="Priority3">
			<p><dsp:valueof param="messageSubject" /></p>	
			<br />
			<table border="1">
			<tr>
				<td>Date & Time</td>
				<td>Total Records Received</td>
				<td>Total Records Processed Successfully</td>
				<td>Total Records Failed To Process</td>
			</tr>
			<tr>
				<td><dsp:valueof
				bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
					<td><dsp:valueof param="totalRecordCount"/></td>
					<td><dsp:valueof param="successRecordCount"/></td>
					<td><dsp:valueof param="totalFailedRecordCount"/></td>
			</tr>
			</table>
			<li><b>Priority : </b><span style="color:red"> <dsp:valueof param="priority" /></span><br/></li>
			<li><b>OS Host Name : </span></b> <dsp:valueof param="OSHostName"/><br/></li>
			<li><b>Detail Exception :</b> <dsp:valueof param="detailException" /><br/></li>		
			<br />
			<br />
		</dsp:oparam>
		<dsp:oparam name="default"> 
			<p><dsp:valueof param="messageSubject" /></p>	
			<br />
			<table border="1">
			<tr>
				<td>Date & Time</td>
				<td>Total Records Received</td>
				<td>Total Records Processed Successfully</td>
				<td>Total Records Failed To Process</td>
			</tr>
			<tr>
				<td><dsp:valueof
				bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
					<td><dsp:valueof param="totalRecordCount"/></td>
					<td><dsp:valueof param="successRecordCount"/></td>
					<td><dsp:valueof param="totalFailedRecordCount"/></td>
			</tr>
			</table>
			<br />
			<br />
		</dsp:oparam>
	</dsp:droplet>
			
</dsp:page>