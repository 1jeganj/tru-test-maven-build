<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
	<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	font-family: "Times New Roman", Georgia, Serif;
	font-size: 13px;
}

.bold {
	font-weight: bold;
}

.red {
	color: red;
}
</style>
	<p>Hi,</p>
		
		<p><dsp:valueof param="messageSubject" /></p>
		<dsp:getvalueof var="tenderPromos" param="TenderList" vartype="java.util.List" /> 
		<dsp:getvalueof var="promotions" param="PromotionList" vartype="java.util.List" /> 
		<table border="1">
			<tr>
				<td>Date</td>
				<td>Promotion ID</td>
				<td>Promotion Type</td>
				<td>Promotion Description</td>
				<td>Discount Till Date</td>
				<td>Capital Amount</td>
			</tr>

		<c:forEach var="promotion" items="${tenderPromos}">
			<tr>
				<td><dsp:valueof bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
				<td><dsp:valueof value="${promotion.promotionId}" /></td>
				<td><dsp:valueof value="${promotion.promotionType}" /></td>
				<td><dsp:valueof value="${promotion.promotionDescription}" /></td>
				<td><dsp:valueof value="${promotion.totalDiscountAmountTillDate}" /></td>
				<td><dsp:valueof value="${promotion.capitalAmount}" /></td>
			</tr>
		</c:forEach>
		<c:forEach var="promotion" items="${promotions}">
			<tr>
				<td><dsp:valueof bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
				<td><dsp:valueof value="${promotion.id}"/></td>
				<c:if test="${promotion.type >= 0 && promotion.type <=3}">
				<td>Item Discount</td>
				</c:if>
				<c:if test="${promotion.type >= 5 && promotion.type <=8}">
				<td>Shipping Discount</td>
				</c:if>
				<c:if test="${promotion.type >= 9 && promotion.type <=12}">
				<td>Order Discount</td>
				</c:if> 
				<td><dsp:valueof value="${promotion.description}"/></td>
				<td>0</td>
				<td><dsp:valueof value="${promotion.tenderAmountLimit}"/></td>
			</tr>
		</c:forEach>
		</table>
			
	<br />
	<br />
	<span class="textFont"> Regards,</span>
	<br />
	<span class="textFont">TRU e-Commerce Platform</span>

</body>
	</html>
</dsp:page>