<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<style>
.textFont {
	font-family: "Times New Roman", Georgia, Serif;
	font-size: 13px;
}

.bold {
	font-weight: bold;
}

.red {
	color: red;
}
</style>
		<p>Hi,</p>
			<b>Power Reviews Feed Email: </b><br /><br />
			<li><b>Mail priority  :</b> <dsp:valueof value="Normal" /><br/></li>
		  		<li><b>User  :</b> <dsp:valueof value="wladmin" /><br/></li>
		  		<li><b>OS Host Name : </b> <dsp:valueof bean="/atg/dynamo/service/ServerName.serverName"/><br/></li>
		  		<li><b>Time-stamp :</b> <dsp:valueof bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /><br/></li>
		  		<li><b style="color:red">Exception :</b> <dsp:valueof param="message" /><br/></li>
		  		
			<br /><br /><span class="textFont"> Regards,</span>
	<br />
	<span class="textFont">TRU e-Commerce Platform</span>
		
</dsp:page>