<%@ taglib prefix="dsp"
	uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1"%>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<html>
<head>
</head>
<body>
	<style>
.textFont {
	font-family: "Times New Roman", Georgia, Serif;
	font-size: 13px;
}

.bold {
	font-weight: bold;
}

.red {
	color: red;
}
</style>
		<p><dsp:valueof param="messageContent"/></p></br>
		<table border="1">
			<tr>
				<td>Date & Time</td>
				<td>Total Records Received</td>
				<td>Total Records Processed Successfully</td>
			</tr>
			<tr>
				<td><dsp:valueof
			bean="/atg/dynamo/service/CurrentDate.timeAsTimeStamp" /></td>
				<td><dsp:valueof param="totalRecordCount"/></td>
				<td><dsp:valueof param="successRecordCount"/></td>
			</tr>
		</table>
			
	<br />
	<br />
	
</body>
	</html>
</dsp:page>