--drop table TRU_RESTRICTED_ZIPCODES;

--drop table TRU_SKU_RESTRICTIONS;

CREATE TABLE TRU_SKU_RESTRICTIONS (
	ASSET_VERSION	number(19)	not null,
	WORKSPACE_ID	varchar2(40)	not null,
	BRANCH_ID	varchar2(40)	not null,
	IS_HEAD	number(1,0)	not null,
	VERSION_DELETED	number(1,0)	not null,
	VERSION_EDITABLE number(1,0)	not null,
	PRED_VERSION	number(19,0)	,
	CHECKIN_DATE timestamp (6), 	
	SKU_ID 			varchar2(254)	NOT NULL,
	ERROR_MESSAGE 		CLOB	NOT NULL,
	PRIMARY KEY(SKU_ID,ASSET_VERSION)
);

CREATE TABLE TRU_RESTRICTED_ZIPCODES (
	ASSET_VERSION	number(19)	not null,
	SKU_ID 			varchar2(254)	NOT NULL ,
	ZIP_CODE 		varchar2(254)	NOT NULL,
	PRIMARY KEY(SKU_ID, ZIP_CODE,ASSET_VERSION)
);
