CREATE TABLE tru_gwp_sku ( promotion_id varchar2(254) NOT NULL, asset_version INTEGER NOT NULL,
sequence_num INTEGER NOT NULL, sku_id varchar2(254) NULL, PRIMARY KEY(promotion_id, asset_version, sequence_num) ); 

CREATE INDEX tru_gwp_sku_promotion_idx ON tru_gwp_sku(promotion_id, asset_version, sequence_num);
