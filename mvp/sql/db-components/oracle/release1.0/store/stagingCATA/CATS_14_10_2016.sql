-- drop table tru_clf_crsref_disp_order;

-- drop table tru_clf_crsref_disp_name;

CREATE TABLE tru_clf_crsref_disp_name (
	category_id 		varchar2(40)	NOT NULL REFERENCES dcs_category(category_id),
	parent_cat_id 		varchar2(40)	NOT NULL,
	display_name 		varchar2(255)	NULL,
	PRIMARY KEY(category_id, parent_cat_id) using index (
	CREATE INDEX tru_clf_crref_dispname_cat_idx ON tru_clf_crsref_disp_name(category_id, parent_cat_id))
);

CREATE TABLE tru_clf_crsref_disp_order (
	category_id 		varchar2(40)	NOT NULL REFERENCES dcs_category(category_id),
	parent_cat_id 		varchar2(40)	NOT NULL,
	display_order 		varchar2(40)	NULL,
	PRIMARY KEY(category_id, parent_cat_id)  using index (
	CREATE INDEX tru_clf_crref_dispord_cat_idx ON tru_clf_crsref_disp_order(category_id, parent_cat_id))
);

alter table tru_sku add primary_path_category varchar2(40) NULL;