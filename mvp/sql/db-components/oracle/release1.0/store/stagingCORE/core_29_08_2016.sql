CREATE TABLE tru_order_dis_share (
	amount_info_id 		varchar2(254)	NOT NULL REFERENCES dcspp_amount_info(amount_info_id),
	discount_share_id 	varchar2(254)	NOT NULL,
	order_discount_share 	number(28, 20)	NULL,
	PRIMARY KEY(amount_info_id, discount_share_id)
);

CREATE TABLE tru_item_dis_share (
	amount_info_id 		varchar2(254)	NOT NULL REFERENCES dcspp_amount_info(amount_info_id),
	discount_share_id 	varchar2(254)	NOT NULL,
	item_discount_share 	number(28, 20)	NULL,
	PRIMARY KEY(amount_info_id, discount_share_id)
);