CREATE TABLE tru_contactus_audit (
	id 			varchar2(254)	NOT NULL,
	customer_name 		varchar2(254)	NULL,
	telephone 		varchar2(254)	NULL,
	subject 		varchar2(254)	NULL,
	customer_email 		varchar2(254)	NULL,
	order_id 		varchar2(254)	NULL,
	comments 		varchar2(254)	NULL,
	PRIMARY KEY(id)
);