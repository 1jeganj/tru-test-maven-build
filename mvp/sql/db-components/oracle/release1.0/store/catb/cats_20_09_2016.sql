drop table tru_sku_elgible_GWPPromos;
CREATE TABLE tru_sku_elgible_GWPPromos (
	sku_id 			varchar2(254)	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	GWP_promotion_data 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, sequence_num));

drop table tru_sku_qualified_promos;
create table tru_sku_qualified_promos (
	sku_id	varchar2(40)	not null,
	sequence_num	integer	not null,
	promotion_data	varchar2(1000)	not null
,constraint tru_sku_qualified_promos_p primary key (sku_id,sequence_num));