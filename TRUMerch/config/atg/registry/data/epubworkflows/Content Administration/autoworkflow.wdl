<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE process SYSTEM "dynamosystemresource:/atg/dtds/pdl/pdl_1.0.dtd">
<process author="admin" creation-time="1361372390102" enabled="true" last-modified-by="admin" modification-time="1384184318389">
  <segment migrate-from="1361434335779,1361434640753,1361787174962,1384180649459,1384182068297,1384183727780,1384184041040,1384184142586" migrate-subjects="true">
    <segment-name>main</segment-name>
    <!--================================-->
    <!--== startWorkflow  -->
    <!--================================-->
    <event id="1">
      <event-name>atg.workflow.StartWorkflow</event-name>
      <filter operator="eq">
        <event-property>
          <property-name>processName</property-name>
        </event-property>
        <constant>/Content Administration/autoworkflow.wdl</constant>
      </filter>
      <filter operator="eq">
        <event-property>
          <property-name>segmentName</property-name>
        </event-property>
        <constant>main</constant>
      </filter>
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>startWorkflow</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Auto WorkFlow</constant>
        </attribute>
      </attributes>
    </event>
    <!--================================-->
    <!--== Create project without a workflow and process' project name  -->
    <!--================================-->
    <action id="2">
      <action-name>createProjectForProcess</action-name>
    </action>
    <!--================================-->
    <!--== author  -->
    <!--================================-->
    <label id="3">
      <attributes>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute;Admin$role$administrators-group:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute;Profile$role$epubAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Create and modify assets for eventual deployment</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>author</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>author.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>author.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Author</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="4">
      <branch id="4.1">
        <!--================================-->
        <!--== approve and deploy project to staging  -->
        <!--================================-->
        <event id="4.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Content Administration/autoworkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>approve and deploy project to staging</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>approve and deploy project to staging</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>approve and deploy project to staging</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Change Project's Current project : Editable to false  -->
        <!--================================-->
        <action id="4.1.2">
          <action-name construct="modify-action">modify</action-name>
          <action-param name="modified">
            <subject-property>
              <property-name>project</property-name>
              <property-name>editable</property-name>
            </subject-property>
          </action-param>
          <action-param name="operator">
            <constant>assign</constant>
          </action-param>
          <action-param name="modifier">
            <constant type="java.lang.Boolean">false</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check assets are up to date  -->
        <!--================================-->
        <action id="4.1.3">
          <action-name>assetsUpToDate</action-name>
        </action>
        <!--================================-->
        <!--== Approve and deploy project to target Staging  -->
        <!--================================-->
        <action id="4.1.4">
          <action-name>approveAndDeployProject</action-name>
          <action-param name="target">
            <constant>Staging</constant>
          </action-param>
        </action>
      </branch>
      <branch id="4.2">
        <!--================================-->
        <!--== delete  -->
        <!--================================-->
        <event id="4.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Content Administration/autoworkflow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Delete Project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>delete</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.delete.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.delete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Delete Project</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Delete project  -->
        <!--================================-->
        <action id="4.2.2">
          <action-name>deleteProject</action-name>
        </action>
      </branch>
    </fork>
    <!--================================-->
    <!--== waitForDeploymentToComplete  -->
    <!--================================-->
    <label id="5">
      <attributes>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">false</constant>
        </attribute>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Admin$role$administrators-group:write,execute;Profile$role$epubAdmin:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Wait for deployment to complete on Staging target</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>waitForDeploymentToComplete</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>waitForStagingDeploymentToComplete.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>waitForStagingDeploymentToComplete.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Wait for Staging Deployment Completion</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="6">
      <branch id="6.1">
        <!--================================-->
        <!--== Wait for deployment to complete on target Staging  -->
        <!--================================-->
        <event id="6.1.1">
          <event-name>atg.deployment.DeploymentStatus</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>targetId</property-name>
            </event-property>
            <constant>Staging</constant>
          </filter>
        </event>
        <fork id="6.1.2">
          <branch id="6.1.2.1">
            <!--================================-->
            <!--== Deployment completed event status is success on target Staging  -->
            <!--================================-->
            <condition id="6.1.2.1.1">
              <filter operator="deploymentCompleted">
                <constant>1</constant>
                <constant>Staging</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Approve and deploy project to target Production  -->
            <!--================================-->
            <action id="6.1.2.1.2">
              <action-name>approveAndDeployProject</action-name>
              <action-param name="target">
                <constant>Production</constant>
              </action-param>
            </action>
          </branch>
          <branch id="6.1.2.2">
            <!--================================-->
            <!--== Deployment completed event status is failure on target Staging  -->
            <!--================================-->
            <condition id="6.1.2.2.1">
              <filter operator="deploymentCompleted">
                <constant>0</constant>
                <constant>Staging</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Release asset locks  -->
            <!--================================-->
            <action id="6.1.2.2.2">
              <action-name>releaseAssetLocks</action-name>
            </action>
            <!--================================-->
            <!--== Reopen project  -->
            <!--================================-->
            <action id="6.1.2.2.3">
              <action-name>reopenProject</action-name>
            </action>
            <jump id="6.1.2.2.4" target="3"/>
          </branch>
        </fork>
        <!--================================-->
        <!--== waitForDeploymentToComplete  -->
        <!--================================-->
        <label id="6.1.3">
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>task</constant>
            </attribute>
            <attribute name="atg.workflow.assignable">
              <constant type="java.lang.Boolean">false</constant>
            </attribute>
            <attribute name="atg.workflow.acl">
              <constant>Profile$role$epubAdmin:write,execute;Profile$role$epubSuperAdmin:write,execute;Admin$role$administrators-group:write,execute</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Wait for deployment to complete on Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>waitForDeploymentToComplete</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Wait for Production Deployment Completion</constant>
            </attribute>
          </attributes>
        </label>
        <fork exclusive="true" id="6.1.4">
          <branch id="6.1.4.1">
            <!--================================-->
            <!--== Wait for deployment to complete on target Production  -->
            <!--================================-->
            <event id="6.1.4.1.1">
              <event-name>atg.deployment.DeploymentStatus</event-name>
              <filter operator="eq">
                <event-property>
                  <property-name>targetId</property-name>
                </event-property>
                <constant>Production</constant>
              </filter>
            </event>
            <fork id="6.1.4.1.2">
              <branch id="6.1.4.1.2.1">
                <!--================================-->
                <!--== Deployment completed event status is success on target Production  -->
                <!--================================-->
                <condition id="6.1.4.1.2.1.1">
                  <filter operator="deploymentCompleted">
                    <constant>1</constant>
                    <constant>Production</constant>
                  </filter>
                </condition>
                <!--================================-->
                <!--== Check in project's workspace  -->
                <!--================================-->
                <action id="6.1.4.1.2.1.2">
                  <action-name>checkInProject</action-name>
                </action>
                <!--================================-->
                <!--== Complete project  -->
                <!--================================-->
                <action id="6.1.4.1.2.1.3">
                  <action-name>completeProject</action-name>
                </action>
                <!--================================-->
                <!--== Complete process  -->
                <!--================================-->
                <action id="6.1.4.1.2.1.4">
                  <action-name>completeProcess</action-name>
                </action>
              </branch>
              <branch id="6.1.4.1.2.2">
                <!--================================-->
                <!--== Deployment completed event status is failure on target Production  -->
                <!--================================-->
                <condition id="6.1.4.1.2.2.1">
                  <filter operator="deploymentCompleted">
                    <constant>0</constant>
                    <constant>Production</constant>
                  </filter>
                </condition>
                <!--================================-->
                <!--== Revert Assets from Staging / Retry Production deployment  -->
                <!--================================-->
                <label id="6.1.4.1.2.2.2">
                  <attributes>
                    <attribute name="atg.workflow.elementType">
                      <constant>task</constant>
                    </attribute>
                    <attribute name="atg.workflow.assignable">
                      <constant type="java.lang.Boolean">false</constant>
                    </attribute>
                    <attribute name="atg.workflow.description">
                      <constant>Revert Assets from Staging / Retry Production deployment</constant>
                    </attribute>
                    <attribute name="atg.workflow.name">
                      <constant>Revert Assets from Staging / Retry Production deployment</constant>
                    </attribute>
                    <attribute name="atg.workflow.displayName">
                      <constant>Revert Assets from Staging / Retry Production deployment</constant>
                    </attribute>
                  </attributes>
                </label>
                <fork exclusive="true" id="6.1.4.1.2.2.3">
                  <branch id="6.1.4.1.2.2.3.1">
                    <!--================================-->
                    <!--== revertAssetsOnStagingNow  -->
                    <!--================================-->
                    <event id="6.1.4.1.2.2.3.1.1">
                      <event-name>atg.workflow.TaskOutcome</event-name>
                      <filter operator="eq">
                        <event-property>
                          <property-name>processName</property-name>
                        </event-property>
                        <constant>/Content Administration/autoworkflow.wdl</constant>
                      </filter>
                      <filter operator="eq">
                        <event-property>
                          <property-name>segmentName</property-name>
                        </event-property>
                        <constant>main</constant>
                      </filter>
                      <filter operator="eq">
                        <event-property>
                          <property-name>outcomeElementId</property-name>
                        </event-property>
                        <constant>6.1.4.1.2.2.3.1.1</constant>
                      </filter>
                      <attributes>
                        <attribute name="atg.workflow.elementType">
                          <constant>outcome</constant>
                        </attribute>
                        <attribute name="atg.workflow.description">
                          <constant>Revert assets from Staging target</constant>
                        </attribute>
                        <attribute name="atg.workflow.name">
                          <constant>revertAssetsOnStagingNow</constant>
                        </attribute>
                        <attribute name="atg.workflow.descriptionResource">
                          <constant>verifyStaging.revertAssetsOnStagingNow.description</constant>
                        </attribute>
                        <attribute name="atg.workflow.displayNameResource">
                          <constant>verifyStaging.revertAssetsOnStagingNow.displayName</constant>
                        </attribute>
                        <attribute name="atg.workflow.displayName">
                          <constant>Revert Assets on Staging Immediately</constant>
                        </attribute>
                      </attributes>
                    </event>
                    <!--================================-->
                    <!--== Revert assets immediately on target Staging  -->
                    <!--================================-->
                    <action id="6.1.4.1.2.2.3.1.2">
                      <action-name>revertAssetsOnTargetNow</action-name>
                      <action-param name="target">
                        <constant>Staging</constant>
                      </action-param>
                    </action>
                    <!--================================-->
                    <!--== Wait for Revert Deployment To Complete  -->
                    <!--================================-->
                    <label id="6.1.4.1.2.2.3.1.3">
                      <attributes>
                        <attribute name="atg.workflow.elementType">
                          <constant>task</constant>
                        </attribute>
                        <attribute name="atg.workflow.assignable">
                          <constant type="java.lang.Boolean">false</constant>
                        </attribute>
                        <attribute name="atg.workflow.description">
                          <constant>Wait for Revert Deployment To Complete</constant>
                        </attribute>
                        <attribute name="atg.workflow.name">
                          <constant>Wait for Revert Deployment To Complete</constant>
                        </attribute>
                        <attribute name="atg.workflow.displayName">
                          <constant>Wait for Revert Deployment To Complete</constant>
                        </attribute>
                      </attributes>
                    </label>
                    <fork exclusive="true" id="6.1.4.1.2.2.3.1.4">
                      <branch id="6.1.4.1.2.2.3.1.4.1">
                        <!--================================-->
                        <!--== Wait for deployment to complete on target Staging  -->
                        <!--================================-->
                        <event id="6.1.4.1.2.2.3.1.4.1.1">
                          <event-name>atg.deployment.DeploymentStatus</event-name>
                          <filter operator="eq">
                            <event-property>
                              <property-name>targetId</property-name>
                            </event-property>
                            <constant>Staging</constant>
                          </filter>
                        </event>
                      </branch>
                    </fork>
                  </branch>
                  <branch id="6.1.4.1.2.2.3.2">
                    <!--================================-->
                    <!--== retryDeploymentOnProductionNow  -->
                    <!--================================-->
                    <event id="6.1.4.1.2.2.3.2.1">
                      <event-name>atg.workflow.TaskOutcome</event-name>
                      <filter operator="eq">
                        <event-property>
                          <property-name>processName</property-name>
                        </event-property>
                        <constant>/Content Administration/autoworkflow.wdl</constant>
                      </filter>
                      <filter operator="eq">
                        <event-property>
                          <property-name>segmentName</property-name>
                        </event-property>
                        <constant>main</constant>
                      </filter>
                      <filter operator="eq">
                        <event-property>
                          <property-name>outcomeElementId</property-name>
                        </event-property>
                        <constant>6.1.4.1.2.2.3.2.1</constant>
                      </filter>
                      <attributes>
                        <attribute name="atg.workflow.elementType">
                          <constant>outcome</constant>
                        </attribute>
                        <attribute name="atg.workflow.description">
                          <constant>Retry deployment on production.</constant>
                        </attribute>
                        <attribute name="atg.workflow.name">
                          <constant>retryDeploymentOnProductionNow</constant>
                        </attribute>
                        <attribute name="atg.workflow.displayName">
                          <constant>Retry deployment on production.</constant>
                        </attribute>
                      </attributes>
                    </event>
                    <!--================================-->
                    <!--== Approve and deploy project to target Production  -->
                    <!--================================-->
                    <action id="6.1.4.1.2.2.3.2.2">
                      <action-name>approveAndDeployProject</action-name>
                      <action-param name="target">
                        <constant>Production</constant>
                      </action-param>
                    </action>
                    <jump id="6.1.4.1.2.2.3.2.3" target="6.1.3"/>
                  </branch>
                </fork>
                <fork id="6.1.4.1.2.2.4">
                  <branch id="6.1.4.1.2.2.4.1">
                    <!--================================-->
                    <!--== Deployment completed event status is success on target Staging  -->
                    <!--================================-->
                    <condition id="6.1.4.1.2.2.4.1.1">
                      <filter operator="deploymentCompleted">
                        <constant>1</constant>
                        <constant>Staging</constant>
                      </filter>
                    </condition>
                    <!--================================-->
                    <!--== Release asset locks  -->
                    <!--================================-->
                    <action id="6.1.4.1.2.2.4.1.2">
                      <action-name>releaseAssetLocks</action-name>
                    </action>
                    <!--================================-->
                    <!--== Reopen project  -->
                    <!--================================-->
                    <action id="6.1.4.1.2.2.4.1.3">
                      <action-name>reopenProject</action-name>
                    </action>
                    <jump id="6.1.4.1.2.2.4.1.4" target="3"/>
                  </branch>
                  <branch id="6.1.4.1.2.2.4.2">
                    <!--================================-->
                    <!--== Deployment completed event status is failure on target Staging  -->
                    <!--================================-->
                    <condition id="6.1.4.1.2.2.4.2.1">
                      <filter operator="deploymentCompleted">
                        <constant>0</constant>
                        <constant>Staging</constant>
                      </filter>
                    </condition>
                    <jump id="6.1.4.1.2.2.4.2.2" target="6.1.4.1.2.2.2"/>
                  </branch>
                </fork>
              </branch>
            </fork>
          </branch>
        </fork>
      </branch>
    </fork>
  </segment>
</process>
