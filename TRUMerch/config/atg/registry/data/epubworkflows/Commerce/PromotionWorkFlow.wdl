<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE process SYSTEM "dynamosystemresource:/atg/dtds/pdl/pdl_1.0.dtd">
<process author="admin" creation-time="1472650808002" enabled="true" last-modified-by="admin" modification-time="1472822796154">
  <segment migrate-from="1472650808031,1472650901491,1472651284185" migrate-subjects="true">
    <segment-name>main</segment-name>
    <!--================================-->
    <!--== startWorkflow  -->
    <!--================================-->
    <event id="1">
      <event-name>atg.workflow.StartWorkflow</event-name>
      <filter operator="eq">
        <event-property>
          <property-name>processName</property-name>
        </event-property>
        <constant>/Commerce/PromotionWorkFlow.wdl</constant>
      </filter>
      <filter operator="eq">
        <event-property>
          <property-name>segmentName</property-name>
        </event-property>
        <constant>main</constant>
      </filter>
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>startWorkflow</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>PromotionWorkFlowTest</constant>
        </attribute>
      </attributes>
    </event>
    <!--================================-->
    <!--== Create project without a workflow and process' project name  -->
    <!--================================-->
    <action id="2">
      <action-name>createProjectForProcess</action-name>
    </action>
    <!--================================-->
    <!--== author  -->
    <!--================================-->
    <label id="3">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute;Admin$role$administrators-group:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute;Profile$role$epubAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Create and modify assets for eventual deployment</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>author</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>author.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>author.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Author</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="4">
      <branch id="4.1">
        <!--================================-->
        <!--== approveAndDeploy  -->
        <!--================================-->
        <event id="4.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Approve project for immediate deployment to Staging target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>approveAndDeploy</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>stagingApproval.approveAndDeploy.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>stagingApproval.approveAndDeploy.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Approve and Deploy to Staging</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Change Project's Current project : Editable to false  -->
        <!--================================-->
        <action id="4.1.2">
          <action-name construct="modify-action">modify</action-name>
          <action-param name="modified">
            <subject-property>
              <property-name>project</property-name>
              <property-name>editable</property-name>
            </subject-property>
          </action-param>
          <action-param name="operator">
            <constant>assign</constant>
          </action-param>
          <action-param name="modifier">
            <constant type="java.lang.Boolean">false</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check assets are up to date  -->
        <!--================================-->
        <action id="4.1.3">
          <action-name>assetsUpToDate</action-name>
        </action>
        <!--================================-->
        <!--== Approve and deploy project to target Staging  -->
        <!--================================-->
        <action id="4.1.4">
          <action-name>approveAndDeployProject</action-name>
          <action-param name="target">
            <constant>Staging</constant>
          </action-param>
        </action>
      </branch>
      <branch id="4.2">
        <!--================================-->
        <!--== delete  -->
        <!--================================-->
        <event id="4.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>4.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Delete the project</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>delete</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>author.delete.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>author.delete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Delete Project</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Delete project  -->
        <!--================================-->
        <action id="4.2.2">
          <action-name>deleteProject</action-name>
        </action>
      </branch>
    </fork>
    <!--================================-->
    <!--== waitForDeploymentToComplete  -->
    <!--================================-->
    <label id="5">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">false</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Admin$role$administrators-group:write,execute;Profile$role$epubAdmin:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Wait for deployment to complete on Staging target</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>waitForDeploymentToComplete</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>waitForStagingDeploymentToComplete.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>waitForStagingDeploymentToComplete.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Wait for Staging Deployment Completion</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="6">
      <branch id="6.1">
        <!--================================-->
        <!--== Wait for deployment to complete on target Staging  -->
        <!--================================-->
        <event id="6.1.1">
          <event-name>atg.deployment.DeploymentStatus</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>targetId</property-name>
            </event-property>
            <constant>Staging</constant>
          </filter>
        </event>
        <fork id="6.1.2">
          <branch id="6.1.2.1">
            <!--================================-->
            <!--== Deployment completed event status is success on target Staging  -->
            <!--================================-->
            <condition id="6.1.2.1.1">
              <filter operator="deploymentCompleted">
                <constant>1</constant>
                <constant>Staging</constant>
              </filter>
            </condition>
          </branch>
          <branch id="6.1.2.2">
            <!--================================-->
            <!--== Deployment completed event status is failure on target Staging  -->
            <!--================================-->
            <condition id="6.1.2.2.1">
              <filter operator="deploymentCompleted">
                <constant>0</constant>
                <constant>Staging</constant>
              </filter>
            </condition>
            <!--================================-->
            <!--== Release asset locks  -->
            <!--================================-->
            <action id="6.1.2.2.2">
              <action-name>releaseAssetLocks</action-name>
            </action>
			<!--================================-->
			<!--== Reopen project  -->
			<!--================================-->
			<action id="6.1.2.2.3">
			  <action-name>reopenProject</action-name>
			</action>
            <jump id="6.1.2.2.4" target="3"/>
          </branch>
        </fork>
      </branch>
    </fork>
    <!--================================-->
    <!--== verifyStaging  -->
    <!--================================-->
    <label id="7">
      <attributes>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubSuperAdmin:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Verify the deployment to Staging was successful and that all deployed assets look and function appropriately</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>verifyStaging</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>verifyStaging.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>verifyStaging.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Verify Staging Deployment</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="8">
      <branch id="8.1">
        <!--================================-->
        <!--== accept  -->
        <!--================================-->
        <event id="8.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>8.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Approve and deploy project to target Production</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>accept</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>Approve and deploy project to target Production</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>Approve and deploy project to target Production</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Approve and deploy project to target Production</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Validate project is deployed on target Staging  -->
        <!--================================-->
        <action id="8.1.2">
          <action-name>validateProjectDeployed</action-name>
          <action-param name="target">
            <constant>Staging</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Approve and deploy project to target Production  -->
        <!--================================-->
        <action id="8.1.3">
          <action-name>approveAndDeployProject</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
      </branch>
      <branch id="8.2">
        <!--================================-->
        <!--== revertAssetsOnStagingNow  -->
        <!--================================-->
        <event id="8.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>8.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Revert assets from Staging target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>revertAssetsOnStagingNow</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>verifyStaging.revertAssetsOnStagingNow.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>verifyStaging.revertAssetsOnStagingNow.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Revert Assets on Staging Immediately</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Revert assets immediately on target Staging  -->
        <!--================================-->
        <action id="8.2.2">
          <action-name>revertAssetsOnTargetNow</action-name>
          <action-param name="target">
            <constant>Staging</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== waitForRevertDeploymentToComplete  -->
        <!--================================-->
        <label id="8.2.3">
          <attributes>
            <attribute name="atg.workflow.assignable">
              <constant type="java.lang.Boolean">false</constant>
            </attribute>
            <attribute name="atg.workflow.elementType">
              <constant>task</constant>
            </attribute>
            <attribute name="atg.workflow.acl">
              <constant>Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute;Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Wait for revert deployment to complete on Staging target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>waitForRevertDeploymentToComplete</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>waitForStagingRevertDeploymentToComplete.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>waitForStagingRevertDeploymentToComplete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Wait for Staging Revert Deployment Completion</constant>
            </attribute>
          </attributes>
        </label>
        <fork exclusive="true" id="8.2.4">
          <branch id="8.2.4.1">
            <!--================================-->
            <!--== Wait for deployment to complete on target Staging  -->
            <!--================================-->
            <event id="8.2.4.1.1">
              <event-name>atg.deployment.DeploymentStatus</event-name>
              <filter operator="eq">
                <event-property>
                  <property-name>targetId</property-name>
                </event-property>
                <constant>Staging</constant>
              </filter>
            </event>
            <fork id="8.2.4.1.2">
              <branch id="8.2.4.1.2.1">
                <!--================================-->
                <!--== Deployment completed event status is success on target Staging  -->
                <!--================================-->
                <condition id="8.2.4.1.2.1.1">
                  <filter operator="deploymentCompleted">
                    <constant>1</constant>
                    <constant>Staging</constant>
                  </filter>
                </condition>
              </branch>
              <branch id="8.2.4.1.2.2">
                <!--================================-->
                <!--== Deployment completed event status is failure on target Staging  -->
                <!--================================-->
                <condition id="8.2.4.1.2.2.1">
                  <filter operator="deploymentCompleted">
                    <constant>0</constant>
                    <constant>Staging</constant>
                  </filter>
                </condition>
                <jump id="8.2.4.1.2.2.2" target="7"/>
              </branch>
            </fork>
          </branch>
        </fork>
        <!--================================-->
        <!--== Reopen project  -->
        <!--================================-->
        <action id="8.2.5">
          <action-name>reopenProject</action-name>
        </action>
        <!--================================-->
        <!--== Release asset locks  -->
        <!--================================-->
        <action id="8.2.6">
          <action-name>releaseAssetLocks</action-name>
        </action>
        <jump id="8.2.7" target="3"/>
      </branch>
    </fork>
    <!--================================-->
    <!--== Wait for deployment to complete on target Production  -->
    <!--================================-->
    <event id="9">
      <event-name>atg.deployment.DeploymentStatus</event-name>
      <filter operator="eq">
        <event-property>
          <property-name>targetId</property-name>
        </event-property>
        <constant>Production</constant>
      </filter>
    </event>
    <fork id="10">
      <branch id="10.1">
        <!--================================-->
        <!--== Deployment completed event status is success on target Production  -->
        <!--================================-->
        <condition id="10.1.1">
          <filter operator="deploymentCompleted">
            <constant>1</constant>
            <constant>Production</constant>
          </filter>
        </condition>
      </branch>
      <branch id="10.2"/>
      <branch id="10.3">
        <!--================================-->
        <!--== Deployment completed event status is failure on target Production  -->
        <!--================================-->
        <condition id="10.3.1">
          <filter operator="deploymentCompleted">
            <constant>0</constant>
            <constant>Production</constant>
          </filter>
        </condition>
        <jump id="10.3.2" target="7"/>
      </branch>
    </fork>
    <!--================================-->
    <!--== verifyProduction  -->
    <!--================================-->
    <label id="11">
      <attributes>
        <attribute name="atg.workflow.elementType">
          <constant>task</constant>
        </attribute>
        <attribute name="atg.workflow.assignable">
          <constant type="java.lang.Boolean">true</constant>
        </attribute>
        <attribute name="atg.workflow.acl">
          <constant>Profile$role$epubUser:write,execute;Profile$role$epubManager:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
        </attribute>
        <attribute name="atg.workflow.description">
          <constant>Verify that the deployment to Production was successful and that all deployed assets look and function appropriately</constant>
        </attribute>
        <attribute name="atg.workflow.name">
          <constant>verifyProduction</constant>
        </attribute>
        <attribute name="atg.workflow.descriptionResource">
          <constant>verifyProduction.description</constant>
        </attribute>
        <attribute name="atg.workflow.displayNameResource">
          <constant>verifyProduction.displayName</constant>
        </attribute>
        <attribute name="atg.workflow.displayName">
          <constant>Verify Production Deployment</constant>
        </attribute>
      </attributes>
    </label>
    <fork exclusive="true" id="12">
      <branch id="12.1">
        <!--================================-->
        <!--== accept  -->
        <!--================================-->
        <event id="12.1.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>12.1.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Accept the deployment to Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>accept</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>verifyProduction.accept.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>verifyProduction.accept.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Accept Production Deployment</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Validate project is deployed on target Production  -->
        <!--================================-->
        <action id="12.1.2">
          <action-name>validateProjectDeployed</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== Check in project's workspace  -->
        <!--================================-->
        <action id="12.1.3">
          <action-name>checkInProject</action-name>
        </action>
        <!--================================-->
        <!--== Complete project  -->
        <!--================================-->
        <action id="12.1.4">
          <action-name>completeProject</action-name>
        </action>
        <!--================================-->
        <!--== Complete process  -->
        <!--================================-->
        <action id="12.1.5">
          <action-name>completeProcess</action-name>
        </action>
      </branch>
      <branch id="12.2">
        <!--================================-->
        <!--== revertAssetsOnlyOnProductionNow  -->
        <!--================================-->
        <event id="12.2.1">
          <event-name>atg.workflow.TaskOutcome</event-name>
          <filter operator="eq">
            <event-property>
              <property-name>processName</property-name>
            </event-property>
            <constant>/Commerce/PromotionWorkFlow.wdl</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>segmentName</property-name>
            </event-property>
            <constant>main</constant>
          </filter>
          <filter operator="eq">
            <event-property>
              <property-name>outcomeElementId</property-name>
            </event-property>
            <constant>12.2.1</constant>
          </filter>
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>outcome</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Revert assets from Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>revertAssetsOnlyOnProductionNow</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>verifyProduction.revertAssetsOnlyOnProductionNow.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>verifyProduction.revertAssetsOnlyOnProductionNow.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Revert Assets on Production Immediately</constant>
            </attribute>
          </attributes>
        </event>
        <!--================================-->
        <!--== Revert assets immediately on target Production  -->
        <!--================================-->
        <action id="12.2.2">
          <action-name>revertAssetsOnTargetNow</action-name>
          <action-param name="target">
            <constant>Production</constant>
          </action-param>
        </action>
        <!--================================-->
        <!--== waitForRevertDeploymentToComplete  -->
        <!--================================-->
        <label id="12.2.3">
          <attributes>
            <attribute name="atg.workflow.elementType">
              <constant>task</constant>
            </attribute>
            <attribute name="atg.workflow.assignable">
              <constant type="java.lang.Boolean">false</constant>
            </attribute>
            <attribute name="atg.workflow.acl">
              <constant>Profile$role$epubManager:write,execute;Profile$role$epubUser:write,execute;Admin$role$managers-group:write,execute;Profile$role$epubSuperAdmin:write,execute</constant>
            </attribute>
            <attribute name="atg.workflow.description">
              <constant>Wait for revert deployment to complete on Production target</constant>
            </attribute>
            <attribute name="atg.workflow.name">
              <constant>waitForRevertDeploymentToComplete</constant>
            </attribute>
            <attribute name="atg.workflow.descriptionResource">
              <constant>waitForRevertDeploymentToComplete.description</constant>
            </attribute>
            <attribute name="atg.workflow.displayNameResource">
              <constant>waitForRevertDeploymentToComplete.displayName</constant>
            </attribute>
            <attribute name="atg.workflow.displayName">
              <constant>Wait for Production Revert Deployment Completion</constant>
            </attribute>
          </attributes>
        </label>
        <fork exclusive="true" id="12.2.4">
          <branch id="12.2.4.1">
            <!--================================-->
            <!--== Wait for deployment to complete on target Production  -->
            <!--================================-->
            <event id="12.2.4.1.1">
              <event-name>atg.deployment.DeploymentStatus</event-name>
              <filter operator="eq">
                <event-property>
                  <property-name>targetId</property-name>
                </event-property>
                <constant>Production</constant>
              </filter>
            </event>
            <fork id="12.2.4.1.2">
              <branch id="12.2.4.1.2.1">
                <!--================================-->
                <!--== Deployment completed event status is success on target Production  -->
                <!--================================-->
                <condition id="12.2.4.1.2.1.1">
                  <filter operator="deploymentCompleted">
                    <constant>1</constant>
                    <constant>Production</constant>
                  </filter>
                </condition>
              </branch>
              <branch id="12.2.4.1.2.2">
                <!--================================-->
                <!--== Deployment completed event status is failure on target Production  -->
                <!--================================-->
                <condition id="12.2.4.1.2.2.1">
                  <filter operator="deploymentCompleted">
                    <constant>0</constant>
                    <constant>Production</constant>
                  </filter>
                </condition>
                <jump id="12.2.4.1.2.2.2" target="7"/>
              </branch>
            </fork>
          </branch>
        </fork>
        <jump id="12.2.5" target="7"/>
      </branch>
    </fork>
  </segment>
</process>
