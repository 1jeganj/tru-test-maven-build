package atg.remote.assetmanager.editor.service;

import java.util.HashMap;
import java.util.ResourceBundle;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.remote.assetmanager.editor.model.PropertyCategoryState;
import atg.remote.assetmanager.editor.model.PropertyState;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import atg.web.viewmapping.MappedItemView;



/**
 * This Class overridden to restrict coupon code length to 20 digits. 
 *
 */
public class TRUCouponAssetViewService extends CouponAssetViewService
{

	/**
	 * Holds resources
	 */
	public static final String ATG_REMOTE_COMMERCE_RESOURCES = "atg.remote.commerce.Resources";
	/**
	 * Holds id label
	 */
	public static final String MERCHANDISING_COUPON_ID_LABEL = "merchandising.coupon.idLabel";
	/**
	 * Input field max width
	 */
	public static final String INPUT_FIELD_MAX_WIDTH = "inputFieldMaxWidth";
	/**
	 *  Input field max length
	 */
	public static final String INPUT_FIELD_MAXLENGTH = "inputFieldMaxlength";
	/**
	 * Id
	 */
	public static final String ID = "$id";
	
	/**
	 * Integer TWENTY
	 */
	public static final int TWENTY =20;
	
	/**
	 * integr 360
	 */
	public static final int THREE_HUNDRED_SIXTY = 360;
	
	/**
	 * Holds Resource bundle
	 */
	private ResourceBundle mResourceBundle;

	/**
	 * Constructor
	 */
	public TRUCouponAssetViewService()
	{
		this.mResourceBundle = LayeredResourceBundle.getBundle(ATG_REMOTE_COMMERCE_RESOURCES, ServletUtil.getUserLocale());
	}

	/**
	 * This method is to restrict the coupon id length to 20 digits.
	 * @param pMappedItemView - Item View
	 * @param pEditorInfo - Editor Info
	 * @param pItem - Item
	 * @return categoryState the categoryState
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PropertyCategoryState createIdPropertyCategoryState(MappedItemView pMappedItemView, AssetEditorInfo pEditorInfo, RepositoryItem pItem)
	{
		if(isLoggingDebug()){
			logDebug("Entered into Class : TRUCouponAssetViewService method : createIdPropertyCategoryState ");
		}
		PropertyCategoryState categoryState = super.createIdPropertyCategoryState(pMappedItemView, pEditorInfo, pItem);
		if(isLoggingDebug()){
			vlogDebug("categoryState : {0}",categoryState);
		}
		for (PropertyState propertyState : categoryState.getPropertyStates()) {
			if (propertyState.getDescriptor().getPropertyName().equalsIgnoreCase(ID))
			{
				propertyState.getDescriptor().setDisplayName(ResourceUtils.getUserMsgResource(MERCHANDISING_COUPON_ID_LABEL, ATG_REMOTE_COMMERCE_RESOURCES, this.mResourceBundle));
				if (propertyState.getDescriptor().getAttributes() == null) {
					propertyState.getDescriptor().setAttributes(new HashMap());
				}
				propertyState.getDescriptor().getAttributes().put(INPUT_FIELD_MAXLENGTH, Integer.valueOf(TWENTY));
				propertyState.getDescriptor().getAttributes().put(INPUT_FIELD_MAX_WIDTH, Integer.valueOf(THREE_HUNDRED_SIXTY));
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCouponAssetViewService method : createIdPropertyCategoryState ");
		}
		return categoryState;
	}
}