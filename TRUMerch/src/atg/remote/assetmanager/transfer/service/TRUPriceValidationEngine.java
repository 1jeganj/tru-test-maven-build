package atg.remote.assetmanager.transfer.service;

import java.util.HashMap;
import java.util.List;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.remote.assetmanager.AssetManagerResources;
import atg.remote.assetmanager.transfer.model.AssetImportState;
import atg.remote.assetmanager.transfer.model.AssetsValidationResultSate;
import atg.remote.assetmanager.transfer.model.PropertyImportState;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;

import com.tru.merchandising.constants.TRUMerchConstants;

/**
 * This Class overridden Price validation engine and customize asset import
 * states to support Price Item update flows.
 * 
 */
public class TRUPriceValidationEngine extends PriceValidationEngine {

	/** The m validation engine. */
	private ValidationEngine mValidationEngine;

	@Override
	public void validateAssets(int pNextAssets) {
		getValidationEngine().setAssetImportStates(getAssetImportStates());
		getValidationEngine().setFileOperation(getFileOperation());
		getValidationEngine().setValidationResultSate(getValidationResultSate());
		getValidationEngine().setValidationState(getValidationState());
		getValidationEngine().validateAssets(pNextAssets);
		getValidationEngine().getValidationResultSate().clear();

		List<AssetImportState> assetImportStates = getValidationEngine().getAssetImportStates();
		AssetsValidationResultSate validationState = getValidationEngine().getValidationResultSate();
		String priceListPropertyName = getPriceListManager().getPriceListPropertyName();
		String skuIdPropertyName = getPriceListManager().getSkuIdPropertyName();
		String productIdPropertyName = getPriceListManager().getProductIdPropertyName();
		String startDatePropertyName = getPriceListManager().getPriceStartDatePropertyName();
		String endDatePropertyName = getPriceListManager().getPriceEndDatePropertyName();
		HashMap regularPriceCountMap = new HashMap();
		for (AssetImportState assetImportState : assetImportStates) {
			boolean isRegularPriceItemFound = false;
			if (assetImportState.getValidateResult().equals(ValidationResult.SUCCESS)) {
				String pricelistId = getImportPropertyValue(assetImportState, priceListPropertyName);
				String skuId = getImportPropertyValue(assetImportState, skuIdPropertyName);
				String productId = getImportPropertyValue(assetImportState, productIdPropertyName);
				String startDate = getImportPropertyValue(assetImportState, startDatePropertyName);
				String endDate = getImportPropertyValue(assetImportState, endDatePropertyName);
				String priceItemId = getImportPropertyValue(assetImportState, PRICELIST_ID_PROPERTY_NAME);
				try {
					RepositoryItem priceList = getPriceListManager().getPriceList(pricelistId);
					if ((startDate != null) && (endDate != null)) {
						validateDateFormat(assetImportState, validationState, startDate, endDate);
					}
					boolean isRegularPricePresent = false;
					List<RepositoryItem> priceItems = getPriceListManager().getSkuPrices(priceList, productId, skuId);
					if ((priceItems != null) && (startDate == null) && (endDate == null)) {
						for (RepositoryItem priceItem : priceItems) {
							if ((priceItem.getPropertyValue(startDatePropertyName) == null) && (priceItem.getPropertyValue(endDatePropertyName) == null)) {
								isRegularPricePresent = true;
								if ((assetImportState.getModified())) {
									isRegularPriceItemFound = false;
									isRegularPricePresent = false;
								} else if (assetImportState.getIsNew()) {
									isRegularPriceItemFound = false;
									isRegularPricePresent = false;
									assetImportState.setModified(true);
									assetImportState.setIsNew(false);
									PropertyImportState property = (PropertyImportState) assetImportState.getPropertyImportStates().get(
											PRICELIST_ID_PROPERTY_NAME);
									property.setValue(priceItem.getRepositoryId());
								}
							}
						}
					}

					if ((!(isRegularPricePresent)) || (priceItems == null)) {
						String priceItemMapKey = null;
						if ((startDate == null) && (endDate == null)) {
							if (skuId != null) {
								priceItemMapKey = skuId + KEY_SEPARATOR + pricelistId;
								if (regularPriceCountMap.get(priceItemMapKey) == null) {
									regularPriceCountMap.put(priceItemMapKey, Integer.valueOf(0));
								} else {
									int count = ((Integer) regularPriceCountMap.get(priceItemMapKey)).intValue();
									regularPriceCountMap.put(priceItemMapKey, Integer.valueOf(++count));
								}
							}
							if (productId != null) {
								priceItemMapKey = productId + KEY_SEPARATOR + pricelistId;
								if (regularPriceCountMap.get(priceItemMapKey) == null) {
									regularPriceCountMap.put(priceItemMapKey, Integer.valueOf(0));
								} else {
									int count = ((Integer) regularPriceCountMap.get(priceItemMapKey)).intValue();
									regularPriceCountMap.put(priceItemMapKey, Integer.valueOf(++count));
								}
							}
							if ((priceItemMapKey != null) && (((Integer) regularPriceCountMap.get(priceItemMapKey)).intValue() > 0)) {
								isRegularPriceItemFound = true;
							}
						}
					}

					if (isRegularPriceItemFound) {
						assetImportState.setValidateResult(ValidationResult.FAILURE);
						validationState.setValidateErrorCount(validationState.getValidateErrorCount() + TRUMerchConstants.INTEGER_NUMBER_ONE);
						validationState.setValidateNewCount(validationState.getValidateNewCount() - TRUMerchConstants.INTEGER_NUMBER_ONE);
						((PropertyImportState) assetImportState.getPropertyImportStates().get(TRUMerchConstants.ASSET_IMPORT_STATE_ID))
								.setErrorMessage(AssetManagerResources.format(TRUMerchConstants.ERROR_EXISTING_PRICE, ServletUtil.getCurrentRequest()));
					}
				} catch (PriceListException e) {
					vlogError("Exception getting price from pricelist " + pricelistId + " for product " + productId + " and sku " + skuId, new Object[] { e });
				}
			}
			getValidationEngine().getValidationResultSate().updateValidationStatistics(assetImportState);
		}

		setFileOperation(getValidationEngine().getFileOperation());
		setValidationResultSate(getValidationEngine().getValidationResultSate());
		setValidationState(getValidationEngine().getValidationState());
		setAssetImportStates(getValidationEngine().getAssetImportStates());
	}

	/**
	 * Gets the validation engine.
	 * 
	 * @return the validation engine
	 */
	public ValidationEngine getValidationEngine() {
		return mValidationEngine;
	}

	/**
	 * Sets the validation engine.
	 * 
	 * @param pValidationEngine
	 *            the new validation engine
	 */
	public void setValidationEngine(ValidationEngine pValidationEngine) {
		this.mValidationEngine = pValidationEngine;
	}
}