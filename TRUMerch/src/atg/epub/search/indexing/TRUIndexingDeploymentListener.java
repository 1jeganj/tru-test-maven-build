package atg.epub.search.indexing;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJBException;
import javax.ejb.FinderException;

import atg.core.util.StringUtils;
import atg.deployment.common.event.DeploymentEvent;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectHome;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.merchandising.constants.TRUMerchConstants;

/**
 * The listener interface for receiving TRUIndexingDeployment events.
 * The class that is interested in processing a TRUIndexingDeployment
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addTRUIndexingDeploymentListener</code> method. When
 * the TRUIndexingDeployment event occurs, that object's appropriate
 * method is invoked.
 *
 * @see TRUIndexingDeploymentEvent
 */
public class TRUIndexingDeploymentListener extends IndexingDeploymentListener {
	/** Holds pormo item type list	 */
	private List<String> mPromtionItemTypeList;
	
	/** The m force baseline on promotioncahnge. */
	private boolean mForceBaselineOnPromotioncahnge;
	
	/** property to hold mConfiguration boolean check. */
	private TRUConfiguration mConfiguration;

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void gatherIocInfoAndIndex(DeploymentEvent pEvent) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUIndexingDeploymentListener.gatherIocInfoAndIndex method.. pEvent:{0} ", pEvent);
			vlogDebug("Deployment Event type : {0}", pEvent.getDeploymentType());
		}
		String[] projects = null;
		ProjectHome projectHome = null;
		String projectName = null;
		projects = pEvent.getDeploymentProjectIDs();
		projectHome = ProjectConstants.getPersistentHomes().getProjectHome();
		if (projects != null && projects.length >= TRUMerchConstants.INTEGER_NUMBER_ONE && projectHome != null) {
			Project project;
			try {
				project = projectHome.findById(projects[0]);
				if (project != null) {
					projectName = project.getDisplayName();
					if(isLoggingDebug()){
						vlogDebug("TRUIndexingDeploymentListener.gatherIocInfoAndIndex() projectName : {0}" , projectName);
					}
					boolean isRunBaseline=false;
					List<String> projectNames=getConfiguration().getCatalogFeedProjectNameList();
					for(String cmsProjectName:projectNames){
						if (StringUtils.isNotBlank(projectName)
								&& projectName.contains(cmsProjectName)) {
							isRunBaseline=true;
							break;
						}
					}
					if (isRunBaseline) {
						Set allIocs = getIndexingOutputConfigsForDeployment(pEvent);
					    Set incrementalIocs = new HashSet();
						Set ancillaryIOCs = getIOCsWithAffectedAncillaryRepositories(pEvent.getTarget(), pEvent.getAffectedRepositories(), pEvent.getAffectedItemTypes());
					    indexThis(getTargetNameForSyncTaskDefinition(pEvent), "bulk", pEvent.getAffectedItemTypes(), getIndexingOutputConfigPaths(allIocs), getIndexingOutputConfigPaths(incrementalIocs), getIndexingOutputConfigPaths(ancillaryIOCs));
						if(isLoggingDebug()){
							logDebug("Forced to Baseline index");
						}
					}else{
						super.gatherIocInfoAndIndex(pEvent);
					}
				}
			} catch (EJBException e) {
				if (isLoggingError()) {
					vlogError("EJB Exception while accessing item ", e);
				}
			} catch (FinderException e) {
				if (isLoggingError()) {
					vlogError("Finder Exception while accessing item ", e);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Deployment Event type : {0}", pEvent.getDeploymentType());
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUIndexingDeploymentListener.gatherIocInfoAndIndex method.. pEvent:{0} ", pEvent);
			vlogDebug("Deployment Event type : {0}", pEvent.getDeploymentType());
		}
	}
	
	
	/**
	 *  This method is used to trigger bulk indexing while deploying a promotion assets
	 *
	 * @param pCATarget the CA target
	 * @param pIndexingType the indexing type
	 * @param pAffectedItemTypes the affected item types
	 * @param pBulkIndexingOutputConfigPaths the bulk indexing output config paths
	 * @param pIncrementalIndexingOutputConfigPaths the incremental indexing output config paths
	 * @param pAncillaryIndexingOutputConfigPaths the ancillary indexing output config paths
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void indexThis(String pCATarget, String pIndexingType, Map<String, String> pAffectedItemTypes,
			Set<String> pBulkIndexingOutputConfigPaths, Set<String> pIncrementalIndexingOutputConfigPaths,
			Set<String> pAncillaryIndexingOutputConfigPaths) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUIndexingDeploymentListener.indexThis method.. pIndexingType:{0} pAffectedItemTypes:{1}", pIndexingType,pAffectedItemTypes);
		}
		String indexingType=pIndexingType;
		if(isForceBaselineOnPromotioncahnge()&&pAffectedItemTypes != null && !pAffectedItemTypes.isEmpty()){
			String itemTypeAsKey = null;
			Iterator<String> iterator =  pAffectedItemTypes.keySet().iterator();
			while(iterator.hasNext()){
				itemTypeAsKey = iterator.next();
				if(!StringUtils.isBlank(itemTypeAsKey)){
					if(isLoggingDebug()) {
						logDebug("TRUIndexingDeploymentListener.indexThis Repository::"+itemTypeAsKey);
					}
					if(itemTypeAsKey.equals(TRUConstants.ATG_COMMERCE_CATALOG_PRODUCT_CATALOG)) {
						Set<String> afftectedItems=null;
						Object itemDesc=pAffectedItemTypes.get(itemTypeAsKey);
						if(itemDesc instanceof HashSet){
							afftectedItems = (Set<String>) itemDesc;
						}
						if(isLoggingDebug()){
							vlogDebug("TRUIndexingDeploymentListener.indexThis afftectedItems : {0}", afftectedItems);
						}
						for (Iterator<String> iterator2 = afftectedItems.iterator(); iterator2.hasNext();) {
							String itemName = iterator2.next().trim();
							if(isLoggingDebug()){
								vlogDebug(" TRUIndexingDeploymentListener.indexThis Promotion Item type list : {0}", getPromtionItemTypeList());
								vlogDebug(" TRUIndexingDeploymentListener.indexThis Current item type from deployment : {0}", itemName);
							}
							if(getPromtionItemTypeList() != null && !getPromtionItemTypeList().isEmpty() && StringUtils.isNotBlank(itemName) && getPromtionItemTypeList().contains(itemName)){
								indexingType=TRUConstants.BULK;
								break;
							}
						} 
					}
				} 
			}
		}
		super.indexThis(pCATarget, indexingType, pAffectedItemTypes, pBulkIndexingOutputConfigPaths,
				pIncrementalIndexingOutputConfigPaths, pAncillaryIndexingOutputConfigPaths);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUIndexingDeploymentListener.indexThis..");
		}
	}

	

/**
 * This method is to get promtionItemTypeList
 *
 * @return the promtionItemTypeList
 */
public List<String> getPromtionItemTypeList() {
	return mPromtionItemTypeList;
}

/**
 * This method sets promtionItemTypeList with pPromtionItemTypeList
 *
 * @param pPromtionItemTypeList the promtionItemTypeList to set
 */
public void setPromtionItemTypeList(List<String> pPromtionItemTypeList) {
	mPromtionItemTypeList = pPromtionItemTypeList;
}


/**
 * Checks if is force baseline on promotioncahnge.
 *
 * @return true, if is force baseline on promotioncahnge
 */
public boolean isForceBaselineOnPromotioncahnge() {
	return mForceBaselineOnPromotioncahnge;
}


/**
 * Sets the force baseline on promotioncahnge.
 *
 * @param pForceBaselineOnPromotioncahnge the new force baseline on promotioncahnge
 */
public void setForceBaselineOnPromotioncahnge(boolean pForceBaselineOnPromotioncahnge) {
	this.mForceBaselineOnPromotioncahnge = pForceBaselineOnPromotioncahnge;
}
/**
 * This method is to get configuration
 *
 * @return the configuration
 */
public TRUConfiguration getConfiguration() {
	return mConfiguration;
}

/**
 * This method sets configuration with pConfiguration
 *
 * @param pConfiguration the configuration to set
 */
public void setConfiguration(TRUConfiguration pConfiguration) {
	this.mConfiguration = pConfiguration;
}
}
