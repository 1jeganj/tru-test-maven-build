/*
 * 
 */
package atg.commerce.promotion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import atg.adapter.version.CurrentVersionItem;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.util.CurrentDate;

import com.tru.merchandising.constants.TRUMerchConstants;

/**
 * This class is used to Archive Promotion with checking of Expired Date and
 * Value of enabled Properties.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUPromotionArchiveTools extends TRUPromotionTools {
	/** This holds the reference for mTenderTypeRepository *. */
	private Repository mTenderTypeRepository;
	/** This property holds tender Type promotion property name. */
	private String mTenderTypePromotionItemType;
	/** The m property type name. */
	private String mPropertyTypeName;
	
	/** The m promotion id. */
	private String mPromotionId;
	

	/**
	 * Gets the property type name.
	 * 
	 * @return the propertyTypeName
	 */
	public String getPropertyTypeName() {
		return mPropertyTypeName;
	}
	/**
	 * Sets the propertyTypeName.
	 * 
	 * @param pPropertyTypeName
	 *            the new property type name
	 */
	public void setPropertyTypeName(String pPropertyTypeName) {
		this.mPropertyTypeName = pPropertyTypeName;
	}

	/** The mPromotionFolderItemDesc. */
	private String mPromotionFolderItemDesc;

	/**
	 * Gets the mPromotionFolderItemDesc.
	 * 
	 * @return the promotionFolderItemDesc
	 */
	public String getPromotionFolderItemDesc() {
		return mPromotionFolderItemDesc;
	}

	/**
	 * Sets the promotionFolderItemDesc.
	 * 
	 * @param pPromotionFolderItemDesc
	 *            the new mPromotionFolderItemDesc
	 */
	public void setPromotionFolderItemDesc(String pPromotionFolderItemDesc) {
		this.mPromotionFolderItemDesc = pPromotionFolderItemDesc;
	}

	/** The parentFoderItemDesc. */
	private String mParentFoderItemDesc;

	/**
	 * Gets the parentFoderItemDesc.
	 * 
	 * @return the mparentFoderItemDesc
	 */
	public String getParentFoderItemDesc() {
		return mParentFoderItemDesc;
	}

	/**
	 * Sets the m parent foder item desc.
	 * 
	 * @param pParentFoderItemDesc
	 *            the new m parent foder item desc
	 */
	public void setParentFoderItemDesc(String pParentFoderItemDesc) {
		this.mParentFoderItemDesc = pParentFoderItemDesc;
	}

	/**
	 * Used to hold the months value for Promotion Archive.
	 */
	private int mDurationInMonths;

	/**
	 * This property is used to declare the PromotionsArchive folderId.
	 */
	private String mPromotionsArchiveFolderId;

	/**
	 * used to declare the Archive folder starting syntax.
	 */
	private String mArchiveFolderStartsWith;
	

	/**
	 * This method is used to get the archiveFolderStartsWith syntax.
	 * 
	 * @return archiveFolderStartsWith
	 */
	public String getArchiveFolderStartsWith() {
		return mArchiveFolderStartsWith;
	}

	/**
	 * Sets the archive folder starts with.
	 * 
	 * @param pArchiveFolderStartsWith
	 *            the new archiveFolderStartsWith
	 */
	public void setArchiveFolderStartsWith(String pArchiveFolderStartsWith) {
		this.mArchiveFolderStartsWith = pArchiveFolderStartsWith;
	}

	/**
	 * Gets the promotionsArchiveFolderId.
	 * 
	 * @return the promotionsArchiveFolderId
	 */
	public String getPromotionsArchiveFolderId() {
		return mPromotionsArchiveFolderId;
	}

	/**
	 * Sets the promotionsArchiveFolderId.
	 * 
	 * @param pPromotionsArchiveFolderId
	 *            the new promotionsArchiveFolderId
	 */
	public void setPromotionsArchiveFolderId(String pPromotionsArchiveFolderId) {
		this.mPromotionsArchiveFolderId = pPromotionsArchiveFolderId;
	}

	/**
	 * Gets the DurationInMonths.
	 * 
	 * @return the durationInMonths
	 */
	public int getDurationInMonths() {
		return mDurationInMonths;
	}

	/**
	 * Sets the durationInMonths.
	 * 
	 * @param pDurationInMonths
	 *            the new durationInMonths
	 */
	public void setDurationInMonths(int pDurationInMonths) {
		this.mDurationInMonths = pDurationInMonths;
	}

	/** Property to hold mCurrentDate. */
	private CurrentDate mCurrentDate;

	/**
	 * Sets the CurrentDate component.
	 * 
	 * @param pCurrentDate
	 *            CurrentDate
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * Gets the CurrentDate component.
	 * 
	 * @return the CurrentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * This is method is used to read all expired Promotions with enabled and
	 * expiredDate properties check.
	 * @return boolean
	 */
	public boolean moveExpiredPromotionsToArchive() {
		RepositoryView promotionItemView = null;
		MutableRepositoryItem[] promotionItems = null;
		List<RepositoryItem> parentFoldersList = null;
		RepositoryItem rootArchieveFolderItem = null;
		RepositoryItem promoParentFolderItem = null;
		MutableRepository promotionRepository = (MutableRepository) this.getPromotions();
		try {
			promotionItemView = promotionRepository.getView(TRUMerchConstants.PROMOTION);
			QueryBuilder promotionItemQueryBuilder = promotionItemView.getQueryBuilder();
			// Querying All disabled promotions
			promotionItems = getDisabledPromoItems(promotionItemView, promotionItemQueryBuilder);
			if (promotionItems != null) {
				// Create root archive folderItem
				rootArchieveFolderItem = createRootFolder(promotionRepository);
				for (MutableRepositoryItem promotionItem : promotionItems) {
					promoParentFolderItem = (RepositoryItem) promotionItem.getPropertyValue(getParentFoderItemDesc());
					if (promoParentFolderItem == null) {
						promotionItem.setPropertyValue(getParentFoderItemDesc(), rootArchieveFolderItem);
					} else if (!promoParentFolderItem.getRepositoryId().contains(getArchiveFolderStartsWith())) {
						parentFoldersList = new ArrayList<RepositoryItem>();
						recursiveCall(promoParentFolderItem, parentFoldersList);
						Collections.reverse(parentFoldersList);
						createDestinationFolderPath(parentFoldersList, rootArchieveFolderItem, promotionRepository,
								promotionItem);
					}
				}
			} else {
				return false;
			}
		} catch (RepositoryException exception) {
			if (isLoggingError()) {
				logError("RepositoryException in @Class::TRUPromotionArchiveTools::@method::getExpiredPromotions()",
						exception);
			}
		} 
		return true;
	}

	/**
	 * This method is used to set the Association to folderItems and
	 * PromotionItems.
	 * 
	 * @param pParentFoldersList
	 *            the parent folders list
	 * @param pArchieveRepositoryItem
	 *            the archieveRepositoryItem
	 * @param pPromotionRepository
	 *            the promotion repository
	 * @param pPromotionItem
	 *            the promotion item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private void createDestinationFolderPath(List<RepositoryItem> pParentFoldersList,
			RepositoryItem pArchieveRepositoryItem, MutableRepository pPromotionRepository,
			MutableRepositoryItem pPromotionItem) throws RepositoryException {
		createArchiveFolderItems(pPromotionRepository, pParentFoldersList);
		// Setting the ArchiveFolder to parentFolder Object
		String id = getArchiveFolderStartsWith() + pParentFoldersList.get(0).getPropertyValue(TRUMerchConstants.ID);
		MutableRepositoryItem folderItem = (MutableRepositoryItem) pPromotionRepository.getItem(id,
				getPromotionFolderItemDesc());
		folderItem.setPropertyValue(getParentFoderItemDesc(), pArchieveRepositoryItem);
		// Setting Folder to Promotion Item.
		RepositoryItem promotionParentFolder = (RepositoryItem) pPromotionItem
				.getPropertyValue(getParentFoderItemDesc());
		String archivedId = getArchiveFolderStartsWith() + promotionParentFolder.getRepositoryId();
		MutableRepositoryItem promotionFolderItem = (MutableRepositoryItem) pPromotionRepository.getItem(archivedId,
				getPromotionFolderItemDesc());
		pPromotionItem.setPropertyValue(getParentFoderItemDesc(), promotionFolderItem);
	}

	/**
	 * This method is used to create the Archive Folder Items to store the
	 * Expired Promotion Items.
	 * 
	 * @param pPromotionRepository
	 *            the promotion repository
	 * @param pParentFoldersList
	 *            the parent folders list
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private void createArchiveFolderItems(MutableRepository pPromotionRepository,
			List<RepositoryItem> pParentFoldersList) throws RepositoryException {
		RepositoryItem currentItem = null;
		String archivedId = null;
		for (RepositoryItem folderItem : pParentFoldersList) {
			String folderName = (String) ((CurrentVersionItem) folderItem).getPropertyValue(getPropertyTypeName());
			String folderId = (String) ((CurrentVersionItem) folderItem).getPropertyValue(TRUMerchConstants.ID);
			if (!folderId.contains(getArchiveFolderStartsWith())) {
				archivedId = getArchiveFolderStartsWith() + folderId;
			} else {
				archivedId = folderId;
			}
			RepositoryItem archieveFolderRepositoryItem = pPromotionRepository.getItem(archivedId,
					getPromotionFolderItemDesc());
			if (archieveFolderRepositoryItem != null) {
				currentItem = archieveFolderRepositoryItem;
			} else {
				MutableRepositoryItem promotionFolderItem = pPromotionRepository.createItem(archivedId,
						getPromotionFolderItemDesc());
				promotionFolderItem.setPropertyValue(getPropertyTypeName(), folderName);
				promotionFolderItem.setPropertyValue(getParentFoderItemDesc(), currentItem);
				RepositoryItem newlyCreatedRepoItem = pPromotionRepository.addItem(promotionFolderItem);
				currentItem = newlyCreatedRepoItem;
			}
		}
	}

	/**
	 * Creating Root folder(archive) to store all disabled Promotion Items into
	 * it.
	 * 
	 * @param pPromotionRepository
	 *            the promotion repository
	 * @return the repository item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private RepositoryItem createRootFolder(MutableRepository pPromotionRepository) throws RepositoryException {
		RepositoryItem archieveRepositoryItem;
		archieveRepositoryItem = pPromotionRepository.getItem(getPromotionsArchiveFolderId(),
				getPromotionFolderItemDesc());
		if (archieveRepositoryItem == null) {
			MutableRepositoryItem promotionFolderItem = pPromotionRepository.createItem(getPromotionsArchiveFolderId(),
					getPromotionFolderItemDesc());
			promotionFolderItem.setPropertyValue(getPropertyTypeName(), getArchiveFolderStartsWith());
			archieveRepositoryItem = pPromotionRepository.addItem(promotionFolderItem);
		}
		return archieveRepositoryItem;
	}

	/**
	 * This method is used to iterate the ParentFolders of the PromotionItem.
	 * 
	 * @param pPromoParentFolderItem
	 *            the promo parent folder item
	 * @param pArrayList
	 *            the array list
	 */
	private void recursiveCall(RepositoryItem pPromoParentFolderItem, List<RepositoryItem> pArrayList) {
		if (pPromoParentFolderItem != null) {
			pArrayList.add(pPromoParentFolderItem);
			RepositoryItem parentfolder = (RepositoryItem) pPromoParentFolderItem
					.getPropertyValue(getParentFoderItemDesc());
			if (null != parentfolder) {
				recursiveCall(parentfolder, pArrayList);
			}
		}
	}

	/**
	 * This method is used to get all expiredPromotions whose endDate is
	 * exceeded with the currentDate+durationInMonths.
	 * 
	 * @param pPromotionItemView
	 *            the promotion item view
	 * @param pPromoQueryBuilder
	 *            the PromoQuery builder
	 * @return the disabled PromoItems
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private MutableRepositoryItem[] getDisabledPromoItems(RepositoryView pPromotionItemView,
			QueryBuilder pPromoQueryBuilder) throws RepositoryException {
		MutableRepositoryItem[] promoItems;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getCurrentDate().getTimeAsDate());
		calendar.add(Calendar.MONTH, -getDurationInMonths());
		QueryExpression expiredDate = pPromoQueryBuilder.createPropertyQueryExpression(TRUMerchConstants.END_USABLE);
		QueryExpression registrationMaxDate = pPromoQueryBuilder.createConstantQueryExpression(calendar.getTime());
		Query dateQuery = pPromoQueryBuilder.createComparisonQuery(expiredDate, registrationMaxDate,
				QueryBuilder.LESS_THAN_OR_EQUALS);
		promoItems = (MutableRepositoryItem[]) pPromotionItemView.executeQuery(dateQuery);
		return promoItems;
	}
	
	
	
	/**
	 * Gets the tender type repository.
	 * 
	 * @return the tenderTypeRepository
	 */
	public Repository getTenderTypeRepository() {
		return mTenderTypeRepository;
	}

	/**
	 * Sets the tender type repository.
	 * 
	 * @param pTenderTypeRepository
	 *            the tenderTypeRepository to set
	 */
	public void setTenderTypeRepository(Repository pTenderTypeRepository) {
		mTenderTypeRepository = pTenderTypeRepository;
	}
	/**
	 * @return the tenderTypePromotionItemType.
	 */
	public String getTenderTypePromotionItemType() {
		return mTenderTypePromotionItemType;
	}
	/**
	 * @param pTenderTypePromotionItemType the tenderTypePromotionItemType to set.
	 */
	public void setTenderTypePromotionItemType(String pTenderTypePromotionItemType) {
		mTenderTypePromotionItemType = pTenderTypePromotionItemType;
	}

	/**
	 * Gets the promotion id.
	 *
	 * @return the promotion id
	 */
	public String getPromotionId() {
		return mPromotionId;
	}

	/**
	 * Sets the promotion id.
	 *
	 * @param pPromotionId the new promotion id
	 */
	public void setPromotionId(String pPromotionId) {
		this.mPromotionId = pPromotionId;
	}
}
