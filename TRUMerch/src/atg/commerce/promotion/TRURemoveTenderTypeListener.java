package atg.commerce.promotion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.EJBException;
import javax.ejb.FinderException;

import atg.deployment.DeploymentConstants;
import atg.deployment.common.event.DeploymentEvent;
import atg.deployment.common.event.DeploymentEventListener;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectHome;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.versionmanager.AssetVersion;
import atg.versionmanager.exceptions.VersionException;

import com.tru.merchandising.constants.TRUMerchConstants;
import com.tru.messaging.TRUBudgetEmailMessageSource;

/**
 * The listener interface for receiving TRURemoveTenderType events.
 * The class that is interested in processing a TRURemoveTenderType
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addTRURemoveTenderTypeListener</code> method. When
 * the TRURemoveTenderType event occurs, that object's appropriate
 * method is invoked.
 *
 * @see TRURemoveTenderTypeEvent
 */
public class TRURemoveTenderTypeListener extends GenericService implements DeploymentEventListener, DeploymentConstants {
	/** Holds reference for PromotionTools. */
	private TRUPromotionArchiveTools mPromotionTools;
	/**
	 * property to hold site name
	 */
	private String mSiteName;
	
	/** The Enabled. */
	private boolean mEnabled;
	
	/** Holds reference for BudgetEmailMessageSource. */
	private TRUBudgetEmailMessageSource mBudgetEmailMessageSource;
	/**
	 * Listener for removing the tender type items.
	 *
	 * @param pDeploymentEvent the deployment event
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void deploymentEvent(DeploymentEvent pDeploymentEvent) {
		vlogDebug("Start of TRURemoveTenderTypeListener.deploymentEvent() removeTenderTypeItems");
		
		if( isEnabled()&&pDeploymentEvent != null
				&& DeploymentEvent.DEPLOYMENT_COMPLETE == pDeploymentEvent.getNewState() && getSiteName().equalsIgnoreCase(pDeploymentEvent.getTarget())){
			
		
		String[] projects = null;
		ProjectHome projectHome = null;
		String projectName = null;
		Set workSpaceAssets = null;
		Iterator<AssetVersion> workspaceAssetsIterator = null;
		RepositoryItem repositoryItem = null;
		try {
			projects = pDeploymentEvent.getDeploymentProjectIDs();
			projectHome = ProjectConstants.getPersistentHomes().getProjectHome();
			if (projects != null && projects.length >= TRUMerchConstants.INTEGER_NUMBER_ONE && projectHome != null) {
				Project project = projectHome.findById(projects[0]);
				if (project != null) {
					workSpaceAssets = project.getAssets();
					projectName = project.getDisplayName();
					vlogDebug("TRURemoveTenderTypeListener.deploymentEvent() projectName : {0}" , projectName);
				}
				if (workSpaceAssets != null && projectName.contains(TRUMerchConstants.PROMOTION_DISABLE_PROJECT_NAME)) {
					workspaceAssetsIterator = (Iterator<AssetVersion>) workSpaceAssets.iterator();
				}
				try {
					if (workspaceAssetsIterator != null) {
						List<String> promotionIds = new ArrayList<String>();
						while (workspaceAssetsIterator.hasNext()) {
							AssetVersion assetVersion = workspaceAssetsIterator.next();
							if (assetVersion != null) {
								repositoryItem = assetVersion.getRepositoryItem();
								promotionIds.add(repositoryItem.getRepositoryId());
							}
						}
						getBudgetEmailMessageSource().sendBudgetPromoMessage(promotionIds);
					}
				} catch (RepositoryException repoExc) {
					if (isLoggingError()) {
						vlogError("Repository Exception while accessing item ", repositoryItem, repoExc);
					}
				} catch (VersionException verExc) {
					if (isLoggingError()) {
						vlogError("Version Exception while accessing item ", repositoryItem, verExc);
					}
				}
			}
		} catch (EJBException e) {
			if (isLoggingError()) {
				vlogError("EJB Exception while accessing item ", e);
			}
		} catch (FinderException e) {
			if (isLoggingError()) {
				vlogError("Finder Exception while accessing item ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRURemoveTenderTypeListener.deploymentEvent() method");
		}
		}
	}
	/**
	 * @return the promotionTools
	 */
	public TRUPromotionArchiveTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * @param pPromotionTools
	 *            the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionArchiveTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}
	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return mSiteName;
	}

	/**
	 * @param pSiteName the siteName to set
	 */
	public void setSiteName(String pSiteName) {
		mSiteName = pSiteName;
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	/**
	 * @return the budgetEmailMessageSource
	 */
	public TRUBudgetEmailMessageSource getBudgetEmailMessageSource() {
		return mBudgetEmailMessageSource;
	}
	/**
	 * @param pBudgetEmailMessageSource the budgetEmailMessageSource to set
	 */
	public void setBudgetEmailMessageSource(TRUBudgetEmailMessageSource pBudgetEmailMessageSource) {
		mBudgetEmailMessageSource = pBudgetEmailMessageSource;
	}
}
