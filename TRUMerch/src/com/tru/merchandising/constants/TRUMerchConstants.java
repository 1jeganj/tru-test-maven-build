package com.tru.merchandising.constants;


/**
 * This class hold all commerce related constants
 * 
 * @author PA
 * @version 1.0
 */
public class TRUMerchConstants {


	/**
	 * Constant for PROMOTION_DISABLE_PROJECT_NAME.
	 */
	public static final String PROMOTION_DISABLE_PROJECT_NAME = "PromotionDisable";
	/**
	 * Constant for UNDER_SCORE.
	 */
	public static final String UNDER_SCORE = "_";
	
	/** property to hold element. */
	public static final int INTEGER_NUMBER_ONE = 1;
	
	/** The Constant CATALOG_FEED_PROJECT_START_NAME. */
	public static final String CATALOG_FEED_PROJECT_START_NAME = "Catalog feed";

	/** The Constant STR_PRJ. */
	public static final String STR_PRJ = "prj";

	/** The Constant STR_PROJECT. */
	public static final String STR_PROJECT = "project";

	/** The Constant STR_INVALID_PRJ_ID. */
	public static final String STR_INVALID_PRJ_ID = "Invalid Project ID";

	/** The Constant STR_WRKSP. */
	public static final String STR_WRKSP = "workspace";

	/** The Constant STR_WRKSP_NOT_FOUND. */
	public static final String STR_WRKSP_NOT_FOUND = "Workspace NOT Found";

	/** The Constant STR_EMPTY_WRKSP. */
	public static final String STR_EMPTY_WRKSP = "Empty Workspace.. NO Assets Found";

	/** The Constant STR_TOTAL_MSG. */
	public static final String STR_TOTAL_MSG = "TOTAL Assets in Project : ";

	/** The Constant STR_PRJ_MSG. */
	public static final String STR_PRJ_MSG = "Please provide a Project ID";

	/** The Constant PROJECT_ID. */
	public static final String PROJECT_ID = "ProjID";

	/** The Constant FORM_ACTION. */
	public static final String FORM_ACTION = "<form action=\"";

	/** The Constant METHOD_POST. */
	public static final String METHOD_POST = "\" method=POST>";

	/** The Constant INPUT_TEXT. */
	public static final String INPUT_TEXT = "<input type='text' name='ProjID'>";

	/** The Constant INPUT_SUBMIT. */
	public static final String INPUT_SUBMIT = "<input type='submit' name='submit' value='Submit'>";

	/** The Constant FORM_CLOSURE. */
	public static final String FORM_CLOSURE = "</form>";
	
	/** The Constant endUsable. */
	public static final String END_USABLE = "endUsable";
	
	/** The Constant enabled. */
	public static final String ENABLED = "enabled";
	
	/** The Constant id. */
	public static final String ID = "id";
	
	/** The Constant promotion. */
	public static final String PROMOTION = "promotion";
	
	/** The Constant ASSET_IMPORT_STATE_ID. */
	public static final String ASSET_IMPORT_STATE_ID = "ID";
	
	/** The Constant ERROR_EXISTING_PRICE. */
	public static final String ERROR_EXISTING_PRICE = "error.import.validateExistingPrice";
}
