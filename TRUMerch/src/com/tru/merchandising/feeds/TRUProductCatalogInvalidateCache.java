package com.tru.merchandising.feeds;

import javax.ejb.EJBException;
import javax.ejb.FinderException;

import atg.adapter.gsa.GSARepository;
import atg.adapter.version.VersionRepository;
import atg.deployment.common.event.DeploymentEvent;
import atg.deployment.common.event.DeploymentEventListener;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectHome;
import atg.nucleus.GenericService;

import com.tru.merchandising.constants.TRUMerchConstants;



/**
 *  This class is used to extend GenericService class to Invalidate Product Catalog Cache 
 * 	The invalidation is done for feed projects if the Product catalog repository is changed and 
 * 	after DEPLOYMENT_COMPLETED even in production site.
 *   
 * @author Professional Access
 * @version 1.0
 */
public class TRUProductCatalogInvalidateCache extends GenericService implements DeploymentEventListener{

	/**
	 * property to hold ProductCatalog
	 */
	private GSARepository mProductCatalogVer;
	
	/**
	 * property to hold ProductCatalog
	 */
	private VersionRepository mProductCatalog;
	


	/**
	 * property to hold site name
	 */
	private String mSiteName;
	
	
	/**
	 * @return the productCatalog
	 */
	public GSARepository getProductCatalogVer() {
		return mProductCatalogVer;
	}

	
	/**
	 * @param pProductCatalogVer the productCatalogVer to set
	 */
	public void setProductCatalogVer(GSARepository pProductCatalogVer) {
		mProductCatalogVer = pProductCatalogVer;
	}
	
	/**
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(VersionRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}


	/**
	 * @return the productCatalog
	 */
	public VersionRepository getProductCatalog() {
		return mProductCatalog;
	}


	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return mSiteName;
	}

	/**
	 * @param pSiteName the siteName to set
	 */
	public void setSiteName(String pSiteName) {
		mSiteName = pSiteName;
	}


	/**
	 * This method will invalidate the product catalog cache.
	 * 
	 * @param pDeploymentEvent of type DeploymentEvent
	 */
	@Override
	public void deploymentEvent(DeploymentEvent pDeploymentEvent) {
		if(isLoggingDebug()){
			logDebug("Entering into TRUProductCatalogInvalidateCache.deploymentEvent() method");
		}

		if ( pDeploymentEvent != null
				&& DeploymentEvent.DEPLOYMENT_COMPLETE == pDeploymentEvent.getNewState() && getSiteName().equalsIgnoreCase(pDeploymentEvent.getTarget())) {
						
			String projectDispalyName = null;
			projectDispalyName = getProjectName(pDeploymentEvent);
			if (projectDispalyName.contains(TRUMerchConstants.CATALOG_FEED_PROJECT_START_NAME)){
				
				getProductCatalogVer().invalidateCaches();
				getProductCatalog().invalidateCaches();
			}
			
		}
		
		if(isLoggingDebug()){
			logDebug("End of TRUProductCatalogInvalidateCache.deploymentEvent() method");
		}
	}

	
	/**
	 * This method returns the projectname from the Event.
	 * 
	 * @param pDeploymentEvent
	 *            deployment event
	 * @return project name           
	 */
	private String getProjectName(DeploymentEvent pDeploymentEvent) {
		if(isLoggingDebug()){
			logDebug("Start of TRUProductCatalogInvalidateCache.getProjectName() method");
		}
		
		String[] projects = null;
		ProjectHome projectHome = null;
		String displayName = null;
		try {
			projects = pDeploymentEvent.getDeploymentProjectIDs();
			projectHome = ProjectConstants.getPersistentHomes().getProjectHome();
			if (projects != null && projects.length >= TRUMerchConstants.INTEGER_NUMBER_ONE && projectHome !=null) {
				Project project = projectHome.findById(projects[0]);
				if (project != null) {
					displayName = project.getDisplayName();
				}
			}
		} catch (EJBException e) {
			if(isLoggingError()){
				logError(e);
			}
		} catch (FinderException e) {
			if(isLoggingError()){
				logError(e);
			}
		}
		
		if(isLoggingDebug()){
			logDebug("End of TRUProductCatalogInvalidateCache.getProjectName() method");
		}
		
		return displayName;
	}
	

}
