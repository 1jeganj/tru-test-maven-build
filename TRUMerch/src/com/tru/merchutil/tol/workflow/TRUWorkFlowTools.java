package com.tru.merchutil.tol.workflow;


/**
 * <p>This class will do the following operations:</p><br>
 * 1. Creating the BCC Project on the specified work flow.
 * 2. Push all feed data to the Project.
 * 
 * @author : PA
 * @version : 1.0
 */
public class TRUWorkFlowTools extends WorkFlowTools  {
	
	
	/** property to hold auto work flow. */
	private String mAutoWorkflow;
	
	/**
	 * Gets the auto workflow.
	 *
	 * @return the mAutoWorkflow
	 */
	public String getAutoWorkflow() {
		return mAutoWorkflow;
	}

	/**
	 * Sets the auto workflow.
	 *
	 * @param pAutoWorkflow            the mAutoWorkflow to set
	 */
	public void setAutoWorkflow(String pAutoWorkflow) {
		this.mAutoWorkflow = pAutoWorkflow;
	}
	
}
