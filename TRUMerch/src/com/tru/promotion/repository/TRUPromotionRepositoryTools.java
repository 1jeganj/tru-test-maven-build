package com.tru.promotion.repository;


import atg.commerce.promotion.PromotionTools;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

/**
 * This class is to perform the promotion related business logic.
 * 
 * @author Professional Access
 * 
 */
public class TRUPromotionRepositoryTools extends GenericService {

	/** The m promotion tools. */

	private PromotionTools mPromotionTools;

	/**  The PromotionEnabled Property Name. */

	private String mPromotionEnabledPropertyName;

	/**
	 * Gets the promotion enabled property name.
	 *
	 * @return the promotionEnabledPropertyName
	 */
	public String getPromotionEnabledPropertyName() {
		return mPromotionEnabledPropertyName;
	}

	/**
	 * Sets the promotion enabled property name.
	 *
	 * @param pPromotionEnabledPropertyName            the promotionEnabledPropertyName to set
	 */
	public void setPromotionEnabledPropertyName(String pPromotionEnabledPropertyName) {
		mPromotionEnabledPropertyName = pPromotionEnabledPropertyName;
	}

	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools
	 */
	public PromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools            the promotionTools to set
	 */
	public void setPromotionTools(PromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	/**
	 * This method will disable the promotions in BCC and deploys the project.
	 * 
	 * @param pPromotionId
	 *            the Promotion ID
	 */
	public void disablePromotionItem(String pPromotionId) {
		if (isLoggingDebug()) {
			logDebug("Start TRUPromotionRepositoryTools.disablePromotionItem method..");
		}
		MutableRepositoryItem mutablePromotionItem = null;
		try {
			final MutableRepository promotionRepository = (MutableRepository) getPromotionTools().getPromotions();
			String promoId = pPromotionId;
				mutablePromotionItem = (MutableRepositoryItem) promotionRepository.getItem(promoId,
						getPromotionTools().getBasePromotionItemType());
				if (mutablePromotionItem != null) {
					mutablePromotionItem.setPropertyValue(getPromotionEnabledPropertyName(), Boolean.FALSE);
					promotionRepository.updateItem(mutablePromotionItem);
				} else {
					if (isLoggingDebug()) {
						vlogDebug(" There is no promotion Item in Repository for the mentioned promotionID ", pPromotionId);
					}
				}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End TRUPromotionRepositoryTools.disablePromotionItem method..");
		}
	}

}
