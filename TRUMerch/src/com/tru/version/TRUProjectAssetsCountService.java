package com.tru.version;

import java.io.IOException;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.nucleus.ServiceAdminServlet;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.versionmanager.VersionManager;
import atg.versionmanager.Workspace;
import atg.versionmanager.exceptions.VersionException;

import com.tru.merchandising.constants.TRUMerchConstants;

/**
 * <p>
 * This class will do the following operations:
 * </p>
 * <br>
 * 1. Provide us the asset count for specified project based on project id
 * 
 * @author : PA
 * @version : 1.0
 */
public class TRUProjectAssetsCountService extends GenericService {

	/** The m version manager. */
	private VersionManager mVersionManager = null;

	/** Property to hold mPublishingRepository. */
	private Repository mPublishingRepository;
	
	/** The m workspace property name. */
	private String mWorkspacePropertyName;


	/**
	 * Gets the asset count.
	 * 
	 * @param pProjectId
	 *            the project id
	 * @return the asset count
	 */
	public String getAssetCount(String pProjectId) {

		RepositoryItem useItem = null;
		String prjId = pProjectId;

		if (prjId != null && prjId.startsWith(TRUMerchConstants.STR_PRJ)) {
			try {
				useItem = getPublishingRepository().getItem(prjId, TRUMerchConstants.STR_PROJECT);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("Exception while fetching Project ID", e);
				}
			}
		}

		if (useItem == null) {
			return TRUMerchConstants.STR_INVALID_PRJ_ID;
		}
		String workspaceId = (String) useItem.getPropertyValue(getWorkspacePropertyName());
		Workspace wksp = null;
		try {
			wksp = this.mVersionManager.getWorkspaceByName(workspaceId);
		} catch (VersionException e) {
			if (isLoggingError()) {
				logError("Exception while fetching Project ID", e);
			}
		}
		if (wksp == null) {
			return TRUMerchConstants.STR_WRKSP_NOT_FOUND;
		}
		Set workingVersions = wksp.getWorkingVersions();
		if (workingVersions == null) {
			return TRUMerchConstants.STR_EMPTY_WRKSP;
		}
		return new StringBuilder().append(TRUMerchConstants.STR_TOTAL_MSG).append(String.valueOf(workingVersions.size())).toString();
	}

	/**
	 * This method will create nucleus component.
	 *
	 * @return the servlet
	 */
	protected Servlet createAdminServlet() {
		return new TRUProjectAssetsAdminServlet(this, getNucleus());
	}

	
	/**
	 * The TRUProjectAssetsAdminServlet.
	 */
	@SuppressWarnings({ "rawtypes", "serial" })
	class TRUProjectAssetsAdminServlet extends ServiceAdminServlet {

		/**
		 * Instantiates a new TRU project assets admin servlet.
		 *
		 * @param pTRUProjectAssetsCountService the TRU project assets count service
		 * @param pNucleus the nucleus
		 */
		@SuppressWarnings("unchecked")
		public TRUProjectAssetsAdminServlet(TRUProjectAssetsCountService pTRUProjectAssetsCountService, Nucleus pNucleus) {
			super(pTRUProjectAssetsCountService, pNucleus);

		}

		/**
		 * This method is used to print the info of the asset count.
		 *
		 * @param pRequest the request
		 * @param pResponse the response
		 * @param pOut the out
		 * @throws ServletException the servlet exception
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		protected void printAdmin(HttpServletRequest pRequest, HttpServletResponse pResponse, ServletOutputStream pOut) throws ServletException,
				IOException {
			TRUProjectAssetsCountService projectAssetService = (TRUProjectAssetsCountService) this.mService;

			printAdminInfo(pRequest, pOut, projectAssetService);
			super.printAdmin(pRequest, pResponse, pOut);
		}

		/**
		 * Prints the admin info.
		 * 
		 * @param pRequest
		 *            the request
		 * @param pOut
		 *            the out
		 * @param pTRUProjectAssetsCountService
		 *            the TRU project assets count service
		 * @throws ServletException
		 *             the servlet exception
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		protected void printAdminInfo(HttpServletRequest pRequest, ServletOutputStream pOut,
				TRUProjectAssetsCountService pTRUProjectAssetsCountService) throws ServletException, IOException {

			String prjID = pRequest.getParameter(TRUMerchConstants.PROJECT_ID);

			if (prjID != null) {
				String assetCountMessage = getAssetCount(prjID);
				pOut.println();
				pOut.println(assetCountMessage);
				pOut.println();

			} else {
				pOut.println(TRUMerchConstants.STR_PRJ_MSG);
			}

			{
				pOut.print(TRUMerchConstants.FORM_ACTION);
				pOut.print(formatServiceName(pRequest.getPathInfo(), pRequest));
				pOut.println(TRUMerchConstants.METHOD_POST);
				pOut.println(TRUMerchConstants.INPUT_TEXT);
				pOut.println(TRUMerchConstants.INPUT_SUBMIT);
				pOut.println(TRUMerchConstants.FORM_CLOSURE);
			}

		}
	}

	/**
	 * Sets the version manager.
	 * 
	 * @param pVersionManager
	 *            the new version manager
	 */
	public void setVersionManager(VersionManager pVersionManager) {
		this.mVersionManager = pVersionManager;
	}

	/**
	 * Gets the version manager.
	 * 
	 * @return the version manager
	 */
	public VersionManager getVersionManager() {
		return this.mVersionManager;
	}

	/**
	 * Returns Repository component.
	 * 
	 * @return the mPublishingRepository
	 */
	public Repository getPublishingRepository() {
		return mPublishingRepository;
	}

	/**
	 * Sets the publishingRepository value to Repository component.
	 * 
	 * @param pPublishingRepository
	 *            - mPublishingRepository
	 */
	public void setPublishingRepository(Repository pPublishingRepository) {
		this.mPublishingRepository = pPublishingRepository;
	}
	
	/**
	 * Gets the workspace property name.
	 * 
	 * @return the workspace property name
	 */
	public String getWorkspacePropertyName() {
		return mWorkspacePropertyName;
	}

	/**
	 * Sets the workspace property name.
	 * 
	 * @param pWorkspacePropertyName
	 *            the new workspace property name
	 */
	public void setWorkspacePropertyName(String pWorkspacePropertyName) {
		this.mWorkspacePropertyName = pWorkspacePropertyName;
	}
}
