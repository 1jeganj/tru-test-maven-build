package com.tru.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * This class is used to schedule the expired promotions Archive job.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUPromoArchiveScheduler extends SingletonSchedulableService {

	/**
	 * This property is used to hold the scheduler enable property.
	 */
	private boolean mEnable;

	/**
	 * Used to hold the PromoArchiveManager.
	 */
	private TRUPromoArchiveManager mPromoArchiveManager;

	/**
	 * This method is used to get the promoArchiveManager.
	 * 
	 * @return TRUPromoArchiveManager
	 */
	public TRUPromoArchiveManager getPromoArchiveManager() {
		return mPromoArchiveManager;
	}

	/**
	 * This method is used to set the promoArchiveManager .
	 * 
	 * @param pPromoArchiveManager
	 *            the new promoArchiveManager
	 */
	public void setPromoArchiveManager(
			TRUPromoArchiveManager pPromoArchiveManager) {
		this.mPromoArchiveManager = pPromoArchiveManager;
	}

	/**
	 * Used to get the boolean value of enable property.
	 * 
	 * @return enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: CategoryImageScheduler.doScheduledTask()");
		}
		if (isEnable()) {
			getPromoArchiveManager().archivePromotions();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: CategoryImageScheduler.doScheduledTask()");
		}
	}
}
