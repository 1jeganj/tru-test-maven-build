package com.tru.scheduler;

import java.util.Calendar;

import atg.commerce.promotion.TRUPromotionArchiveTools;
import atg.nucleus.GenericService;
import atg.process.action.ActionException;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;

import com.tru.merchandising.constants.TRUMerchConstants;
import com.tru.merchutil.tol.workflow.TRUWorkFlowTools;

/**
 * This class is used to initiate the Archive WorkFlow with the expired
 * promotions.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUPromoArchiveManager extends GenericService {

	/**
	 * Used to declare the PromotionTools.
	 */
	private TRUPromotionArchiveTools mPromotionTools;

	/**
	 * Used to declare the WorkFlowTools.
	 */
	private TRUWorkFlowTools mWorkFlowTools;

	/**
	 * Used to declare the ProjectName.
	 */
	private String mProjectName;

	/**
	 * This method is used to get the PromotionsArchive ProjectName.
	 * 
	 * @return projectName
	 */
	public String getProjectName() {
		return mProjectName;
	}

	/**
	 * Sets the projectName.
	 * 
	 * @param pProjectName
	 *            the new project name
	 */
	public void setProjectName(String pProjectName) {
		this.mProjectName = pProjectName;
	}

	/**
	 * This method is used to get the PromotionTools.
	 * 
	 * @return promotionTools
	 */
	public TRUPromotionArchiveTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotionTools.
	 * 
	 * @param pPromotionTools
	 *            the new promotion tools
	 */
	public void setPromotionTools(TRUPromotionArchiveTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * This method is used to get the WorkFlowTools.
	 * 
	 * @return workFlowTools
	 */
	public TRUWorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the workFlowTools.
	 * 
	 * @param pWorkFlowTools
	 *            the new work flow tools
	 */
	public void setWorkFlowTools(TRUWorkFlowTools pWorkFlowTools) {
		this.mWorkFlowTools = pWorkFlowTools;
	}

	/**
	 * This method is used to Initiate the workFlow with archive Promotion
	 * Items.
	 */
	public void archivePromotions() {
		boolean isPromotionsExsistforArchive = getPromotionTools().moveExpiredPromotionsToArchive();
		if(isPromotionsExsistforArchive) {
			StringBuffer projectName = new StringBuffer();
			Calendar calendar = Calendar.getInstance();
			projectName.append(getProjectName());
			projectName.append(TRUMerchConstants.UNDER_SCORE);
			projectName.append(calendar.getTime());
			String workFlowName = getWorkFlowTools().getAutoWorkflow();
			atg.epub.project.Process process = getWorkFlowTools().initiateWorkflow(projectName.toString(), workFlowName);
			try {
				getWorkFlowTools().advanceWorkflow(process, getWorkFlowTools().getTaskOutcomeId());
				getWorkFlowTools().finishTask();
				if (isLoggingDebug()) {
					logDebug("End TRUPromoArchiveManager.archivePromotions method..");
				}
			} catch (MissingWorkflowDescriptionException pExe) {
				if (isLoggingError()) {
					vlogError(
							"MissingWorkflowDescriptionException: While creating workflow to Archive promotion Items with exception : {0}",
							pExe);
				}
			} catch (ActorAccessException pExe) {
				if (isLoggingError()) {
					vlogError(
							"ActorAccessException: While accessing rest service to Archive promotion Items with exception : {0}",
							pExe);
				}
			} catch (ActionException pExe) {
				if (isLoggingError()) {
					vlogError("ActorAccessException: Archive promotion Items with exception : {0}", pExe);
				}
			}
		} else {
			if(isLoggingDebug()) {
				logDebug("There is No Expired Promotions to move archive Folder.");
				logDebug("End TRUPromoArchiveManager.archivePromotions method..");
			}
		}
	}
}
