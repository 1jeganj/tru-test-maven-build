/*
 * 
 */
package com.tru.feedprocessor.email;

import java.util.List;

/**
 * The Class TRUFeedEnvConfiguration.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedEnvConfiguration {

	/** The mail priority. */
	private String mMailPriorityRegular;

	/** The user. */
	private String mUser;

	/** The action type. */
	private String mActionType;

	/** The price status. */
	private String mPriceSuccessStatus;

	/** The site id. */
	private String mSiteId;

	/** The report symptom. */
	private String mReportSymptom;

	/** The queue. */
	private String mQueue;

	/** The notes. */
	private String mNotes;

	/** The software. */
	private String mSoftware;

	/** The OS name. */
	private String mOSName;

	/** The java version. */
	private String mJavaVersion;

	/** The OS host name. */
	private String mOSHostName;

	/** The price success subject. */
	private String mPriceSuccessSubject;

	/** The price processed with errors subject. */
	private String mPriceProcessedWithErrorsSubject;

	/** The price no feed file subject. */
	private String mPriceNoFeedFileSubject;

	/** The price invalid subject. */
	private String mPriceInvalidSubject;

	/** The detailed exception for no feed. */
	private String mDetailedExceptionForNoFeed;

	/** The detailed exception for invalid feed. */
	private String mDetailedExceptionForInvalidFeed;

	/** The failure mail priority. */
	private String mFailureMailPriority2;

	/** The Attachment item id label. */
	private String mAttachmentItemIdLabel;

	/** The Attachment item id label. */
	private String mFileName;

	/** The Attachment exception reason label. */
	private String mAttachmentExceptionReasonLabel;

	/** The Price feed name. */
	private String mPriceFeedName;

	/** The Cat success subject. */
	private String mCatSuccessSubject;

	/** The Cat processed with errors subject. */
	private String mCatProcessedWithErrorsSubject;

	/** The Registry processed with errors subject. */
	private String mRegistryProcessedWithErrorsSubject;

	/** The Taxonomy processed with errors subject. */
	private String mTaxonomyProcessedWithErrorsSubject;

	/** The Cat no feed file subject. */
	private String mCatNoFeedFileSubject;

	/** The Cat invalid subject. */
	private String mCatInvalidSubject;

	/** The Cat feed name. */
	private String mCatFeedName;

	/** The Cat success status. */
	private String mCatSuccessStatus;

	/** The Page identifier label. */
	private String mPageIdentifierLabel;

	/** The Project name label. */
	private String mProjectNameLabel;

	/** The Version label. */
	private String mVersionLabel;

	/** The Page identifier label. */
	private String mRecordNumber;

	/** The Project name label. */
	private String mTimeStamp;

	/** The Version label. */
	private String mClassification;

	/** The store success subject. */
	private String mStoreSuccessSubject;

	/** The store processed with errors subject. */
	private String mStoreProcessedWithErrorsSubject;

	/** The store no feed file subject. */
	private String mStoreNoFeedFileSubject;

	/** The store invalid subject. */
	private String mStoreInvalidSubject;

	/** The store invalid subject. */
	private String mStoreSuccessStatus;

	/** The m registry threshold mail subject. */
	private String mRegistryThresholdMailSubject;

	/** The m website threshold mail subject. */
	private String mWebsiteThresholdMailSubject;

	/** The Registry feed name. */
	private String mRegistryFeedName;

	/** The Website feed name. */
	private String mWebsiteFeedName;

	/** The Lower cutoff alert priority3. */
	private double mLowerCutoffAlertPriority3;

	/** The Upper cutoff alert priority3. */
	private double mUpperCutoffAlertPriority3;

	/** The Lower cutoff alert priority2. */
	private double mLowerCutoffAlertPriority2;

	/** The Upper cutoff alert priority2. */
	private double mUpperCutoffAlertPriority2;

	/** The Lower cutoff exception alert priority3. */
	private double mLowerCutoffExceptionAlertPriority3;

	/** The Upper cutoff exception alert priority3. */
	private double mUpperCutoffExceptionAlertPriority3;

	/** The Cutoff exception alert priority2. */
	private double mCutoffExceptionAlertPriority2;

	/** The Lower cutoff exception alert priority regular. */
	private double mLowerCutoffExceptionAlertPriorityRegular;

	/** The Upper cutoff exception alert priority regular. */
	private double mUpperCutoffExceptionAlertPriorityRegular;

	/** The Failure mail priority3. */
	private String mFailureMailPriority3;

	/** The m attachmentProjectNameLabel threshold mail subject. */
	private String mAttachmentProjectNameLabel;

	/** The Price feed message from. */
	private String mPriceFeedMessageFrom;

	/** The Price feed message to. */
	private List<String> mPriceFeedMessageTo;

	/** The Price feed message from. */
	private String mStoreFeedMessageFrom;

	/** The Price feed message to. */
	private List<String> mStoreFeedMessageTo;

	/** The Store threshold mail subject. */
	private String mStoreThresholdMailSubject;

	/** The registry success subject. */
	private String mRegistrySuccessSubject;

	/** The taxonomy success subject. */
	private String mTaxonomySuccessSubject;

	/** The catalog feed message from. */
	private String mCatalogFeedMessageFrom;

	/** The catalog feed message to. */
	private List<String> mCatalogFeedMessageTo;

	/** The Cat no feed file subject. */
	private String mCatFeedFilesNoSequence;

	/** The Cat no feed file subject. */
	private String mCatFeedFilesSequenceNoMatch;

	/** The m price feed files no sequence. */
	private String mPriceFeedFilesNoSequence;

	/** The m price feed files sequence no match. */
	private String mPriceFeedFilesSequenceNoMatch;

	/** The m store feed files sequence no match. */
	private String mStoreFeedFilesSequenceNoMatch;

	/** The Cat threshold exceeded subject. */
	private String mCatThresholdExceededSubject;

	/** The Registry threshold exceeded subject. */
	private String mRegistryThresholdExceededSubject;

	/** The Taxonomy threshold exceeded subject. */
	private String mTaxonomyThresholdExceededSubject;

	/** The Taxonomy threshold exceeded subject. */
	private String mEnv;

	/** The Taxonomy threshold exceeded subject. */
	private String mCatProdFeedName;

	/** The Taxonomy threshold exceeded subject. */
	private String mProduct;

	/** The Taxonomy threshold exceeded subject. */
	private String mSku;

	/** The Taxonomy threshold exceeded subject. */
	private String mPid;

	/** The m detailed exception for seq miss. */
	private String mDetailedExceptionForSeqMiss;

	/** The m store feed name. */
	private String mStoreFeedName;

	/** The m store feed success sub. */
	private String mStoreFeedSuccessSub;

	/** The m store feed error sub. */
	private String mStoreFeedErrorSub;

	/** The m store feed failure sub. */
	private String mStoreFeedFailureSub;

	/** The m store feed error detail excep. */
	private String mStoreFeedErrorDetailExcep;

	/** The m invalid store detail excep. */
	private String mInvalidStoreDetailExcep;

	/** The m price feed success sub. */
	private String mPriceFeedSuccessSub;

	/** The m price feed error sub. */
	private String mPriceFeedErrorSub;

	/** The m price feed failure sub. */
	private String mPriceFeedFailureSub;

	/** The m price feed error detail excep. */
	private String mPriceFeedErrorDetailExcep;

	/** The Sequence processed. */
	private String mSequenceProcessed;

	/** The Seo feed success sub. */
	private String mSeoFeedSuccessSub;

	/** The Seo feed error sub. */
	private String mSeoFeedErrorSub;

	/** The Seo feed failure sub. */
	private String mSeoFeedFailureSub;

	/** The Seo feed error detail excep. */
	private String mSeoFeedErrorDetailExcep;

	/** The Seo feed message from. */
	private String mSeoFeedMessageFrom;

	/** The Seo feed message to. */
	private String mSeoFeedMessageTo;

	/**
	 * Gets the sequence processed.
	 * 
	 * @return the sequenceProcessed
	 */
	public String getSequenceProcessed() {
		return mSequenceProcessed;
	}

	/**
	 * Sets the sequence processed.
	 * 
	 * @param pSequenceProcessed
	 *            the sequenceProcessed to set
	 */
	public void setSequenceProcessed(String pSequenceProcessed) {
		mSequenceProcessed = pSequenceProcessed;
	}

	/**
	 * Gets the product.
	 * 
	 * @return the product
	 */
	public String getProduct() {
		return mProduct;
	}

	/**
	 * Sets the product.
	 * 
	 * @param pProduct
	 *            the product to set
	 */
	public void setProduct(String pProduct) {
		mProduct = pProduct;
	}

	/**
	 * Gets the sku.
	 * 
	 * @return the sku
	 */
	public String getSku() {
		return mSku;
	}

	/**
	 * Sets the sku.
	 * 
	 * @param pSku
	 *            the sku to set
	 */
	public void setSku(String pSku) {
		mSku = pSku;
	}

	/**
	 * Gets the pid.
	 * 
	 * @return the pid
	 */
	public String getPid() {
		return mPid;
	}

	/**
	 * Sets the pid.
	 * 
	 * @param pPid
	 *            the pid to set
	 */
	public void setPid(String pPid) {
		mPid = pPid;
	}

	/**
	 * Gets the cat prod feed name.
	 * 
	 * @return the catProdFeedName
	 */
	public String getCatProdFeedName() {
		return mCatProdFeedName;
	}

	/**
	 * Sets the cat prod feed name.
	 * 
	 * @param pCatProdFeedName
	 *            the catProdFeedName to set
	 */
	public void setCatProdFeedName(String pCatProdFeedName) {
		mCatProdFeedName = pCatProdFeedName;
	}

	/**
	 * Gets the env.
	 * 
	 * @return the env
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * Sets the env.
	 * 
	 * @param pEnv
	 *            the env to set
	 */
	public void setEnv(String pEnv) {
		mEnv = pEnv;
	}

	/**
	 * Gets the cat threshold exceeded subject.
	 * 
	 * @return the catThresholdExceededSubject
	 */
	public String getCatThresholdExceededSubject() {
		return mCatThresholdExceededSubject;
	}

	/**
	 * Sets the cat threshold exceeded subject.
	 * 
	 * @param pCatThresholdExceededSubject
	 *            the catThresholdExceededSubject to set
	 */
	public void setCatThresholdExceededSubject(String pCatThresholdExceededSubject) {
		mCatThresholdExceededSubject = pCatThresholdExceededSubject;
	}

	/**
	 * Gets the registry threshold exceeded subject.
	 * 
	 * @return the registryThresholdExceededSubject
	 */
	public String getRegistryThresholdExceededSubject() {
		return mRegistryThresholdExceededSubject;
	}

	/**
	 * Sets the registry threshold exceeded subject.
	 * 
	 * @param pRegistryThresholdExceededSubject
	 *            the registryThresholdExceededSubject to set
	 */
	public void setRegistryThresholdExceededSubject(String pRegistryThresholdExceededSubject) {
		mRegistryThresholdExceededSubject = pRegistryThresholdExceededSubject;
	}

	/**
	 * Gets the taxonomy threshold exceeded subject.
	 * 
	 * @return the taxonomyThresholdExceededSubject
	 */
	public String getTaxonomyThresholdExceededSubject() {
		return mTaxonomyThresholdExceededSubject;
	}

	/**
	 * Sets the taxonomy threshold exceeded subject.
	 * 
	 * @param pTaxonomyThresholdExceededSubject
	 *            the taxonomyThresholdExceededSubject to set
	 */
	public void setTaxonomyThresholdExceededSubject(String pTaxonomyThresholdExceededSubject) {
		mTaxonomyThresholdExceededSubject = pTaxonomyThresholdExceededSubject;
	}

	/**
	 * Gets the cat feed files no sequence.
	 * 
	 * @return the catFeedFilesNoSequence
	 */
	public String getCatFeedFilesNoSequence() {
		return mCatFeedFilesNoSequence;
	}

	/**
	 * Sets the cat feed files no sequence.
	 * 
	 * @param pCatFeedFilesNoSequence
	 *            the catFeedFilesNoSequence to set
	 */
	public void setCatFeedFilesNoSequence(String pCatFeedFilesNoSequence) {
		mCatFeedFilesNoSequence = pCatFeedFilesNoSequence;
	}

	/**
	 * Gets the cat feed files sequence no match.
	 * 
	 * @return the catFeedFilesSequenceNoMatch
	 */
	public String getCatFeedFilesSequenceNoMatch() {
		return mCatFeedFilesSequenceNoMatch;
	}

	/**
	 * Sets the cat feed files sequence no match.
	 * 
	 * @param pCatFeedFilesSequenceNoMatch
	 *            the catFeedFilesSequenceNoMatch to set
	 */
	public void setCatFeedFilesSequenceNoMatch(String pCatFeedFilesSequenceNoMatch) {
		mCatFeedFilesSequenceNoMatch = pCatFeedFilesSequenceNoMatch;
	}

	/**
	 * Gets the registry processed with errors subject.
	 * 
	 * @return the registryProcessedWithErrorsSubject
	 */
	public String getRegistryProcessedWithErrorsSubject() {
		return mRegistryProcessedWithErrorsSubject;
	}

	/**
	 * Sets the registry processed with errors subject.
	 * 
	 * @param pRegistryProcessedWithErrorsSubject
	 *            the registryProcessedWithErrorsSubject to set
	 */
	public void setRegistryProcessedWithErrorsSubject(String pRegistryProcessedWithErrorsSubject) {
		mRegistryProcessedWithErrorsSubject = pRegistryProcessedWithErrorsSubject;
	}

	/**
	 * Gets the taxonomy processed with errors subject.
	 * 
	 * @return the taxonomyProcessedWithErrorsSubject
	 */
	public String getTaxonomyProcessedWithErrorsSubject() {
		return mTaxonomyProcessedWithErrorsSubject;
	}

	/**
	 * Sets the taxonomy processed with errors subject.
	 * 
	 * @param pTaxonomyProcessedWithErrorsSubject
	 *            the taxonomyProcessedWithErrorsSubject to set
	 */
	public void setTaxonomyProcessedWithErrorsSubject(String pTaxonomyProcessedWithErrorsSubject) {
		mTaxonomyProcessedWithErrorsSubject = pTaxonomyProcessedWithErrorsSubject;
	}

	/**
	 * Gets the page identifier label.
	 * 
	 * @return the pageIdentifierLabel
	 */
	public String getPageIdentifierLabel() {
		return mPageIdentifierLabel;
	}

	/**
	 * Sets the page identifier label.
	 * 
	 * @param pPageIdentifierLabel
	 *            the pageIdentifierLabel to set
	 */
	public void setPageIdentifierLabel(String pPageIdentifierLabel) {
		mPageIdentifierLabel = pPageIdentifierLabel;
	}

	/**
	 * Gets the project name label.
	 * 
	 * @return the projectNameLabel
	 */
	public String getProjectNameLabel() {
		return mProjectNameLabel;
	}

	/**
	 * Sets the project name label.
	 * 
	 * @param pProjectNameLabel
	 *            the projectNameLabel to set
	 */
	public void setProjectNameLabel(String pProjectNameLabel) {
		mProjectNameLabel = pProjectNameLabel;
	}

	/**
	 * Gets the version label.
	 * 
	 * @return the versionLabel
	 */
	public String getVersionLabel() {
		return mVersionLabel;
	}

	/**
	 * Sets the version label.
	 * 
	 * @param pVersionLabel
	 *            the versionLabel to set
	 */
	public void setVersionLabel(String pVersionLabel) {
		mVersionLabel = pVersionLabel;
	}

	/**
	 * Gets the cat success subject.
	 * 
	 * @return the catSuccessSubject
	 */
	public String getCatSuccessSubject() {
		return mCatSuccessSubject;
	}

	/**
	 * Sets the cat success subject.
	 * 
	 * @param pCatSuccessSubject
	 *            the catSuccessSubject to set
	 */
	public void setCatSuccessSubject(String pCatSuccessSubject) {
		mCatSuccessSubject = pCatSuccessSubject;
	}

	/**
	 * Gets the cat processed with errors subject.
	 * 
	 * @return the catProcessedWithErrorsSubject
	 */
	public String getCatProcessedWithErrorsSubject() {
		return mCatProcessedWithErrorsSubject;
	}

	/**
	 * Sets the cat processed with errors subject.
	 * 
	 * @param pCatProcessedWithErrorsSubject
	 *            the catProcessedWithErrorsSubject to set
	 */
	public void setCatProcessedWithErrorsSubject(String pCatProcessedWithErrorsSubject) {
		mCatProcessedWithErrorsSubject = pCatProcessedWithErrorsSubject;
	}

	/**
	 * Gets the cat no feed file subject.
	 * 
	 * @return the catNoFeedFileSubject
	 */
	public String getCatNoFeedFileSubject() {
		return mCatNoFeedFileSubject;
	}

	/**
	 * Sets the cat no feed file subject.
	 * 
	 * @param pCatNoFeedFileSubject
	 *            the catNoFeedFileSubject to set
	 */
	public void setCatNoFeedFileSubject(String pCatNoFeedFileSubject) {
		mCatNoFeedFileSubject = pCatNoFeedFileSubject;
	}

	/**
	 * Gets the cat invalid subject.
	 * 
	 * @return the catInvalidSubject
	 */
	public String getCatInvalidSubject() {
		return mCatInvalidSubject;
	}

	/**
	 * Sets the cat invalid subject.
	 * 
	 * @param pCatInvalidSubject
	 *            the catInvalidSubject to set
	 */
	public void setCatInvalidSubject(String pCatInvalidSubject) {
		mCatInvalidSubject = pCatInvalidSubject;
	}

	/**
	 * Gets the cat feed name.
	 * 
	 * @return the catFeedName
	 */
	public String getCatFeedName() {
		return mCatFeedName;
	}

	/**
	 * Sets the cat feed name.
	 * 
	 * @param pCatFeedName
	 *            the catFeedName to set
	 */
	public void setCatFeedName(String pCatFeedName) {
		mCatFeedName = pCatFeedName;
	}

	/**
	 * Gets the cat success status.
	 * 
	 * @return the catSuccessStatus
	 */
	public String getCatSuccessStatus() {
		return mCatSuccessStatus;
	}

	/**
	 * Sets the cat success status.
	 * 
	 * @param pCatSuccessStatus
	 *            the catSuccessStatus to set
	 */
	public void setCatSuccessStatus(String pCatSuccessStatus) {
		mCatSuccessStatus = pCatSuccessStatus;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public String getUser() {
		return mUser;
	}

	/**
	 * Sets the user.
	 * 
	 * @param pUser
	 *            the new user
	 */
	public void setUser(String pUser) {
		mUser = pUser;
	}

	/**
	 * Gets the action type.
	 * 
	 * @return the action type
	 */
	public String getActionType() {
		return mActionType;
	}

	/**
	 * Sets the action type.
	 * 
	 * @param pActionType
	 *            the new action type
	 */
	public void setActionType(String pActionType) {
		mActionType = pActionType;
	}

	/**
	 * Gets the price success status.
	 * 
	 * @return the price success status
	 */
	public String getPriceSuccessStatus() {
		return mPriceSuccessStatus;
	}

	/**
	 * Sets the price success status.
	 * 
	 * @param pPriceSuccessStatus
	 *            the new price success status
	 */
	public void setPriceSuccessStatus(String pPriceSuccessStatus) {
		mPriceSuccessStatus = pPriceSuccessStatus;
	}

	/**
	 * Gets the site id.
	 * 
	 * @return the site id
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the site id.
	 * 
	 * @param pSiteId
	 *            the new site id
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	/**
	 * Gets the report symptom.
	 * 
	 * @return the report symptom
	 */
	public String getReportSymptom() {
		return mReportSymptom;
	}

	/**
	 * Sets the report symptom.
	 * 
	 * @param pReportSymptom
	 *            the new report symptom
	 */
	public void setReportSymptom(String pReportSymptom) {
		mReportSymptom = pReportSymptom;
	}

	/**
	 * Gets the queue.
	 * 
	 * @return the queue
	 */
	public String getQueue() {
		return mQueue;
	}

	/**
	 * Sets the queue.
	 * 
	 * @param pQueue
	 *            the new queue
	 */
	public void setQueue(String pQueue) {
		mQueue = pQueue;
	}

	/**
	 * Gets the notes.
	 * 
	 * @return the notes
	 */
	public String getNotes() {
		return mNotes;
	}

	/**
	 * Sets the notes.
	 * 
	 * @param pNotes
	 *            the new notes
	 */
	public void setNotes(String pNotes) {
		mNotes = pNotes;
	}

	/**
	 * Gets the software.
	 * 
	 * @return the software
	 */
	public String getSoftware() {
		return mSoftware;
	}

	/**
	 * Sets the software.
	 * 
	 * @param pSoftware
	 *            the new software
	 */
	public void setSoftware(String pSoftware) {
		mSoftware = pSoftware;
	}

	/**
	 * Gets the OS name.
	 * 
	 * @return the OS name
	 */
	public String getOSName() {
		return mOSName;
	}

	/**
	 * Sets the OS name.
	 * 
	 * @param pOSName
	 *            the new OS name
	 */
	public void setOSName(String pOSName) {
		mOSName = pOSName;
	}

	/**
	 * Gets the java version.
	 * 
	 * @return the java version
	 */
	public String getJavaVersion() {
		return mJavaVersion;
	}

	/**
	 * Sets the java version.
	 * 
	 * @param pJavaVersion
	 *            the new java version
	 */
	public void setJavaVersion(String pJavaVersion) {
		mJavaVersion = pJavaVersion;
	}

	/**
	 * Gets the OS host name.
	 * 
	 * @return the OS host name
	 */
	public String getOSHostName() {
		return mOSHostName;
	}

	/**
	 * Sets the OS host name.
	 * 
	 * @param pOSHostName
	 *            the new OS host name
	 */
	public void setOSHostName(String pOSHostName) {
		mOSHostName = pOSHostName;
	}

	/**
	 * Gets the price success subject.
	 * 
	 * @return the price success subject
	 */
	public String getPriceSuccessSubject() {
		return mPriceSuccessSubject;
	}

	/**
	 * Sets the price success subject.
	 * 
	 * @param pPriceSuccessSubject
	 *            the new price success subject
	 */
	public void setPriceSuccessSubject(String pPriceSuccessSubject) {
		mPriceSuccessSubject = pPriceSuccessSubject;
	}

	/**
	 * Gets the price processed with errors subject.
	 * 
	 * @return the price processed with errors subject
	 */
	public String getPriceProcessedWithErrorsSubject() {
		return mPriceProcessedWithErrorsSubject;
	}

	/**
	 * Sets the price processed with errors subject.
	 * 
	 * @param pPriceProcessedWithErrorsSubject
	 *            the new price processed with errors subject
	 */
	public void setPriceProcessedWithErrorsSubject(String pPriceProcessedWithErrorsSubject) {
		mPriceProcessedWithErrorsSubject = pPriceProcessedWithErrorsSubject;
	}

	/**
	 * Gets the file name.
	 * 
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 * 
	 * @param pFileName
	 *            the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the record number.
	 * 
	 * @return the recordNumber
	 */
	public String getRecordNumber() {
		return mRecordNumber;
	}

	/**
	 * Sets the record number.
	 * 
	 * @param pRecordNumber
	 *            the recordNumber to set
	 */
	public void setRecordNumber(String pRecordNumber) {
		mRecordNumber = pRecordNumber;
	}

	/**
	 * Gets the time stamp.
	 * 
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets the time stamp.
	 * 
	 * @param pTimeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(String pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}

	/**
	 * Gets the classification.
	 * 
	 * @return the classification
	 */
	public String getClassification() {
		return mClassification;
	}

	/**
	 * Sets the classification.
	 * 
	 * @param pClassification
	 *            the classification to set
	 */
	public void setClassification(String pClassification) {
		mClassification = pClassification;
	}

	/**
	 * Gets the price no feed file subject.
	 * 
	 * @return the price no feed file subject
	 */
	public String getPriceNoFeedFileSubject() {
		return mPriceNoFeedFileSubject;
	}

	/**
	 * Gets the price feed name.
	 * 
	 * @return the price feed name
	 */
	public String getPriceFeedName() {
		return mPriceFeedName;
	}

	/**
	 * Sets the price feed name.
	 * 
	 * @param pPriceFeedName
	 *            the new price feed name
	 */
	public void setPriceFeedName(String pPriceFeedName) {
		mPriceFeedName = pPriceFeedName;
	}

	/**
	 * Sets the price no feed file subject.
	 * 
	 * @param pPriceNoFeedFileSubject
	 *            the new price no feed file subject
	 */
	public void setPriceNoFeedFileSubject(String pPriceNoFeedFileSubject) {
		mPriceNoFeedFileSubject = pPriceNoFeedFileSubject;
	}

	/**
	 * Gets the price invalid subject.
	 * 
	 * @return the price invalid subject
	 */
	public String getPriceInvalidSubject() {
		return mPriceInvalidSubject;
	}

	/**
	 * Sets the price invalid subject.
	 * 
	 * @param pPriceInvalidSubject
	 *            the new price invalid subject
	 */
	public void setPriceInvalidSubject(String pPriceInvalidSubject) {
		mPriceInvalidSubject = pPriceInvalidSubject;
	}

	/**
	 * Gets the detailed exception for no feed.
	 * 
	 * @return the detailed exception for no feed
	 */
	public String getDetailedExceptionForNoFeed() {
		return mDetailedExceptionForNoFeed;
	}

	/**
	 * Sets the detailed exception for no feed.
	 * 
	 * @param pDetailedExceptionForNoFeed
	 *            the new detailed exception for no feed
	 */
	public void setDetailedExceptionForNoFeed(String pDetailedExceptionForNoFeed) {
		mDetailedExceptionForNoFeed = pDetailedExceptionForNoFeed;
	}

	/**
	 * Gets the detailed exception for invalid feed.
	 * 
	 * @return the detailed exception for invalid feed
	 */
	public String getDetailedExceptionForInvalidFeed() {
		return mDetailedExceptionForInvalidFeed;
	}

	/**
	 * Sets the detailed exception for invalid feed.
	 * 
	 * @param pDetailedExceptionForInvalidFeed
	 *            the new detailed exception for invalid feed
	 */
	public void setDetailedExceptionForInvalidFeed(String pDetailedExceptionForInvalidFeed) {
		mDetailedExceptionForInvalidFeed = pDetailedExceptionForInvalidFeed;
	}

	/**
	 * Gets the attachment item id label.
	 * 
	 * @return the attachment item id label
	 */
	public String getAttachmentItemIdLabel() {
		return mAttachmentItemIdLabel;
	}

	/**
	 * Sets the attachment item id label.
	 * 
	 * @param pAttachmentItemIdLabel
	 *            the new attachment item id label
	 */
	public void setAttachmentItemIdLabel(String pAttachmentItemIdLabel) {
		mAttachmentItemIdLabel = pAttachmentItemIdLabel;
	}

	/**
	 * Gets the attachment exception reason label.
	 * 
	 * @return the attachment exception reason label
	 */
	public String getAttachmentExceptionReasonLabel() {
		return mAttachmentExceptionReasonLabel;
	}

	/**
	 * Sets the attachment exception reason label.
	 * 
	 * @param pAttachmentExceptionReasonLabel
	 *            the new attachment exception reason label
	 */
	public void setAttachmentExceptionReasonLabel(String pAttachmentExceptionReasonLabel) {
		mAttachmentExceptionReasonLabel = pAttachmentExceptionReasonLabel;
	}

	/**
	 * Gets the store success subject.
	 * 
	 * @return the mStoreSuccessSubject
	 */
	public String getStoreSuccessSubject() {
		return mStoreSuccessSubject;
	}

	/**
	 * Sets the store success subject.
	 * 
	 * @param pStoreSuccessSubject
	 *            the mStoreSuccessSubject to set
	 */
	public void setStoreSuccessSubject(String pStoreSuccessSubject) {
		this.mStoreSuccessSubject = pStoreSuccessSubject;
	}

	/**
	 * Gets the store processed with errors subject.
	 * 
	 * @return the mStoreProcessedWithErrorsSubject
	 */
	public String getStoreProcessedWithErrorsSubject() {
		return mStoreProcessedWithErrorsSubject;
	}

	/**
	 * Sets the store processed with errors subject.
	 * 
	 * @param pStoreProcessedWithErrorsSubject
	 *            the mStoreProcessedWithErrorsSubject to set
	 */
	public void setStoreProcessedWithErrorsSubject(String pStoreProcessedWithErrorsSubject) {
		this.mStoreProcessedWithErrorsSubject = pStoreProcessedWithErrorsSubject;
	}

	/**
	 * Gets the store no feed file subject.
	 * 
	 * @return the mStoreNoFeedFileSubject
	 */
	public String getStoreNoFeedFileSubject() {
		return mStoreNoFeedFileSubject;
	}

	/**
	 * Sets the store no feed file subject.
	 * 
	 * @param pStoreNoFeedFileSubject
	 *            the mStoreNoFeedFileSubject to set
	 */
	public void setStoreNoFeedFileSubject(String pStoreNoFeedFileSubject) {
		this.mStoreNoFeedFileSubject = pStoreNoFeedFileSubject;
	}

	/**
	 * Gets the store invalid subject.
	 * 
	 * @return the mStoreInvalidSubject
	 */
	public String getStoreInvalidSubject() {
		return mStoreInvalidSubject;
	}

	/**
	 * Sets the store invalid subject.
	 * 
	 * @param pStoreInvalidSubject
	 *            the mStoreInvalidSubject to set
	 */
	public void setStoreInvalidSubject(String pStoreInvalidSubject) {
		this.mStoreInvalidSubject = pStoreInvalidSubject;
	}

	/**
	 * Gets the store success status.
	 * 
	 * @return the mStoreSuccessStatus
	 */
	public String getStoreSuccessStatus() {
		return mStoreSuccessStatus;
	}

	/**
	 * Sets the store success status.
	 * 
	 * @param pStoreSuccessStatus
	 *            the mStoreSuccessStatus to set
	 */
	public void setStoreSuccessStatus(String pStoreSuccessStatus) {
		this.mStoreSuccessStatus = pStoreSuccessStatus;
	}

	/**
	 * Gets the registry threshold mail subject.
	 * 
	 * @return the registry threshold mail subject
	 */
	public String getRegistryThresholdMailSubject() {
		return mRegistryThresholdMailSubject;
	}

	/**
	 * Sets the registry threshold mail subject.
	 * 
	 * @param pRegistryThresholdMailSubject
	 *            the new registry threshold mail subject
	 */
	public void setRegistryThresholdMailSubject(String pRegistryThresholdMailSubject) {
		mRegistryThresholdMailSubject = pRegistryThresholdMailSubject;
	}

	/**
	 * Gets the website threshold mail subject.
	 * 
	 * @return the website threshold mail subject
	 */
	public String getWebsiteThresholdMailSubject() {
		return mWebsiteThresholdMailSubject;
	}

	/**
	 * Sets the website threshold mail subject.
	 * 
	 * @param pWebsiteThresholdMailSubject
	 *            the new website threshold mail subject
	 */
	public void setWebsiteThresholdMailSubject(String pWebsiteThresholdMailSubject) {
		mWebsiteThresholdMailSubject = pWebsiteThresholdMailSubject;
	}

	/**
	 * Gets the registry feed name.
	 * 
	 * @return the registry feed name
	 */
	public String getRegistryFeedName() {
		return mRegistryFeedName;
	}

	/**
	 * Sets the registry feed name.
	 * 
	 * @param pRegistryFeedName
	 *            the new registry feed name
	 */
	public void setRegistryFeedName(String pRegistryFeedName) {
		mRegistryFeedName = pRegistryFeedName;
	}

	/**
	 * Gets the website feed name.
	 * 
	 * @return the website feed name
	 */
	public String getWebsiteFeedName() {
		return mWebsiteFeedName;
	}

	/**
	 * Sets the website feed name.
	 * 
	 * @param pWebsiteFeedName
	 *            the new website feed name
	 */
	public void setWebsiteFeedName(String pWebsiteFeedName) {
		mWebsiteFeedName = pWebsiteFeedName;
	}

	/**
	 * Gets the lower cutoff alert priority3.
	 * 
	 * @return the lower cutoff alert priority3
	 */
	public double getLowerCutoffAlertPriority3() {
		return mLowerCutoffAlertPriority3;
	}

	/**
	 * Sets the lower cutoff alert priority3.
	 * 
	 * @param pLowerCutoffAlertPriority3
	 *            the new lower cutoff alert priority3
	 */
	public void setLowerCutoffAlertPriority3(double pLowerCutoffAlertPriority3) {
		mLowerCutoffAlertPriority3 = pLowerCutoffAlertPriority3;
	}

	/**
	 * Gets the upper cutoff alert priority3.
	 * 
	 * @return the upper cutoff alert priority3
	 */
	public double getUpperCutoffAlertPriority3() {
		return mUpperCutoffAlertPriority3;
	}

	/**
	 * Sets the upper cutoff alert priority3.
	 * 
	 * @param pUpperCutoffAlertPriority3
	 *            the new upper cutoff alert priority3
	 */
	public void setUpperCutoffAlertPriority3(double pUpperCutoffAlertPriority3) {
		mUpperCutoffAlertPriority3 = pUpperCutoffAlertPriority3;
	}

	/**
	 * Gets the lower cutoff exception alert priority3.
	 * 
	 * @return the lower cutoff exception alert priority3
	 */
	public double getLowerCutoffExceptionAlertPriority3() {
		return mLowerCutoffExceptionAlertPriority3;
	}

	/**
	 * Sets the lower cutoff exception alert priority3.
	 * 
	 * @param pLowerCutoffExceptionAlertPriority3
	 *            the new lower cutoff exception alert priority3
	 */
	public void setLowerCutoffExceptionAlertPriority3(double pLowerCutoffExceptionAlertPriority3) {
		mLowerCutoffExceptionAlertPriority3 = pLowerCutoffExceptionAlertPriority3;
	}

	/**
	 * Gets the upper cutoff exception alert priority3.
	 * 
	 * @return the upper cutoff exception alert priority3
	 */
	public double getUpperCutoffExceptionAlertPriority3() {
		return mUpperCutoffExceptionAlertPriority3;
	}

	/**
	 * Sets the upper cutoff exception alert priority3.
	 * 
	 * @param pUpperCutoffExceptionAlertPriority3
	 *            the new upper cutoff exception alert priority3
	 */
	public void setUpperCutoffExceptionAlertPriority3(double pUpperCutoffExceptionAlertPriority3) {
		mUpperCutoffExceptionAlertPriority3 = pUpperCutoffExceptionAlertPriority3;
	}

	/**
	 * Gets the cutoff exception alert priority2.
	 * 
	 * @return the cutoff exception alert priority2
	 */
	public double getCutoffExceptionAlertPriority2() {
		return mCutoffExceptionAlertPriority2;
	}

	/**
	 * Sets the cutoff exception alert priority2.
	 * 
	 * @param pCutoffExceptionAlertPriority2
	 *            the new cutoff exception alert priority2
	 */
	public void setCutoffExceptionAlertPriority2(double pCutoffExceptionAlertPriority2) {
		mCutoffExceptionAlertPriority2 = pCutoffExceptionAlertPriority2;
	}

	/**
	 * Gets the lower cutoff exception alert priority regular.
	 * 
	 * @return the lower cutoff exception alert priority regular
	 */
	public double getLowerCutoffExceptionAlertPriorityRegular() {
		return mLowerCutoffExceptionAlertPriorityRegular;
	}

	/**
	 * Sets the lower cutoff exception alert priority regular.
	 * 
	 * @param pLowerCutoffExceptionAlertPriorityRegular
	 *            the new lower cutoff exception alert priority regular
	 */
	public void setLowerCutoffExceptionAlertPriorityRegular(double pLowerCutoffExceptionAlertPriorityRegular) {
		mLowerCutoffExceptionAlertPriorityRegular = pLowerCutoffExceptionAlertPriorityRegular;
	}

	/**
	 * Gets the upper cutoff exception alert priority regular.
	 * 
	 * @return the upper cutoff exception alert priority regular
	 */
	public double getUpperCutoffExceptionAlertPriorityRegular() {
		return mUpperCutoffExceptionAlertPriorityRegular;
	}

	/**
	 * Sets the upper cutoff exception alert priority regular.
	 * 
	 * @param pUpperCutoffExceptionAlertPriorityRegular
	 *            the new upper cutoff exception alert priority regular
	 */
	public void setUpperCutoffExceptionAlertPriorityRegular(double pUpperCutoffExceptionAlertPriorityRegular) {
		mUpperCutoffExceptionAlertPriorityRegular = pUpperCutoffExceptionAlertPriorityRegular;
	}

	/**
	 * Gets the lower cutoff alert priority2.
	 * 
	 * @return the lower cutoff alert priority2
	 */
	public double getLowerCutoffAlertPriority2() {
		return mLowerCutoffAlertPriority2;
	}

	/**
	 * Sets the lower cutoff alert priority2.
	 * 
	 * @param pLowerCutoffAlertPriority2
	 *            the new lower cutoff alert priority2
	 */
	public void setLowerCutoffAlertPriority2(double pLowerCutoffAlertPriority2) {
		mLowerCutoffAlertPriority2 = pLowerCutoffAlertPriority2;
	}

	/**
	 * Gets the upper cutoff alert priority2.
	 * 
	 * @return the upper cutoff alert priority2
	 */
	public double getUpperCutoffAlertPriority2() {
		return mUpperCutoffAlertPriority2;
	}

	/**
	 * Sets the upper cutoff alert priority2.
	 * 
	 * @param pUpperCutoffAlertPriority2
	 *            the new upper cutoff alert priority2
	 */
	public void setUpperCutoffAlertPriority2(double pUpperCutoffAlertPriority2) {
		mUpperCutoffAlertPriority2 = pUpperCutoffAlertPriority2;
	}

	/**
	 * Gets the failure mail priority3.
	 * 
	 * @return the failure mail priority3
	 */
	public String getFailureMailPriority3() {
		return mFailureMailPriority3;
	}

	/**
	 * Sets the failure mail priority3.
	 * 
	 * @param pFailureMailPriority3
	 *            the new failure mail priority3
	 */
	public void setFailureMailPriority3(String pFailureMailPriority3) {
		mFailureMailPriority3 = pFailureMailPriority3;
	}

	/**
	 * Gets the mail priority regular.
	 * 
	 * @return the mail priority regular
	 */
	public String getMailPriorityRegular() {
		return mMailPriorityRegular;
	}

	/**
	 * Sets the mail priority regular.
	 * 
	 * @param pMailPriorityRegular
	 *            the new mail priority regular
	 */
	public void setMailPriorityRegular(String pMailPriorityRegular) {
		mMailPriorityRegular = pMailPriorityRegular;
	}

	/**
	 * Gets the failure mail priority2.
	 * 
	 * @return the failure mail priority2
	 */
	public String getFailureMailPriority2() {
		return mFailureMailPriority2;
	}

	/**
	 * Sets the failure mail priority2.
	 * 
	 * @param pFailureMailPriority2
	 *            the new failure mail priority2
	 */
	public void setFailureMailPriority2(String pFailureMailPriority2) {
		mFailureMailPriority2 = pFailureMailPriority2;
	}

	/**
	 * Gets the attachment project name label.
	 * 
	 * @return the mAttachmentProjectNameLabel
	 */
	public String getAttachmentProjectNameLabel() {
		return mAttachmentProjectNameLabel;
	}

	/**
	 * Sets the attachment project name label.
	 * 
	 * @param pAttachmentProjectNameLabel
	 *            the mAttachmentProjectNameLabel to set
	 */
	public void setAttachmentProjectNameLabel(String pAttachmentProjectNameLabel) {
		this.mAttachmentProjectNameLabel = pAttachmentProjectNameLabel;
	}

	/**
	 * Gets the price feed message from.
	 * 
	 * @return the price feed message from
	 */
	public String getPriceFeedMessageFrom() {
		return mPriceFeedMessageFrom;
	}

	/**
	 * Sets the price feed message from.
	 * 
	 * @param pPriceFeedMessageFrom
	 *            the new price feed message from
	 */
	public void setPriceFeedMessageFrom(String pPriceFeedMessageFrom) {
		mPriceFeedMessageFrom = pPriceFeedMessageFrom;
	}

	/**
	 * Gets the price feed message to.
	 * 
	 * @return the price feed message to
	 */
	public List<String> getPriceFeedMessageTo() {
		return mPriceFeedMessageTo;
	}

	/**
	 * Sets the price feed message to.
	 * 
	 * @param pPriceFeedMessageTo
	 *            the new price feed message to
	 */
	public void setPriceFeedMessageTo(List<String> pPriceFeedMessageTo) {
		mPriceFeedMessageTo = pPriceFeedMessageTo;
	}

	/**
	 * Gets the store feed message from.
	 * 
	 * @return the storeFeedMessageFrom
	 */
	public String getStoreFeedMessageFrom() {
		return mStoreFeedMessageFrom;
	}

	/**
	 * Sets the store feed message from.
	 * 
	 * @param pStoreFeedMessageFrom
	 *            the storeFeedMessageFrom to set
	 */
	public void setStoreFeedMessageFrom(String pStoreFeedMessageFrom) {
		mStoreFeedMessageFrom = pStoreFeedMessageFrom;
	}

	/**
	 * Gets the store feed message to.
	 * 
	 * @return the storeFeedMessageTo
	 */
	public List<String> getStoreFeedMessageTo() {
		return mStoreFeedMessageTo;
	}

	/**
	 * Sets the store feed message to.
	 * 
	 * @param pStoreFeedMessageTo
	 *            the storeFeedMessageTo to set
	 */
	public void setStoreFeedMessageTo(List<String> pStoreFeedMessageTo) {
		mStoreFeedMessageTo = pStoreFeedMessageTo;
	}

	/**
	 * Gets the store threshold mail subject.
	 * 
	 * @return the storeThresholdMailSubject
	 */
	public String getStoreThresholdMailSubject() {
		return mStoreThresholdMailSubject;
	}

	/**
	 * Sets the store threshold mail subject.
	 * 
	 * @param pStoreThresholdMailSubject
	 *            the storeThresholdMailSubject to set
	 */
	public void setStoreThresholdMailSubject(String pStoreThresholdMailSubject) {
		mStoreThresholdMailSubject = pStoreThresholdMailSubject;
	}

	/**
	 * Gets the registry success subject.
	 * 
	 * @return the mRegistrySuccessSubject
	 */
	public String getRegistrySuccessSubject() {
		return mRegistrySuccessSubject;
	}

	/**
	 * Sets the registry success subject.
	 * 
	 * @param pRegistrySuccessSubject
	 *            the mRegistrySuccessSubject to set
	 */
	public void setRegistrySuccessSubject(String pRegistrySuccessSubject) {
		this.mRegistrySuccessSubject = pRegistrySuccessSubject;
	}

	/**
	 * Gets the taxonomy success subject.
	 * 
	 * @return the mTaxonomySuccessSubject
	 */
	public String getTaxonomySuccessSubject() {
		return mTaxonomySuccessSubject;
	}

	/**
	 * Sets the taxonomy success subject.
	 * 
	 * @param pTaxonomySuccessSubject
	 *            the new taxonomy success subject
	 */
	public void setTaxonomySuccessSubject(String pTaxonomySuccessSubject) {
		this.mTaxonomySuccessSubject = pTaxonomySuccessSubject;
	}

	/**
	 * Gets the catalog feed message from.
	 * 
	 * @return the mCatalogFeedMessageFrom
	 */
	public String getCatalogFeedMessageFrom() {
		return mCatalogFeedMessageFrom;
	}

	/**
	 * Sets the catalog feed message from.
	 * 
	 * @param pCatalogFeedMessageFrom
	 *            the new catalog feed message from
	 */
	public void setCatalogFeedMessageFrom(String pCatalogFeedMessageFrom) {
		this.mCatalogFeedMessageFrom = pCatalogFeedMessageFrom;
	}

	/**
	 * Gets the catalog feed message to.
	 * 
	 * @return the mCatalogFeedMessageTo
	 */
	public List<String> getCatalogFeedMessageTo() {
		return mCatalogFeedMessageTo;
	}

	/**
	 * Sets the catalog feed message to.
	 * 
	 * @param pCatalogFeedMessageTo
	 *            the new catalog feed message to
	 */
	public void setCatalogFeedMessageTo(List<String> pCatalogFeedMessageTo) {
		this.mCatalogFeedMessageTo = pCatalogFeedMessageTo;
	}

	/**
	 * Gets the price feed files no sequence.
	 * 
	 * @return the mPriceFeedFilesNoSequence
	 */
	public String getPriceFeedFilesNoSequence() {
		return mPriceFeedFilesNoSequence;
	}

	/**
	 * Sets the price feed files no sequence.
	 * 
	 * @param pPriceFeedFilesNoSequence
	 *            the new price feed files no sequence
	 */
	public void setPriceFeedFilesNoSequence(String pPriceFeedFilesNoSequence) {
		this.mPriceFeedFilesNoSequence = pPriceFeedFilesNoSequence;
	}

	/**
	 * Gets the price feed files sequence no match.
	 * 
	 * @return the mPriceFeedFilesSequenceNoMatch
	 */
	public String getPriceFeedFilesSequenceNoMatch() {
		return mPriceFeedFilesSequenceNoMatch;
	}

	/**
	 * Sets the price feed files sequence no match.
	 * 
	 * @param pPriceFeedFilesSequenceNoMatch
	 *            the new price feed files sequence no match
	 */
	public void setPriceFeedFilesSequenceNoMatch(String pPriceFeedFilesSequenceNoMatch) {
		this.mPriceFeedFilesSequenceNoMatch = pPriceFeedFilesSequenceNoMatch;
	}

	/**
	 * Gets the store feed files sequence no match.
	 * 
	 * @return the mStoreFeedFilesSequenceNoMatch
	 */
	public String getStoreFeedFilesSequenceNoMatch() {
		return mStoreFeedFilesSequenceNoMatch;
	}

	/**
	 * Sets the store feed files sequence no match.
	 * 
	 * @param pStoreFeedFilesSequenceNoMatch
	 *            the new store feed files sequence no match
	 */
	public void setStoreFeedFilesSequenceNoMatch(String pStoreFeedFilesSequenceNoMatch) {
		this.mStoreFeedFilesSequenceNoMatch = pStoreFeedFilesSequenceNoMatch;
	}

	/**
	 * Gets the detailed exception for seq miss.
	 * 
	 * @return the mDetailedExceptionForSeqMiss
	 */
	public String getDetailedExceptionForSeqMiss() {
		return mDetailedExceptionForSeqMiss;
	}

	/**
	 * Sets the detailed exception for seq miss.
	 * 
	 * @param pDetailedExceptionForSeqMiss
	 *            the new detailed exception for seq miss
	 */
	public void setDetailedExceptionForSeqMiss(String pDetailedExceptionForSeqMiss) {
		this.mDetailedExceptionForSeqMiss = pDetailedExceptionForSeqMiss;
	}

	/**
	 * Gets the store feed name.
	 * 
	 * @return the store feed name
	 */
	public String getStoreFeedName() {
		return mStoreFeedName;
	}

	/**
	 * Sets the store feed name.
	 * 
	 * @param pStoreFeedName
	 *            the new store feed name
	 */
	public void setStoreFeedName(String pStoreFeedName) {
		this.mStoreFeedName = pStoreFeedName;
	}

	/**
	 * Gets the store feed success sub.
	 * 
	 * @return the store feed success sub
	 */
	public String getStoreFeedSuccessSub() {
		return mStoreFeedSuccessSub;
	}

	/**
	 * Sets the store feed success sub.
	 * 
	 * @param pStoreFeedSuccessSub
	 *            the new store feed success sub
	 */
	public void setStoreFeedSuccessSub(String pStoreFeedSuccessSub) {
		this.mStoreFeedSuccessSub = pStoreFeedSuccessSub;
	}

	/**
	 * Gets the store feed error sub.
	 * 
	 * @return the store feed error sub
	 */
	public String getStoreFeedErrorSub() {
		return mStoreFeedErrorSub;
	}

	/**
	 * Sets the store feed error sub.
	 * 
	 * @param pStoreFeedErrorSub
	 *            the new store feed error sub
	 */
	public void setStoreFeedErrorSub(String pStoreFeedErrorSub) {
		this.mStoreFeedErrorSub = pStoreFeedErrorSub;
	}

	/**
	 * Gets the store feed failure sub.
	 * 
	 * @return the store feed failure sub
	 */
	public String getStoreFeedFailureSub() {
		return mStoreFeedFailureSub;
	}

	/**
	 * Sets the store feed failure sub.
	 * 
	 * @param pStoreFeedFailureSub
	 *            the new store feed failure sub
	 */
	public void setStoreFeedFailureSub(String pStoreFeedFailureSub) {
		this.mStoreFeedFailureSub = pStoreFeedFailureSub;
	}

	/**
	 * Gets the store feed error detail excep.
	 * 
	 * @return the store feed error detail excep
	 */
	public String getStoreFeedErrorDetailExcep() {
		return mStoreFeedErrorDetailExcep;
	}

	/**
	 * Sets the store feed error detail excep.
	 * 
	 * @param pStoreFeedErrorDetailExcep
	 *            the new store feed error detail excep
	 */
	public void setStoreFeedErrorDetailExcep(String pStoreFeedErrorDetailExcep) {
		this.mStoreFeedErrorDetailExcep = pStoreFeedErrorDetailExcep;
	}

	/**
	 * Gets the invalid store detail excep.
	 * 
	 * @return the invalid store detail excep
	 */
	public String getInvalidStoreDetailExcep() {
		return mInvalidStoreDetailExcep;
	}

	/**
	 * Sets the invalid store detail excep.
	 * 
	 * @param pInvalidStoreDetailExcep
	 *            the new invalid store detail excep
	 */
	public void setInvalidStoreDetailExcep(String pInvalidStoreDetailExcep) {
		this.mInvalidStoreDetailExcep = pInvalidStoreDetailExcep;
	}

	/**
	 * Gets the price feed success sub.
	 * 
	 * @return the price feed success sub
	 */
	public String getPriceFeedSuccessSub() {
		return mPriceFeedSuccessSub;
	}

	/**
	 * Sets the price feed success sub.
	 * 
	 * @param pPriceFeedSuccessSub
	 *            the new price feed success sub
	 */
	public void setPriceFeedSuccessSub(String pPriceFeedSuccessSub) {
		this.mPriceFeedSuccessSub = pPriceFeedSuccessSub;
	}

	/**
	 * Gets the price feed error sub.
	 * 
	 * @return the price feed error sub
	 */
	public String getPriceFeedErrorSub() {
		return mPriceFeedErrorSub;
	}

	/**
	 * Sets the price feed error sub.
	 * 
	 * @param pPriceFeedErrorSub
	 *            the new price feed error sub
	 */
	public void setPriceFeedErrorSub(String pPriceFeedErrorSub) {
		this.mPriceFeedErrorSub = pPriceFeedErrorSub;
	}

	/**
	 * Gets the price feed failure sub.
	 * 
	 * @return the price feed failure sub
	 */
	public String getPriceFeedFailureSub() {
		return mPriceFeedFailureSub;
	}

	/**
	 * Sets the price feed failure sub.
	 * 
	 * @param pPriceFeedFailureSub
	 *            the new price feed failure sub
	 */
	public void setPriceFeedFailureSub(String pPriceFeedFailureSub) {
		this.mPriceFeedFailureSub = pPriceFeedFailureSub;
	}

	/**
	 * Gets the price feed error detail excep.
	 * 
	 * @return the price feed error detail excep
	 */
	public String getPriceFeedErrorDetailExcep() {
		return mPriceFeedErrorDetailExcep;
	}

	/**
	 * Sets the price feed error detail excep.
	 * 
	 * @param pPriceFeedErrorDetailExcep
	 *            the new price feed error detail excep
	 */
	public void setPriceFeedErrorDetailExcep(String pPriceFeedErrorDetailExcep) {
		this.mPriceFeedErrorDetailExcep = pPriceFeedErrorDetailExcep;
	}

	/**
	 * Gets the seo feed success sub.
	 * 
	 * @return the seoFeedSuccessSub
	 */
	public String getSeoFeedSuccessSub() {
		return mSeoFeedSuccessSub;
	}

	/**
	 * Sets the seo feed success sub.
	 * 
	 * @param pSeoFeedSuccessSub
	 *            the seoFeedSuccessSub to set
	 */
	public void setSeoFeedSuccessSub(String pSeoFeedSuccessSub) {
		mSeoFeedSuccessSub = pSeoFeedSuccessSub;
	}

	/**
	 * Gets the seo feed error sub.
	 * 
	 * @return the seoFeedErrorSub
	 */
	public String getSeoFeedErrorSub() {
		return mSeoFeedErrorSub;
	}

	/**
	 * Sets the seo feed error sub.
	 * 
	 * @param pSeoFeedErrorSub
	 *            the seoFeedErrorSub to set
	 */
	public void setSeoFeedErrorSub(String pSeoFeedErrorSub) {
		mSeoFeedErrorSub = pSeoFeedErrorSub;
	}

	/**
	 * Gets the seo feed failure sub.
	 * 
	 * @return the seoFeedFailureSub
	 */
	public String getSeoFeedFailureSub() {
		return mSeoFeedFailureSub;
	}

	/**
	 * Sets the seo feed failure sub.
	 * 
	 * @param pSeoFeedFailureSub
	 *            the seoFeedFailureSub to set
	 */
	public void setSeoFeedFailureSub(String pSeoFeedFailureSub) {
		mSeoFeedFailureSub = pSeoFeedFailureSub;
	}

	/**
	 * Gets the seo feed error detail excep.
	 * 
	 * @return the seoFeedErrorDetailExcep
	 */
	public String getSeoFeedErrorDetailExcep() {
		return mSeoFeedErrorDetailExcep;
	}

	/**
	 * Sets the seo feed error detail excep.
	 * 
	 * @param pSeoFeedErrorDetailExcep
	 *            the seoFeedErrorDetailExcep to set
	 */
	public void setSeoFeedErrorDetailExcep(String pSeoFeedErrorDetailExcep) {
		mSeoFeedErrorDetailExcep = pSeoFeedErrorDetailExcep;
	}

	/**
	 * Gets the seo feed message from.
	 * 
	 * @return the seoFeedMessageFrom
	 */
	public String getSeoFeedMessageFrom() {
		return mSeoFeedMessageFrom;
	}

	/**
	 * Sets the seo feed message from.
	 * 
	 * @param pSeoFeedMessageFrom
	 *            the seoFeedMessageFrom to set
	 */
	public void setSeoFeedMessageFrom(String pSeoFeedMessageFrom) {
		mSeoFeedMessageFrom = pSeoFeedMessageFrom;
	}

	/**
	 * Gets the seo feed message to.
	 * 
	 * @return the seoFeedMessageTo
	 */
	public String getSeoFeedMessageTo() {
		return mSeoFeedMessageTo;
	}

	/**
	 * Sets the seo feed message to.
	 * 
	 * @param pSeoFeedMessageTo
	 *            the seoFeedMessageTo to set
	 */
	public void setSeoFeedMessageTo(String pSeoFeedMessageTo) {
		mSeoFeedMessageTo = pSeoFeedMessageTo;
	}

}
