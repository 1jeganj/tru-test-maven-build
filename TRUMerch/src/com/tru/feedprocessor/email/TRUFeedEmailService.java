package com.tru.feedprocessor.email;

import java.io.File;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

/**
 * This class extends OOTB DynamoServlet to Email operation.
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedEmailService extends GenericService {

	/** property to hold Promotion Export Configuration. */
	private ServiceMap mEmailFeedConfiguartion;

	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;

	/**
	 * Send feed success email.
	 * 
	 * @param pFeedType
	 *            the feed type
	 * @param pParams
	 *            the params
	 */
	public void sendFeedSuccessEmail(String pFeedType, Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: sendFeedSuccessEmail()");
		try {
			TRUFeedEmailConfiguration truFeedEmailConfiguration = (TRUFeedEmailConfiguration) getEmailFeedConfiguartion()
					.get(pFeedType);
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getSuccessTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: sendFeedSuccessEmail()");
	}

	/**
	 * Send feed failure email.
	 * 
	 * @param pFeedType
	 *            the feed type
	 * @param pParams
	 *            the params
	 */
	public void sendFeedFailureEmail(String pFeedType, Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: sendFeedFailureEmail()");
		try {
			TRUFeedEmailConfiguration truFeedEmailConfiguration = (TRUFeedEmailConfiguration) getEmailFeedConfiguartion()
					.get(pFeedType);
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getFailureTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: sendFeedFailureEmail()");
	}

	/**
	 * Clear email info.
	 * 
	 * @param pEmailInfo
	 *            the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: TRUFeedEmailService : @Method: clearEmailInfo()");
	}

	/**
	 * Send feed processed with errors email.
	 * 
	 * @param pFeedType
	 *            the feed type
	 * @param pParams
	 *            the params
	 * @param pAttachementFiles
	 *            the attachement files
	 */
	public void sendFeedProcessedWithErrorsEmail(String pFeedType, Map<String, Object> pParams, File[] pAttachementFiles) {
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: sendFeedProcessedWithErrorsEmail()");
		try {
			TRUFeedEmailConfiguration truFeedEmailConfiguration = (TRUFeedEmailConfiguration) getEmailFeedConfiguartion()
					.get(pFeedType);
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getFailureTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			if (pAttachementFiles != null) {

				emailInfo.setMessageAttachments(pAttachementFiles);
			}
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("End:@Class: TRUFeedEmailService : @Method: sendFeedProcessedWithErrorsEmail()");
	}

	/**
	 * Gets the email feed configuartion.
	 * 
	 * @return the email feed configuartion
	 */
	public ServiceMap getEmailFeedConfiguartion() {
		return mEmailFeedConfiguartion;
	}

	/**
	 * Sets the email feed configuartion.
	 * 
	 * @param pEmailFeedConfiguartion
	 *            the new email feed configuartion
	 */
	public void setEmailFeedConfiguartion(ServiceMap pEmailFeedConfiguartion) {
		mEmailFeedConfiguartion = pEmailFeedConfiguartion;
	}

	/**
	 * Gets the template email sender.
	 * 
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the mTemplateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
	
	/**
	 * Send feed success email
	 * @param pServiceMap service map
	 * @param pInvalidRecordsFile invalid record file
	 */
	public void sendFeedSuccessEmail(Map<String, Object> pServiceMap,
			File pInvalidRecordsFile) {
		// TODO Auto-generated method stub
		
	}
}
