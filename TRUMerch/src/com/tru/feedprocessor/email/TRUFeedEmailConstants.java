package com.tru.feedprocessor.email;

/**
 * The Class TRUFeedEmailConstants.
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedEmailConstants {
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_SUBJECT = "messageSubject";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_CONTENT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_CONTENT = "messageContent";
	
	/** The Constant TEMPLATE_PARAMETER_SITE_ID. */
	public static final String TEMPLATE_PARAMETER_SITE_ID = "siteId";
	
	/** The Constant TEMPLATE_PARAMETER_REPORT_SYMPTOM. */
	public static final String TEMPLATE_PARAMETER_REPORT_SYMPTOM = "reportSymptom";
	
	/** The Constant TEMPLATE_PARAMETER_PRIORITY. */
	public static final String TEMPLATE_PARAMETER_PRIORITY = "priority";
	
	/** The Constant TEMPLATE_PARAMETER_QUEUE. */
	public static final String TEMPLATE_PARAMETER_QUEUE = "queue";
	
	/** The Constant TEMPLATE_PARAMETER_NOTES. */
	public static final String TEMPLATE_PARAMETER_NOTES = "notes";
	
	/** The Constant TEMPLATE_PARAMETER_SOFTWARE. */
	public static final String TEMPLATE_PARAMETER_SOFTWARE = "software";
	
	/** The Constant TEMPLATE_PARAMETER_OS_NAME. */
	public static final String TEMPLATE_PARAMETER_OS_NAME = "OSName";
	
	/** The Constant TEMPLATE_PARAMETER_JAVA_VERSION. */
	public static final String TEMPLATE_PARAMETER_JAVA_VERSION = "javaVersion";
	
	/** The Constant TEMPLATE_PARAMETER_USER_HOME. */
	public static final String TEMPLATE_PARAMETER_USER_HOME = "userHome";
	
	/** The Constant TEMPLATE_PARAMETER_USER_NAME. */
	public static final String TEMPLATE_PARAMETER_USER_NAME = "userName";
	
	/** The Constant TEMPLATE_PARAMETER_DETAIL_EXCEPTION. */
	public static final String TEMPLATE_PARAMETER_DETAIL_EXCEPTION = "detailException";
	
	/** The Constant TEMPLATE_PARAMETER_OS_HOST_NAME. */
	public static final String TEMPLATE_PARAMETER_OS_HOST_NAME = "OSHostName";
	
	/** The Constant TEMPLATE_PARAMETER_STATUS. */
	public static final String TEMPLATE_PARAMETER_STATUS = "status";
	
	/** The Constant TEMPLATE_PARAMETER_TIME_STAMP. */
	public static final String TEMPLATE_PARAMETER_TIME_STAMP = "timeStamp";
	
	/** The Constant TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT = "totalRecordCount";
	
	/** The Constant TEMPLATE_PARAMETER_PROJECT_NAME. */
	public static final String TEMPLATE_PARAMETER_PROJECT_NAME = "projectName";
	
	/** The Constant TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT = "totalFailedRecordCount";
	
	/** The Constant TEMPLATE_PARAMETER_ERROR_FILE_PATH. */
	public static final String TEMPLATE_PARAMETER_ERROR_FILE_PATH = "errorFilePath";
	
	/** The Constant TEMPLATE_PARAMETER_ACTION_TYPE. */
	public static final String TEMPLATE_PARAMETER_ACTION_TYPE = "actionType";
	
	/** The Constant STORE_FEED. */
	public static final String STORE_FEED = "Store Feed";
	
	/** The Constant TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT = "successRecordCount";
	
	/** The Constant TEMPLATE_PARAMETER_SEQUENCE_NUMBER. */
	public static final String TEMPLATE_PARAMETER_SEQUENCE_NUMBER = "sequenceNumber";
}
