

-- drop table ral_errorhandler_xlate;
-- drop table ral_errorhandler_error_xlate;
-- drop table ral_errorhandler;


CREATE TABLE ral_errorhandler (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0)	,
	checkin_date timestamp (6), 
	error_id varchar2(40) NOT NULL,
	error_key varchar2(254) NOT NULL,
	error_message varchar2(4000) NULL,
	constraint ral_errorhandler_p PRIMARY KEY(error_id,error_key,asset_version) 
	);

CREATE TABLE ral_errorhandler_error_xlate ( 
    asset_version	number(19)	not null,
	error_id varchar2(40) NOT NULL, 
	locale varchar2(254) NOT NULL, 
	translation_id varchar2(254) NOT NULL,
	constraint ral_errorhandler_error_xlate_p primary key (error_id,locale,asset_version)
	);

CREATE TABLE ral_errorhandler_xlate (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0)	,
	checkin_date timestamp (6), 
	translation_id varchar2(254) NOT NULL, 
	error_message varchar2(4000) NULL, 
	constraint ral_errorhandler_xlate_p PRIMARY KEY(translation_id,asset_version)
	);
	
