alter table tru_catalog modify root_nav_cat varchar2(40);
alter table tru_classification add is_compareProduct number(1);

alter table tru_product modify registry_classification_id varchar2(40);

alter table tru_prd_site_eligibility modify product_id varchar2(40);

alter table tru_prd_root_parent_cat modify root_parent_category varchar2(40);

alter table tru_prd_parent_clfs modify parent_classification varchar2(40);

alter table tru_sku_strl_extra modify sku_id varchar2(40);

alter table tru_sku modify sku_id varchar2(40);

alter table tru_sku modify original_parent_product varchar2(40);

ALTER TABLE tru_product MODIFY rus_item_number Number(20);

alter table tru_sku add swatch_image varchar2(254) NULL;
alter table tru_sku add	primary_image varchar2(254)	NULL;
alter table tru_sku add	secondary_image varchar2(254) NULL;
alter table tru_sku add long_desc clob null;
alter table tru_sku add	interest varchar2(254) NULL;

ALTER TABLE tru_sku_alt_image MODIFY alternate_image varchar2(254);

alter table tru_sku drop column swatch_image_count;
alter table tru_sku drop column primary_image_count;
alter table tru_sku drop column secondary_image_count;
alter table tru_sku drop column alternate_image_count;
alter table tru_sku drop column cross_sells_batteries;
alter table tru_sku drop column uid_battery_cross_sell;
alter table tru_sku drop column battery_type;
alter table tru_sku drop column battery_quantity;

CREATE TABLE tru_sku_alt_image (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	alternate_image 	varchar2(40)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

alter table tru_sku_parent_clfs modify parent_classification varchar2(40);

alter table tru_sku_root_parent_cats modify root_parent_category varchar2(40);


CREATE TABLE tru_sku_crosssell_battery (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crosssell_battery 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE TABLE tru_sku_battery (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	battery 		varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);
alter table tru_classification modify suggested_quantity varchar2(40);

alter table tru_classification modify parent_category varchar2(40);
alter table tru_classification modify root_parent_category varchar2(40);

alter table tru_parnt_clfs modify parent_classification varchar2(40);

CREATE TABLE tru_clf_rltd_media (
	classification_id 	varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	related_media_id 	varchar2(254)	NULL,
	PRIMARY KEY(classification_id, asset_version, sequence_num)
);

CREATE TABLE tru_cross_sells_batteries (
	cross_sells_batteries_id varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	cross_sells_batteries 	varchar2(254)	NULL,
	uid_battery_cross_sell 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(cross_sells_batteries_id, asset_version)
);

CREATE TABLE tru_battery (
	battery_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	battery_type 		varchar2(254)	NULL,
	battery_quantity 	INTEGER	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(battery_id, asset_version)
);


drop table tru_html_content_item cascade constraints;

drop table tru_banner_content_item cascade constraints;

drop table tru_content_item cascade constraints;

alter table tru_how_to_get_it_msg modify msg_id varchar2(40);

alter table tru_product add why_we_love_it CLOB NULL;

ALTER TABLE tru_site_configuration ADD newProduct_Threshold varchar2(254) NULL;