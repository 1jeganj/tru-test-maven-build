

 drop table tru_how_to_get_it_msg;

 drop table tru_bpp;

 drop table tru_sku_alt_image;
 drop table tru_sku_crosssell_battery;
 drop table tru_sku_battery;
 drop table tru_clf_rltd_media;
 drop table tru_cross_sells_batteries;
 drop table tru_battery;

 drop table tru_clf_cros_refernces;

 drop table tru_child_clfs;

 drop table tru_parnt_clfs;

 drop table tru_classification;

  drop table tru_wcm_media_content;


 drop table tru_sku_lower48_map;

 drop table tru_sku_item_dims_map;

 drop table tru_sku_akhi_map;

 drop table tru_sku_assembly_map;

 drop table tru_sku_prod_featuers;

 drop table tru_sku_online_collec_name;

 drop table tru_sku_root_parent_cats;

 drop table tru_sku_parent_clfs;

 drop table tru_sku_safety_warning;

 drop table tru_sku_upc_numbers;

 drop table tru_sku_brd_name_teritary_thme;

 drop table tru_sku_brd_name_sec;

 drop table tru_sku_cross_sell;

 drop table tru_sku;

 drop table TRU_SKU_VIDEO_GAME;

 drop table tru_book_cd_dvd_sku_map;

 drop table tru_sku_car_seat_whatisimp;

 drop table tru_sku_car_seat_type;

 drop table tru_sku_car_seat_feature;

 drop table tru_car_seat_sku_map;

 drop table tru_sku_crib_whatisimp;

 drop table tru_sku_crib_type;

 drop table tru_sku_crib_style;

 drop table tru_sku_crib_material_type;

 drop table tru_crib_sku_map;

 drop table tru_sku_strl_whatisimp;

 drop table tru_sku_strl_type;

 drop table tru_sku_strl_extra;

 drop table tru_sku_strl_best_use;

 drop table tru_strl_sku_map;


 drop table tru_prd_style_dims;

 drop table tru_prd_parent_clfs;

 drop table tru_prd_root_parent_cat;

 drop table tru_prd_site_eligibility;

 drop table tru_product;



 drop table tru_catalog;

 drop table tru_collection_product;
 
 drop table tru_how_to_get_it_msg;
 
 drop table tru_giftfinder;
 
 drop table tru_gift_finder_stack_list;

CREATE TABLE tru_giftfinder (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	name 			varchar2(254)	NULL,
	type 			INTEGER	NULL,
	nvalue 			varchar2(254)	NULL,
	imageURL 		varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(id, asset_version)
);


CREATE TABLE tru_gift_finder_stack_list (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	gift_finder_stack_list 	varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, sequence_num)
);

CREATE INDEX tru_gift_stack_list_idx ON tru_gift_finder_stack_list(id, asset_version, sequence_num);

CREATE TABLE tru_catalog (
	catalog_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	root_nav_cat 		varchar2(40)	NULL,
	PRIMARY KEY(catalog_id, asset_version)
);

CREATE INDEX tru_catalog_idx ON tru_catalog(catalog_id, asset_version);
CREATE TABLE tru_classification (
	classification_id 	varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	name 			varchar2(255)	NULL,
	longDescription 	varchar2(2500)	NULL,
	user_type_id 		varchar2(30)	NULL,
	display_status 		varchar2(6)	NULL,
	display_order 		INTEGER	NULL,
	collection_image 	varchar2(255)	NULL,
	linked_node 		varchar2(40)	NULL,
	have_value 		varchar2(20)	NULL,
	suggested_quantity 	INTEGER	NULL,
	classification_type 	varchar2(20)	NULL,
	parent_category 	varchar2(40)	NULL,
	root_parent_category 	varchar2(40)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(classification_id, asset_version)
);

CREATE TABLE tru_wcm_media_content (
                id                               varchar2(254)    NOT NULL,
                asset_version                    INTEGER              NOT NULL,
                htmlContent                      CLOB     NULL,
                PRIMARY KEY(id, asset_version)
);

CREATE INDEX tru_wcm_media_content_idx ON tru_wcm_media_content(id, asset_version);


CREATE TABLE tru_product (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	channel_availability 	INTEGER	NULL,
	special_assembly_instructions varchar2(255)	NULL,
	rus_item_number 	Number(20)	NULL,
	gender 			varchar2(10)	NULL,
	cost 			varchar2(20)	NULL,
	vendors_product_demo_url varchar2(255)	NULL,
	EWASTE_SURCHARGE_STATE 	varchar2(5)	NULL,
	EWASTE_SURCHARGE_SKU 	varchar2(10)	NULL,
	BATTERY_REQUIRED 	varchar2(1)	NULL,
	BATTERY_INCLUDED 	varchar2(1)	NULL,
	brand_name_primary 	varchar2(100)	NULL,
	item_in_store_pick_up 	varchar2(3)	NULL,
	ship_from_store_eligible varchar2(30)	NULL,
	tax_code 		varchar2(364)	NULL,
	tax_product_value_code 	varchar2(3)	NULL,
	manufacturer_style_number varchar2(20)	NULL,
	non_merchandise_flag 	varchar2(1)	NULL,
	countrye_eligibility 	varchar2(3)	NULL,
	super_display_flag 	varchar2(1)	NULL,
	registry_classification_id varchar2(40) NULL ,
	vendor_classification_id varchar2(40)	NULL,
	product_htgit_msg 	INTEGER	NULL,
	PRIMARY KEY(product_id, asset_version)
);

CREATE INDEX tru_product_idx ON tru_product(product_id, asset_version);

CREATE TABLE tru_prd_site_eligibility (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	site_eligibility 	varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_prd_site_eligibility_idx ON tru_prd_site_eligibility(product_id, asset_version, sequence_num);

CREATE TABLE tru_prd_root_parent_cat (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	root_parent_category 	varchar2(40)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_prd_root_parent_cat_idx ON tru_prd_root_parent_cat(product_id, asset_version, sequence_num);

CREATE TABLE tru_prd_parent_clfs (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_prd_parent_clfs_idx ON tru_prd_parent_clfs(product_id, asset_version, sequence_num);

CREATE TABLE tru_prd_style_dims (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	style_dimension 	varchar2(20)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_prd_style_dims_idx ON tru_prd_style_dims(product_id, asset_version, sequence_num);


CREATE TABLE tru_strl_sku_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	strl_sku_id 		varchar2(40)	NOT NULL,
	strl_properties_map 	varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, strl_sku_id)
);

CREATE INDEX tru_strl_sku_map_idx ON tru_strl_sku_map(id, asset_version, strl_sku_id);

CREATE TABLE tru_sku_strl_best_use (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	strl_best_use 		varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_strl_best_use_idx ON tru_sku_strl_best_use(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_strl_extra (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	strl_extra 		varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_strl_extra_idx ON tru_sku_strl_extra(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_strl_type (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	strl_type 		varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_strl_type_idx ON tru_sku_strl_type(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_strl_whatisimp (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	strl_whatisimp 		varchar2(255)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_strl_whatisimp_idx ON tru_sku_strl_whatisimp(sku_id, asset_version, sequence_num);

CREATE TABLE tru_crib_sku_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	crib_sku_id 		varchar2(40)	NOT NULL,
	crib_properties_map 	varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, crib_sku_id)
);

CREATE INDEX tru_crib_sku_map_idx ON tru_crib_sku_map(id, asset_version, crib_sku_id);

CREATE TABLE tru_sku_crib_material_type (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crib_material_type 	varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_crib_material_type_idx ON tru_sku_crib_material_type(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_crib_style (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crib_style 		varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_crib_style_sku_idx ON tru_sku_crib_style(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_crib_type (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crib_type 		varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_crib_type_sku_idx ON tru_sku_crib_type(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_crib_whatisimp (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crib_whatisimp 		varchar2(50)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_crib_whatisimp_idx ON tru_sku_crib_whatisimp(sku_id, asset_version, sequence_num);

CREATE TABLE tru_car_seat_sku_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	car_seat_sku_id 	varchar2(40)	NOT NULL,
	car_seat_properties_map varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, car_seat_sku_id)
);

CREATE INDEX tru_car_seat_sku_map_idx ON tru_car_seat_sku_map(id, asset_version, car_seat_sku_id);

CREATE TABLE tru_sku_car_seat_feature (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	car_seat_feature 	varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_car_seat_feature_idx ON tru_sku_car_seat_feature(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_car_seat_type (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	car_seat_type 		varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_car_seat_type_idx ON tru_sku_car_seat_type(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_car_seat_whatisimp (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	car_seat_whatisimp 	varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_car_seat_whatisimp_idx ON tru_sku_car_seat_whatisimp(sku_id, asset_version, sequence_num);

CREATE TABLE tru_book_cd_dvd_sku_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	book_cd_dvd_sku_id 	varchar2(40)	NOT NULL,
	book_cd_dvd_properties_map varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, book_cd_dvd_sku_id)
);

CREATE INDEX tru_book_cd_dvd_sku_map_idx ON tru_book_cd_dvd_sku_map(id, asset_version, book_cd_dvd_sku_id);

CREATE TABLE TRU_SKU_VIDEO_GAME (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	VIDEO_GAME_GENRE 	varchar2(50)	NULL,
	VIDEO_GAME_PLATFORM 	varchar2(50)	NULL,
	VIDEO_GAME_ESRB 	varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version)
);

CREATE INDEX TRU_SKU_VIDEO_GAME_sku_idx ON TRU_SKU_VIDEO_GAME(sku_id, asset_version);

CREATE TABLE tru_sku (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	back_order_status 	varchar2(1)	NULL,
	browse_hidden_keyword 	varchar2(255)	NULL,
	CAN_BE_GIFT_WRAPPED 	varchar2(1)	NULL,
	child_weight_max 	varchar2(3)	NULL,
	child_weight_min 	varchar2(3)	NULL,
	COLOR_CODE 		varchar2(40)	NULL,
	color_upc_level 	varchar2(40)	NULL,
	CUSTOMER_PURCHASE_LIMIT varchar2(10)	NULL,
	drop_ship_flag 		varchar2(1)	NULL,
	FLEXIBLE_SHIPPING_PLAN 	varchar2(100)	NULL,
	FREIGHT_CLASS 			varchar2(100)	NULL,
	furniture_finish 	varchar2(50)	NULL,
	INCREMENTAL_SAFETY_STOCK_UNITS varchar2(10)	NULL,
	juvenile_product_size 	varchar2(30)	NULL,
	max_target_age_tru_websites varchar2(50)	NULL,
	mfr_suggested_age_max 	varchar2(50)	NULL,
	mfr_suggested_age_min 	varchar2(50)	NULL,
	min_target_age_tru_websites varchar2(50)	NULL,
	NMWA_PERCENTAGE 	varchar2(255)	NULL,
	ship_window_max 	varchar2(10)	NULL,
	ship_window_min 	varchar2(10)	NULL,
	online_pid 		varchar2(40)	NULL,
	original_pid 		varchar2(40)	NULL,
	original_parent_product varchar2(40)	NULL ,
	street_date 		DATE	NULL,
	OTHER_FIXED_SURCHARGE 	varchar2(1)	NULL,
	OTHER_STD_F_S_DOLLAR 	varchar2(40)	NULL,
	outlet 			varchar2(1)	NULL,
	PRESELL_QUANTITY_UNITS 	varchar2(10)	NULL,
	price_display 		varchar2(100)	NULL,
	product_awards 		varchar2(2000)	NULL,
	promotional_sticker_display varchar2(50)	NULL,
	registry_warning_indicator varchar2(10)	NULL,
	registry_eligibility 	varchar2(1)	NULL,
	SAFETY_STOCK 		varchar2(10)	NULL,
	SHIP_IN_ORIG_CONTAINER 	varchar2(1)	NULL,
	SUPRESS_IN_SEARCH 	varchar2(1)	NULL,
	SYSTEM_REQUIREMENTS 	varchar2(255)	NULL,
	NMWA 			varchar2(1)	NULL,
	web_disp_flag 		varchar2(1)	NULL,
	legal_notice 		varchar2(2000)	NULL,
	super_display_flag 	varchar2(1)	NULL,
	swatch_image 		varchar2(254)	NULL,
	primary_image 		varchar2(254)	NULL,
	secondary_image 	varchar2(254)	NULL,
	bpp_eligible 		varchar2(3)	NULL,
	bpp_name 		varchar2(200)	NULL,
	slapper_item 		varchar2(3)	NULL,
	ship_to_store_eligible 	varchar2(3)	NULL,
	review_rating 		varchar2(40)	NULL,
	review_count 		number(28, 10)	NULL,
	bpp_id 			number(28, 10)	NULL,
	long_desc 		CLOB	NULL,
	interest 	varchar2(254) NULL,
	CHECK (drop_ship_flag IN (0, 1)),
	CHECK (outlet IN (0, 1)),
	CHECK (registry_eligibility IN (0, 1)),
	CHECK (SHIP_IN_ORIG_CONTAINER IN (0, 1)),
	CHECK (SUPRESS_IN_SEARCH IN (0, 1)),
	CHECK (NMWA IN (0, 1)),
	CHECK (web_disp_flag IN (0, 1)),
	CHECK (bpp_eligible IN (0, 1)),
	CHECK (slapper_item IN (0, 1)),
	CHECK (ship_to_store_eligible IN (0, 1)),
	PRIMARY KEY(sku_id, asset_version)
);

CREATE INDEX tru_sku_sku_idx ON tru_sku(sku_id, asset_version);

CREATE TABLE tru_sku_cross_sell (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	cross_sells_sku_id 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_cross_sell_idx ON tru_sku_cross_sell(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_alt_image (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	alternate_image 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_alt_image_sku_idx ON tru_sku_alt_image(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_brd_name_sec (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	brand_name_secondary 	varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_brd_name_sec_idx ON tru_sku_brd_name_sec(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_brd_name_teritary_thme (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	brand_name_tertiary_theme varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_brd_name_thme_idx ON tru_sku_brd_name_teritary_thme(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_upc_numbers (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	upc_number 		varchar2(13)	NOT NULL,
	PRIMARY KEY(sku_id, asset_version, upc_number)
);

CREATE INDEX tru_sku_upc_numbers_idx ON tru_sku_upc_numbers(sku_id, asset_version, upc_number);

CREATE TABLE tru_sku_safety_warning (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	safety_warning 		varchar2(100)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_safety_warn_idx ON tru_sku_safety_warning(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_parent_clfs (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_parent_clfs_idx ON tru_sku_parent_clfs(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_root_parent_cats (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	root_parent_category 	varchar2(40)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_root_parent_cats_idx ON tru_sku_root_parent_cats(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_online_collec_name (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	online_collection_name 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_online_collec_name_idx ON tru_sku_online_collec_name(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_prod_featuers (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	prod_feature_id 	varchar2(254)	NOT NULL,
	prod_feature 		varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, prod_feature_id)
);

CREATE INDEX tru_sku_prod_featuers_idx ON tru_sku_prod_featuers(id, asset_version, prod_feature_id);

CREATE TABLE tru_sku_assembly_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	assembly_map_id 	varchar2(254)	NOT NULL,
	assembly_map 		varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, assembly_map_id)
);

CREATE INDEX tru_sku_assembly_map_idx ON tru_sku_assembly_map(id, asset_version, assembly_map_id);

CREATE TABLE tru_sku_akhi_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	akhi_map_id 		varchar2(254)	NOT NULL,
	akhi_map 		varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, akhi_map_id)
);

CREATE INDEX tru_sku_akhi_map_idx ON tru_sku_akhi_map(id, asset_version, akhi_map_id);

CREATE TABLE tru_sku_item_dims_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	item_dimensions_map_id 	varchar2(254)	NOT NULL,
	item_dimension_map 	varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, item_dimensions_map_id)
);

CREATE INDEX tru_sku_item_dims_map_idx ON tru_sku_item_dims_map(id, asset_version, item_dimensions_map_id);

CREATE TABLE tru_sku_lower48_map (
	id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	lower48_map_id 		varchar2(254)	NOT NULL,
	lower48_map 		varchar2(254)	NULL,
	PRIMARY KEY(id, asset_version, lower48_map_id)
);

CREATE INDEX tru_sku_lower48_map_idx ON tru_sku_lower48_map(id, asset_version, lower48_map_id);

CREATE TABLE tru_sku_crosssell_battery (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	crosssell_battery 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_crosssell_battery_sku_idx ON tru_sku_crosssell_battery(sku_id, asset_version, sequence_num);

CREATE TABLE tru_sku_battery (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	battery 		varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);

CREATE INDEX tru_sku_battery_sku_idx ON tru_sku_battery(sku_id, asset_version, sequence_num);



CREATE TABLE tru_parnt_clfs (
	classification_id 	varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL,
	PRIMARY KEY(classification_id, asset_version, sequence_num)
);

CREATE INDEX tru_parnt_clfs_clf_idx ON tru_parnt_clfs(classification_id, asset_version, sequence_num);

CREATE TABLE tru_child_clfs (
	classification_id 	varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	child_classification 	varchar2(40)	NULL,
	PRIMARY KEY(classification_id, asset_version, sequence_num)
);

CREATE INDEX tru_child_clfs_clf_idx ON tru_child_clfs(classification_id, asset_version, sequence_num);

CREATE TABLE tru_clf_cros_refernces (
	classification_id 	varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	cross_reference 	varchar2(40)	NULL,
	PRIMARY KEY(classification_id, asset_version, sequence_num)
);

CREATE INDEX tru_clf_cros_refernces_idx ON tru_clf_cros_refernces(classification_id, asset_version, sequence_num);

CREATE TABLE tru_clf_rltd_media (
       classification_id                varchar2(254)    NOT NULL,
       asset_version                    INTEGER              NOT NULL,
      classification_media_key         varchar2(254)    NOT NULL,
      related_media_id            varchar2(254)    NULL,
      PRIMARY KEY(classification_id, asset_version, classification_media_key)
);


CREATE INDEX tru_clf_rltd_media_clf_idx ON tru_clf_rltd_media(classification_id, asset_version, classification_media_key);

CREATE TABLE tru_cross_sells_batteries (
	cross_sells_batteries_id varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	cross_sells_batteries 	varchar2(254)	NULL,
	uid_battery_cross_sell 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(cross_sells_batteries_id, asset_version)
);

CREATE TABLE tru_battery (
	battery_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	battery_type 		varchar2(254)	NULL,
	battery_quantity 	INTEGER	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(battery_id, asset_version)
);

CREATE TABLE tru_bpp (
	bpp_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	group_number 		varchar2(40)	NULL,
	bpp_name 		varchar2(255)	NULL,
	low 			INTEGER	 NULL,
	high 			INTEGER	 NULL,
	term 			varchar2(50)	NULL,
	retail 			INTEGER	 NULL,
	description 		varchar2(255)	NULL,
	jsd_description 	varchar2(256)	NULL,
	jda_sku 		varchar2(40)	NULL,
	jda_short_desc 		varchar2(40)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(bpp_id, asset_version)
);

CREATE TABLE tru_collection_product (
	product_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	child_prd_id 		varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_coll_prd_idx ON tru_collection_product(product_id, asset_version, sequence_num);

CREATE TABLE tru_how_to_get_it_msg (
	msg_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	store_message_key 	INTEGER	NULL,
	store_Message 		CLOB	NULL,
	freight_key 		INTEGER	NULL,
	freight_message 	CLOB	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(msg_id, asset_version)
);

-- drop table tru_product_attributes;

CREATE TABLE tru_product_attributes (
	product_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	product_attributes 	varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);

CREATE INDEX tru_product_attributes_product_idx ON tru_product_attributes(product_id, asset_version, sequence_num);

CREATE TABLE tru_attribute (
	id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	attribute_name 		varchar2(254)	NULL,
	attribute_value 	CLOB	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(id, asset_version)
);

CREATE TABLE tru_attribute_name (
	attribute_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	attribute_name 		varchar2(254)	NULL,
	attribute_group 	varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(attribute_id, asset_version)
);

CREATE TABLE tru_attribute_group (
	attribute_group_id 	varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	attribute_group_name 	varchar2(254)	NULL,
	attribute_group_sequence varchar2(254)	NULL,
	version_deleted 	number(1)	NULL,
	version_editable 	number(1)	NULL,
	pred_version 		INTEGER	NULL,
	workspace_id 		varchar2(254)	NULL,
	branch_id 		varchar2(254)	NULL,
	is_head 		number(1)	NULL,
	checkin_date 		DATE	NULL,
	CHECK (version_deleted IN (0, 1)),
	CHECK (version_editable IN (0, 1)),
	CHECK (is_head IN (0, 1)),
	PRIMARY KEY(attribute_group_id, asset_version)
);
