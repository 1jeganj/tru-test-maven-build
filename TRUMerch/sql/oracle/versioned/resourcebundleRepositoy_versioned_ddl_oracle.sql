-- drop table ral_resourcebundle_xlate;
-- drop table ral_resourcebundle_key_xlate;
-- drop table ral_resourcebundle;

CREATE TABLE ral_resourcebundle ( 
    ASSET_VERSION    NUMBER(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR2(40) NOT NULL,
    BRANCH_ID        VARCHAR2(40) NOT NULL,
    IS_HEAD          NUMBER(1,0) NOT NULL,
    VERSION_DELETED  NUMBER(1,0) NOT NULL,
    VERSION_EDITABLE NUMBER(1,0) NOT NULL,
    PRED_VERSION     NUMBER(19,0) NULL,
    CHECKIN_DATE TIMESTAMP(6) NULL,
    key_id varchar2(40) NOT NULL,
    key_name varchar2(254)NOT NULL,
    key_value varchar2(4000) NULL,
    constraint ral_resourcebundle_p PRIMARY KEY(key_id,key_name,asset_version) ); 
	
CREATE TABLE ral_resourcebundle_key_xlate ( 
	asset_version INTEGER NOT NULL,
	key_id varchar2(40) NOT NULL, 
	locale varchar2(254) NOT NULL, 
	translation_id varchar2(254) NULL, 
	constraint ral_resourcebundle_key_xlate_p PRIMARY KEY(key_id, locale, asset_version) ); 
	

CREATE INDEX ral_resourcebundle_key_xlate_idx ON ral_resourcebundle_key_xlate(key_id, asset_version, locale);

CREATE TABLE ral_resourcebundle_xlate ( 
	ASSET_VERSION    NUMBER(19,0) NOT NULL,
    WORKSPACE_ID     VARCHAR2(40) NOT NULL,
    BRANCH_ID        VARCHAR2(40) NOT NULL,
    IS_HEAD          NUMBER(1,0) NOT NULL,
    VERSION_DELETED  NUMBER(1,0) NOT NULL,
    VERSION_EDITABLE NUMBER(1,0) NOT NULL,
    PRED_VERSION     NUMBER(19,0) NULL,
    CHECKIN_DATE TIMESTAMP(6) NULL,
	translation_id varchar2(254) NOT NULL,
	key_value varchar2(4000) NULL, 
	constraint ral_resourcebundle_xlate_p PRIMARY KEY(translation_id, asset_version) );