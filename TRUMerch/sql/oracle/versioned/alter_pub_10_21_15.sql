drop table tru_sku_root_parent_cats;
drop table tru_prd_root_parent_cat;
drop table tru_clf_rltd_media;
drop table tru_parnt_clfs;
drop table tru_child_clfs;
drop table tru_classification;

CREATE TABLE tru_classification (
	category_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	user_type_id 		varchar2(254)	NULL,
	display_status 		varchar2(6)	NULL,
	display_order 		INTEGER	NULL,
	collection_image 	varchar2(255)	NULL,
	linked_node 		varchar2(40)	NULL,
	have_value 		varchar2(20)	NULL,
	suggested_quantity 	varchar2(40)	NULL,
	is_deleted 		number(1)	NULL,
	CHECK (is_deleted IN (0, 1)),
	PRIMARY KEY(category_id, asset_version)
);


CREATE TABLE tru_clf_rltd_media (
	category_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	classification_media_key varchar2(254)	NOT NULL,
	related_media_id 	varchar2(40)	NULL,
	PRIMARY KEY(category_id, asset_version, classification_media_key)
);

alter table tru_sku add display_order INTEGER	NULL;
alter table tru_sku	add un_cartable 	number(1)	NULL;
alter table tru_sku	add is_deleted 		number(1)	NULL;

drop table tru_clf_cros_refernces;
CREATE TABLE tru_clf_cros_refernces (
                category_id                        varchar2(254)    NOT NULL,
                asset_version                    INTEGER              NOT NULL,
                sequence_num                                INTEGER              NOT NULL,
                cross_reference               varchar2(254)    NULL,
                PRIMARY KEY(category_id, asset_version, sequence_num)
);

drop table tru_product;

CREATE TABLE tru_product (
	product_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	channel_availability 	INTEGER	NULL,
	special_assembly_instructions varchar2(254)	NULL,
	rus_item_number 	number(19, 0)	NULL,
	gender 			INTEGER	NULL,
	cost 			varchar2(254)	NULL,
	vendors_product_demo_url varchar2(254)	NULL,
	EWASTE_SURCHARGE_STATE 	varchar2(254)	NULL,
	EWASTE_SURCHARGE_SKU 	varchar2(254)	NULL,
	BATTERY_REQUIRED 	varchar2(254)	NULL,
	BATTERY_INCLUDED 	varchar2(254)	NULL,
	item_in_store_pick_up 	varchar2(254)	NULL,
	ship_from_store_eligible varchar2(254)	NULL,
	tax_code 		varchar2(254)	NULL,
	tax_product_value_code 	varchar2(254)	NULL,
	manufacturer_style_number varchar2(254)	NULL,
	non_merchandise_flag 	varchar2(254)	NULL,
	countrye_eligibility 	varchar2(254)	NULL,
	super_display_flag 	varchar2(254)	NULL,
	registry_classification_id varchar2(254)	NULL,
	vendor_classification_id varchar2(254)	NULL,
	product_htgit_msg 	INTEGER	NULL,
	size_chart_name 	varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version)
);

drop table tru_sku_parent_clfs;

CREATE TABLE tru_sku_parent_clfs (
	sku_id 			varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, asset_version, sequence_num)
);


drop table tru_prd_parent_clfs;

CREATE TABLE tru_prd_parent_clfs (
	product_id 		varchar2(254)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(254)	NULL,
	PRIMARY KEY(product_id, asset_version, sequence_num)
);


