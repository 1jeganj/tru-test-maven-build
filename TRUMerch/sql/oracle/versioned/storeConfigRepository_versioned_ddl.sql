CREATE TABLE TRU_BIN_RANGE (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0)	,
	checkin_date timestamp (6), 
	id 			varchar2(254)	NOT NULL,
	LOWER_RANGE 		number(7)	NULL,
	UPPER_RANGE 		number(7)	NULL,
	CREDITCARD_TYPE 	INTEGER	NULL,
	constraint TRU_BIN_RANGE_p PRIMARY KEY(id,asset_version) 
	);

commit;
---START: Added table for  storing profanity words (to be filtered out from gift message)
--Added on 16th Dec 2015
CREATE TABLE TRU_PROFANITY_WORDS (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0)	,
	checkin_date timestamp (6), 
	id 			varchar2(254)	NOT NULL,
	WORD        VARCHAR2(254) NULL,
	constraint TRU_PROFANITY_WORDS_p PRIMARY KEY(id,asset_version) 
	);

commit;
---END: Added table for  storing profanity words (to be filtered out from gift message)