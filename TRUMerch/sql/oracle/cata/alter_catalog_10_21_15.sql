drop table tru_sku_root_parent_cats;
drop table tru_prd_root_parent_cat;
drop table tru_clf_rltd_media;
drop table tru_parnt_clfs;
drop table tru_child_clfs;
drop table tru_product;

CREATE TABLE tru_product (
	product_id 		varchar2(40)	NOT NULL REFERENCES dcs_product(product_id),
	channel_availability 	INTEGER	NULL,
	special_assembly_instructions varchar2(255)	NULL,
	rus_item_number 	number(20)	NULL,
	gender 			varchar2(10)	NULL,
	cost 			varchar2(20)	NULL,
	vendors_product_demo_url varchar2(255)	NULL,
	EWASTE_SURCHARGE_STATE 	varchar2(5)	NULL,
	EWASTE_SURCHARGE_SKU 	varchar2(10)	NULL,
	BATTERY_REQUIRED 	varchar2(1)	NULL,
	BATTERY_INCLUDED 	varchar2(1)	NULL,
	item_in_store_pick_up 	varchar2(3)	NULL,
	ship_from_store_eligible varchar2(30)	NULL,
	tax_code 		varchar2(364)	NULL,
	tax_product_value_code 	varchar2(3)	NULL,
	manufacturer_style_number varchar2(20)	NULL,
	non_merchandise_flag 	varchar2(1)	NULL,
	countrye_eligibility 	varchar2(3)	NULL,
	super_display_flag 	varchar2(1)	NULL,
	registry_classification_id varchar2(40)	NULL REFERENCES dcs_category(category_id),
	vendor_classification_id varchar2(40)	NULL,
	product_htgit_msg 	INTEGER	NULL,
	size_chart_name varchar2(100)	NULL,
	PRIMARY KEY(product_id)
);

drop table tru_sku_parent_clfs;

CREATE TABLE tru_sku_parent_clfs (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL REFERENCES dcs_category(category_id),
	PRIMARY KEY(sku_id, sequence_num)
);

drop table tru_prd_parent_clfs;

CREATE TABLE tru_prd_parent_clfs (
	product_id 		varchar2(40)	NOT NULL REFERENCES dcs_product(product_id),
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL REFERENCES dcs_category(category_id),
	PRIMARY KEY(product_id, sequence_num)
);
drop table tru_clf_cros_refernces;
CREATE TABLE tru_clf_cros_refernces (
	category_id 		varchar2(40)	NOT NULL REFERENCES dcs_category(category_id),
	sequence_num 		INTEGER	NOT NULL,
	cross_reference 	varchar2(40)	NULL REFERENCES dcs_category(category_id),
	PRIMARY KEY(category_id, sequence_num)
);

drop table tru_classification;

CREATE TABLE tru_classification (
	category_id 		varchar2(40)	NOT NULL REFERENCES dcs_category(category_id),
	user_type_id 		varchar2(254)	NULL,
	display_status 		varchar2(6)	NULL,
	display_order 		INTEGER	NULL,
	collection_image 	varchar2(255)	NULL,
	linked_node 		varchar2(40)	NULL,
	have_value 		varchar2(20)	NULL,
	suggested_quantity 	varchar2(40)	NULL,
	is_deleted 		number(1)	NULL,
	CHECK (is_deleted IN (0, 1)),
	PRIMARY KEY(category_id)
);
CREATE INDEX tru_clf_category_idx ON tru_classification(category_id);

CREATE TABLE tru_clf_rltd_media (
	category_id 		varchar2(40)	NOT NULL REFERENCES dcs_category(category_id),
    classification_media_key         varchar2(254)    NOT NULL,
    related_media_id            varchar2(40)    NULL REFERENCES wcm_media_content(id),
	PRIMARY KEY(category_id, classification_media_key)
);

CREATE INDEX tru_clf_rltd_media_category_idx ON tru_clf_rltd_media(category_id, classification_media_key);

alter table tru_sku add display_order 		INTEGER	NULL;
alter table tru_sku	add un_cartable 		number(1)	NULL;
alter table tru_sku	add is_deleted 		number(1)	NULL;


