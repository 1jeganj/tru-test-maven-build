alter table tru_catalog modify root_nav_cat varchar2(40);
alter table tru_classification add is_compareProduct number(1);

alter table tru_classification modify 
	(parent_category 	varchar2(40),
	root_parent_category 	varchar2(40));

alter table tru_product modify registry_classification_id varchar2(40);

alter table tru_prd_root_parent_cat modify root_parent_category varchar2(40);

alter table tru_prd_parent_clfs modify parent_classification varchar2(40);

alter table tru_sku_strl_extra modify sku_id varchar2(40);

alter table tru_crib_sku_map modify id varchar2(40);

alter table tru_sku modify original_parent_product varchar2(40);

ALTER TABLE tru_product MODIFY rus_item_number Number(20);

ALTER TABLE tru_sku_alt_image MODIFY alternate_image varchar2(254);

alter table tru_sku drop column swatch_image_count;
alter table tru_sku drop column primary_image_count;
alter table tru_sku drop column secondary_image_count;
alter table tru_sku drop column alternate_image_count;
alter table tru_sku drop column cross_sells_batteries;
alter table tru_sku drop column uid_battery_cross_sell;
alter table tru_sku drop column battery_type;
alter table tru_sku drop column battery_quantity;

alter table tru_sku add swatch_image varchar2(254) NULL;
alter table tru_sku add	primary_image varchar2(254)	NULL;
alter table tru_sku add	secondary_image varchar2(254) NULL;
alter table tru_sku add long_desc clob null;
alter table tru_sku add	interest varchar2(254) NULL;

create table tru_sku_alt_image (
	sku_id 			varchar2(40)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	alternate_image 	varchar2(254)	null,
	primary key(sku_id, sequence_num)
);


alter table tru_sku_parent_clfs modify parent_classification varchar2(40);

alter table tru_sku_root_parent_cats modify root_parent_category varchar2(40);

alter table tru_sku_lower48_map modify id varchar2(40);
alter table tru_classification modify suggested_quantity varchar2(40);
alter table tru_parnt_clfs modify parent_classification varchar2(40);

alter table tru_child_clfs modify classification_id varchar2(40);

alter table tru_child_clfs modify child_classification varchar2(40);

create table tru_clf_rltd_media (
	classification_id 	varchar2(40)	not null references tru_classification(classification_id),
	sequence_num 		integer	not null,
	related_media_id 	varchar2(40)	null references wcm_media_content(id),
	primary key(classification_id, sequence_num)
);

create table tru_cross_sells_batteries (
	cross_sells_batteries_id varchar2(40)	not null,
	cross_sells_batteries 	varchar2(254)	null,
	uid_battery_cross_sell 	varchar2(254)	null,
	primary key(cross_sells_batteries_id)
);

create table tru_battery (
	battery_id 		varchar2(40)	not null,
	battery_type 		varchar2(254)	null,
	battery_quantity 	integer	null,
	primary key(battery_id)
);

create table tru_sku_crosssell_battery (
	sku_id 			varchar2(254)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	crosssell_battery 	varchar2(40)	null references tru_cross_sells_batteries(cross_sells_batteries_id),
	primary key(sku_id, sequence_num)
);

create table tru_sku_battery (
	sku_id 			varchar2(40)	not null references dcs_sku(sku_id),
	sequence_num 		integer	not null,
	battery 		varchar2(40)	null references tru_battery(battery_id),
	primary key(sku_id, sequence_num)
);

drop table tru_content_item cascade constraints;

drop table tru_banner_content_item cascade constraints;

drop table tru_html_content_item cascade constraints;

alter table tru_how_to_get_it_msg modify msg_id varchar2(40);

alter table tru_product add why_we_love_it CLOB NULL;

ALTER TABLE tru_site_configuration ADD newProduct_Threshold varchar2(254) NULL;