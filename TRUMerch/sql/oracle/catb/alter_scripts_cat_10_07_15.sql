alter table tru_classification add is_deleted varchar2(1) NULL;
alter table tru_product add size_chart_name varchar2(100) NULL;	
alter table tru_sku add review_count_decimal number(28, 10)	NULL;
alter table tru_sku add sales_volume number(20)	NULL;

drop table tru_prd_site_eligibility;