create or replace PACKAGE TRU_PROFILE_MIG_PKG
/*
***************************************************************TRU_PROFILE_MIG_PKG**********************************************************************************************************
**    i.  Purpose
**
**            This package use to migrate data from XML file to all profile related target table
**
**
**    ii. Procedures

***         i.PROFILE_MIG_STAGE_MAIN
            -------------------------
               This procedure insert the XML file in temp table(TRU_TEMP_PROFILE_XML) and call PROFILE_XML_STAGE_LOAD procedure to load profiles records into staging tables.

			   IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))


***         ii.    PROFILE_XML_STAGE_LOAD
			-----------------------------
                This procedure parse the XML file and load data into staging tables
				   TRU_TEMP_PROFILE_USERINFO  	: table holds user info details
				   TRU_TEMP_PROFILE_ADDRESS		: table holds user address  details
				   TRU_TEMP_PROFILE_CREDITCARD	: table holds user credit cards  details

				IN parameter --    P_FILE_NAME (it holds XML file name,  it  picks the profile from the XML file and load staging table)


***         iii.PROFILE_MIG_TARGT
			-----------------------
				This procedure migrate the profiles data from stagin tables into ATG target tables
				List of ATG target table is going to migrate

					DPS_USER
					TRU_DPSX_USER
					DPS_USER_ADDRESS
					DPS_CONTACT_INFO
					TRU_DPSX_CONTACT_INFO
					DPS_OTHER_ADDR
					DPS_CREDIT_CARD
					TRU_DPSX_CREDIT_CARD
					DPS_USR_CREDITCARD

				IN parameter --    P_LAOD_TYPE(value should be 'initial' or 'delta' to decide the profile migration type)


***         iV.PROFILE_MIG_TARGT_BATCH
			-----------------------
				This procedure migrate the profiles data from staging tables into ATG target tables by batch wise commit. ( commit interval is 10000 records)

				IN parameter --    P_LAOD_TYPE,P_COMMIT_INTRVL(P_LAOD_TYPE value should be 'initial' or 'delta' to decide the profile migration type and P_COMMIT_INTRVL value should be greater than 10000)


***         v. PROFILE_STAGE_STATE_LOAD
			-----------------------------
                This procedure resd the external file and load into TRU_TEMP_PROFILE_STATE  table

				IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))

***         vi. PROFILE_STAGE_COUNTRY_LOAD
			-----------------------------
                This procedure resd the external file and load into TRU_TEMP_PROFILE_COUNTRY  table

				IN parameter --    P_DIR_NAME,P_FILE_NAME and P_TRUNCATE (It holds XML file directory name , it's file name  and truncate flag(y or n))


**   iii. Functions

            i.XML_FILEEXISTS
            ----------------------
			IN parameter --    P_DIRNAME,P_FILENAME (It holds XML file directory name and it's file name respectively)
            This function use to identify the XML file exists or not in the directory.


**    iv. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          19/Jan/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/


AS
	G_START                            number;
    G_END                              NUMBER;
	G_BFILE  BFILE;
	G_PASWD VARCHAR2(255) := '748c97e6fad37513800acf4b81e2ba8af4249e7e';
	G_SALT_PASWD VARCHAR2(255) := 'ed3fd1cd845d2aba2dcbfaffeeafdc6458030a69';
	G_FILE_NAME  VARCHAR2(255);
	G_FILE_SIZE NUMBER(15,2);
	G_MIG_LOAD_ID  NUMBER;

	FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,
		P_FILENAME IN VARCHAR2
	) RETURN VARCHAR2;
	PROCEDURE PROFILE_MIG_STAGE_MAIN(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
    PROCEDURE PROFILE_XML_STAGE_LOAD(P_FILE_NAME VARCHAR2);
    PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  varchar2,P_ENABLE varchar2);
	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  varchar2,P_COMMIT_INTRVL PLS_INTEGER);
	PROCEDURE PROFILE_STAGE_STATE_LOAD(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
	PROCEDURE PROFILE_STAGE_COUNTRY_LOAD(P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2);
	FUNCTION  GET_MAPPED_STATE_CODE(P_STATE_CODE IN VARCHAR2) RETURN VARCHAR2;
	FUNCTION  GET_MAPPED_COUNTRY_CODE(P_COUNTRY_CODE IN VARCHAR2) RETURN VARCHAR2;
end TRU_PROFILE_MIG_PKG;