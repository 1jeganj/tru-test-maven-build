DROP TABLE radial_user;

CREATE TABLE radial_user (
	id 			varchar2(254)	NOT NULL REFERENCES dps_user(id),
	radial_user 		number(1)	NULL,
	radial_password 	varchar2(254)	NULL,
	CHECK (radial_user IN (0, 1)),
	PRIMARY KEY(id)
);
COMMIT;

