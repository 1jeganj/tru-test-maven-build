CREATE TABLE TRU_COHERENCE_CONFIG (
       ID                   VARCHAR2(254) NOT NULL,
       COHERENCE_OVERRIDE                 NUMBER(1) NULL,
       PRIMARY KEY(ID)
);
commit;