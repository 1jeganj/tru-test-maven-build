CREATE TABLE TRU_COHERENCE_CONFIG (
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0)	,
	checkin_date timestamp (6), 
	ID	VARCHAR2(254) NOT NULL,
    COHERENCE_OVERRIDE	NUMBER(1) NULL,
	PRIMARY KEY(ID)
	);

commit;