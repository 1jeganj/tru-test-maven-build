CREATE TABLE tru_profilesync_error_msg (
	id 			varchar2(40)	NOT NULL,
	profile_id 		varchar2(40)	NULL,
	last_name 		varchar2(254)	NULL,
	first_name 		varchar2(254)	NULL,
	email 			varchar2(254)	NULL,
	reward_number 		varchar2(40)	NULL,
	registration_date 	DATE	NULL,
	lastProfile_update 	DATE	NULL,
	password 		varchar2(254)	NULL,
	profile_sync_error_message varchar2(2024)	NULL,
	file_name 		varchar2(254)	NULL,
	created_timestamp 	DATE	NULL,
	PRIMARY KEY(id)
);