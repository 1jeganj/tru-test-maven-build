CREATE TABLE tru_usr_promostat (
	status_id 		varchar2(254)	NOT NULL REFERENCES dcs_usr_promostat(status_id),
	applied_code 		number(1)	NULL,
	applied_code_value 	varchar2(254)	NULL,
	CHECK (applied_code IN (0, 1)),
	PRIMARY KEY(status_id)
);