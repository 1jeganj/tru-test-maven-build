-- Start  -  Added for Payment Retry Count- 14th Nov 2016

ALTER TABLE TRU_SITE_CONFIGURATION ADD PAYMENT_RETRY_COUNT INT DEFAULT(5);

-- End  -  Added for Payment Retry Count- 14th Nov 2016
-- Start - Added for Enable Inventory - 14th Nov 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_INVENTORY NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable Inventory - 14th Nov 2015 