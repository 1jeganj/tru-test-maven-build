#!/bin/bash
#======================================================================================================================
#
# FILE: EndecaInstall_Universal.sh
#
# USAGE: EndecaInstall_Universal.sh
#
# DESCRIPTION: Configures and installs the primary Endeca Software applications for Guided Search 6,
#              Endeca Commerce 3.x and Endeca Commerce 11.x:
#              MDEX
#              Platform Services
#              Workbench/ToolsAndFrameworks
#              CAS
#
# OPTIONS:
# REQUIREMENTS: This script will install Guided Search 6x, Endeca Commerce 3.x and Endeca Commerce 11.x
# BUGS:
# NOTES:
# AUTHOR: Ewan Innes, ewan.innes@oracle.com
# COMPANY: Oracle Corporation
# VERSION: 1.1
# CREATED: 09/06/2013
# REVISION: 6
#
# UPDATES: 2/13/2014 - Added support for Oracle Commerce 11 install.
#
#======================================================================================================================

echo "Setting Variables"
echo

#----------------------------------------------------------------------------------------------------------------------
# Are we installing Oracle Endeca Guided Search (6.x) or Oracle Endeca Commerce 3.x?
# Guided Search (6.x) = 6
# Endeca Commerce 3.x = 3.1.1 or 3.1.2
# Endeca Commerce 11  = 11
#----------------------------------------------------------------------------------------------------------------------
ENDECA_VERSION=11.2

#----------------------------------------------------------------------------------------------------------------------
# Set the location and names of the install files. The INSTALLER_LOCATION var should be the location where your source
# files were downloaded to as this is built to assume the INSTALLER_LOCATION is not on the same machine.
#----------------------------------------------------------------------------------------------------------------------

INSTALL_FILES=/data/endeca/installs11.2
BIN_DIR=/data/endeca/bin

if [ "$ENDECA_VERSION" == "3.1.1" ]; then
    #Set the product versions being installed
    MDEX_VERSION=6.4.0
    PLATFORM_VERSION=6.1.3
    TOOLS_VERSION=3.1.1
    CAS_VERSION=3.1.1

    #Set the Installer Location
    INSTALLER_LOCATION=/data/app/endeca/software/Linux_Software/Oracle_Endeca_Commerce_3.1.1

    #Set the installer file paths
    MDEX_PATH=$INSTALLER_LOCATION/MDEX_Engine_6.4.0/
    PLATFORM_PATH=$INSTALLER_LOCATION/Platform_Services_6.1.3/
    TOOLS_PATH=$INSTALLER_LOCATION/ToolsFrameworks_XMGR_3.1.1/
    CAS_PATH=$INSTALLER_LOCATION/CAS_3.1.1/

    #Set the installer file names
    MDEX_FILE=mdex_6.4.0.692722_x86_64pc-linux.sh
    PLATFORM_FILE=platformservices_613654721_x86_64pc-linux.sh
    TOOLS_FILE=xmgr_3.1.1.6904_linux.zip
    CAS_FILE=cas-3.1.1-x86_64pc-linux.sh

elif [ "$ENDECA_VERSION" == "3.1.2" ]; then
    #Set the product versions being installed
    MDEX_VERSION=6.4.1
    PLATFORM_VERSION=6.1.3
    TOOLS_VERSION=3.1.2
    CAS_VERSION=3.1.2

    #Set the Installer Location
    INSTALLER_LOCATION=/data/app/endeca/software/Linux_Software/Oracle_Endeca_Commerce_3.1.2

    #Set the installer file paths
    MDEX_PATH=$INSTALLER_LOCATION/V37714-01_MDEX_Engine_6.4.1/V37714-01/
    PLATFORM_PATH=$INSTALLER_LOCATION/V33316-01_Platform_Services_6.1.3/V33316-01/
    TOOLS_PATH=$INSTALLER_LOCATION/V37716-01_Tools_and_Frameworks_with_Experience_Manager_3.1.2/
    CAS_PATH=$INSTALLER_LOCATION/V37711-01_Content_Acquisition_System_3.1.2/V37711-01/

    #Set the installer file names
    MDEX_FILE=mdex_6.4.1.715066_x86_64pc-linux.sh
    PLATFORM_FILE=platformservices_613654721_x86_64pc-linux.sh
    TOOLS_FILE=V37716-01.zip
    CAS_FILE=cas-3.1.2-x86_64pc-linux.sh

elif [ "$ENDECA_VERSION" == "6" ]; then
    #Set the product versions being installed
    MDEX_VERSION=6.2.2
    PLATFORM_VERSION=6.1.3
    TOOLS_VERSION=2.1.2
    CAS_VERSION=3.0.2

    #Set the Installer Location
    INSTALLER_LOCATION=/data/app/endeca/software/Linux_Software/Oracle_Endeca_Guided_Search_6.x

    #Set the installer file paths
    MDEX_PATH=$INSTALLER_LOCATION/V31176-01_MDEX_Engine_6.2.2/V31176-01/
    PLATFORM_PATH=$INSTALLER_LOCATION/V33316-01_Platform_Services_6.1.3/V33316-01/
    TOOLS_PATH=$INSTALLER_LOCATION/V31175-01_Experience_Manager_2.1.2/V31175-01/
    CAS_PATH=$INSTALLER_LOCATION/V31482-01_Content_Acquisition_System_3.0.2/V31482-01/

    #Set the installer file names
    MDEX_FILE=mdex_6.2.2.621294_x86_64pc-linux.sh
    PLATFORM_FILE=platformservices_613654721_x86_64pc-linux.sh
    TOOLS_FILE=xmgrworkbench_x86_64pc-linux_2.1.2.sh
    CAS_FILE=cas-3.0.2-x86_64pc-linux.sh

elif [ "$ENDECA_VERSION" == "11.2" ]; then
    #Set the product versions being installed
    MDEX_VERSION=6.5.2
    PLATFORM_VERSION=11.2.0
    TOOLS_VERSION=11.2.0
    CAS_VERSION=11.2.0

    #Set the Installer Location
    INSTALLER_LOCATION=/data/endeca/installs11.2

    #Set the installer file paths
    MDEX_PATH=$INSTALLER_LOCATION
    PLATFORM_PATH=$INSTALLER_LOCATION
    TOOLS_PATH=$INSTALLER_LOCATION
    CAS_PATH=$INSTALLER_LOCATION

    #Set the installer file names
    MDEX_FILE=OCmdex6.5.2-Linux64_962107.bin
    PLATFORM_FILE=OCplatformservices11.2.0-Linux64.bin
    TOOLS_FILE=V78229-01.zip
    CAS_FILE=OCcas11.2.0-Linux64.bin
fi

#----------------------------------------------------------------------------------------------------------------------
# Set the base location where Endeca will be installed.
#----------------------------------------------------------------------------------------------------------------------
INSTALL_ROOT=/data/endeca

#----------------------------------------------------------------------------------------------------------------------
# Set the Endeca install root location
#----------------------------------------------------------------------------------------------------------------------
ENDECADIR_ROOT=/data/endeca

#----------------------------------------------------------------------------------------------------------------------
# Check if the various install directories exist, if not create them. Remove the Tools directory if it exists
#----------------------------------------------------------------------------------------------------------------------

if [ ! -d "$ENDECADIR_ROOT" ]; then
  echo "Creating Endeca directory"
  echo
  mkdir $ENDECADIR_ROOT
  chown -R oracle:oracle $ENDECADIR_ROOT
elif [ ! -d "$INSTALL_FILES" ]; then
  echo "Creating Installs directory"
  echo
  mkdir $INSTALL_FILES
elif [ ! -d "$BIN_DIR" ]; then
#  echo "Creating bin directory"
  echo
#  mkdir $BIN_DIR
elif [ -d "$INSTALL_FILES/Tools" ]; then
  echo "Removing Tools temp directory"
  echo
  rm -rf $INSTALL_FILES/Tools
fi

#----------------------------------------------------------------------------------------------------------------------
#Set the root locations for the various applications
#----------------------------------------------------------------------------------------------------------------------

#MDEX
MDEX_INSTALL_ROOT=$ENDECADIR_ROOT/MDEX/$MDEX_VERSION

#Platform Services
PLATFORM_INI_ROOT=$ENDECADIR_ROOT/PlatformServices/workspace/setup/
PLATFORM_STARTUP_ROOT=$ENDECADIR_ROOT/PlatformServices/$PLATFORM_VERSION/tools/server/bin
PLATFORM_LOG_FILE=$PLATFORM_STARTUP_ROOT/../../../../workspace/logs/catalina.out


#Workbench/Tools and Frameworks

if [ "$ENDECA_VERSION" == "3.1.1" ] || [ "$ENDECA_VERSION" == "3.1.2" ]; then
    ENDECA_TOOLS_ROOT=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION
    ENDECA_TOOLS_CONF=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION/server/workspace/
    ENDECA_TOOLS_STARTUP_ROOT=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION/server/bin/
elif [ "$ENDECA_VERSION" == "6" ]; then
    ENDECA_TOOLS_INI_ROOT=$ENDECADIR_ROOT/Workbench/workspace/setup/
    ENDECA_TOOLS_ROOT=$ENDECADIR_ROOT/Workbench/$TOOLS_VERSION
    ENDECA_TOOLS_CONF=$ENDECADIR_ROOT/Workbench/workspace/
    ENDECA_TOOLS_STARTUP_ROOT=$ENDECADIR_ROOT/Workbench/$TOOLS_VERSION/server/bin/
elif [ "$ENDECA_VERSION" == "11.2" ]; then
    ORACLE_HOME_NAME=ToolsAndFrameworks
    ORACLE_HOME_LOCATION=$ENDECADIR_ROOT/ToolsAndFrameworks/
    ADMIN_USER_PASSWORD=admin
    TOOLS_INSTALL_FILE=$INSTALL_FILES/Tools/cd/Disk1/install/silent_install.sh
    TOOLS_RESPONSE_FILE=$INSTALL_FILES/tools_answers.rsp
    ENDECA_TOOLS_ROOT=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION
    ENDECA_TOOLS_CONF=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION/server/workspace/
    ENDECA_TOOLS_STARTUP_ROOT=$ENDECADIR_ROOT/ToolsAndFrameworks/$TOOLS_VERSION/server/bin/
fi

TOOLS_LOG_FILE=$ENDECA_TOOLS_ROOT/server/workspace/logs/catalina.out

#CAS
CAS_STARTUP_ROOT=$ENDECADIR_ROOT/CAS/$CAS_VERSION/bin
CAS_LOG_FILE=$ENDECADIR_ROOT/CAS/workspace/logs/cas-service.log

echo "Creating Answer Files"
echo

#Create the Answer file for Platform Services
#----------------------------------------------------------------------------------------------------------------------
echo 8888 > $INSTALL_FILES/platform_answers.txt
echo 8090 >> $INSTALL_FILES/platform_answers.txt
echo 8088 >> $INSTALL_FILES/platform_answers.txt
echo "Y" >> $INSTALL_FILES/platform_answers.txt
echo "$MDEX_INSTALL_ROOT" >> $INSTALL_FILES/platform_answers.txt
echo "Y" >> $INSTALL_FILES/platform_answers.txt

#----------------------------------------------------------------------------------------------------------------------
#Create the Answer file for CAS
#----------------------------------------------------------------------------------------------------------------------
echo 8500 > $INSTALL_FILES/cas_answers.txt
echo 8506 >> $INSTALL_FILES/cas_answers.txt
echo "localhost" >> $INSTALL_FILES/cas_answers.txt

#----------------------------------------------------------------------------------------------------------------------
#Create the Workbench Answer file if this is a 6.x install
#----------------------------------------------------------------------------------------------------------------------
if [ "$ENDECA_VERSION" == "6" ]; then
    echo "localhost" > $INSTALL_FILES/tools_answers.txt
    echo 8006 >> $INSTALL_FILES/tools_answers.txt
    echo 8084 >> $INSTALL_FILES/tools_answers.txt
    echo "localhost" >> $INSTALL_FILES/tools_answers.txt
    echo 8888 >> $INSTALL_FILES/tools_answers.txt
fi

#----------------------------------------------------------------------------------------------------------------------
#Create the Tools Answer file if this is a 11.x install
#----------------------------------------------------------------------------------------------------------------------
if [ "$ENDECA_VERSION" == "11.2" ]; then

    echo "USER_INSTALL_DIR=/data" > $INSTALL_FILES/mdex_response.properties
    echo "-fileOverwrite_/data/endeca/MDEX/6.5.2/Uninstall/Uninstall_Oracle Commerce MDEX Engine 6.5.2 x64 Edition.lax=Yes">>  $INSTALL_FILES/mdex_response.properties

    echo "USER_INSTALL_DIR=/data" > $INSTALL_FILES/platform_response.properties
    echo "CHOSEN_FEATURE_LIST=PS,ACS,ACA" >> $INSTALL_FILES/platform_response.properties
    echo "CHOSEN_INSTALL_FEATURE_LIST=PS,ACS,ACA" >> $INSTALL_FILES/platform_response.properties
    echo "CHOSEN_INSTALL_SET=Typical" >> $INSTALL_FILES/platform_response.properties
    echo "ETOOLS_HTTP_PORT=8888" >> $INSTALL_FILES/platform_response.properties
    echo "ETOOLS_SERVER_PORT=8090" >> $INSTALL_FILES/platform_response.properties
    echo "EAC_MDEX_ROOT=/data/endeca/MDEX/6.5.2" >> $INSTALL_FILES/platform_response.properties
    echo "-fileOverwrite_/data/endeca/PlatformServices/11.2.0/Uninstall/Uninstall_Oracle Commerce Guided Search Platform Services 11.2.0.lax=Yes" >> $INSTALL_FILES/platform_response.properties

    echo "RESPONSEFILE_VERSION=2.2.1.0.0" > $INSTALL_FILES/tools_answers.rsp
    echo "UNIX_GROUP_NAME=\"Oracle\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "FROM_LOCATION=\"$INSTALL_FILES/Tools/cd/Disk1/stage/products.xml\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "FROM_LOCATION_CD_LABEL=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "# ORACLE_HOME=\"\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "ORACLE_BASE=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "# ORACLE_HOME_NAME=\"\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_WELCOME_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_CUSTOM_TREE_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_COMPONENT_LOCATIONS_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_SUMMARY_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_INSTALL_PROGRESS_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_REQUIRED_CONFIG_TOOL_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_CONFIG_TOOL_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_RELEASE_NOTES=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_ROOTSH_CONFIRMATION=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_END_SESSION_PAGE=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_EXIT_CONFIRMATION=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "NEXT_SESSION=false" >> $INSTALL_FILES/tools_answers.rsp
    echo "NEXT_SESSION_ON_FAIL=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "NEXT_SESSION_RESPONSE=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "DEINSTALL_LIST={\"oracle.endeca.commerce\",\"11.2.0.0.0\"}" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_DEINSTALL_CONFIRMATION=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_DEINSTALL_PROGRESS=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "CLUSTER_NODES={}" >> $INSTALL_FILES/tools_answers.rsp
    echo "ACCEPT_LICENSE_AGREEMENT=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "METALINK_LOCATION=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "METALINK_USERNAME=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "METALINK_PASSWORD=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "PROXY_HOST=\"\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "PROXY_PORT=\"\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "PROXY_REALM=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "PROXY_USER=\"\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "PROXY_PWD=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "DONT_PROXY_FOR=<Value Unspecified>" >> $INSTALL_FILES/tools_answers.rsp
    echo "TOPLEVEL_COMPONENT={\"oracle.endeca.commerce\",\"11.2.0.0.0\"}" >> $INSTALL_FILES/tools_answers.rsp
    echo "SHOW_SPLASH_SCREEN=true" >> $INSTALL_FILES/tools_answers.rsp
    echo "SELECTED_LANGUAGES={\"en\"}" >> $INSTALL_FILES/tools_answers.rsp
    echo "COMPONENT_LANGUAGES={\"en\"}" >> $INSTALL_FILES/tools_answers.rsp
    echo "INSTALL_TYPE=\"Complete Installation\"" >> $INSTALL_FILES/tools_answers.rsp
    echo "# PASSWORD_VALIDATION=\"\"" >> $INSTALL_FILES/tools_answers.rsp
fi

echo "Copying Files...."
echo

#----------------------------------------------------------------------------------------------------------------------
#Copy the install files from the source location to the installer location if needed
#----------------------------------------------------------------------------------------------------------------------
if [ ! -e "$INSTALL_FILES/$MDEX_FILE" ]; then
    echo "Copying MDEX File...."
    install -m 755 $MDEX_PATH/$MDEX_FILE $INSTALL_FILES/$MDEX_FILE
    echo "Copying MDEX File Complete"
fi


if [ ! -e "$INSTALL_FILES/$PLATFORM_FILE" ]; then
    echo "Copying Platform File...."
    install -m 755 $PLATFORM_PATH/$PLATFORM_FILE $INSTALL_FILES/$PLATFORM_FILE
    echo "Copying Platform File Complete"
fi

if [ ! -e "$INSTALL_FILES/$TOOLS_FILE" ]; then
    echo "Copying Tools File...."
    install -m 755 $TOOLS_PATH/$TOOLS_FILE $INSTALL_FILES/$TOOLS_FILE
    echo "Copying Tools File Complete"
fi

if [ ! -e "$INSTALL_FILES/$CAS_FILE" ]; then
    echo "Copying CAS File...."
    install -m 755 $CAS_PATH/$CAS_FILE $INSTALL_FILES/$CAS_FILE
    echo "Copying CAS File Complete"
fi


#----------------------------------------------------------------------------------------------------------------------
#Copy the endeca-control script from the source location to the endeca/bin location.
#If not being used, this line can be commented out.
#----------------------------------------------------------------------------------------------------------------------
#install -m 755 $INSTALLER_LOCATION/../bin/endeca-control $ENDECADIR_ROOT/bin/endeca-control

#----------------------------------------------------------------------------------------------------------------------
#Install MDEX
#----------------------------------------------------------------------------------------------------------------------
echo "Installing MDEX"
echo "$INSTALL_FILES/$MDEX_FILE -i silent -f mdex_response.properties"
$INSTALL_FILES/$MDEX_FILE -i silent -f mdex_response.properties
echo "Done installing MDEX"

#uncoment to install!
#source /data/endeca/endeca/MDEX/6.5.2/mdex_setup_csh.ini
#source $MDEX_INSTALL_ROOT/mdex_setup_csh.ini


#----------------------------------------------------------------------------------------------------------------------
#Pause 2 seconds to ensure our variables are set
#----------------------------------------------------------------------------------------------------------------------
sleep 2

#----------------------------------------------------------------------------------------------------------------------
#Install Platform Services
#----------------------------------------------------------------------------------------------------------------------
# uncomment to install
echo "Installing Platform "
echo "Running: $INSTALL_FILES/$PLATFORM_FILE -i silent -f platform_response.properties"
$INSTALL_FILES/$PLATFORM_FILE -i silent -f platform_response.properties
echo "Done installing platform"

#----------------------------------------------------------------------------------------------------------------------
#Pause 2 seconds to ensure our Platform Services install script has finished
#----------------------------------------------------------------------------------------------------------------------
sleep 2

#----------------------------------------------------------------------------------------------------------------------
#Set the Platform Services environment variables
#----------------------------------------------------------------------------------------------------------------------
#source $PLATFORM_INI_ROOT/installer_sh.ini

#----------------------------------------------------------------------------------------------------------------------
#Start Platform Services
#----------------------------------------------------------------------------------------------------------------------
# clear old log file
#echo "" > $PLATFORM_LOG_FILE

#echo "Starting Platform"
#echo

#nohup $PLATFORM_STARTUP_ROOT/startup.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
#Loop while for as long as it takes to start up
#----------------------------------------------------------------------------------------------------------------------
#STARTED=`grep -o 'INFO: Server startup in' $PLATFORM_LOG_FILE`
#while [ "$STARTED" = "" ]; do
#    echo "..."
#    sleep 5
#    STARTED=`grep -o 'INFO: Server startup in' $PLATFORM_LOG_FILE`
#done

#----------------------------------------------------------------------------------------------------------------------
#Install Tools and Frameworks
#----------------------------------------------------------------------------------------------------------------------
if [ "$ENDECA_VERSION" == "3.1.1" ] || [ "$ENDECA_VERSION" == "3.1.2" ]; then
    unzip $INSTALL_FILES/$TOOLS_FILE -d $ENDECADIR_ROOT
elif [ "$ENDECA_VERSION" == "6" ]; then
    $INSTALL_FILES/$TOOLS_FILE --silent --target $INSTALL_ROOT < $INSTALL_FILES/tools_answers.txt
    source $ENDECA_TOOLS_INI_ROOT/installer_sh.ini
elif [ "$ENDECA_VERSION" == "11.2" ]; then
    echo "unzip:  $INSTALL_FILES/$TOOLS_FILE -d $INSTALL_FILES/Tools"
	    unzip $INSTALL_FILES/$TOOLS_FILE -d $INSTALL_FILES/Tools

	echo "Installing ToolsAndFramework"
	 echo "Running $INSTALL_FILES/Tools/silent_install.sh	xm_response.rsp ToolsAndFramework /data/endeca"
	$INSTALL_FILES/Tools/cd/Disk1/install/silent_install.sh	$INSTALL_FILES/xm_response.rsp ToolsAndFrameworks $INSTALL_ROOT/ToolsAndFrameworks
	echo "Done installing ToolsAndFrameworks"
fi

#----------------------------------------------------------------------------------------------------------------------
#Start Tools and Frameworks
#----------------------------------------------------------------------------------------------------------------------
# clear old log file
#echo "" > $TOOLS_LOG_FILE

#echo "Starting Tools"
#echo

#nohup $ENDECA_TOOLS_STARTUP_ROOT/startup.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
# Loop while for as long as it takes to start up Tools and Frameworks
#----------------------------------------------------------------------------------------------------------------------

#STARTED=`grep -o 'INFO: Server startup in' $TOOLS_LOG_FILE`
#while [ "$STARTED" = "" ]; do
#    echo "..."
#    sleep 5
#    STARTED=`grep -o 'INFO: Server startup in' $TOOLS_LOG_FILE`
#done

#----------------------------------------------------------------------------------------------------------------------
#Install CAS
#----------------------------------------------------------------------------------------------------------------------
#$INSTALL_FILES/$CAS_FILE --silent --target $INSTALL_ROOT --endeca_tools_root $ENDECA_TOOLS_ROOT --endeca_tools_conf  $ENDECA_TOOLS_CONF < $INSTALL_FILES/cas_answers.txt
echo "Installing CAS"
echo "Running $INSTALL_FILES/$CAS_FILE -i silent -f cas_response.rsp "
$INSTALL_FILES/$CAS_FILE -i silent -f cas_response.rsp

echo "Done cas installation"
exit

#----------------------------------------------------------------------------------------------------------------------
#Start CAS
#----------------------------------------------------------------------------------------------------------------------
# clear old log file
echo "" > $CAS_LOG_FILE

echo "Starting CAS"
echo

nohup $CAS_STARTUP_ROOT/cas-service.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
# Loop while for as long as it takes to start up CAS
#----------------------------------------------------------------------------------------------------------------------
STARTED=`grep -o 'Successfully started CAS services' $CAS_LOG_FILE`
while [ "$STARTED" = "" ]; do
    echo "..."
    sleep 5
    STARTED=`grep -o 'Successfully started CAS services' $CAS_LOG_FILE`
done

#----------------------------------------------------------------------------------------------------------------------
# Refresh the profile settings as the paths didn't previously exist
#----------------------------------------------------------------------------------------------------------------------
source ~/.bash_profile

echo "restarting Endeca"
echo

#----------------------------------------------------------------------------------------------------------------------
#Stop the endeca services
#----------------------------------------------------------------------------------------------------------------------
nohup $PLATFORM_STARTUP_ROOT/shutdown.sh > /dev/null &
nohup $ENDECA_TOOLS_STARTUP_ROOT/shutdown.sh > /dev/null &
nohup $CAS_STARTUP_ROOT/cas-service-shutdown.sh > /dev/null &

sleep 5

#----------------------------------------------------------------------------------------------------------------------
#Restart the endeca services
#----------------------------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------------------
#Start Platform Services
#----------------------------------------------------------------------------------------------------------------------
# clear old log file
echo "" > $PLATFORM_LOG_FILE

echo "Starting Platform"
echo

nohup $PLATFORM_STARTUP_ROOT/startup.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
#Loop while for as long as it takes to start up
#----------------------------------------------------------------------------------------------------------------------
STARTED=`grep -o 'INFO: Server startup in' $PLATFORM_LOG_FILE`
while [ "$STARTED" = "" ]; do
    echo "..."
    sleep 5
    STARTED=`grep -o 'INFO: Server startup in' $PLATFORM_LOG_FILE`
done

#----------------------------------------------------------------------------------------------------------------------
#Start Tools and Frameworks
#----------------------------------------------------------------------------------------------------------------------

# clear old log file
echo "" > $TOOLS_LOG_FILE

echo "Starting Tools"
echo

#nohup $ENDECA_TOOLS_STARTUP_ROOT/startup.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
# Loop while for as long as it takes to start up Tools and Frameworks
#----------------------------------------------------------------------------------------------------------------------

STARTED=`grep -o 'INFO: Server startup in' $TOOLS_LOG_FILE`
while [ "$STARTED" = "" ]; do
    echo "..."
    sleep 5
    STARTED=`grep -o 'INFO: Server startup in' $TOOLS_LOG_FILE`
done

#----------------------------------------------------------------------------------------------------------------------
#Start CAS
#----------------------------------------------------------------------------------------------------------------------

# clear old log file
echo "" > $CAS_LOG_FILE

echo "Starting CAS"
echo

nohup $CAS_STARTUP_ROOT/cas-service.sh > /dev/null &

#----------------------------------------------------------------------------------------------------------------------
# Loop while for as long as it takes to start up CAS
#----------------------------------------------------------------------------------------------------------------------
STARTED=`grep -o 'Successfully started CAS services' $CAS_LOG_FILE`
while [ "$STARTED" = "" ]; do
    echo "..."
    sleep 5
    STARTED=`grep -o 'Successfully started CAS services' $CAS_LOG_FILE`
done

