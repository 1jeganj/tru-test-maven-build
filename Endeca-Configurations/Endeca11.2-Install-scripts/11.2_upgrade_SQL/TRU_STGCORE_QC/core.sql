
-- migration scritps 11.1 to 11.2

-- file agent_profile_ddl
alter table agent_profile_cmts
	add updated_date	timestamp	null;

-- file endeca_ddl
alter table srch_end_state
	add sched_tz	varchar2(254)	null;

-- file search_ddl
alter table srch_config
add schema_checksum	number(19,0)	null;

alter table srch_update_queue 
add sub_desc_name	varchar2(64)	null;

alter table srch_update_vqueue 
add sub_desc_name	varchar2(64)	null;	

-- file site_ddl
alter table site_configuration
	add type	number(10)	null;

-- file user_giftlist_ddl
alter table dcs_giftitem
add
(	qty_with_fraction_desired	number(19,7)	null,
	qty_with_fraction_purchased	number(19,7)	null);

-- file priceLists_ddl
alter table dcs_price 
add
(	start_date	timestamp	null,
	end_date	timestamp	null);

-- file order_returns_ddl
alter table csr_exch_item
add
(	qty_with_fraction_to_return	number(19,7)	null,
	qty_with_fraction_to_repl	number(19,7)	null,
	qty_with_fraction_received	number(19,7)	null);
	

alter table csr_item_adj
add	quantity_with_fraction_adj	number(19,7)	null;	

-- file order_markers_ddl
alter table dcs_gwp_order_markers
add
(	quantity_with_fraction		number(19,7)	 null,
	targeted_qty_with_fraction	number(19,7)	 null,
	automatic_qty_with_fraction	number(19,7)	 null,
	selected_qty_with_fraction	number(19,7)	 null,
	removed_qty_with_fraction	number(19,7)	 null,
	failed_qty_with_fraction	number(19,7)	 null);
	
update dcs_gwp_order_markers
set quantity_with_fraction = 0.0,
    targeted_qty_with_fraction = 0.0,
    automatic_qty_with_fraction = 0.0,
    selected_qty_with_fraction = 0.0,
    removed_qty_with_fraction	 = 0.0,
    failed_qty_with_fraction = 0.0;
  
commit; 

alter table dcs_gwp_order_markers modify
(	quantity_with_fraction		not	 null,
	targeted_qty_with_fraction	not	 null,
	automatic_qty_with_fraction	not	 null,
	selected_qty_with_fraction	not	 null,
	removed_qty_with_fraction	not	 null,
	failed_qty_with_fraction	not	 null);

alter table dcspp_gwp_item_markers
add
(	targeted_qty_with_fraction	number(19,7)	 null,
	automatic_qty_with_fraction	number(19,7)	 null,
	selected_qty_with_fraction	number(19,7)	 null,
	remaining_qty_with_fraction	number(19,7)	 null);
	
update dcspp_gwp_item_markers
set targeted_qty_with_fraction = 0.0,
    automatic_qty_with_fraction = 0.0,
    selected_qty_with_fraction = 0.0,
    remaining_qty_with_fraction = 0.0;	
    
commit;

alter table dcspp_gwp_item_markers modify
(	targeted_qty_with_fraction	not	 null,
	automatic_qty_with_fraction	not	 null,
	selected_qty_with_fraction	not	 null,
	remaining_qty_with_fraction	not	 null);

-- file order_ddl
alter table dcspp_order
	add configurator_id	varchar2(254)	null;

alter table dcspp_item
add 
(	external_id		varchar2(40)	null,
	quantity_with_fraction	number(19,7)	null);
	
alter table dcspp_hand_inst
add 	quantity_with_fraction	number(19,7)	null;

alter table dcspp_config_item
add
(	configured	number(1)	null,
	configurator_id	varchar2(254)	null);
	
alter table dcspp_subsku_item
add
(	config_prop_id	varchar2(40)	null,
	config_opt_id	varchar2(40)	null);
	
create table dcspp_config_subsku_item (
	subsku_item_id	varchar2(40)	not null,
	config_prop_id	varchar2(40)	null,
	config_opt_id	varchar2(40)	null
,constraint dcspp_cfg_subsku_ite_p primary key (subsku_item_id)
,constraint dcspp_cfg_subsku_tm_f foreign key (subsku_item_id) references dcspp_item (commerce_item_id));


create table dcspp_token_crdt_crd (
	payment_group_id	varchar2(40)	not null,
	token	varchar2(40)	not null
,constraint dcspp_token_crdt_crd_p primary key (payment_group_id)
,constraint dcspp_tokencrdtcrd__f foreign key (payment_group_id) references dcspp_pay_group (payment_group_id));




alter table dcspp_shipitem_rel 
add
(	quantity_with_fraction		number(19,7)	null,
	returned_qty_with_fraction	number(19,7)	null);
	

alter table dcspp_rel_range
add
(	low_bound_with_fraction		number(19,7)	null,
	high_bound_with_fraction	number(19,7)	null);
	
alter table dcspp_payitem_rel 	
add 	quantity_with_fraction	number(19,7)	null;


alter table dcspp_item_price
add
(	qty_with_fraction_discounted	number(19,7)	null,
        qty_with_fraction_as_qualifier	number(19,7)	null);
        

alter table dcspp_price_adjust
	add  qty_with_fraction_adjusted	number(19,7)	null;
	
alter table dcspp_det_price
add 
(	quantity_with_fraction	number(19,7)	null,
	qty_with_fraction_as_qualifier	number(19,7)	null);
	
alter  table dcspp_det_range
add
(	low_bound_with_fraction	number(19,7)	null,
	high_bound_with_fraction	number(19,7)	null);
	

alter table dbcpp_ccitem_rel
add
(	quantity_with_fraction	number(19,7)	null);


create table dcspp_appeasement (
	appeasement_id	varchar2(40)	not null,
	order_id	varchar2(40)	not null,
	type	varchar2(40)	not null,
	status	varchar2(40)	null,
	creation_date	timestamp	not null,
	agent_id	varchar2(40)	null,
	profile_id	varchar2(40)	not null,
	origin_of_appeasement	number(10)	not null,
	reason_code	varchar2(40)	not null,
	comments	varchar2(254)	null,
	processed	number(3)	null,
	appeasement_amount	number(19,7)	not null
,constraint dcs_appeasement_p primary key (appeasement_id));


create table dcspp_appeasement_reasons (
	id	varchar2(40)	not null,
	description	varchar2(254)	not null
,constraint appeasement_reasons_p primary key (id));


create table dcspp_appeasement_refund (
	id	varchar2(40)	not null,
	type	number(10)	not null,
	amount	number(19,7)	null
,constraint appsmt_refund_p primary key (id));


create table dcspp_appeasement_refunds (
	appeasement_id	varchar2(40)	not null,
	appeasement_refund_id	varchar2(40)	not null
,constraint appsmt_refunds_p primary key (appeasement_id,appeasement_refund_id)
,constraint appeasement_refund_d_f foreign key (appeasement_id) references dcspp_appeasement (appeasement_id)
,constraint appeasement_refund_m_f foreign key (appeasement_refund_id) references dcspp_appeasement_refund (id));

create index appsmt_refund_id on dcspp_appeasement_refunds (appeasement_refund_id);

create table dcspp_appeasement_refund_cc (
	appeasement_refund_id	varchar2(40)	not null,
	payment_group_id	varchar2(40)	not null
,constraint appsmt_refund_cc_p primary key (appeasement_refund_id)
,constraint appeasement_cc_m_f foreign key (appeasement_refund_id) references dcspp_appeasement_refund (id));


create table dcspp_appeasement_refund_sc (
	appeasement_refund_id	varchar2(40)	not null,
	payment_group_id	varchar2(40)	null,
	sc_id	varchar2(40)	null
,constraint appsmt_refund_sc_p primary key (appeasement_refund_id)
,constraint appeasement_sc_m_f foreign key (appeasement_refund_id) references dcspp_appeasement_refund (id));

-- file inventory_ddl
alter table dcs_inventory
add
(	stock_level_with_fraction	number(19,7)	null,
        backorder_level_with_fraction	number(19,7)	null,
	preorder_level_with_fraction	number(19,7)	null);


alter table dcs_inv_atp
add 	quantity_with_fraction	number(19,7)	null;

-- file user_ddl
alter table dps_user
   modify password	varchar2(200);

alter table dps_mailing
   add site_id	varchar2(40)	null;

alter table dps_user_prevpwd
  modify prevpwd	varchar2(200);
  
-- file scenario_ddl
alter table dss_ind_scenario
   add expiration_time number(19,0)	null;
   
-- file ticket_ddl
alter table tkt_upd_props
   modify property_name	varchar2(254);
 
-- file DCS-CSR_ticketing_ddl
alter table csrt_recv_rtrn_itm
add	quantity_with_fraction			number(19,7)	null;


alter table csrt_ci_event 
add 
(		old_quantity_with_fraction	number(19,7)	null,
		new_quantity_with_fraction	number(19,7)	null);
		

alter table csrt_split_sg
add	quantity_with_fraction			number(19,7)	null;

alter table csrt_split_sg
modify 	quantity				null;

alter table csrt_split_cc
add 	quantity_with_fraction			number(19,7)	null;

alter table csrt_split_cc
modify 	quantity				null;


create table csrt_aps_appr_event (
	id					varchar2(40)	not null,
	appeasement_id				varchar2(40)	not null
,constraint csrtapsapprevt_p primary key (id)
,constraint csrtapsapprevt_f foreign key (id) references csrt_appr_event (id));


alter table csrt_gi_event
add
(		old_quantity_with_fraction	number(19,7)	null,
		new_quantity_with_fraction	number(19,7)	null);
