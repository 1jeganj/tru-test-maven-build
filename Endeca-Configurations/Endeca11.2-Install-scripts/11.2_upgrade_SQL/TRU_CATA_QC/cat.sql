
-- migration scritps 11.1 to 11.2

-- file content_mgmt_ddl
alter table wcm_media_content
	add file_upload	number(1,0)	null;
	
alter table wcm_article
	add article_type	number(10)	null;

create index wcm_article_rltd_media_idx on wcm_article_rltd_media (related_media_id);
create index wcm_media_rltd_article2_x on wcm_media_rltd_article (related_article_id);

-- file wcm_backing_maps_ddl
-- Tables for backing maps for item descriptors enables for dynamic properties
create table wcm_dyn_prop_map_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	varchar2(400)	null
,constraint wcm_dyn_prop_map_str_p primary key (id,prop_name));


create table wcm_dyn_prop_map_big_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	clob	null
,constraint wcm_dyn_prop_map_big_str_p primary key (id,prop_name));


create table wcm_dyn_prop_map_double (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19,7)	null
,constraint wcm_dyn_prop_map_double_p primary key (id,prop_name));


create table wcm_dyn_prop_map_long (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19)	null
,constraint wcm_dyn_prop_map_long_p primary key (id,prop_name));

-- file wcm_backing_maps_ddl
alter table site_configuration
	add type	number(10)	null;
alter table site_group 
	add type	number(10)	null;
update site_configuration
     set type = 1301;
update site_group
      set type = 1400;      
  commit;
  
-- file dynamic_metadata_ddl  
-- Tables for Dynamic Repository Metadata storage

create table das_gsa_dyn_type (
	id	varchar2(40)	not null,
	type_name	varchar2(254)	null,
	item_descriptor	varchar2(254)	null,
	root_item_descriptor	varchar2(254)	null,
	repository	varchar2(254)	null,
	sub_type_value	varchar2(254)	null,
	sub_type_num	number(10)	null,
	removed	number(1)	null
,constraint das_gsa_dyn_type_p primary key (id));


create table das_gsa_dyn_type_attr (
	id	varchar2(40)	not null,
	attribute_key	varchar2(254)	not null,
	attribute_value	varchar2(254)	not null
,constraint das_gsa_dyn_type_attr_p primary key (id,attribute_key));


create table das_gsa_dyn_prop (
	id	varchar2(40)	not null,
	property_name	varchar2(254)	not null,
	item_descriptor	varchar2(254)	null,
	repository	varchar2(254)	null,
	data_type	varchar2(254)	null,
	backing_map	varchar2(254)	null,
	removed	number(1)	null
,constraint das_gsa_dyn_prop_p primary key (id));


create table das_gsa_dyn_prop_enum (
	id	varchar2(40)	not null,
	enumeration_values	varchar2(254)	not null,
	enumeration_value_num	number(10)	not null
,constraint das_gsa_dyn_prop_enum_p primary key (id,enumeration_values));


create table das_gsa_dyn_prop_eorder (
	id	varchar2(40)	not null,
	seq_num	number(10)	not null,
	enumeration_order	varchar2(254)	not null
,constraint das_gsa_dyn_prop_eorder_p primary key (id,seq_num));


create table das_gsa_dyn_prop_attr (
	id	varchar2(40)	not null,
	attribute_key	varchar2(254)	not null,
	attribute_value	varchar2(254)	not null
,constraint das_gsa_dyn_prop_attr_p primary key (id,attribute_key));

-- file backing_maps_ddl
-- Tables for backing maps for item descriptors enables for dynamic properties
create table das_dyn_prop_map_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	varchar2(400)	null
,constraint das_dyn_prop_map_str_p primary key (id,prop_name));


create table das_dyn_prop_map_big_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	clob	null
,constraint das_dyn_prop_map_big_str_p primary key (id,prop_name));


create table das_dyn_prop_map_double (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19,7)	null
,constraint das_dyn_prop_map_double_p primary key (id,prop_name));


create table das_dyn_prop_map_long (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19)	null
,constraint das_dyn_prop_map_long_p primary key (id,prop_name));


create table das_dyn_prop_map_str_2 (
	id1	varchar2(40)	not null,
	id2	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	varchar2(400)	null
,constraint das_dyn_prop_map_str_2_p primary key (id1,id2,prop_name));


create table das_dyn_prop_map_big_str_2 (
	id1	varchar2(40)	not null,
	id2	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	clob	null
,constraint das_dyn_prop_mp_bg_str_2_p primary key (id1,id2,prop_name));


create table das_dyn_prop_map_double_2 (
	id1	varchar2(40)	not null,
	id2	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19,7)	null
,constraint das_dyn_prop_mp_dbl_2_p primary key (id1,id2,prop_name));


create table das_dyn_prop_map_long_2 (
	id1	varchar2(40)	not null,
	id2	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19)	null
,constraint das_dyn_prop_mp_lng_2_p primary key (id1,id2,prop_name));

-- file promotion_ddl
alter  table dcs_promotion
add 	fil_Qual_Acted_As_Qual	number(1)	null;

create table dcs_promo_payment (
	promotion_id	varchar2(40)	not null,
	payment_type	varchar2(40)	not null);
	
-- file product_catalog_ddl
alter table dcs_product
add
(	fractional_quantities_allowed	number(1)	null,
	unit_of_measure			integer		null);
	
alter table dcs_sku 
add 	fractional_quantities_allowed	number(1)	null;

alter table dcs_sku_link
add 	quantity_with_fraction	number(19,7)	null;

alter table dcs_sku_link
modify 	quantity null;

create table dcs_config_sku (
	sku_id			varchar2(40)	not null,
	configurator_id		varchar2(254)	null
,constraint dcs_config_sku_p primary key (sku_id)
,constraint dcs_cnfskusku_d_f foreign key (sku_id) references dcs_sku (sku_id));

alter table dcs_config_prop 
add 	configurator_id			varchar2(254)	null;

-- file priceLists_ddl
alter table dcs_price 
add
(	start_date	timestamp	null,
	end_date	timestamp	null);

-- file dcs_backing_maps_ddl
-- Tables for backing maps for item descriptors enables for dynamic properties in product catalog repository

create table dcs_dyn_prop_map_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	varchar2(400)	null
,constraint dcs_dyn_prop_map_str_p primary key (id,prop_name));


create table dcs_dyn_prop_map_big_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	clob	null
,constraint dcs_dyn_prop_map_big_str_p primary key (id,prop_name));


create table dcs_dyn_prop_map_double (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19,7)	null
,constraint dcs_dyn_prop_map_double_p primary key (id,prop_name));


create table dcs_dyn_prop_map_long (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19)	null
,constraint dcs_dyn_prop_map_long_p primary key (id,prop_name));

-- Tables for backing maps for sku item descriptor

create table dcs_sku_dyn_prop_map_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	varchar2(400)	null
,constraint dcs_sku_dyn_prop_map_str_p primary key (id,prop_name));


create table dcs_sku_dyn_prop_map_big_str (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	clob	null
,constraint dcs_sku_dynmc_prp_mp_bg_str_p primary key (id,prop_name));


create table dcs_sku_dyn_prop_map_double (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19,7)	null
,constraint dcs_sku_dyn_prp_mp_dbl_p primary key (id,prop_name));


create table dcs_sku_dyn_prop_map_long (
	id	varchar2(40)	not null,
	prop_name	varchar2(254)	not null,
	prop_value	number(19)	null
,constraint dcs_sku_dyn_prp_mp_lng_p primary key (id,prop_name));

-- file user_ddl
alter table dps_user
   modify password	varchar2(200);

alter table dps_mailing
   add site_id	varchar2(40)	null;

alter table dps_user_prevpwd
  modify prevpwd	varchar2(200);

