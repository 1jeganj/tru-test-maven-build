
-- migration scritps 11.1 to 11.2
-- file agent_profile_ddl
alter table agent_profile_cmts
	add updated_date	timestamp	null;

-- fiel internal_user_ddl
alter table dpi_user
  modify password	varchar2(200);

alter table dpi_user_prevpwd
  modify prevpwd	varchar2(200);		

-- file user_ddl
alter table dps_user
   modify password	varchar2(200);

alter table dps_mailing
   add site_id	varchar2(40)	null;

alter table dps_user_prevpwd
  modify prevpwd	varchar2(200);
  
-- file scenario_ddl
alter table dss_ind_scenario
   add expiration_time number(19,0)	null; 
   
-- file DCS-CSR_approvals_ddl
create table csr_appeasement_approval (
	approval_id	varchar2(40)	not null,
	appeasement_id	varchar2(40)	not null,
	customer_email	varchar2(40)	null,
	appeasement_total	number(19,7)	not null,
	order_total	number(19,7)	not null
,constraint appeasement_appr_p primary key (approval_id)
,constraint csrappeasementappr_f foreign key (approval_id) references csr_approval (approval_id));

-- file DCS-CSR_logging_ddl
alter table csr_recv_rtrn_item
add	quantity_with_fraction	number(19,7)	null;

alter table csr_ci_event
add 
(		old_quantity_with_fraction	number(19,7)	null,
		new_quantity_with_fraction	number(19,7)	null);

alter table csr_split_sg
add	quantity_with_fraction	number(19,7)	null;

alter table csr_split_sg
modify 		quantity			null;


alter table csr_split_cc
add	quantity_with_fraction	number(19,7)	null;

alter table csr_split_cc
modify 		quantity			null;


alter table csr_upd_props
modify 	property_name	varchar2(254);


create table csr_aps_appr_event (
	id		varchar2(40)	not null,
	appeasement_id	varchar2(40)	not null
,constraint csrapsapprevt_p primary key (id)
,constraint csrapsapprevt_f foreign key (id) references csr_appr_event (id));


alter table csr_gi_event
add
(		old_quantity_with_fraction	number(19,7)	null,
		new_quantity_with_fraction	number(19,7)	null);
		
