
#definition for DVALID_MAPPINGS script

export DVALID_MAPPINGS_SRC_MAPPINGS_DIR=/data/endeca/apps/TRU/data/dvalid_mappings_archive

export DVALID_MAPPINGS_TARGET_MAPPINGS_DIR=/perf_atg_endeca_share/staging

export DVALID_MAPPINGS_TARGET_HOST=10.228.70.166

export DVALID_MAPPINGS_TARGET_PORT=8500

#definition for prod_promote script
export PROD_PROMOTE_TARGET_EAC_HOST=10.228.70.166

export PROD_PROMOTE_TARGET_TARGET_EAC_PORT=8888

export PROD_PROMOTE_TARGET_EAC_APP=TRU

#definition for promote config staging
export PROMOTE_CONFIG_SRC_WORKBENCH=/data/endeca/apps/TRU/data/workbench/config_snapshots

export PROMOTE_CONFIG_SRC_DGRAPH=/data/endeca/apps/TRU/data/dgraphcluster/AuthoringDgraphCluster/config_snapshots

export PROMOTE_CONFIG_STAGING_EXPORT=/perf_atg_endeca_share/staging

#CAS ROOT FOR STAGING PATH 
export CAS_ROOT_STAGE=/data/endeca/CAS/11.2.0
