#used by copy_content_from_staging.sh
export WORKBENCH_SNAPSHOT_SRC="/perf_atg_endeca_share/staging"
export AUTHORDGRAPH_SRC="/perf_atg_endeca_share/staging/AuthoringDgraphCluster/config_snapshots"

export WORKBENCH_EXPORT_TARGET="/perf_atg_endeca_share/live"
export LIVEDGRAPH_SNAPSHOT_TARGET="/data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster/config_snapshots"

#copy_content_to_dr.sh target host
export DRHOST="" #set to blank for lower env

