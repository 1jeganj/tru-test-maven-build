#!/bin/bash

ENV_TYPE=$1
ENV_FUNCTION=$2
INSTALLATION=$3

WORKING_DIR=`pwd`

echo "WORKING DIR: ${WORKING_DIR}"


if [  -z "$1" ] || [ -z "$2"   ] || [ -z "$3" ]  ; then
       echo "Please specify pky_dev3, pky_perf, pky_prod, san_prod as 1st parameter, 2nd parameter as stage|live, 3rd parameter where the base app is installed"
       echo "For example: pky_dev3 live /data/endeca/apps/TRU"
	exit
fi

#######################################################
##  VERIFY PARAMETERS
####################################################### 
# check if the source directory exists
SCRIPT_SRC="${WORKING_DIR}/env/${ENV_TYPE}"
if [ -d $SCRIPT_SRC ] ; then 
	echo "Processing $SCRIPT_SRC "
else
	echo "Unable to find environment specified: ${ENV_TYPE} exiting, specify blank to see options"
	exit
fi

# check if it is stage or live
if [ "$ENV_FUNCTION" = "live" ]  || [ "$ENV_FUNCTION" = "stage" ] ; then
	echo "Specified : $ENV_FUNCTION"	
else
	echo "Please specify stage or live , exiting"
	exit
fi

# check if the installation directory exists
if [ -d $INSTALLATION ] ; then
	echo "target endeca app path : $INSTALLATION"
else 
	echo "Please specify endeca app path"
	exit
fi

# target scripts
SCRIPT_TARGET="${WORKING_DIR}/../config/script"
CONTROL_TARGET="${WORKING_DIR}/../config/control"
echo "TARGET: ${SCRIPT_TARGET}"

#########################################
# preparing the config and control directory
########################################
if [ "$ENV_FUNCTION" == "live" ] ; then
	echo "Installing live"
	echo "Installing environments"
	# copying over the environment files
	cp ${SCRIPT_SRC}/environment-live.properties ${SCRIPT_TARGET}/environment.properties
	cp ${SCRIPT_SRC}/environment-live.sh ${SCRIPT_TARGET}/environment.sh
	cp ${SCRIPT_SRC}/environment-live.xml ${SCRIPT_TARGET}/environment.xml
	cp ${SCRIPT_SRC}/LiveAppServerCluster-live.xml ${SCRIPT_TARGET}/LiveAppServerCluster-live.xml
	cp ${SCRIPT_SRC}/LiveDgraphCluster-live.xml ${SCRIPT_TARGET}/LiveDgraphCluster-live.xml
	cp ${SCRIPT_SRC}/set_environment-live.sh ${SCRIPT_TARGET}/set_environment.sh
	# copy over the burst files if exist
	if [ -f "${SCRIPT_SRC}/BurstDgraphCluster-live.xml" ] ; then
		echo Installing" BurstDgraphCluster-live"
		cp ${SCRIPT_SRC}/BurstDgraphCluster-live.xml ${SCRIPT_TARGET}/BurstDgraphCluster-live.xml
	fi
	if [ -f "${SCRIPT_SRC}/BurstAppServerCluster-live.xml" ] ; then
		echo "Installing  BurstAppServerCluster-live"
		cp ${SCRIPT_SRC}/BurstAppServerCluster-live.xml ${SCRIPT_TARGET}/BurstAppServerCluster-live.xml
	fi
fi

#######################
#copy over staging
#######################
if [ "$ENV_FUNCTION" == "stage" ] ;  then
	# removing config/script files that are not for stage
	if [ -f "${SCRIPT_TARGET}/LiveAppServerCluster-live.xml" ]; then
		echo "Removing /LiveAppServerCluster-live.xml "
		rm ${SCRIPT_TARGET}/LiveAppServerCluster-live.xml
	fi
	if [ -f "${SCRIPT_TARGET}/LiveDgraphCluster-live.xml" ]; then
		echo "Removing LiveDgraphCluster-live.xml"
		rm ${SCRIPT_TARGET}/LiveDgraphCluster-live.xml
	fi
	if [ -f "${SCRIPT_TARGET}/BurstDgraphCluster-live.xml" ]; then
		echo "Removing BurstDgraphCluster-live.xml"
		rm ${SCRIPT_TARGET}/BurstDgraphCluster-live.xml
	fi
	if [ -f "${SCRIPT_TARGET}/BurstAppServerCluster-live.xml" ]; then 
		echo "Removing BurstAppServerCluster-live.xml"
		rm ${SCRIPT_TARGET}/BurstAppServerCluster-live.xml
	fi
	
	#copy over the stage files
	echo "Installing environments (stage)"
	cp ${SCRIPT_SRC}/environment-stage.properties ${SCRIPT_TARGET}/environment.properties
	cp ${SCRIPT_SRC}/environment-stage.sh ${SCRIPT_TARGET}/environment.sh
	cp ${SCRIPT_SRC}/environment-stage.xml ${SCRIPT_TARGET}/environment.xml
	cp ${SCRIPT_SRC}/set_environment-stage.sh ${SCRIPT_TARGET}/set_environment.sh
fi

#####################################
#backup target directory application
#backup stamp
stamp=`date +"%m-%d-%y-%H-%m-%s"`
echo "$stamp"
# backup the target directories first
echo "backing up target installation in $WORKING_DIR/../../backuptruconfig.$stamp.tar"

cd $INSTALLATION
tar cvzf  $WORKING_DIR/../../backuptruconfig.$stamp.tar  config  control >backup.log


#######################################
# deploying configurations
#######################################
cd $WORKING_DIR


# now copy config and controls to installations
# clean up target config and control (wipe clean)
echo "cleaning installation directories"
rm -fR $INSTALLATION/config
rm -f $INSTALLATION/control
rm -fR $INSTALLATION/test_data

echo "Making target directory just in case if it is a new installation: config, control, data, logs, persist, reports, test_data"
mkdir -p $INSTALLATION/config
mkdir -p $INSTALLATION/control
mkdir -p $INSTALLATION/data
mkdir -p $INSTALLATION/logs
mkdir -p $INSTALLATION/persist
mkdir -p $INSTALLATION/reports
mkdir -p $INSTALLATION/test_data

# deploy the latest configurations
echo " cp -R $WORKING_DIR/../config $INSTALLATION "
cp -R $WORKING_DIR/../config $INSTALLATION

echo " cp -R $WORKING_DIR/../control $INSTALLATION "
cp -R $WORKING_DIR/../control $INSTALLATION

echo "cp -R $WORKING_DIR/../test_data $INSTALLATION"
cp -R $WORKING_DIR/../test_data $INSTALLATION

chmod uog+x $INSTALLATION/control/*.sh
echo "Finished"




 
