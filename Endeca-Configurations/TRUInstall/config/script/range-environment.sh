#Environment variables used to create Range Facets like Price, Age and Weight

#Variables related to CAS location host and port
export CAS_ROOT="/data/endeca/CAS/11.2.0"
export CAS_HOST="localhost"
export CAS_PORT="9500"

#Record Store Names
export PRICE_RS_NAME="TRU-Price"
export AGE_RS_NAME="TRU-Age"
export WEIGHT_RS_NAME="TRU-Weight"

#xml config file location for each Record Store
export PRICE_FILE_LOCATION="/data/endeca/apps/TRU/test_data/baseline/TRU-Price.xml"
export AGE_FILE_LOCATION="/data/endeca/apps/TRU/test_data/baseline/TRU-Age.xml"
export WEIGHT_FILE_LOCATION="/data/endeca/apps/TRU/test_data/baseline/TRU-Weight.xml"

#Application CAS last mile Crawl name
export LAST_MILE_CRAWL_LOCATION="/data/endeca/apps/TRU/config/cas/last-mile-crawl.xml"
export LAST_MILE_CRAWL_NAME="TRU-last-mile-crawl"
