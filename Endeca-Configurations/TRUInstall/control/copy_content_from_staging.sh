#!/bin/sh

WORKING_DIR=`dirname ${0} 2>/dev/null`
echo "Working directory: ${WORKING_DIR}"

. "${WORKING_DIR}/../config/script/set_environment.sh"
. "${WORKING_DIR}/../config/script/environment.sh"

echo "Copying content from staging"
echo "rsync -av  $WORKBENCH_SNAPSHOT_SRC/* $WORKBENCH_EXPORT_TARGET"
rsync -av  $WORKBENCH_SNAPSHOT_SRC/* $WORKBENCH_EXPORT_TARGET


echo "mkdir -p  $LIVEDGRAPH_SNAPSHOT_TARGET"
mkdir -p  $LIVEDGRAPH_SNAPSHOT_TARGET

echo "rsync -av  $AUTHORDGRAPH_SRC/* $LIVEDGRAPH_SNAPSHOT_TARGET"
rsync -av  $AUTHORDGRAPH_SRC/* $LIVEDGRAPH_SNAPSHOT_TARGET


#replace the path in current search pointer file
sed -i   's/AuthoringDgraphCluster/LiveDgraphCluster/' "/data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster/config_snapshots/current_search_config.txt"
