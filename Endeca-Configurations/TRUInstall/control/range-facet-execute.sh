#!/bin/sh

WORKING_DIR=`dirname ${0} 2>/dev/null`
echo "Working directory: ${WORKING_DIR}"

. "${WORKING_DIR}/../config/script/range-environment.sh"

#Remove the Existing Record Stores
echo "Removing Record Store $PRICE_RS_NAME (ignore errors if Record Store doesn't exist)" 
  ${CAS_ROOT}/bin/component-manager-cmd.sh delete-component -h ${CAS_HOST} -p ${CAS_PORT} -n ${PRICE_RS_NAME}
echo "Removing Record Store $AGE_RS_NAME (ignore errors if Record Store doesn't exist)" 
  ${CAS_ROOT}/bin/component-manager-cmd.sh delete-component -h ${CAS_HOST} -p ${CAS_PORT} -n ${AGE_RS_NAME}
echo "Removing Record Store $WEIGHT_RS_NAME (ignore errors if Record Store doesn't exist)" 
  ${CAS_ROOT}/bin/component-manager-cmd.sh delete-component -h ${CAS_HOST} -p ${CAS_PORT} -n ${WEIGHT_RS_NAME}
  
  
#Create New Record stores  
echo "Creating Record Store $PRICE_RS_NAME"
${CAS_ROOT}/bin/component-manager-cmd.sh create-component -h ${CAS_HOST} -p ${CAS_PORT} -t RecordStore -n ${PRICE_RS_NAME}
if [ $? != 0 ] ; then
 echo "Failure to create Record Store."
 exit 1
fi

echo "Creating Record Store $AGE_RS_NAME"
${CAS_ROOT}/bin/component-manager-cmd.sh create-component -h ${CAS_HOST} -p ${CAS_PORT} -t RecordStore -n ${AGE_RS_NAME}
if [ $? != 0 ] ; then
 echo "Failure to create Record Store."
 exit 1
fi

echo "Creating Record Store $WEIGHT_RS_NAME"
${CAS_ROOT}/bin/component-manager-cmd.sh create-component -h ${CAS_HOST} -p ${CAS_PORT} -t RecordStore -n ${WEIGHT_RS_NAME}
if [ $? != 0 ] ; then
 echo "Failure to create Record Store."
 exit 1
fi


#Write data to Record Stores
echo "Loading /opt/endeca/apps/TRU/test_data/baseline/${PRICE_FILE_NAME} into $PRICE_RS_NAME"
sh ${CAS_ROOT}/bin/recordstore-cmd.sh write -b -a ${PRICE_RS_NAME} -h ${CAS_HOST} -p ${CAS_PORT} -f ${PRICE_FILE_LOCATION}

echo "Loading /opt/endeca/apps/TRU/test_data/baseline/${AGE_FILE_NAME} into $AGE_RS_NAME"
sh ${CAS_ROOT}/bin/recordstore-cmd.sh write -b -a ${AGE_RS_NAME} -h ${CAS_HOST} -p ${CAS_PORT} -f ${AGE_FILE_LOCATION}

echo "Loading /opt/endeca/apps/TRU/test_data/baseline/${WEIGHT_FILE_NAME} into $WEIGHT_RS_NAME"
sh ${CAS_ROOT}/bin/recordstore-cmd.sh write -b -a ${WEIGHT_RS_NAME} -h ${CAS_HOST} -p ${CAS_PORT} -f ${WEIGHT_FILE_LOCATION}


# Update the existing crawl after creating the new Record Stores for Price, Age and Weight
echo "Updating crawl ${LAST_MILE_CRAWL_NAME}"
${CAS_ROOT}/bin/cas-cmd.sh updateCrawls -h ${CAS_HOST} -p ${CAS_PORT} -f ${LAST_MILE_CRAWL_LOCATION}
if [ $? != 0 ] ; then
	echo "Failure to Update crawl."
	exit 1
fi