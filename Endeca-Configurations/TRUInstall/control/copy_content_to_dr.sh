#!/bin/sh

WORKING_DIR=`dirname ${0} 2>/dev/null`
echo "Working directory: ${WORKING_DIR}"

. "${WORKING_DIR}/../config/script/set_environment.sh"
. "${WORKING_DIR}/../config/script/environment.sh"

echo "/usr/bin/rsync -avz /prod_atg_endeca_share/live/TRU endeca@${DRHOST}:/prod_atg_endeca_share/live"
/usr/bin/rsync -avz /prod_atg_endeca_share/live/TRU endeca@${DRHOST}:/prod_atg_endeca_share/live
echo "/usr/bin/rsync -avz /data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster/config_snapshots endeca@${DRHOST}:/data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster"
/usr/bin/rsync -avz /data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster/config_snapshots endeca@${DRHOST}:/data/endeca/apps/TRU/data/dgraphcluster/LiveDgraphCluster

#disable distributeindexandapply, as it will be handled by baseline update scheduler - calling this may couple PROD with DR 
#ssh endeca@${DRHOST} "cd /data/endeca/apps/TRU/control; ./runcommand.sh DistributeIndexAndApply"




