#!/bin/sh

WORKING_DIR=`dirname ${0} 2>/dev/null`
. "${WORKING_DIR}/../config/script/set_environment.sh"
. "${WORKING_DIR}/../config/script/environment.sh"

#specifies the LIVE ITL and PORT
TARGET_EAC_HOST=${PROD_PROMOTE_TARGET_EAC_HOST}
echo ${PROD_PROMOTE_TARGET_EAC_HOST}

TARGET_EAC_PORT=${PROD_PROMOTE_TARGET_TARGET_EAC_PORT}
echo ${PROD_PROMOTE_TARGET_TARGET_EAC_PORT}

TARGET_EAC_APP=${PROD_PROMOTE_TARGET_EAC_APP}
echo ${PROD_PROMOTE_TARGET_EAC_APP}


echo "chmod og+r -R $PROMOTE_CONFIG_STAGING_EXPORT"
echo $PROMOTE_CONFIG_STAGING_EXPORT
chmod og+r -R $PROMOTE_CONFIG_STAGING_EXPORT

echo "Attempting to ingest from Live ITL"
eaccmd.sh ${TARGET_EAC_HOST}:${TARGET_EAC_PORT} start --app ${TARGET_EAC_APP} --script PromoteAuthoringToLive

