# Specify  the path and target HOST (LIVE ITL)
#!/bin/sh
WORKING_DIR=`dirname ${0} 2>/dev/null`
. "${WORKING_DIR}/../config/script/set_environment.sh"
. "${WORKING_DIR}/../config/script/environment.sh"

SRC_MAPPINGS_DIR=${DVALID_MAPPINGS_SRC_MAPPINGS_DIR}

TARGET_MAPPINGS_DIR=${DVALID_MAPPINGS_TARGET_MAPPINGS_DIR}

echo "Mkdir ${TARGET_MAPPINGS_DIR}"

mkdir -p ${TARGET_MAPPINGS_DIR}

echo "rsync -av  ${SRC_MAPPINGS_DIR}  ${TARGET_MAPPINGS_DIR}"
rsync -av  ${SRC_MAPPINGS_DIR}  ${TARGET_MAPPINGS_DIR}

echo "chmod  ${TARGET_MAPPINGS_DIR}/*"
chmod og+r -R  ${TARGET_MAPPINGS_DIR}/*

TARGET_HOST=${DVALID_MAPPINGS_TARGET_HOST}

TARGET_PORT=${DVALID_MAPPINGS_TARGET_PORT}

TARGET_USING_SSL=false
TARGET_DVALID_MGR=TRU-dimension-value-id-manager

#need to map this to the target server directory
$CAS_ROOT_STAGE/bin/cas-cmd.sh importDimensionValueIdMappings -f "${TARGET_MAPPINGS_DIR}/dvalid_mappings_archive/dvalid_mappings.csv" -h  "${TARGET_HOST}" -p "${TARGET_PORT}" -l "${TARGET_USING_SSL}" -m "${TARGET_DVALID_MGR}"


