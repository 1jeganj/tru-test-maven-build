# Specify the appropriate path for each env
#!/bin/sh
WORKING_DIR=`dirname ${0} 2>/dev/null`
. "${WORKING_DIR}/../config/script/set_environment.sh"
. "${WORKING_DIR}/../config/script/environment.sh"

# specify the stage path for appropriate env

SRC_AUTHOR_DGRAPH=${PROMOTE_CONFIG_SRC_DGRAPH}

STAGING_EXPORT=${PROMOTE_CONFIG_STAGING_EXPORT}

mkdir -p "$STAGING_EXPORT/AuthoringDgraphCluster/config_snapshots"

echo "cp -R $SRC_AUTHOR_DGRAPH/  $STAGING_EXPORT/AuthoringDgraphCluster/ "
cp -R $SRC_AUTHOR_DGRAPH/  $STAGING_EXPORT/AuthoringDgraphCluster/
chmod og+r $STAGING_EXPORT/AuthoringDgraphCluster/
